﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class CareerWeBelieve
    {
        public int Id { get; set; }

        public string Description { get; set; }

        public List<CareerWeBelieve>CareerWeBelieveList {get;set;}

        public string Sr { get; set; }
    }
}