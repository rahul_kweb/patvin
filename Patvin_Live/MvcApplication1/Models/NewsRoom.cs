﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class NewsRoom
    {
        public int NewsId { get; set; }

        [Required(ErrorMessage = "Select any one type.")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Image is required.")]
        public string Image { get; set; }

        [Required(ErrorMessage = "Date is required.")]
        public string Date { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        public string imgPreview { get; set; }

        public string Sr { get; set; }
        public List<NewsRoom> NewsRoomList { get; set; }

        public string Slug { get; set; }

        public string Img_Alt_Tag { get; set; }

        public string Category { get; set; }

    }
}