﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class TechnologyImageVideoCaption
    {

        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Image is required.")]
        public string Image { get; set; }

        [Required(ErrorMessage = "Video Url is required.")]
        public string Video { get; set; }

        [Required(ErrorMessage = "Select any one type.")]
        public string Type { get; set; }

        public string Sr { get; set; }

        public string imgPreview { get; set; }
        public List<TechnologyImageVideoCaption> TechnologyImageVideoCaptionList { get; set; }
    }
}