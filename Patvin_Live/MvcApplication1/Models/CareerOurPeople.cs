﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class CareerOurPeople
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is Required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Position is Required.")]
        public string Position { get; set; }

        [Required(ErrorMessage = "Description is Required ")]
        public string Description { get; set; }

        public int DisplayOrder { get; set; }

        [Required(ErrorMessage = "Image is required.")]

        public string Image { get; set; }

        public List<CareerOurPeople> CareerOurPeopleList { get; set; }

        public string Sr { get; set; }

        public string imgPreview { get; set; }

        public string Img_Alt_Tag { get; set; }
    }
}