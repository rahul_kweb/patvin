﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Web;

namespace Patvin.Models
{

    public class PageMetaDetail
    {
        public static string UpdateMetaDetails(string pageUrl)
        {
            Utility util = new Utility();
            //--- StringBuilder object to store MetaTags information.
            StringBuilder sbMetaTags = new StringBuilder();

            //--Step1 Get data from database.
            DataTable dt = new DataTable();
            dt = util.Display("exec Proc_MetaTag 'bindMetatTag',0,'"+ pageUrl.ToString() + "'");
            if (dt.Rows.Count > 0)
            {

                //---- Step2 In this step we will add <title> tag to our StringBuilder Object.
                sbMetaTags.Append("<title>" +dt.Rows[0]["Title"].ToString() + "</title>");

                //---- Step3 In this step we will add "Meta Description" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='description' content="+ dt.Rows[0]["MetaDescription"].ToString() + " >");
         
                //---- Step4 In this step we will add "Meta Keywords" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='keywords' content =" + dt.Rows[0]["MetaKeywords"].ToString() + ">");
            }
            return sbMetaTags.ToString();
        }

    }
}