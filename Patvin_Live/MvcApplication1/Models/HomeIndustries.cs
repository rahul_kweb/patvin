﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class HomeIndustries
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Heading is required.")]
        public string Heading { get; set; }

        [Required(ErrorMessage = "Icon  is required.")]
        public string Icon { get; set; }

        [Required(ErrorMessage = "Url is reuired.")]
        public string Url { get; set; }

        public string Sr { get; set; }
        public string imgPreview { get; set; }
        public List<HomeIndustries> HomeIndustriestList { get; set; }

        public string Img_Alt_Tag { get; set; }

    }
}