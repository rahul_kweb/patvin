﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class InnerBanner
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Inner banner is required.")]
        public string Image { get; set; }

        public string PageName { get; set; }

        public string Sr { get; set; }

        public List<InnerBanner> InnerBannerList { get; set; }

        public string imgPreview { get; set; }

        public string Img_Alt_Tag { get; set; }
    }
}