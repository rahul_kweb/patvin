﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;


namespace Patvin.Models
{
    public class SolutionsTechnologyIndustryDetail
    {

        public int Id { get; set; }

        public int SubId { get; set; }

        public int MasterId { get; set; }

        [Required(ErrorMessage = "Heading is required.")]
        public string Heading { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        public string Menu { get; set; }

        [Required(ErrorMessage = "PageName is required.")]
        public string PageName { get; set; }

        public List<SolutionsTechnologyIndustryDetail> SolutionsTechnologyIndustryDetailList { get; set; }

        public IEnumerable<SelectListItem> MasterList { get; set; }

        public string Sr { get; set; }

        [Required(ErrorMessage = "BannerImage is required.")]
        public string Image { get; set; }

        public string imgPreview { get; set; }

        public string Img_Alt_Tag { get; set; }

    }
}