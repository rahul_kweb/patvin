﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class RangeOfProduct
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Finishing Equipment is required.")]
        public string Equipment { get; set; }

        [Required(ErrorMessage = "Sealant and Adhesive Applications is required.")]
        public string Applications { get; set; }

        [Required(ErrorMessage = "Pumps is required.")]
        public string Pumps { get; set; }

        public List<RangeOfProduct> GetRangeOfProduct { get; set; }


    }
}