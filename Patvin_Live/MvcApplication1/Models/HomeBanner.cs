﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class HomeBanner
    {
        public int BannerId { get; set; }

        [Required(ErrorMessage = "Image file is required.")]
        public string Image { get; set;}

        [Required(ErrorMessage = "BannerText1  file is required.")]
        public string BannerText1 { get; set; }

        [Required(ErrorMessage = "BannerText2  file is required.")]
        public string BannerText2 { get; set; }


        [Required(ErrorMessage = "BannerText3  file is required.")]
        public string BannerText3 { get; set; }


        public int DisplayOrder { get; set; }

        public List<HomeBanner> HomeBannerList { get; set; }

        public string Sr { get; set; }

        public string imgPreview { get; set;}

        public string Url { get; set; }

        public string Img_Alt_Tag { get; set; }


    }
}