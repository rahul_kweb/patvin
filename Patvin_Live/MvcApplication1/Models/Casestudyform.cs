﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class Casestudyform
    {
        public int caseId { get; set; }

        public string Title { get; set; }

        [Required(ErrorMessage ="Name is required")]
        public string Name { get; set;}

        [Required(ErrorMessage = "Company Name is required")]
        public string CompanyName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }


        public string ApplyDate { get; set; }

        public string Sr { get; set; }

        public List<Casestudyform> CasestudyformList { get; set; }

    }
}