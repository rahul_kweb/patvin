﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class Testimonials
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Description is reqiured")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Name is reqiured")]    
        public string Name { get; set; }

        [Required(ErrorMessage = "Position is reqiured")]
        public string Position { get; set; }

        [Required(ErrorMessage = "Company Name is reqiured")]
        public string CompanyName { get; set; }

        public int DisplayOrder { get; set; }

        public List<Testimonials> TestimonialsList { get; set; }

        public string Sr { get; set; }
    }
}