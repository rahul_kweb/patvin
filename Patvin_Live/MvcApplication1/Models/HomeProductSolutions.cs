﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class HomeProductSolutions
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is Required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Url is Required.")]
        public string Url { get; set; }

        [Required(ErrorMessage = "Description is Required ")]
        public string Description { get; set; }


        [Required(ErrorMessage = "Image is required.")]

        public string Image { get; set; }

        public List<HomeProductSolutions> HomeProductSolutionsList { get; set; }

        public string Sr { get; set; }

        public string imgPreview { get; set; }

        public string Img_Alt_Tag { get; set; }
    }
}