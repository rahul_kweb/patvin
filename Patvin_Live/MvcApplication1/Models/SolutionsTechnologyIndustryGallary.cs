﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Patvin.Models
{
    public class SolutionsTechnologyIndustryGallary
    {
        public int GalleryId { get; set; } 

        public int Id { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Select any one type.")]
        public string Type { get; set; }

        [Required(ErrorMessage = "Image is required.")]
        public string Image { get; set; }

        [Required(ErrorMessage = "Video Url is required.")]
        public string Url { get; set; }

        [Required(ErrorMessage = "ThumbImg is required.")]
        public string ThumbImg { get; set; }

        public string imgPreview { get; set; }

        public string ThumbImgPreview { get; set; }

        public string Sr { get; set; }
        public List<SolutionsTechnologyIndustryGallary> SolutionsTechnologyIndustryGallaryList { get; set; }

        public string Img_Alt_Tag { get; set; }
    }
}