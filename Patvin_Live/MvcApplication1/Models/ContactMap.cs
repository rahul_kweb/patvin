﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class ContactMap
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is require")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is require")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Latitude is require")]
        public string Latitude { get; set; }

        [Required(ErrorMessage = "Longitude is require")]
        public string Longitude { get; set; }

        [Required(ErrorMessage = "Icon is require")]
        public string Image { get; set; }

        public string imgPreview { get; set; }

        public List<ContactMap> ContactMapList { get; set; }

        public string Sr { get; set; }

    }
}