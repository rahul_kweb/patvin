﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class CurrentOpenings
    {
        public int Id { get; set; }

        public string Title { get; set; }

        public string Location { get; set; }

        public int Post { get; set; }

        public string Qualification { get; set; }

        public string Experience { get; set; }

        public string Description { get; set; }

        public string Sr{get;set;}

        public List<CurrentOpenings> CurrentOpeningsList{get;set;}

    }
}