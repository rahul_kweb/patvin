﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class ReachUs
    {
        public int Id { get; set; }

        [Required(ErrorMessage ="Heading is require")]
        public string Heading { get; set; }

        [Required(ErrorMessage = "Description is require")]
        public string Description { get; set; }

        public string Contact { get; set; }

        [Required(ErrorMessage = "Image is require")]
        public string Image { get; set; }

        public string imgPreview { get; set; }

        public List<ReachUs> ReachUsList { get; set; }

        public string Fax { get; set; }

        public string Sr { get; set;}

        [Required(ErrorMessage = "Latitude is require")]
        public string Latitude { get; set; }

        [Required(ErrorMessage = "Longitude is require")]
        public string Longitude { get; set; }


        [Required(ErrorMessage = "Title is require")]
        public string Title { get; set; }
        
    }
}