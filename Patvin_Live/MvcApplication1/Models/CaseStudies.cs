﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class CaseStudies
    {
        public int Id { get; set; }

        [Required (ErrorMessage ="Image is required")]
        public string Image { get; set; }

        [Required(ErrorMessage = "Heading is required")]
        public string Heading { get; set; }

        [Required(ErrorMessage = "Category is required")]
        public string Category { get; set; }

        public string imgPreview { get; set; }
        
        public int DisplayOrder { get; set; }

        public string Sr { get; set; }

        public List<CaseStudies> CaseStudiesList { get; set; }

        public string Img_Alt_Tag { get; set; }
        

    }
}