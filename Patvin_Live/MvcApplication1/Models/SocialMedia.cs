﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class SocialMedia
    {

        public int Id { get; set; }

        public string CallUs { get; set; }

        public string MailUs { get; set; }

        public string opentime { get; set; }

        public string Sales { get; set; }

        public string Services { get; set; }

        public string Purchasing { get; set; }

        public string Dispatches { get; set; }

        public string Sales1 { get; set; }
    }
}