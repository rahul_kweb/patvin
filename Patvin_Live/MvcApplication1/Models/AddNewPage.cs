﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class AddNewPage
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "PageName is required.")]
        public string PageName { get; set; }

        [Required(ErrorMessage = "Banner Image is required.")]
        public string BannerImage { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        public string imgPreview { get; set; }

        public string Sr { get; set; }
        public List<AddNewPage> NewPageList { get; set; }

        public string Slug { get; set; }

        public string Img_Alt_Tag { get; set; }

    }
}