﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class QualityMissionVison
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Title is Required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Description is Required ")]
        public string Description { get; set; }
        public int DisplayOrder { get; set; }

        [Required(ErrorMessage = "Icon is Required ")]
        public string Icon { get; set; }

        public List<QualityMissionVison> QualityMissionVisonList { get; set; }

        public string Sr { get; set; }
    }
}