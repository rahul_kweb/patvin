﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class Services
    {

       public int ServId { get; set; }

        [Required(ErrorMessage = "Title is required")]
        public string Title { get; set; } 

        [Required(ErrorMessage = "Description is Required ")]
        public string Description { get; set; }

        [Required(ErrorMessage = "Icon is required")]
        public string Icon { get; set; }

        [Required(ErrorMessage ="Image is required")]
        public string Image { get; set; }

        public string iconPreview { get; set; }

        public string imgPreview { get; set; }

        public string Sr { get; set; }

        public List<Services> ServicesList { get; set; }

        public string Img_Alt_Tag { get; set; }
        public string Icon_Alt_Tag { get; set; }
    }
}