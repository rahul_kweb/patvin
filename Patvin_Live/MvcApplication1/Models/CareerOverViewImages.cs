﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class CareerOverViewImages
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Image is required.")]

        public string Image { get; set; }

        public List<CareerOverViewImages> CareerOverViewImagesList { get; set; }

        public int DisplayOrder { get; set; }

        public string Sr { get; set; }

        public string imgPreview { get; set; }

        public string Img_Alt_Tag { get; set; }
    }
}