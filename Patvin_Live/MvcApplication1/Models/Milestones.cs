﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class Milestone
    {
        public int MilesId { get; set; }

        public string Year { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        public List<Milestone> MilestoneList { get; set; }

        public string Sr { get; set; }
    }
}