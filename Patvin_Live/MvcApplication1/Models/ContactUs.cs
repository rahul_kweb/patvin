﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class ContactUs
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string FullName { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }


        [Required(ErrorMessage = "Phone number is required.")]
        [DataType(DataType.PhoneNumber)]
        [StringLength(10)]
        //[RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Enter a valid mobile number.")]
        [RegularExpression("^[0 - 9]{10}$|^[0-9]{10,12}$", ErrorMessage = "Enter a valid mobile number.")]
        public string ContactNo { get; set; }

        [Required(ErrorMessage = "Select any one Subject.")]
        public string Subject { get; set; }

        [Required(ErrorMessage = "Message is required.")]
        public string Comment { get; set; }

        public string ApplyDate { get; set; }

        public string Sr { get; set; }

        public List<ContactUs> ContactUsList { get; set; }

    }
}