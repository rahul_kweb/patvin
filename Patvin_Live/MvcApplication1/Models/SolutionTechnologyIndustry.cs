﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Patvin.Models
{
    public class SolutionsTechnologyIndustry
    {
        public int SubId { get; set; }

        public int MasterId { get; set; }

        //[Required(ErrorMessage = "Menu is required.")]
        public string Menu { get; set; }

        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        public string Slug { get; set; }

        public string Sr { get; set; }
        public List<SolutionsTechnologyIndustry> SolutionsTechnologyIndustrylist { get; set; }

        public IEnumerable<SelectListItem> MasterList { get; set; }

        public int DisplayOrder { get; set; }
    }
}