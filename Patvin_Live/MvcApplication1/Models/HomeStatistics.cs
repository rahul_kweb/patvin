﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class HomeStatistics
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Icon is reqiured")]
        public string Icon { get; set; }

        [Required(ErrorMessage = "Count is reqiured")]
        public string Count { get; set; }

        [Required(ErrorMessage ="Description is reqiured")]
        public string Description { get; set; }


        public int DisplayOrder { get; set; }

        public List<HomeStatistics> HomeStatisticsList { get; set; }

        public string Sr { get; set; }


    }
}