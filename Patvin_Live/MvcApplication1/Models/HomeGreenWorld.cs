﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class HomeGreenWorld
    {
        public int Id { get; set; }

        public string Image { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string Description { get; set; }

        public string Sr { get; set; }

        public List<HomeGreenWorld> HomeGreenWorldList { get; set; }

        public string imgPreview { get; set; }
    }
}