﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class HomeSolutions
    {

        public int Id { get; set; }

        [Required(ErrorMessage ="Image file is required")]
        public string Image { get; set; }

        public int DisplayOrder { get; set; }

        public List<HomeSolutions> HomeSolutionsList { get; set; }

        public string Sr { get; set; }

        public string imgPreview { get; set; }

        public string Img_Alt_Tag { get; set; }

    }

}