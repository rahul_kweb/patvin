﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class ApplyForJob
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "Name is required.")]
        public string Name { get; set; }

        [Required(ErrorMessage = "Email address is required.")]
        [RegularExpression("^[a-zA-Z0-9_\\.-]+@([a-zA-Z0-9-]+\\.)+[a-zA-Z]{2,6}$", ErrorMessage = "Invalid email address.")]
        public string Email { get; set; }
 
        public string Role { get; set; }
        public string Location { get; set; }


        [Required(ErrorMessage = "mobile number is required.")]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression("^[0 - 9]{10}$|^[0-9]{10,12}$", ErrorMessage = "Enter a valid mobile number.")]

        public string ContactNo { get; set; }

        [Required(ErrorMessage = "Resume is required.")]
        [RegularExpression(@"([a-zA-Z0-9\s_\\.\-:])+(.doc|.docx|.pdf)$", ErrorMessage = "Only Document files allowed.")]
        public string Resume { get; set; }

        public string ApplyDate { get; set; }

        public string Sr { get; set; }

        public List<ApplyForJob> ApplyForJobList { get; set; }


        public string fakePath { get; set; }
    }
}