﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Patvin.Models
{
    public class MetaTag
    {
        public int Id { get; set; }

        [Required(ErrorMessage = "PageUrl is required.")]
        public string PageUrl { get; set; }


        [Required(ErrorMessage = "Title is required.")]
        public string Title { get; set; }

        [Required(ErrorMessage = "Keywords is required.")]
        public string MetaKeyword { get; set; }

        [Required(ErrorMessage = "Description is required.")]
        public string MetaDescription { get; set; }

        public string Sr { get; set; }
        public List<MetaTag> MetaTagList { get; set; }

        public string CanonicalTag { get; set; }


    }
}