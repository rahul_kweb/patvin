﻿using Patvin.Models;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace Patvin.Controllers
{
    public class EmailSettingController : Controller
    {
        //
        // GET: /EmailSetting/

        public ActionResult Index()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Index(EmailSetting objEmail)
        {
            try
            {
                string[] to = objEmail.To.Split(',');
                string from = objEmail.From;
                string[] cc = null;
                string[] bcc = null;

                if (objEmail.CC != null)
                {
                    cc = objEmail.CC.Split(',');
                }

                if (objEmail.BCC != null)
                {
                    bcc = objEmail.BCC.Split(',');
                }

                string subject = "TEST MAIL";
                string body = objEmail.Body;

                string email_user = objEmail.Username;
                string email_pwd = objEmail.Password;

                MailMessage message = new MailMessage();
                message.From = new MailAddress(from);
                foreach (string to_item in to)
                {
                    if (!string.IsNullOrEmpty(to_item))
                    {
                        message.To.Add(to_item.Trim());
                    }
                }

                if (objEmail.CC != null)
                {
                    foreach (string cc_item in cc)
                    {
                        if (!string.IsNullOrEmpty(cc_item))
                        {
                            message.CC.Add(cc_item.Trim());
                        }
                    }
                }

                if (objEmail.BCC != null)
                {
                    foreach (string bcc_item in bcc)
                    {
                        if (!string.IsNullOrEmpty(bcc_item))
                        {
                            message.Bcc.Add(bcc_item.Trim());
                        }
                    }
                }

                message.Subject = subject;
                message.Body = objEmail.IsBodyHtml ? string.Format("<b>{0}</b>", body) : body;
                message.IsBodyHtml = objEmail.IsBodyHtml;
                SmtpClient client = new SmtpClient(objEmail.ServerName);
                client.Port = int.Parse(objEmail.Port);
                client.UseDefaultCredentials = objEmail.UserDefaultCredentials;
                client.EnableSsl = objEmail.EnableSSL;
                client.Credentials = new NetworkCredential(email_user, email_pwd);


                if (objEmail.WriteEmailToDisk)
                {
                    client.DeliveryMethod = SmtpDeliveryMethod.SpecifiedPickupDirectory;
                    client.PickupDirectoryLocation = Server.MapPath("~/emails");
                }
                else
                {
                    client.DeliveryMethod = SmtpDeliveryMethod.Network;
                }

                client.Send(message);

                SetResult("Success", Color.Green);
            }
            catch (Exception ex)
            {
                //lblResponse.Text = "Failed due to!!<br />" + ex.Message + "<br />" + ex.StackTrace + "<br />" + ex.Source + "<br />";
                SetResult("Failed due to!!<br />" + GetErrorMessage(ex), Color.Red);
            }

            return View();
        }

        public void SetResult(string text, Color color)
        {
            ViewBag.Result = string.Format("Result : {0}", text);
        }

        public string GetErrorMessage(Exception ex)
        {
            Exception exp = ex;
            string error = string.Empty;

            if (ex != null)
            {
                error += exp.Message;
                error += "<br />=========================================";
                error += exp.StackTrace;

                exp = exp.InnerException;
                while (exp != null)
                {
                    error += "<br />=========================================";
                    error += exp.Message;
                    error += "<br />=========================================";
                    error += exp.StackTrace;
                }
            }

            return error;
        }

    }
}
