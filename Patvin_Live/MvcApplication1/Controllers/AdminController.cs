﻿using Patvin.Authentication;
using Patvin.Models;
using ClosedXML.Excel;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Patvin.Controllers
{
    [CustomAdminAccessFilter]
    public class AdminController : Controller
    {
        //
        // GET: /BillingDashboard/
        Utility util = new Utility();

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Admin Index --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Index()
        {
            var count = Counting();
            ViewBag.CaseStudy = count["CaseStudy"].ToString();
            ViewBag.ApplyForJob = count["ApplyForJob"].ToString();
            ViewBag.ContactUs = count["ContactUs"].ToString();
            ViewBag.GetInTouch = count["GetInTouch"].ToString();
            return View();
        }

        public Dictionary<string, string> Counting()
        {
            Dictionary<string, string> objDic = new Dictionary<string, string>();
            StringBuilder strbuil = new StringBuilder();
            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_CMS 'adminIndexCount'");
            if (ds.Tables.Count > 0)
            {
                //Case Study
                objDic.Add(ds.Tables[0].Rows[0]["Tbl"].ToString(), ds.Tables[0].Rows[0]["Count"].ToString());

                //ApplyForJob
                objDic.Add(ds.Tables[1].Rows[0]["Tbl"].ToString(), ds.Tables[1].Rows[0]["Count"].ToString());

                //ContactUs
                objDic.Add(ds.Tables[2].Rows[0]["Tbl"].ToString(), ds.Tables[2].Rows[0]["Count"].ToString());

                //Get In Touch
                objDic.Add(ds.Tables[3].Rows[0]["Tbl"].ToString(), ds.Tables[3].Rows[0]["Count"].ToString());
            }
            return objDic;
        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--- Admin Profile Section Start---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult UpdateProfile()
        {
            DataTable dt = GetAdminDataForProfileUpdate();
            Login log = new Login();

            log.Id = dt.Rows[0]["Id"].ToString();
            log.Name = dt.Rows[0]["Name"].ToString();
            log.Emailid = dt.Rows[0]["Emailid"].ToString();
            log.Mobileno = dt.Rows[0]["Mobileno"].ToString();
            return View(log);
        }

        public DataTable GetAdminDataForProfileUpdate()
        {
            DataTable dt = new DataTable();
            try
            {
                dt = util.Display("Execute Proc_SuperAdminLogin 'GETDETAIL_FOR_UPDATE'");
            }
            catch (Exception ex)
            {

                throw ex;
            }
            return dt;
        }

        [HttpPost]
        public JsonResult UpdateProfile(Login logobj)
        {
            var response = "";

            try
            {
                response = UpdateProfileDetails(logobj.Id, logobj.Name, logobj.Emailid, logobj.Mobileno);
            }
            catch
            {
                response = "";
            }
            string Url = string.Empty;
            string Responsetype = response == "" ? "Fail" : "Success";

            return Json(new { Responsetype = Responsetype }, JsonRequestBehavior.AllowGet);
        }

        public string UpdateProfileDetails(string id, string name, string emailid, string mobileno)
        {
            string Response = string.Empty;
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@PARA", "UPDATEPROFILEDETAILS");
                    cmd.Parameters.AddWithValue("@NAME", name);
                    cmd.Parameters.AddWithValue("@EMAILID", emailid);
                    cmd.Parameters.AddWithValue("@MOBILENO", mobileno);
                    cmd.Parameters.AddWithValue("@ID", id);
                    if (util.Execute(cmd))
                    {
                        Response = "success";
                    }
                    else
                    {
                        Response = "";
                    }
                }
            }
            catch
            {

                Response = "";
            }
            return Response;
        }


        public ActionResult ChangePassword()
        {
            return View();
        }
        [HttpPost]
        public ActionResult ChangePassword(Login obj)
        {
            using (SqlCommand cmd = new SqlCommand("Proc_SuperAdminLogin"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@PARA", "CHANGE_PASSWORD");
                cmd.Parameters.AddWithValue("@USERNAME", Request.Cookies["SuperAdminusername"].Value);
                cmd.Parameters.AddWithValue("@PASSWORD", obj.OldPassword);
                cmd.Parameters.AddWithValue("@NEWPASSWORD", obj.Password);
                DataTable dt = new DataTable();
                dt = util.Display(cmd);
                if (dt.Rows[0]["update"].ToString() == "correct")
                {
                    ViewBag.message = "Success";
                    ViewBag.message1 = "Your Password has been changed Successfully!";
                }
                else
                {
                    ViewBag.message = "fail";
                    ViewBag.message1 = "Sorry ! Password Could not be changed Successfully!";
                }
                ModelState.Clear(); //make blank to class property
            }
            return View();
        }

        #endregion xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx--- Billing Profile Section End---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Home Banner --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult HomeBanner()
        {
            HomeBanner objHomeBanner = new HomeBanner();
            objHomeBanner.HomeBannerList = GetHomeBannerList();
            return View(objHomeBanner);
        }

        public List<HomeBanner> GetHomeBannerList()
        {
            try
            {
                List<HomeBanner> objHomeBanner = new List<HomeBanner>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Banner 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        HomeBanner banner = new HomeBanner();
                        banner.Sr = dr["Sr"].ToString();
                        banner.BannerId = int.Parse(dr["BannerId"].ToString());
                        banner.Image = dr["Image"].ToString();
                        banner.BannerText1 = dr["BannerText1"].ToString();
                        banner.BannerText2 = dr["BannerText2"].ToString();
                        banner.BannerText3 = dr["BannerText3"].ToString();
                        banner.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        banner.Url = dr["Url"].ToString();
                        banner.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objHomeBanner.Add(banner);
                    }
                }
                return objHomeBanner;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HomeBanner(HomeBanner objHomeBanner, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objHomeBanner.BannerId != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/HomeBanner/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("HomeBanner");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Banner-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objHomeBanner.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objHomeBanner.BannerId == 0)
                    {
                        if (SaveHomeBanner(objHomeBanner))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Banner added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeBanner");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Banner could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeBanner");
                        }
                    }
                    else
                    {
                        if (UpdateHomeBanner(objHomeBanner))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Banner updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeBanner");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Banner could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeBanner");
                        }
                    }
                }
                return View("HomeBanner", objHomeBanner);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("HomeBanner");
            }

        }

        public bool SaveHomeBanner(HomeBanner objHomeBanner)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objHomeBanner.Image);
                cmd.Parameters.AddWithValue("@BannerText1", objHomeBanner.BannerText1);
                cmd.Parameters.AddWithValue("@BannerText2", objHomeBanner.BannerText2);
                cmd.Parameters.AddWithValue("@BannerText3", objHomeBanner.BannerText3);
                cmd.Parameters.AddWithValue("@DisplayOrder", objHomeBanner.DisplayOrder);
                cmd.Parameters.AddWithValue("@Url", objHomeBanner.Url);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomeBanner.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateHomeBanner(HomeBanner objHomeBanner)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@BannerId", objHomeBanner.BannerId);
                cmd.Parameters.AddWithValue("@Image", objHomeBanner.Image);
                cmd.Parameters.AddWithValue("@BannerText1", objHomeBanner.BannerText1);
                cmd.Parameters.AddWithValue("@BannerText2", objHomeBanner.BannerText2);
                cmd.Parameters.AddWithValue("@BannerText3", objHomeBanner.BannerText3);
                cmd.Parameters.AddWithValue("@DisplayOrder", objHomeBanner.DisplayOrder);
                cmd.Parameters.AddWithValue("@Url", objHomeBanner.Url);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomeBanner.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditHomeBanner(int? Id)
        {
            HomeBanner objHomeBanner = new HomeBanner();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Banner 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objHomeBanner.BannerId = Convert.ToInt32(dt.Rows[0]["BannerId"].ToString());
                objHomeBanner.Image = dt.Rows[0]["Image"].ToString();
                objHomeBanner.imgPreview = dt.Rows[0]["Image"].ToString();
                objHomeBanner.BannerText1 = dt.Rows[0]["BannerText1"].ToString();
                objHomeBanner.BannerText2 = dt.Rows[0]["BannerText2"].ToString();
                objHomeBanner.BannerText3 = dt.Rows[0]["BannerText3"].ToString();
                objHomeBanner.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objHomeBanner.Url = dt.Rows[0]["Url"].ToString();
                objHomeBanner.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objHomeBanner.HomeBannerList = GetHomeBannerList();

                ViewBag.ActionType = "Update";
                return View("HomeBanner", objHomeBanner);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("HomeBanner", objHomeBanner);
            }
        }

        public ActionResult DeleteHomeBanner(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Banner"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@BannerId", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Banner deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeBanner");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Banner could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeBanner");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("HomeBanner");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx ---- Home Solution ---xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult HomeSolutions()
        {
            HomeSolutions objHomeSolutions = new HomeSolutions();
            objHomeSolutions.HomeSolutionsList = GetHomeSolutionsList();
            return View(objHomeSolutions);
        }


        public List<HomeSolutions> GetHomeSolutionsList()
        {
            try
            {
                List<HomeSolutions> objHomeSolutions = new List<HomeSolutions>();
                DataTable dt = new DataTable();
                dt = util.Display("Exec Proc_HomeSolutions 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        HomeSolutions objSolution = new HomeSolutions();
                        objSolution.Id = int.Parse(dr["Id"].ToString());
                        objSolution.Sr = dr["Sr"].ToString();
                        objSolution.Image = dr["Image"].ToString();
                        objSolution.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        objSolution.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objHomeSolutions.Add(objSolution);
                    }

                }

                return objHomeSolutions;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";
                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        [HttpPost]
        [ValidateInput(false)]

        public ActionResult HomeSolutions(HomeSolutions objHomesolution, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objHomesolution.Id != 0)
                {
                    ModelState.Remove("Image");
                }
                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/HomeSolutions/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            if (file != null)
                            {

                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("HomeSolutions");
                                }

                                else
                                {
                                    var fileName = "HomeSolution-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img = fileName + ",";
                                }
                            }

                        }
                        if (Img != "")
                        {
                            objHomesolution.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objHomesolution.Id == 0)
                    {
                        if (SaveHomeSolution(objHomesolution))
                        {
                            ModelState.Clear();
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Image added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeSolutions");

                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Image could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeSolutions");
                        }
                    }
                    else
                    {
                        if (UpdateHomeSolution(objHomesolution))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Image updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeSolutions");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Image could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeSolutions");

                        }
                    }

                }
                return View("HomeSolutions", objHomesolution);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("HomeBanner");
            }

        }

        public bool SaveHomeSolution(HomeSolutions objHomesolution)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeSolutions"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "add");
                cmd.Parameters.AddWithValue("@Image", objHomesolution.Image);
                cmd.Parameters.AddWithValue("@DisplayOrder", objHomesolution.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomesolution.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateHomeSolution(HomeSolutions objHomesolution)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeSolutions"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "update");
                cmd.Parameters.AddWithValue("@Id", objHomesolution.Id);
                cmd.Parameters.AddWithValue("@Image", objHomesolution.Image);
                cmd.Parameters.AddWithValue("@DisplayOrder", objHomesolution.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomesolution.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditHomeSolutions(int? Id)
        {
            HomeSolutions objHomesolution = new HomeSolutions();
            DataTable dt = new DataTable();
            dt = util.Display("Execute  Proc_HomeSolutions 'getbyId','" + Id + "' ");
            if (dt.Rows.Count > 0)
            {
                objHomesolution.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                objHomesolution.Image = dt.Rows[0]["Image"].ToString();
                objHomesolution.DisplayOrder = int.Parse(dt.Rows[0]["DisplayOrder"].ToString());
                objHomesolution.imgPreview = dt.Rows[0]["Image"].ToString();
                objHomesolution.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objHomesolution.HomeSolutionsList = GetHomeSolutionsList();

                ViewBag.ActionType = "Update";
                return View("HomeSolutions", objHomesolution);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("HomeSolutions", objHomesolution);
            }


        }



        public ActionResult DeleteHomeSolutions(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_HomeSolutions"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Banner deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeSolutions");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Banner could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeSolutions");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("HomeSolutions");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Home Statistics --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult HomeStatistics()
        {
            HomeStatistics objstatistics = new HomeStatistics();
            objstatistics.HomeStatisticsList = GetHomeStatisticsList();

            return View(objstatistics);
        }


        public List<HomeStatistics> GetHomeStatisticsList()
        {
            try
            {
                List<HomeStatistics> objHomeStatistics = new List<HomeStatistics>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_HomeStatistic 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        HomeStatistics objstatistics = new HomeStatistics();
                        objstatistics.Sr = dr["Sr"].ToString();
                        objstatistics.Id = int.Parse(dr["Id"].ToString());
                        objstatistics.Icon = dr["Icon"].ToString();
                        objstatistics.Count = dr["Counting"].ToString();
                        objstatistics.Description = dr["Description"].ToString();
                        //objstatistics.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        objHomeStatistics.Add(objstatistics);


                    }
                }
                return objHomeStatistics;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HomeStatistics(HomeStatistics objStatistics)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (objStatistics.Id == 0)
                    {
                        if (SaveHomeStatistics(objStatistics))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Home Statastics added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeStatistics");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Home Statastics could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeStatistics");
                        }
                    }
                    else
                    {
                        if (UpdateHomeStatistics(objStatistics))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Home Statastics updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeStatistics");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Home Statastics could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeStatistics");
                        }
                    }
                }
                return View("HomeStatistics", objStatistics);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("HomeStatistics");
            }

        }


        public bool SaveHomeStatistics(HomeStatistics objStatistics)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeStatistic"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "Add");
                cmd.Parameters.AddWithValue("@Icon", objStatistics.Icon);
                cmd.Parameters.AddWithValue("@Counting", objStatistics.Count);
                cmd.Parameters.AddWithValue("@Description", objStatistics.Description);
                //cmd.Parameters.AddWithValue("@DisplayOrder", objStatistics.DisplayOrder);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }



        public bool UpdateHomeStatistics(HomeStatistics objStatistics)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeStatistic"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objStatistics.Id);
                cmd.Parameters.AddWithValue("@Icon", objStatistics.Icon);
                cmd.Parameters.AddWithValue("@Counting", objStatistics.Count);
                cmd.Parameters.AddWithValue("@Description", objStatistics.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }




        public ActionResult EditHomeStatistics(int? Id)
        {
            HomeStatistics objStatistics = new HomeStatistics();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeStatistic 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objStatistics.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objStatistics.Icon = dt.Rows[0]["Icon"].ToString();
                objStatistics.Count = dt.Rows[0]["Counting"].ToString();
                objStatistics.Description = dt.Rows[0]["Description"].ToString();
                //objStatistics.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());

                objStatistics.HomeStatisticsList = GetHomeStatisticsList();

                ViewBag.ActionType = "Update";
                return View("HomeStatistics", objStatistics);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("HomeStatistics", objStatistics);
            }
        }


        public ActionResult DeleteHomeStatistics(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_HomeStatistic"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Home Statastics deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeStatistics");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Home Statastics could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeStatistics");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("HomeStatistics");
                //throw;
            }

        }
        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Home Industries -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult HomeIndustries()
        {
            HomeIndustries objHomeIndustries = new HomeIndustries();
            objHomeIndustries.HomeIndustriestList = GetHomeIndustries();
            return View(objHomeIndustries);
        }


        public List<HomeIndustries> GetHomeIndustries()
        {
            try
            {
                List<HomeIndustries> objHomeIndustries = new List<HomeIndustries>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_HomeIndustries 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        HomeIndustries objIndustries = new HomeIndustries();
                        objIndustries.Sr = dr["Sr"].ToString();
                        objIndustries.Id = Convert.ToInt32(dr["Id"].ToString());
                        objIndustries.Heading = dr["Heading"].ToString();
                        objIndustries.Icon = dr["Icon"].ToString();
                        objIndustries.Url = dr["Url"].ToString();
                        objIndustries.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objHomeIndustries.Add(objIndustries);
                    }
                }
                return objHomeIndustries;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HomeIndustries(HomeIndustries objHomeIndustries, HttpPostedFileBase[] Icon)
        {
            try
            {
                if (objHomeIndustries.Id != 0)
                {
                    ModelState.Remove("Icon");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/HomeIndustries/";
                    string Img = string.Empty;

                    if (Icon != null)
                    {
                        foreach (HttpPostedFileBase file in Icon)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("HomeIndustries");
                                }
                                else
                                {
                                    var fileName = "HomeIndustries-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objHomeIndustries.Icon = Img.Substring(0, Img.Length - 1);
                        }
                    }


                    if (objHomeIndustries.Id == 0)
                    {
                        if (SaveHomeIndustries(objHomeIndustries))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Industries added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeIndustries");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Industries could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeIndustries");
                        }
                    }
                    else
                    {
                        if (UpdateHomeIndustries(objHomeIndustries))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Industries updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeIndustries");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Industries could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeIndustries");
                        }
                    }
                }
                return View("HomeIndustries", objHomeIndustries);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("HomeIndustries");
            }

        }


        public bool SaveHomeIndustries(HomeIndustries objHomeIndustries)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeIndustries"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Heading", objHomeIndustries.Heading);
                cmd.Parameters.AddWithValue("@Icon", objHomeIndustries.Icon);
                cmd.Parameters.AddWithValue("@Url", objHomeIndustries.Url);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomeIndustries.Img_Alt_Tag);
                
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public bool UpdateHomeIndustries(HomeIndustries objHomeIndustries)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeIndustries"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objHomeIndustries.Id);
                cmd.Parameters.AddWithValue("@Heading", objHomeIndustries.Heading);
                cmd.Parameters.AddWithValue("@Icon", objHomeIndustries.Icon);
                cmd.Parameters.AddWithValue("@Url", objHomeIndustries.Url);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomeIndustries.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditHomeIndustries(int? Id)
        {
            HomeIndustries objHomeIndustries = new HomeIndustries();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeIndustries 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objHomeIndustries.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objHomeIndustries.Heading = dt.Rows[0]["Heading"].ToString();
                objHomeIndustries.Icon = dt.Rows[0]["Icon"].ToString();
                objHomeIndustries.Url = dt.Rows[0]["Url"].ToString();
                objHomeIndustries.imgPreview = dt.Rows[0]["Icon"].ToString();
                objHomeIndustries.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();
               

                objHomeIndustries.HomeIndustriestList = GetHomeIndustries();

                ViewBag.ActionType = "Update";
                return View("HomeIndustries", objHomeIndustries);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("HomeIndustries", objHomeIndustries);
            }
        }

        public ActionResult DeleteHomeIndustries(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_HomeIndustries"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Industries deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeIndustries");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Industries could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeIndustries");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("HomeIndustries");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxx -- Home Home GreenWorld -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult HomeGreenWorld()
        {
            HomeGreenWorld objHomeGreenWorld = new HomeGreenWorld();
            objHomeGreenWorld.HomeGreenWorldList = GetHomeGreenWorld();
            return View(objHomeGreenWorld);
        }


        public List<HomeGreenWorld> GetHomeGreenWorld()
        {
            try
            {
                List<HomeGreenWorld> objHomeGreenWorld = new List<HomeGreenWorld>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_HomeGreenWorld 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        HomeGreenWorld obj = new HomeGreenWorld();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = Convert.ToInt32(dr["Id"].ToString());
                        obj.Description = dr["Descrption"].ToString();
                        obj.Image = dr["Image"].ToString();


                        objHomeGreenWorld.Add(obj);
                    }
                }
                return objHomeGreenWorld;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HomeGreenWorld(HomeGreenWorld objHomeGreenWorld, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objHomeGreenWorld.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("objHomeGreenWorld");
                                }
                                else
                                {
                                    var fileName = "HomeIndustries-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objHomeGreenWorld.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }


                    if (objHomeGreenWorld.Id == 0)
                    {
                        if (SaveHomeIndustries(objHomeGreenWorld))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Industries added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeGreenWorld");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Industries could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeGreenWorld");
                        }
                    }
                    else
                    {
                        if (UpdateHomeGreenWorld(objHomeGreenWorld))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Industries updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeGreenWorld");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Industries could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeIndustries");
                        }
                    }
                }
                return View("HomeGreenWorld", objHomeGreenWorld);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("HomeGreenWorld");
            }

        }



        public bool SaveHomeIndustries(HomeGreenWorld objHomeGreenWorld)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeGreenWorld"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Descrption", objHomeGreenWorld.Description);
                cmd.Parameters.AddWithValue("@Image", objHomeGreenWorld.Image);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public bool UpdateHomeGreenWorld(HomeGreenWorld objHomeGreenWorld)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeGreenWorld"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objHomeGreenWorld.Id);
                cmd.Parameters.AddWithValue("@Descrption", objHomeGreenWorld.Description);
                cmd.Parameters.AddWithValue("@Image", objHomeGreenWorld.Image);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public ActionResult EditHomeGreenWorld(int? Id)
        {
            HomeGreenWorld objHomeGreenWorld = new HomeGreenWorld();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeGreenWorld 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objHomeGreenWorld.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objHomeGreenWorld.Description = dt.Rows[0]["Descrption"].ToString();
                objHomeGreenWorld.Image = dt.Rows[0]["Image"].ToString();
                objHomeGreenWorld.imgPreview = dt.Rows[0]["Image"].ToString();

                objHomeGreenWorld.HomeGreenWorldList = GetHomeGreenWorld();

                ViewBag.ActionType = "Update";
                return View("HomeGreenWorld", objHomeGreenWorld);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("HomeGreenWorld", objHomeGreenWorld);
            }
        }

        public ActionResult DeleteHomeGreenWorld(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_HomeGreenWorld"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeGreenWorld");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeGreenWorld");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("HomeGreenWorld");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxx -- Home Product Solution -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult HomeProductSolutions()
        {
            HomeProductSolutions objHomeProductSolutions = new HomeProductSolutions();
            objHomeProductSolutions.HomeProductSolutionsList = GetHomeProductSolutionsList();
            return View(objHomeProductSolutions);
        }

        public List<HomeProductSolutions> GetHomeProductSolutionsList()
        {
            try
            {
                List<HomeProductSolutions> objHomeProductSolutions = new List<HomeProductSolutions>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_HomeProductSolutions 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        HomeProductSolutions obj = new HomeProductSolutions();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Title = dr["Title"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.Url = dr["Url"].ToString();
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objHomeProductSolutions.Add(obj);
                    }
                }
                return objHomeProductSolutions;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult HomeProductSolutions(HomeProductSolutions objHomeProductSolutions, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objHomeProductSolutions.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("HomeProductSolutions");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "HomeProductSolutions-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objHomeProductSolutions.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objHomeProductSolutions.Id == 0)
                    {
                        if (SaveHomeProductSolutions(objHomeProductSolutions))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeProductSolutions");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeProductSolutions");
                        }
                    }
                    else
                    {
                        if (UpdateHomeProductSolutions(objHomeProductSolutions))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeProductSolutions");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("HomeProductSolutions");
                        }
                    }
                }
                return View("CareerOurPeople", objHomeProductSolutions);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("HomeProductSolutions");
            }

        }

        public bool SaveHomeProductSolutions(HomeProductSolutions objHomeProductSolutions)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeProductSolutions"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objHomeProductSolutions.Image);
                cmd.Parameters.AddWithValue("@Title", objHomeProductSolutions.Title);
                cmd.Parameters.AddWithValue("@Url", objHomeProductSolutions.Url);
                cmd.Parameters.AddWithValue("@Description", objHomeProductSolutions.Description);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomeProductSolutions.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateHomeProductSolutions(HomeProductSolutions objHomeProductSolutions)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_HomeProductSolutions"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objHomeProductSolutions.Id);
                cmd.Parameters.AddWithValue("@Image", objHomeProductSolutions.Image);
                cmd.Parameters.AddWithValue("@Title", objHomeProductSolutions.Title);
                cmd.Parameters.AddWithValue("@Url", objHomeProductSolutions.Url);
                cmd.Parameters.AddWithValue("@Description", objHomeProductSolutions.Description);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objHomeProductSolutions.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditHomeProductSolutions(int? Id)
        {
            HomeProductSolutions objHomeProductSolutions = new HomeProductSolutions();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeProductSolutions 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objHomeProductSolutions.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objHomeProductSolutions.Image = dt.Rows[0]["Image"].ToString();
                objHomeProductSolutions.imgPreview = dt.Rows[0]["Image"].ToString();
                objHomeProductSolutions.Title = dt.Rows[0]["Title"].ToString();
                objHomeProductSolutions.Url  = dt.Rows[0]["Url"].ToString();
                objHomeProductSolutions.Description = dt.Rows[0]["Description"].ToString();
                objHomeProductSolutions.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objHomeProductSolutions.HomeProductSolutionsList = GetHomeProductSolutionsList();

                ViewBag.ActionType = "Update";
                return View("HomeProductSolutions", objHomeProductSolutions);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("HomeProductSolutions", objHomeProductSolutions);
            }
        }

        public ActionResult DeleteHomeProductSolutions(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_HomeProductSolutions"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeProductSolutions");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("HomeProductSolutions");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("HomeProductSolutions");
                //throw;
            }

        }

        #endregion

        #region  xxxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Who We Are Quality Mission Vison---- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        public ActionResult QualityMissionVison()
        {
            QualityMissionVison objQualityMissionVison = new QualityMissionVison();
            objQualityMissionVison.QualityMissionVisonList = GetQualityMissionVisonList();
            return View(objQualityMissionVison);
        }

        public List<QualityMissionVison> GetQualityMissionVisonList()
        {
            try
            {
                List<QualityMissionVison> objQualityMissionVisonList = new List<QualityMissionVison>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_QualityMissionVison 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        QualityMissionVison obj = new QualityMissionVison();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Title = dr["Title"].ToString();
                        obj.Icon = dr["Icon"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());

                        objQualityMissionVisonList.Add(obj);
                    }
                }
                return objQualityMissionVisonList;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult QualityMissionVison(QualityMissionVison objQualityMissionVison)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (objQualityMissionVison.Id == 0)
                    {
                        if (SaveQualityMissionVison(objQualityMissionVison))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("QualityMissionVison");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("QualityMissionVison");
                        }
                    }
                    else
                    {
                        if (UpdateQualityMissionVison(objQualityMissionVison))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("QualityMissionVison");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("QualityMissionVison");
                        }
                    }
                }
                return View("QualityMissionVison");
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("QualityMissionVison");
            }

        }

        public bool SaveQualityMissionVison(QualityMissionVison objQualityMissionVison)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_QualityMissionVison"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Title", objQualityMissionVison.Title);
                cmd.Parameters.AddWithValue("@Icon", objQualityMissionVison.Icon);
                cmd.Parameters.AddWithValue("@Description", objQualityMissionVison.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objQualityMissionVison.DisplayOrder);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateQualityMissionVison(QualityMissionVison objQualityMissionVison)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_QualityMissionVison"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objQualityMissionVison.Id);
                cmd.Parameters.AddWithValue("@Title", objQualityMissionVison.Title);
                cmd.Parameters.AddWithValue("@Icon", objQualityMissionVison.Icon);
                cmd.Parameters.AddWithValue("@Description", objQualityMissionVison.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objQualityMissionVison.DisplayOrder);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditQualityMissionVison(int? Id)
        {
            QualityMissionVison objQualityMissionVison = new QualityMissionVison();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_QualityMissionVison 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objQualityMissionVison.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                objQualityMissionVison.Title = dt.Rows[0]["Title"].ToString();
                objQualityMissionVison.Icon = dt.Rows[0]["Icon"].ToString();
                objQualityMissionVison.Description = dt.Rows[0]["Description"].ToString();
                objQualityMissionVison.DisplayOrder = int.Parse(dt.Rows[0]["DisplayOrder"].ToString());




                objQualityMissionVison.QualityMissionVisonList = GetQualityMissionVisonList();

                ViewBag.ActionType = "Update";
                return View("QualityMissionVison", objQualityMissionVison);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("QualityMissionVison", objQualityMissionVison);
            }
        }

        public ActionResult DeleteQualityMissionVison(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_QualityMissionVison"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);

                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("QualityMissionVison");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("QualityMissionVison");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("QualityMissionVison");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --- Who We Are Milestones --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        public ActionResult Milestone()
        {
            Milestone objMilestone = new Milestone();
            objMilestone.MilestoneList = GetMilestoneList();
            return View(objMilestone);
        }

        public List<Milestone> GetMilestoneList()
        {
            try
            {
                List<Milestone> objMilestone = new List<Milestone>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Milestones 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Milestone milestone = new Milestone();
                        milestone.Sr = dr["Sr"].ToString();
                        milestone.MilesId = int.Parse(dr["MilesId"].ToString());
                        milestone.Year = dr["Year"].ToString();
                        milestone.Description = dr["Description"].ToString();

                        objMilestone.Add(milestone);
                    }
                }
                return objMilestone;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        public ActionResult Milestone(Milestone objMilestone)
        {
            try
            {

                if (ModelState.IsValid)
                {
                    if (objMilestone.MilesId == 0)
                    {
                        if (SaveMilestone(objMilestone))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Milestone added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Milestone");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Milestone could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Milestone");
                        }
                    }
                    else
                    {
                        if (UpdateMilestone(objMilestone))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Milestone updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Milestone");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Milestone could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Milestone");
                        }
                    }
                }
                return View("Milestone", objMilestone);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Milestone");
            }

        }

        public bool SaveMilestone(Milestone objMilestone)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Milestones"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Year", objMilestone.Year);
                cmd.Parameters.AddWithValue("@Description", objMilestone.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateMilestone(Milestone objMilestone)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Milestones"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@MilesId", objMilestone.MilesId);
                cmd.Parameters.AddWithValue("@Year", objMilestone.Year);
                cmd.Parameters.AddWithValue("@Description", objMilestone.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditMilestone(int? Id)
        {
            Milestone objMilestone = new Milestone();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Milestones 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objMilestone.MilesId = Convert.ToInt32(dt.Rows[0]["MilesId"].ToString());
                objMilestone.Year = dt.Rows[0]["Year"].ToString();
                objMilestone.Description = dt.Rows[0]["Description"].ToString();

                objMilestone.MilestoneList = GetMilestoneList();

                ViewBag.ActionType = "Update";
                return View("Milestone", objMilestone);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Milestone", objMilestone);
            }
        }

        public ActionResult DeleteMilestone(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Milestones"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@MilesId", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Milestone deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Milestone");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Milestone could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Milestone");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Milestone");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Who We Are Infrastructure & Facilities -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        public ActionResult InfrastructureFacilities()
        {
            InfrastructureFacilities objInfrastructureFacilities = new InfrastructureFacilities();
            objInfrastructureFacilities.InfrastructureFacilitiesList = GetInfrastructureFacilitiesList();
            return View(objInfrastructureFacilities);
        }

        public List<InfrastructureFacilities> GetInfrastructureFacilitiesList()
        {
            try
            {
                List<InfrastructureFacilities> objInfrastructureFacilities = new List<InfrastructureFacilities>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_InfrastructureFacilities 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        InfrastructureFacilities obj = new InfrastructureFacilities();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Heading = dr["Heading"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objInfrastructureFacilities.Add(obj);
                    }
                }
                return objInfrastructureFacilities;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult InfrastructureFacilities(InfrastructureFacilities objInfrastructureFacilities, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objInfrastructureFacilities.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("InfrastructureFacilities");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "InfrastructureFacilities-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objInfrastructureFacilities.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objInfrastructureFacilities.Id == 0)
                    {
                        if (SaveInfrastructureFacilities(objInfrastructureFacilities))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InfrastructureFacilities");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InfrastructureFacilities");
                        }
                    }
                    else
                    {
                        if (UpdateInfrastructureFacilities(objInfrastructureFacilities))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InfrastructureFacilities");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InfrastructureFacilities");
                        }
                    }
                }
                return View("InfrastructureFacilities", objInfrastructureFacilities);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("InfrastructureFacilities");
            }

        }

        public bool SaveInfrastructureFacilities(InfrastructureFacilities objInfrastructureFacilities)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_InfrastructureFacilities"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objInfrastructureFacilities.Image);
                cmd.Parameters.AddWithValue("@Heading", objInfrastructureFacilities.Heading);
                cmd.Parameters.AddWithValue("@DisplayOrder", objInfrastructureFacilities.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objInfrastructureFacilities.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateInfrastructureFacilities(InfrastructureFacilities objInfrastructureFacilities)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_InfrastructureFacilities"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objInfrastructureFacilities.Id);
                cmd.Parameters.AddWithValue("@Image", objInfrastructureFacilities.Image);
                cmd.Parameters.AddWithValue("@Heading", objInfrastructureFacilities.Heading);
                cmd.Parameters.AddWithValue("@DisplayOrder", objInfrastructureFacilities.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objInfrastructureFacilities.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditInfrastructureFacilities(int? Id)
        {
            InfrastructureFacilities objInfrastructureFacilities = new InfrastructureFacilities();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_InfrastructureFacilities 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objInfrastructureFacilities.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objInfrastructureFacilities.Image = dt.Rows[0]["Image"].ToString();
                objInfrastructureFacilities.imgPreview = dt.Rows[0]["Image"].ToString();
                objInfrastructureFacilities.Heading = dt.Rows[0]["Heading"].ToString();
                objInfrastructureFacilities.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objInfrastructureFacilities.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objInfrastructureFacilities.InfrastructureFacilitiesList = GetInfrastructureFacilitiesList();

                ViewBag.ActionType = "Update";
                return View("InfrastructureFacilities", objInfrastructureFacilities);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("InfrastructureFacilities", objInfrastructureFacilities);
            }
        }

        public ActionResult DeleteInfrastructureFacilities(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_InfrastructureFacilities"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("InfrastructureFacilities");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("InfrastructureFacilities");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("InfrastructureFacilities");
                //throw;
            }

        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Management (Leadership Team) -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult Management()
        {
            Management objManagement = new Management();
            objManagement.ManagementList = GetManagementList();
            return View(objManagement);
        }

        public List<Management> GetManagementList()
        {
            try
            {
                List<Management> objManagement = new List<Management>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Management 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Management obj = new Management();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Heading = dr["Heading"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Designation = dr["Designation"].ToString();
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();


                        objManagement.Add(obj);
                    }
                }
                return objManagement;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Management(Management objManagement, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objManagement.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Management");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Management-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objManagement.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objManagement.Id == 0)
                    {
                        if (SaveManagement(objManagement))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Management");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Management");
                        }
                    }
                    else
                    {
                        if (UpdateManagement(objManagement))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Management");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Management");
                        }
                    }
                }
                return View("Management", objManagement);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Management");
            }

        }

        public bool SaveManagement(Management objManagement)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Management"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objManagement.Image);
                cmd.Parameters.AddWithValue("@Heading", objManagement.Heading);
                cmd.Parameters.AddWithValue("@Description", objManagement.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objManagement.DisplayOrder);
                cmd.Parameters.AddWithValue("@Designation", objManagement.Designation);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objManagement.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateManagement(Management objManagement)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Management"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objManagement.Id);
                cmd.Parameters.AddWithValue("@Image", objManagement.Image);
                cmd.Parameters.AddWithValue("@Heading", objManagement.Heading);
                cmd.Parameters.AddWithValue("@Description", objManagement.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objManagement.DisplayOrder);
                cmd.Parameters.AddWithValue("@Designation", objManagement.Designation);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objManagement.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditManagement(int? Id)
        {
            Management objManagement = new Management();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Management 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objManagement.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objManagement.Image = dt.Rows[0]["Image"].ToString();
                objManagement.imgPreview = dt.Rows[0]["Image"].ToString();
                objManagement.Heading = dt.Rows[0]["Heading"].ToString();
                objManagement.Description = dt.Rows[0]["Description"].ToString();
                objManagement.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objManagement.Designation = dt.Rows[0]["Designation"].ToString();
                objManagement.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objManagement.ManagementList = GetManagementList();

                ViewBag.ActionType = "Update";
                return View("Management", objManagement);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Management", objManagement);
            }
        }

        public ActionResult DeleteManagement(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Management"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Management");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Management");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Management");
                //throw;
            }

        }


        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Our Values --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxc

        public ActionResult OurValues()
        {
            OurValues objOurValues = new OurValues();
            objOurValues.OurValuesList = GetOurValuesList();
            return View(objOurValues);
        }

        public List<OurValues> GetOurValuesList()
        {
            try
            {
                List<OurValues> objOurValues = new List<OurValues>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_OurValues 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        OurValues obj = new OurValues();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Heading = dr["Heading"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objOurValues.Add(obj);
                    }
                }
                return objOurValues;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult OurValues(OurValues objOurValues, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objOurValues.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("OurValues");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "OurValues-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objOurValues.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objOurValues.Id == 0)
                    {
                        if (SaveOurValues(objOurValues))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("OurValues");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("OurValues");
                        }
                    }
                    else
                    {
                        if (UpdateOurValues(objOurValues))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("OurValues");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("OurValues");
                        }
                    }
                }
                return View("OurValues", objOurValues);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("OurValues");
            }

        }

        public bool SaveOurValues(OurValues objOurValues)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_OurValues"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objOurValues.Image);
                cmd.Parameters.AddWithValue("@Heading", objOurValues.Heading);
                cmd.Parameters.AddWithValue("@Description", objOurValues.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objOurValues.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objOurValues.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateOurValues(OurValues objOurValues)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_OurValues"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objOurValues.Id);
                cmd.Parameters.AddWithValue("@Image", objOurValues.Image);
                cmd.Parameters.AddWithValue("@Heading", objOurValues.Heading);
                cmd.Parameters.AddWithValue("@Description", objOurValues.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objOurValues.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objOurValues.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditOurValues(int? Id)
        {
            OurValues objOurValues = new OurValues();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_OurValues 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objOurValues.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objOurValues.Image = dt.Rows[0]["Image"].ToString();
                objOurValues.imgPreview = dt.Rows[0]["Image"].ToString();
                objOurValues.Heading = dt.Rows[0]["Heading"].ToString();
                objOurValues.Description = dt.Rows[0]["Description"].ToString();
                objOurValues.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objOurValues.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objOurValues.OurValuesList = GetOurValuesList();

                ViewBag.ActionType = "Update";
                return View("OurValues", objOurValues);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("OurValues", objOurValues);
            }
        }

        public ActionResult DeleteOurValues(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_OurValues"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("OurValues");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("OurValues");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("OurValues");
                //throw;
            }

        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Products Partners-- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult ProductPartners()
        {
            ProductPartners objPartners = new ProductPartners();
            objPartners.ProductPartnersList = GetProductPartnersList();
            return View(objPartners);
        }

        public List<ProductPartners> GetProductPartnersList()
        {
            try
            {
                List<ProductPartners> objProductPartners = new List<ProductPartners>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_ProductPartners 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        ProductPartners obj = new ProductPartners();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Heading = dr["Heading"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objProductPartners.Add(obj);
                    }
                }
                return objProductPartners;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ProductPartners(ProductPartners objProductPartners, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objProductPartners.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("ProductPartners");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "ProductPartners-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objProductPartners.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objProductPartners.Id == 0)
                    {
                        if (SaveProductPartners(objProductPartners))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ProductPartners");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ProductPartners");
                        }
                    }
                    else
                    {
                        if (UpdateProductPartners(objProductPartners))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ProductPartners");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("OurValues");
                        }
                    }
                }
                return View("ProductPartners", objProductPartners);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("ProductPartners");
            }

        }

        public bool SaveProductPartners(ProductPartners objProductPartners)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ProductPartners"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objProductPartners.Image);
                cmd.Parameters.AddWithValue("@Heading", objProductPartners.Heading);
                cmd.Parameters.AddWithValue("@Description", objProductPartners.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objProductPartners.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objProductPartners.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateProductPartners(ProductPartners objProductPartners)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ProductPartners"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objProductPartners.Id);
                cmd.Parameters.AddWithValue("@Image", objProductPartners.Image);
                cmd.Parameters.AddWithValue("@Heading", objProductPartners.Heading);
                cmd.Parameters.AddWithValue("@Description", objProductPartners.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objProductPartners.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objProductPartners.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditProductPartners(int? Id)
        {
            ProductPartners objProductPartners = new ProductPartners();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ProductPartners 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objProductPartners.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objProductPartners.Image = dt.Rows[0]["Image"].ToString();
                objProductPartners.imgPreview = dt.Rows[0]["Image"].ToString();
                objProductPartners.Heading = dt.Rows[0]["Heading"].ToString();
                objProductPartners.Description = dt.Rows[0]["Description"].ToString();
                objProductPartners.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objProductPartners.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objProductPartners.ProductPartnersList = GetProductPartnersList();

                ViewBag.ActionType = "Update";
                return View("ProductPartners", objProductPartners);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("ProductPartners", objProductPartners);
            }
        }

        public ActionResult DeleteProductPartners(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ProductPartners"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ProductPartners");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ProductPartners");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("ProductPartners");
                //throw;
            }

        }



        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxx -- Product Range Of Products -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult RangeOfProduct()
        {
            int Id = 2;
            RangeOfProduct obj = new RangeOfProduct();
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_RangeOfProduct 'get'," + Id + "");
            if (dt.Rows.Count > 0)
            {
                obj.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                obj.Equipment = dt.Rows[0]["Equipment"].ToString();
                obj.Applications = dt.Rows[0]["Applications"].ToString();
                obj.Pumps = dt.Rows[0]["Pumps"].ToString();

                ViewBag.ActionType = "Update";
                return View("RangeOfProduct", obj);
            }
            else
            {
                return View("RangeOfProduct", obj);
            }

        }


        public List<RangeOfProduct> GetRangeOfProduct()
        {
            int Id = 2;
            try
            {
                List<RangeOfProduct> objRangeOfProduct = new List<RangeOfProduct>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_RangeOfProduct 'get'," + Id + "");
                if (dt.Rows.Count > 0)
                {
                    RangeOfProduct obj = new RangeOfProduct();
                    obj.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                    obj.Equipment = dt.Rows[0]["Equipment"].ToString();
                    obj.Applications = dt.Rows[0]["Applications"].ToString();
                    obj.Pumps = dt.Rows[0]["Pumps"].ToString();
                    objRangeOfProduct.Add(obj);

                }
                return objRangeOfProduct;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }


        [HttpPost]
        [ValidateInput(false)]
        public ActionResult RangeOfProduct(RangeOfProduct obj)
        {
            int Id = obj.Id;
            try
            {

                if (ModelState.IsValid)
                {
                    if (UpdateRangeOfProduct(obj))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record updated successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("RangeOfProduct");
                    }

                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not added successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("RangeOfProduct");
                    }
                }

                return RedirectToAction("RangeOfProduct");
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("RangeOfProduct");
            }

        }



        public bool SaveRangeOfProduct(RangeOfProduct objRangeOfProduct)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_RangeOfProduct"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Add");
                cmd.Parameters.AddWithValue("@Equipment", objRangeOfProduct.Equipment);
                cmd.Parameters.AddWithValue("@Applications", objRangeOfProduct.Applications);
                cmd.Parameters.AddWithValue("@Pumps", objRangeOfProduct.Pumps);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }
        public bool UpdateRangeOfProduct(RangeOfProduct objRangeOfProduct)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_RangeOfProduct"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@Para", "Update");
                cmd.Parameters.AddWithValue("@Id", objRangeOfProduct.Id);
                cmd.Parameters.AddWithValue("@Equipment", objRangeOfProduct.Equipment);
                cmd.Parameters.AddWithValue("@Applications", objRangeOfProduct.Applications);
                cmd.Parameters.AddWithValue("@Pumps", objRangeOfProduct.Pumps);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Product Service & Spare Parts  -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult ServiceSpareParts()
        {
            ServiceSpareParts objServiceSpareParts = new ServiceSpareParts();
            objServiceSpareParts.ServiceSparePartsList = GetServiceSparePartsList();
            return View(objServiceSpareParts);
        }

        public List<ServiceSpareParts> GetServiceSparePartsList()
        {
            try
            {
                List<ServiceSpareParts> objServiceSpareParts = new List<ServiceSpareParts>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_ServiceSpareParts 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        ServiceSpareParts obj = new ServiceSpareParts();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Heading = dr["Heading"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objServiceSpareParts.Add(obj);
                    }
                }
                return objServiceSpareParts;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ServiceSpareParts(ServiceSpareParts objServiceSpareParts, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objServiceSpareParts.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("ServiceSpareParts");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "ServiceSpareParts-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objServiceSpareParts.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objServiceSpareParts.Id == 0)
                    {
                        if (SaveServiceSpareParts(objServiceSpareParts))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ServiceSpareParts");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ServiceSpareParts");
                        }
                    }
                    else
                    {
                        if (UpdateServiceSpareParts(objServiceSpareParts))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ServiceSpareParts");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("OurValues");
                        }
                    }
                }
                return View("ServiceSpareParts", objServiceSpareParts);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("ServiceSpareParts");
            }

        }

        public bool SaveServiceSpareParts(ServiceSpareParts objServiceSpareParts)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ServiceSpareParts"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objServiceSpareParts.Image);
                cmd.Parameters.AddWithValue("@Heading", objServiceSpareParts.Heading);
                cmd.Parameters.AddWithValue("@Description", objServiceSpareParts.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objServiceSpareParts.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objServiceSpareParts.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateServiceSpareParts(ServiceSpareParts objServiceSpareParts)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ServiceSpareParts"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objServiceSpareParts.Id);
                cmd.Parameters.AddWithValue("@Image", objServiceSpareParts.Image);
                cmd.Parameters.AddWithValue("@Heading", objServiceSpareParts.Heading);
                cmd.Parameters.AddWithValue("@Description", objServiceSpareParts.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objServiceSpareParts.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objServiceSpareParts.Img_Alt_Tag);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditServiceSpareParts(int? Id)
        {
            ServiceSpareParts objServiceSpareParts = new ServiceSpareParts();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ServiceSpareParts 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objServiceSpareParts.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objServiceSpareParts.Image = dt.Rows[0]["Image"].ToString();
                objServiceSpareParts.imgPreview = dt.Rows[0]["Image"].ToString();
                objServiceSpareParts.Heading = dt.Rows[0]["Heading"].ToString();
                objServiceSpareParts.Description = dt.Rows[0]["Description"].ToString();
                objServiceSpareParts.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objServiceSpareParts.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objServiceSpareParts.ServiceSparePartsList = GetServiceSparePartsList();

                ViewBag.ActionType = "Update";
                return View("ServiceSpareParts", objServiceSpareParts);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("ServiceSpareParts", objServiceSpareParts);
            }
        }

        public ActionResult DeleteServiceSpareParts(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ServiceSpareParts"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ServiceSpareParts");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ServiceSpareParts");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("ServiceSpareParts");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxx -- Testimonials -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult Testimonials()
        {
            Testimonials objTestimonials = new Testimonials();
            objTestimonials.TestimonialsList = GetTestimonialsList();
            return View(objTestimonials);
        }

        public List<Testimonials> GetTestimonialsList()
        {
            try
            {
                List<Testimonials> objTestimonials = new List<Testimonials>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Testimonials 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Testimonials obj = new Testimonials();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Name = dr["Name"].ToString();
                        obj.Position = dr["Position"].ToString();
                        obj.CompanyName = dr["CompanyName"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());

                        objTestimonials.Add(obj);
                    }
                }
                return objTestimonials;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Testimonials(Testimonials objTestimonials)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    if (objTestimonials.Id == 0)
                    {
                        if (SaveTestimonials(objTestimonials))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Testimonials");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Testimonials");
                        }
                    }
                    else
                    {
                        if (UpdateTestimonials(objTestimonials))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Testimonials");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Testimonials");
                        }
                    }
                }
                return View("Testimonials", objTestimonials);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("Testimonials", objTestimonials);
            }

        }

        public bool SaveTestimonials(Testimonials objTestimonials)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Testimonials"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", objTestimonials.Name);
                cmd.Parameters.AddWithValue("@Position", objTestimonials.Position);
                cmd.Parameters.AddWithValue("@CompanyName", objTestimonials.CompanyName);
                cmd.Parameters.AddWithValue("@Description", objTestimonials.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objTestimonials.DisplayOrder);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }

        public bool UpdateTestimonials(Testimonials objTestimonials)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Testimonials"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objTestimonials.Id);
                cmd.Parameters.AddWithValue("@Name", objTestimonials.Name);
                cmd.Parameters.AddWithValue("@Position", objTestimonials.Position);
                cmd.Parameters.AddWithValue("@CompanyName", objTestimonials.CompanyName);
                cmd.Parameters.AddWithValue("@Description", objTestimonials.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objTestimonials.DisplayOrder);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }

        public ActionResult EditTestimonials(int? Id)
        {
            Testimonials objTestimonials = new Testimonials();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Testimonials 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objTestimonials.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                objTestimonials.Name = dt.Rows[0]["Name"].ToString();
                objTestimonials.Position = dt.Rows[0]["Position"].ToString();
                objTestimonials.CompanyName = dt.Rows[0]["CompanyName"].ToString();
                objTestimonials.Description = dt.Rows[0]["Description"].ToString();
                objTestimonials.DisplayOrder = int.Parse(dt.Rows[0]["DisplayOrder"].ToString());

                objTestimonials.TestimonialsList = GetTestimonialsList();

                ViewBag.ActionType = "Update";
                return View("Testimonials", objTestimonials);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Testimonials", objTestimonials);
            }
        }

        public ActionResult DeleteTestimonials(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Testimonials"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Testimonials");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Testimonials");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Testimonials");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --Solutions Technology Industry -- xxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult SolutionsTechnologyIndustry()
        {
            //ViewData["DropdownMenu"] = GetMenuDropDown(0);

            SolutionsTechnologyIndustry obj = new SolutionsTechnologyIndustry();
            int SelectedValue = obj.MasterId;
            obj.SolutionsTechnologyIndustrylist = GetSolutionsTechnologyIndustryList();
            obj.MasterList = BindMasterList(obj);
            return View(obj);
        }

        public List<SelectListItem> BindMasterList(SolutionsTechnologyIndustry obj)
        {
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_SolAndTechSubTable 'binddropdown'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["Title"].ToString();
                    list.Value = dr["MasterId"].ToString();

                    if (obj.MasterId == int.Parse(dr["MasterId"].ToString()))
                    {
                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }

        public List<SolutionsTechnologyIndustry> GetSolutionsTechnologyIndustryList()
        {
            try
            {
                List<SolutionsTechnologyIndustry> objSolutionsTechnologyIndustry = new List<SolutionsTechnologyIndustry>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_SolAndTechSubTable 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        SolutionsTechnologyIndustry obj = new SolutionsTechnologyIndustry();
                        obj.Sr = dr["Sr"].ToString();
                        obj.SubId = int.Parse(dr["SubId"].ToString());
                        obj.MasterId = int.Parse(dr["MasterId"].ToString());
                        obj.Menu = dr["MasterTitle"].ToString();
                        obj.Title = dr["Title"].ToString();
                        obj.Slug = dr["Slug"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());


                        objSolutionsTechnologyIndustry.Add(obj);
                    }
                }
                return objSolutionsTechnologyIndustry;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SolutionsTechnologyIndustry(SolutionsTechnologyIndustry objSolutionsTechnologyIndustry)
        {

            try
            {
                //int MastId=objSolutionsTechnologyIndustry.MasterId;
                //objSolutionsTechnologyIndustry.MasterList = BindMasterList(objSolutionsTechnologyIndustry);
                //int Id = objSolutionsTechnologyIndustry.SubId;
                if (ModelState.IsValid)
                {
                    if (objSolutionsTechnologyIndustry.SubId == 0)
                    {
                        if (SaveSolutionsTechnologyIndustry(objSolutionsTechnologyIndustry))
                        {
                            ViewBag.MasterId = objSolutionsTechnologyIndustry.MasterId;
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustry");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustry");
                        }
                    }
                    else
                    {
                        if (UpdateSolutionsTechnologyIndustry(objSolutionsTechnologyIndustry))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustry");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustry");
                        }
                    }
                }

                return View("SolutionsTechnologyIndustry", objSolutionsTechnologyIndustry);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("SolutionsTechnologyIndustry", objSolutionsTechnologyIndustry);
            }

        }

        public bool SaveSolutionsTechnologyIndustry(SolutionsTechnologyIndustry objSolutionsTechnologyIndustry)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechSubTable"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@MasterId", objSolutionsTechnologyIndustry.MasterId);
                cmd.Parameters.AddWithValue("@Title", objSolutionsTechnologyIndustry.Title);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objSolutionsTechnologyIndustry.Title));
                cmd.Parameters.AddWithValue("@DisplayOrder", objSolutionsTechnologyIndustry.DisplayOrder);





                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }

        public bool UpdateSolutionsTechnologyIndustry(SolutionsTechnologyIndustry objSolutionsTechnologyIndustry)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechSubTable"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@SubId", objSolutionsTechnologyIndustry.SubId);
                cmd.Parameters.AddWithValue("@MasterId", objSolutionsTechnologyIndustry.MasterId);
                cmd.Parameters.AddWithValue("@Title", objSolutionsTechnologyIndustry.Title);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objSolutionsTechnologyIndustry.Title));
                cmd.Parameters.AddWithValue("@DisplayOrder", objSolutionsTechnologyIndustry.DisplayOrder);

            
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            objSolutionsTechnologyIndustry.MasterList = BindMasterList(objSolutionsTechnologyIndustry);
            return response;

        }

        public ActionResult EditSolutionsTechnologyIndustry(int Id)
        {
            SolutionsTechnologyIndustry objSolutionsTechnologyIndustry = new SolutionsTechnologyIndustry();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_SolAndTechSubTable 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objSolutionsTechnologyIndustry.SubId = int.Parse(dt.Rows[0]["SubId"].ToString());
                objSolutionsTechnologyIndustry.MasterId = int.Parse(dt.Rows[0]["MasterId"].ToString());
                objSolutionsTechnologyIndustry.Title = dt.Rows[0]["Title"].ToString();
                objSolutionsTechnologyIndustry.Slug = dt.Rows[0]["Slug"].ToString();
                objSolutionsTechnologyIndustry.Menu = dt.Rows[0]["MasterTitle"].ToString();
                objSolutionsTechnologyIndustry.DisplayOrder = int.Parse(dt.Rows[0]["DisplayOrder"].ToString());

                //ViewData["DropdownMenu"] = GetMenuDropDown(objSolutionsTechnologyIndustry.MasterId);

                objSolutionsTechnologyIndustry.MasterList = BindMasterList(objSolutionsTechnologyIndustry);


                objSolutionsTechnologyIndustry.SolutionsTechnologyIndustrylist = GetSolutionsTechnologyIndustryList();

                ViewBag.ActionType = "Update";
                return View("SolutionsTechnologyIndustry", objSolutionsTechnologyIndustry);
            }

            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("SolutionsTechnologyIndustry", objSolutionsTechnologyIndustry);
            }
        }


        public ActionResult DeleteSolutionsTechnologyIndustry(int Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechSubTable"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@SubId", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustry");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustry");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("SolutionsTechnologyIndustry");
                //throw;
            }

        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Solutions Technology Industry Detail Page -- xxxxxxxxxxxxxxxxxx

        public ActionResult SolutionsTechnologyIndustryDetail()
        {
            SolutionsTechnologyIndustryDetail obj = new SolutionsTechnologyIndustryDetail();

            ViewBag.MasterId = obj.MasterId;
            ViewBag.SubId = obj.SubId;

            obj.MasterList = BindMasterListForDetail(obj);



            obj.SolutionsTechnologyIndustryDetailList = GetSolutionsTechnologyIndustryDetailList();
            return View(obj);
        }


        public List<SelectListItem> BindMasterListForDetail(SolutionsTechnologyIndustryDetail obj)
        {
            DataTable dt = new DataTable();

            dt = util.Display("Execute Proc_SolAndTechSubTable 'binddropdown'");

            List<SelectListItem> objList = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    SelectListItem list = new SelectListItem();
                    list.Text = dr["Title"].ToString();
                    list.Value = dr["MasterId"].ToString();

                    if (obj.MasterId == int.Parse(dr["MasterId"].ToString()))
                    {

                        list.Selected = true;
                    }

                    objList.Add(list);
                }

            }
            return objList.ToList();
        }


        public JsonResult BindSubListForDetail(int Id)
        {
            DataTable dt = new DataTable();
            SolutionsTechnologyIndustryDetail obj = new SolutionsTechnologyIndustryDetail();
            dt = util.Display("Execute Proc_SolAndTechDetailsTable 'bindsubdropdown',0,0," + Id + "");
            List<SelectListItem> li = new List<SelectListItem>();
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    li.Add(new SelectListItem { Text = dr["PageName"].ToString(), Value = dr["SubId"].ToString() });
                    //obj.SubId = int.Parse(dr["SubId"].ToString());


                }
            }


            return Json(li, JsonRequestBehavior.AllowGet);
        }

        public List<SolutionsTechnologyIndustryDetail> GetSolutionsTechnologyIndustryDetailList()
        {
            try
            {
                List<SolutionsTechnologyIndustryDetail> objSolutionsTechnologyIndustryDetailList = new List<SolutionsTechnologyIndustryDetail>();

                DataTable dt = util.Display("Execute Proc_SolAndTechDetailsTable 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        SolutionsTechnologyIndustryDetail obj = new SolutionsTechnologyIndustryDetail();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.MasterId = int.Parse(dr["MasterId"].ToString());
                        obj.SubId = int.Parse(dr["SubId"].ToString());
                        obj.Heading = dr["Heading"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.Menu = dr["MasterTitle"].ToString();
                        obj.PageName = dr["PageName"].ToString();
                        obj.Image = dr["Image"].ToString();
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objSolutionsTechnologyIndustryDetailList.Add(obj);
                    }
                }
                return objSolutionsTechnologyIndustryDetailList;
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SolutionsTechnologyIndustryDetail(SolutionsTechnologyIndustryDetail obj, HttpPostedFileBase[] Image)
        {
            int MId = obj.MasterId;

            BindSubListForDetail(MId);
            try
            {

                if (obj.Id != 0)
            {
                ModelState.Remove("Image");
            }
          
                    if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/InnerBanner/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("SolutionsTechnologyIndustryDetail");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "InnerBanner-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            obj.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (obj.Id == 0)
                    {
                        if (SaveSolutionsTechnologyIndustryDetail(obj))
                        {
                            ModelState.Clear();

                            ViewBag.MId = obj.MasterId;
                            ViewBag.SId = obj.SubId;

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustryDetail");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustryDetail");
                        }
                    }

                    if (UpdateSolutionsTechnologyIndustryDetail(obj))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record updated successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustryDetail");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not updated successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustryDetail");
                    }
                }
                return View("SolutionsTechnologyIndustryDetail", obj);
            }


            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("SolutionsTechnologyIndustryDetail", obj);
            }

        }


        public bool SaveSolutionsTechnologyIndustryDetail(SolutionsTechnologyIndustryDetail obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechDetailsTable"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@MasterId", obj.MasterId);
                cmd.Parameters.AddWithValue("@SubId", obj.PageName);
                cmd.Parameters.AddWithValue("@Heading", obj.Heading);
                cmd.Parameters.AddWithValue("@Description", obj.Description);
                cmd.Parameters.AddWithValue("@Image", obj.Image);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", obj.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }


        public bool UpdateSolutionsTechnologyIndustryDetail(SolutionsTechnologyIndustryDetail obj)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechDetailsTable"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", obj.Id);
                cmd.Parameters.AddWithValue("@MasterId", obj.MasterId);
                cmd.Parameters.AddWithValue("@SubId", obj.PageName);
                cmd.Parameters.AddWithValue("@Heading", obj.Heading);
                cmd.Parameters.AddWithValue("@Description", obj.Description);
                cmd.Parameters.AddWithValue("@Image", obj.Image);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", obj.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;

        }

        public ActionResult EditSolutionsTechnologyIndustryDetail(int? Id)
        {
            SolutionsTechnologyIndustryDetail obj = new SolutionsTechnologyIndustryDetail();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_SolAndTechDetailsTable 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                obj.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                obj.MasterId = int.Parse(dt.Rows[0]["MasterId"].ToString());
                obj.SubId = int.Parse(dt.Rows[0]["SubId"].ToString());
                obj.Heading = dt.Rows[0]["Heading"].ToString();
                obj.Description = dt.Rows[0]["Description"].ToString();
                obj.PageName = dt.Rows[0]["PageName"].ToString();
                obj.Image = dt.Rows[0]["Image"].ToString();
                obj.imgPreview = dt.Rows[0]["Image"].ToString();

                obj.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();


                obj.MasterList = BindMasterListForDetail(obj);
                BindSubListForDetail(obj.MasterId);
                //obj.SubList = BindSubListForDetail(obj.MasterId);
                obj.SolutionsTechnologyIndustryDetailList = GetSolutionsTechnologyIndustryDetailList();

                ViewBag.ActionType = "Update";
                return View("SolutionsTechnologyIndustryDetail", obj);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("SolutionsTechnologyIndustryDetail", obj);
            }
        }

        public ActionResult DeleteSolutionsTechnologyIndustryDetail(int Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechDetailsTable"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustryDetail");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustryDetail");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("SolutionsTechnologyIndustryDetail");
                //throw;
            }

        }



        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --Solutions Technology Industry Gallary -- xxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult SolutionsTechnologyIndustryGallary(int Id)

        {

            SolutionsTechnologyIndustryGallary obj = new SolutionsTechnologyIndustryGallary();
            obj.Id =Id;
            obj.SolutionsTechnologyIndustryGallaryList = GetSolutionsTechnologyIndustryGallaryList(Id);
            //obj.MasterList = BindMasterListForGalary(obj);
            return View(obj);
        }

        public List<SolutionsTechnologyIndustryGallary> GetSolutionsTechnologyIndustryGallaryList(int? Id)
        {
            try
            {
                List<SolutionsTechnologyIndustryGallary> objSolutionsTechnologyIndustryGallary = new List<SolutionsTechnologyIndustryGallary>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_SolAndTechGalleryTable 'get',0,"+ Id + "");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        SolutionsTechnologyIndustryGallary obj = new SolutionsTechnologyIndustryGallary();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.GalleryId = int.Parse(dr["GalleryId"].ToString());
                        obj.Title = dr["Title"].ToString();
                        obj.Image = dr["Image"].ToString();
                        obj.ThumbImg = dr["ThumbImg"].ToString();
                        obj.Url = dr["Url"].ToString();
                        obj.Type = dr["Type"].ToString();
                        obj.imgPreview = dr["Image"].ToString();
                        obj.ThumbImgPreview = dr["ThumbImg"].ToString();
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();


                        objSolutionsTechnologyIndustryGallary.Add(obj);
                    }
                }
                return objSolutionsTechnologyIndustryGallary;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }

        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SolutionsTechnologyIndustryGallary(SolutionsTechnologyIndustryGallary objSolutionsTechnologyIndustryGallary, HttpPostedFileBase[] Image, HttpPostedFileBase[] ThumbImg)
        {
            int galId = objSolutionsTechnologyIndustryGallary.GalleryId;
            int Id = objSolutionsTechnologyIndustryGallary.Id;
            try
            {
                if (galId == 0)
                {
                    if (objSolutionsTechnologyIndustryGallary.Type == "Video")
                    {
                        ModelState.Remove("Image");
                    }
                    else if (objSolutionsTechnologyIndustryGallary.Type == "Image")
                    {
                        ModelState.Remove("Url");
                        ModelState.Remove("ThumbImg");
                    }
                }
                else
                {
                    ModelState.Remove("Image");
                    ModelState.Remove("Url");
                    ModelState.Remove("ThumbImg");
                }

                string VirtualFile = "~/Content/uploads/Images/";
                string Img = string.Empty;
                string ThumbImg1 = string.Empty;

                if (Image != null)
                {
                    foreach (HttpPostedFileBase file in Image)
                    {
                        if (file != null)
                        {
                            string ext = Path.GetExtension(file.FileName).ToLower();
                            if (!util.IsValidImageFileExtension(ext))
                            {
                                ViewBag.Message = "warning";
                                ViewBag.Message1 = "Only Image files are allowed!";

                                TempData["Message"] = ViewBag.Message;
                                TempData["Message1"] = ViewBag.Message1;
                                return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                            }
                            else
                            {
                                var fileName = "Img-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                Img += fileName + ",";
                            }
                        }
                    }
                    
                }

                if (ThumbImg != null)
                {
                    foreach (HttpPostedFileBase file in ThumbImg)
                    {
                        if (file != null)
                        {
                            string ext = Path.GetExtension(file.FileName).ToLower();
                            if (!util.IsValidImageFileExtension(ext))
                            {
                                ViewBag.Message = "warning";
                                ViewBag.Message1 = "Only Image files are allowed!";

                                TempData["Message"] = ViewBag.Message;
                                TempData["Message1"] = ViewBag.Message1;
                                return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                            }
                            else
                            {
                                var fileName = "ThumbImg-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                ThumbImg1 += fileName + ",";
                            }
                        }
                    }
                }


               

                    if (Img != "")
                    {
                        objSolutionsTechnologyIndustryGallary.Image = Img.Substring(0, Img.Length - 1);
                    }
               
                    if (ThumbImg1 != "")
                    {
                        objSolutionsTechnologyIndustryGallary.ThumbImg = ThumbImg1.Substring(0, ThumbImg1.Length - 1);
                    }
                


                if (ModelState.IsValid)
                {
                    if (objSolutionsTechnologyIndustryGallary.GalleryId == 0)
                    {
                        if (SaveSolutionsTechnologyIndustryGallary(objSolutionsTechnologyIndustryGallary))
                        {
                            ViewBag.Id = objSolutionsTechnologyIndustryGallary.Id;
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                        }
                    }
                    else
                    {
                        if (UpdateSolutionsTechnologyIndustryGallary(objSolutionsTechnologyIndustryGallary))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                        }
                    }
                }

                return View("SolutionsTechnologyIndustryGallary", objSolutionsTechnologyIndustryGallary);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("SolutionsTechnologyIndustryGallary", new { Id });
            }

        }

        public bool SaveSolutionsTechnologyIndustryGallary(SolutionsTechnologyIndustryGallary objSolutionsTechnologyIndustryGallary)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechGalleryTable"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Id", objSolutionsTechnologyIndustryGallary.Id);
                cmd.Parameters.AddWithValue("@Title", objSolutionsTechnologyIndustryGallary.Title);
                cmd.Parameters.AddWithValue("@Image", objSolutionsTechnologyIndustryGallary.Image);
                cmd.Parameters.AddWithValue("@Type", objSolutionsTechnologyIndustryGallary.Type);
                cmd.Parameters.AddWithValue("@ThumbImg", objSolutionsTechnologyIndustryGallary.ThumbImg);
                cmd.Parameters.AddWithValue("@Url", objSolutionsTechnologyIndustryGallary.Url);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objSolutionsTechnologyIndustryGallary.Img_Alt_Tag);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            //objSolutionsTechnologyIndustryGallary.MasterList = BindMasterListForGalary(objSolutionsTechnologyIndustryGallary);
            return response;

        }

        public bool UpdateSolutionsTechnologyIndustryGallary(SolutionsTechnologyIndustryGallary objSolutionsTechnologyIndustryGallary)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechGalleryTable"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objSolutionsTechnologyIndustryGallary.Id);
                cmd.Parameters.AddWithValue("@GalleryId", objSolutionsTechnologyIndustryGallary.GalleryId);
                cmd.Parameters.AddWithValue("@Title", objSolutionsTechnologyIndustryGallary.Title);
                cmd.Parameters.AddWithValue("@Type", objSolutionsTechnologyIndustryGallary.Type);
                cmd.Parameters.AddWithValue("@Image", objSolutionsTechnologyIndustryGallary.Image);
                cmd.Parameters.AddWithValue("@ThumbImg", objSolutionsTechnologyIndustryGallary.ThumbImg);
                cmd.Parameters.AddWithValue("@Url", objSolutionsTechnologyIndustryGallary.Url);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objSolutionsTechnologyIndustryGallary.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            //objSolutionsTechnologyIndustryGallary.MasterList = BindMasterListForGalary(objSolutionsTechnologyIndustryGallary);
            return response;

        }

        public ActionResult EditSolutionsTechnologyIndustryGallary(int gId,int Id)
        {
            SolutionsTechnologyIndustryGallary objSolutionsTechnologyIndustryGallary = new SolutionsTechnologyIndustryGallary();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_SolAndTechGalleryTable 'getbyId','" + gId + "','"+Id+"'");
            if (dt.Rows.Count > 0)
            {
                objSolutionsTechnologyIndustryGallary.Id = int.Parse(dt.Rows[0]["Id"].ToString());
                objSolutionsTechnologyIndustryGallary.GalleryId = int.Parse(dt.Rows[0]["GalleryId"].ToString());
                objSolutionsTechnologyIndustryGallary.Title = dt.Rows[0]["Title"].ToString();
                objSolutionsTechnologyIndustryGallary.Type = dt.Rows[0]["Type"].ToString();
                objSolutionsTechnologyIndustryGallary.Image = dt.Rows[0]["Image"].ToString();
                objSolutionsTechnologyIndustryGallary.imgPreview = dt.Rows[0]["Image"].ToString();
                objSolutionsTechnologyIndustryGallary.Url = dt.Rows[0]["Url"].ToString();
                objSolutionsTechnologyIndustryGallary.ThumbImg = dt.Rows[0]["ThumbImg"].ToString();
                objSolutionsTechnologyIndustryGallary.ThumbImgPreview = dt.Rows[0]["ThumbImg"].ToString();
                objSolutionsTechnologyIndustryGallary.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                //ViewData["DropdownMenu"] = GetMenuDropDown(objSolutionsTechnologyIndustry.MasterId);

                //objSolutionsTechnologyIndustryGallary.MasterList = BindMasterListForGalary(objSolutionsTechnologyIndustryGallary);


                objSolutionsTechnologyIndustryGallary.SolutionsTechnologyIndustryGallaryList = GetSolutionsTechnologyIndustryGallaryList(objSolutionsTechnologyIndustryGallary.Id);

                ViewBag.ActionType = "Update";
                return View("SolutionsTechnologyIndustryGallary", objSolutionsTechnologyIndustryGallary);
            }

            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("SolutionsTechnologyIndustryGallary", objSolutionsTechnologyIndustryGallary);
            }
        }


        public ActionResult DeleteSolutionsTechnologyIndustryGallary(int Id,int gId)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_SolAndTechGalleryTable"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@GalleryId", gId);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("SolutionsTechnologyIndustryGallary", new { Id });
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxx --- Case studies -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult CaseStudies()
        {
            CaseStudies obj = new CaseStudies();
            obj.CaseStudiesList = GetCaseStudiesList();
            return View(obj);

        }


        public List<CaseStudies> GetCaseStudiesList()
        {
            try
            {
                List<CaseStudies> objCaseStudies = new List<CaseStudies>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_CaseStudies 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CaseStudies obj = new CaseStudies();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Heading = dr["Heading"].ToString();
                        obj.Category = dr["Category"].ToString();                  
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();
                        objCaseStudies.Add(obj);
                    }
                }
                return objCaseStudies;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CaseStudies(CaseStudies objCaseStudies, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objCaseStudies.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/CaseStudies/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("CaseStudies");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "CaseStudies-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objCaseStudies.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objCaseStudies.Id == 0)
                    {
                        if (SaveCaseStudies(objCaseStudies))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CaseStudies");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CaseStudies");
                        }
                    }
                    else
                    {
                        if (UpdateCaseStudies(objCaseStudies))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CaseStudies");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CaseStudies");
                        }
                    }
                }
                return View("CaseStudies", objCaseStudies);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("CaseStudies");
            }

        }

        public bool SaveCaseStudies(CaseStudies objCaseStudies)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CaseStudies"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objCaseStudies.Image);
                cmd.Parameters.AddWithValue("@Heading", objCaseStudies.Heading);
                cmd.Parameters.AddWithValue("@Category", objCaseStudies.Category);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCaseStudies.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objCaseStudies.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateCaseStudies(CaseStudies objCaseStudies)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CaseStudies"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objCaseStudies.Id);
                cmd.Parameters.AddWithValue("@Image", objCaseStudies.Image);
                cmd.Parameters.AddWithValue("@Heading", objCaseStudies.Heading);
                cmd.Parameters.AddWithValue("@Category", objCaseStudies.Category);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCaseStudies.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objCaseStudies.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditCaseStudies(int? Id)
        {
            CaseStudies objCaseStudies = new CaseStudies();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CaseStudies 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objCaseStudies.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objCaseStudies.Image = dt.Rows[0]["Image"].ToString();
                objCaseStudies.imgPreview = dt.Rows[0]["Image"].ToString();
                objCaseStudies.Heading = dt.Rows[0]["Heading"].ToString();
                objCaseStudies.Category = dt.Rows[0]["Category"].ToString();
                objCaseStudies.imgPreview = dt.Rows[0]["Image"].ToString();
                objCaseStudies.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objCaseStudies.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objCaseStudies.CaseStudiesList = GetCaseStudiesList();

                ViewBag.ActionType = "Update";
                return View("CaseStudies", objCaseStudies);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CaseStudies", objCaseStudies);
            }
        }

        public ActionResult DeleteCaseStudies(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CaseStudies"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CaseStudies");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CaseStudies");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CaseStudies");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxx -- Case Studies Register List -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult CasestudyformRegister()
        {
            Casestudyform objCasestudyform = new Casestudyform();
            objCasestudyform.CasestudyformList = GetCasestudyformList();
            return View(objCasestudyform);
        }

        public List<Casestudyform> GetCasestudyformList()
        {
            try
            {
                List<Casestudyform> objCasestudyform = new List<Casestudyform>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Casestudyform 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Casestudyform apply = new Casestudyform();
                        apply.Sr = dr["Sr"].ToString();
                        apply.caseId = int.Parse(dr["caseId"].ToString());
                        apply.Title = dr["Title"].ToString(); 
                        apply.CompanyName = dr["CompanyName"].ToString();                      
                        apply.Email = dr["Email"].ToString();
                        apply.Name = dr["Name"].ToString();
                        apply.ApplyDate = dr["ApplyDate"].ToString();

                        objCasestudyform.Add(apply);
                    }
                }
                return objCasestudyform;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult DeleteCasestudyformRegister(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Casestudyform"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@caseId", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CasestudyformRegister");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CasestudyformRegister");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CasestudyformRegister");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxx -- Get In Touch List -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult GetInTouchForm()
        {
            GetInTouchForm objGetInTouchForm = new GetInTouchForm();
            objGetInTouchForm.GetInTouchFormList = GetInTouchFormList();
            return View(objGetInTouchForm);
        }

        public List<GetInTouchForm> GetInTouchFormList()
        {
            try
            {
                List<GetInTouchForm> objContactUs = new List<GetInTouchForm>();
                DataSet ds = new DataSet();
                ds = util.Display1("Execute Proc_GetInTouchForm 'get'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        GetInTouchForm obj = new GetInTouchForm();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.FullName = dr["Name"].ToString();
                        obj.Email = dr["Email"].ToString();
                        obj.ContactNo = dr["ContactNo"].ToString();
                        obj.Comment = dr["Comment"].ToString();
                        obj.ApplyDate = dr["ApplyDate"].ToString();


                        objContactUs.Add(obj);

                    }
                }
                return objContactUs;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult DeleteGetInTouchForm(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_GetInTouchForm"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("GetInTouchForm");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("GetInTouchForm");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("GetInTouchForm");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxx --- Services  -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult Services()
        {
            Services obj = new Services();
            obj.ServicesList = GetServicesList();
            return View(obj);

        }



        public List<Services> GetServicesList()
        {
            try
            {
                List<Services> objServices = new List<Services>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_Services 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        Services obj = new Services();
                        //obj.Sr = dr["Sr"].ToString();
                        obj.ServId = int.Parse(dr["ServId"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Icon = dr["Icon"].ToString();
                        obj.Title = dr["Title"].ToString();
                        obj.Description = dr["Description"].ToString();

                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();
                        obj.Icon_Alt_Tag = dr["Icon_Alt_Tag"].ToString();


                        objServices.Add(obj);
                    }
                }
                return objServices;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult Services(Services objServices, HttpPostedFileBase[] Image, HttpPostedFileBase[] Icon)
        {
            try
            {
                if (objServices.ServId != 0)
                {
                    ModelState.Remove("Image");
                    ModelState.Remove("Icon");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Services/";
                    string Img = string.Empty;
                    string Icon1 = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                        
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Services");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Servicesimg-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objServices.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }



                    if (Icon != null)
                    {
                        foreach (HttpPostedFileBase file in Icon)
                        {

                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("Services");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Servicesicon-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Icon1 += fileName + ",";
                                }
                            }
                        }

                        if (Icon1 != "")
                        {
                            objServices.Icon = Icon1.Substring(0, Icon1.Length - 1);
                        }
                    }

                    if (objServices.ServId == 0)
                    {
                        if (SaveServices(objServices))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Services");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Services");
                        }
                    }
                    else
                    {
                        if (UpdateServices(objServices))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Services");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Services");
                        }
                    }
                }
                return View("Services", objServices);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Services");
            }

        }

        public bool SaveServices(Services objServices)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Services"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objServices.Image);
                cmd.Parameters.AddWithValue("@Icon", objServices.Icon);
                cmd.Parameters.AddWithValue("@Title", objServices.Title);
                cmd.Parameters.AddWithValue("@Description", objServices.Description);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objServices.Img_Alt_Tag);
                cmd.Parameters.AddWithValue("@Icon_Alt_Tag", objServices.Icon_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateServices(Services objServices)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_Services"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@ServId", objServices.ServId);
                cmd.Parameters.AddWithValue("@Image", objServices.Image);
                cmd.Parameters.AddWithValue("@Icon", objServices.Icon);
                cmd.Parameters.AddWithValue("@Title", objServices.Title);
                cmd.Parameters.AddWithValue("@Description", objServices.Description);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objServices.Img_Alt_Tag);
                cmd.Parameters.AddWithValue("@Icon_Alt_Tag", objServices.Icon_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditServices(int? ServId)
        {
            Services objServices = new Services();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Services 'getbyId','" + ServId + "'");
            if (dt.Rows.Count > 0)
            {
                objServices.ServId = Convert.ToInt32(dt.Rows[0]["ServId"].ToString());
                objServices.Image = dt.Rows[0]["Image"].ToString();
                objServices.imgPreview = dt.Rows[0]["Image"].ToString();
                objServices.Title = dt.Rows[0]["Title"].ToString();
                objServices.Description = dt.Rows[0]["Description"].ToString();
                objServices.Icon = dt.Rows[0]["Icon"].ToString();
                objServices.iconPreview = dt.Rows[0]["Icon"].ToString();

                objServices.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();
                objServices.Icon_Alt_Tag = dt.Rows[0]["Icon_Alt_Tag"].ToString();

                objServices.ServicesList= GetServicesList();

                ViewBag.ActionType = "Update";
                return View("Services", objServices);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("Services", objServices);
            }
        }

        public ActionResult DeleteServices(int? ServId)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_Services"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@ServId", ServId);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Services");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("Services");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("Services");
                //throw;
            }

        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- NewsRoom -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult NewsRoom()
        {
            NewsRoom objNewsRoom = new NewsRoom();
            objNewsRoom.NewsRoomList = GetNewsRoomList();
            return View(objNewsRoom);

        }
        public List<NewsRoom> GetNewsRoomList()
        {
            try
            {
                List<NewsRoom> objNewsRoom = new List<NewsRoom>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_NewsRoom 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        NewsRoom obj = new NewsRoom();
                        //obj.Sr = dr["Sr"].ToString();
                        obj.NewsId = int.Parse(dr["NewsId"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Title = dr["Title"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.Type = dr["Type"].ToString();
                        obj.Date = dr["Date"].ToString();
                        obj.Slug = dr["Slug"].ToString();
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();
                        obj.Category = dr["Category"].ToString();
                        

                        objNewsRoom.Add(obj);
                    }
                }
                return objNewsRoom;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult NewsRoom(NewsRoom objNewsRoom, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objNewsRoom.NewsId != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/NewsRoom/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("NewsRoom");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "NewsRoom-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objNewsRoom.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objNewsRoom.NewsId == 0)
                    {
                        if (SaveNewsRoom(objNewsRoom))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("NewsRoom");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("NewsRoom");
                        }
                    }
                    else
                    {
                        if (UpdateNewsRoom(objNewsRoom))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("NewsRoom");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("NewsRoom");
                        }
                    }
                }
                return View("NewsRoom", objNewsRoom);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("NewsRoom");
            }

        }

        public bool SaveNewsRoom(NewsRoom objNewsRoom)
        {
            bool response;
            string postDate = DateTime.ParseExact(objNewsRoom.Date, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
            using (SqlCommand cmd = new SqlCommand("Proc_NewsRoom"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Type", objNewsRoom.Type);
                cmd.Parameters.AddWithValue("@Title", objNewsRoom.Title);
                cmd.Parameters.AddWithValue("@Image", objNewsRoom.Image);
                cmd.Parameters.AddWithValue("@Date", postDate);
                cmd.Parameters.AddWithValue("@Description", objNewsRoom.Description);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objNewsRoom.Title));

                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objNewsRoom.Img_Alt_Tag);
                cmd.Parameters.AddWithValue("@Category", objNewsRoom.Category);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateNewsRoom(NewsRoom objNewsRoom)
        {
            string postDate = DateTime.ParseExact(objNewsRoom.Date, "dd-MM-yyyy", null).ToString("yyyy/MM/dd");
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_NewsRoom"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@NewsId", objNewsRoom.NewsId);
                cmd.Parameters.AddWithValue("@Type", objNewsRoom.Type);
                cmd.Parameters.AddWithValue("@Title", objNewsRoom.Title);
                cmd.Parameters.AddWithValue("@Image", objNewsRoom.Image);
                cmd.Parameters.AddWithValue("@Date", postDate);
                cmd.Parameters.AddWithValue("@Description", objNewsRoom.Description);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objNewsRoom.Title));

                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objNewsRoom.Img_Alt_Tag);
                cmd.Parameters.AddWithValue("@Category", objNewsRoom.Category);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditNewsRoom(int? NewsId)
        {
            NewsRoom objNewsRoom = new NewsRoom();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_NewsRoom 'getbyId','" + NewsId + "'");
            if (dt.Rows.Count > 0)
            {
                objNewsRoom.NewsId = Convert.ToInt32(dt.Rows[0]["NewsId"].ToString());
                objNewsRoom.Image = dt.Rows[0]["Image"].ToString();
                objNewsRoom.imgPreview = dt.Rows[0]["Image"].ToString();
                objNewsRoom.Title = dt.Rows[0]["Title"].ToString();
                objNewsRoom.Type = dt.Rows[0]["Type"].ToString();
                objNewsRoom.Description = dt.Rows[0]["Description"].ToString();
                objNewsRoom.Date = dt.Rows[0]["Date"].ToString();
                objNewsRoom.Slug = dt.Rows[0]["Slug"].ToString();
                objNewsRoom.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();
                objNewsRoom.Category = dt.Rows[0]["Category"].ToString();

                objNewsRoom.NewsRoomList = GetNewsRoomList();

                ViewBag.ActionType = "Update";
                return View("NewsRoom", objNewsRoom);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("NewsRoom", objNewsRoom);
            }
        }

        public ActionResult DeleteNewsRoom(int? NewsId)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_NewsRoom"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@NewsId", NewsId);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("NewsRoom");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("NewsRoom");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("NewsRoom");
                //throw;
            }

        }



        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Inner Banner -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx 

        public ActionResult InnerBanner()
        {
            InnerBanner objInnerBanner = new InnerBanner();
            objInnerBanner.InnerBannerList = GetInnerBannerList();
            return View(objInnerBanner);

        }

        public List<InnerBanner> GetInnerBannerList()
        {
            try
            {
                List<InnerBanner> objInnerBanner = new List<InnerBanner>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_InnerBanner 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        InnerBanner banner = new InnerBanner();
                        banner.Sr = dr["Sr"].ToString();
                        banner.Id = int.Parse(dr["Id"].ToString());
                        banner.Image = dr["Image"].ToString();
                        banner.PageName = dr["PageName"].ToString();
                        banner.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objInnerBanner.Add(banner);
                    }

                }
                return objInnerBanner;

            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        public ActionResult InnerBanner(InnerBanner objInnerBanner, HttpPostedFileBase[] Image)
        {
            try
            {

                if (objInnerBanner.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/InnerBanner/";
                    string Img = string.Empty;
                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("InnerBanner");
                                }
                                else
                                {
                                    var fileName = "InnerBanner-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }

                        }

                        if (Img != "")
                        {
                            objInnerBanner.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objInnerBanner.Id == 0)
                    {
                        if (SaveInnerBanner(objInnerBanner))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Inner banner added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InnerBanner");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Inner banner could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InnerBanner");
                        }
                    }
                    else
                    {
                        if (UpdateInnerBanner(objInnerBanner))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Inner banner updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InnerBanner");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Inner banner could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("InnerBanner");
                        }
                    }
                }

                return View("InnerBanner", objInnerBanner);
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("InnerBanner");
            }

        }

        public bool SaveInnerBanner(InnerBanner objInnerBanner)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_InnerBanner"))
            {
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objInnerBanner.Image);
                cmd.Parameters.AddWithValue("@PageName", objInnerBanner.PageName);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objInnerBanner.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateInnerBanner(InnerBanner objInnerBanner)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_InnerBanner"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objInnerBanner.Id);
                cmd.Parameters.AddWithValue("@Image", objInnerBanner.Image);
                cmd.Parameters.AddWithValue("@PageName", objInnerBanner.PageName);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objInnerBanner.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditInnerBanner(int? Id)
        {
            InnerBanner objInnerBanner = new InnerBanner();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_InnerBanner 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objInnerBanner.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objInnerBanner.Image = dt.Rows[0]["Image"].ToString();
                objInnerBanner.imgPreview = dt.Rows[0]["Image"].ToString();
                objInnerBanner.PageName = dt.Rows[0]["PageName"].ToString();
                objInnerBanner.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objInnerBanner.InnerBannerList = GetInnerBannerList();

                ViewBag.ActionType = "Update";
                return View("InnerBanner", objInnerBanner);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("InnerBanner", objInnerBanner);
            }
        }

        public ActionResult DeleteInnerBanner(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_InnerBanner"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Inner banner deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("InnerBanner");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Inner banner could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("InnerBanner");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("InnerBanner");
                //throw;
            }

        }

        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- CMS --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult CMS(int Id)
        {
            CMS cms = new CMS();
            DataTable dt = new DataTable();
            dt = util.Display("Execute Proc_CMS 'get','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                cms.CmsId = int.Parse(dt.Rows[0]["CmsId"].ToString());
                cms.Heading = dt.Rows[0]["Heading"].ToString();
                cms.Description = dt.Rows[0]["Description"].ToString();

                ViewBag.ActionType = "Update";
                return View("CMS", cms);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CMS", cms);
            }
            //cms.CmsId = Id;
            //ViewBag.ActionType = "Update";
            //return View(objCMS);
        }

        public List<CMS> GetCMSList(int? Id)
        {
            try
            {
                List<CMS> objCMS = new List<CMS>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_CMS 'get','" + Id + "'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CMS cms = new CMS();
                        cms.CmsId = int.Parse(dr["CmsId"].ToString());
                        cms.Heading = dr["Heading"].ToString();
                        cms.Description = dr["Description"].ToString();

                        objCMS.Add(cms);
                    }
                }
                return objCMS;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CMS(CMS objCMS)
        {
            int Id = objCMS.CmsId;
            try
            {
                if (ModelState.IsValid)
                {
                    //if (objCMS.CmsId == 0)
                    //{
                    //    if (SaveCMS(objCMS))
                    //    {
                    //        ModelState.Clear();

                    //        ViewBag.Message = "success";
                    //        ViewBag.Message1 = "Inner banner added successfully.";

                    //        TempData["Message"] = ViewBag.Message;
                    //        TempData["Message1"] = ViewBag.Message1;

                    //        return RedirectToAction("InnerBanner");
                    //    }
                    //    else
                    //    {
                    //        ViewBag.Message = "warning";
                    //        ViewBag.Message1 = "Inner banner could not added successfully.";

                    //        TempData["Message"] = ViewBag.Message;
                    //        TempData["Message1"] = ViewBag.Message1;

                    //        return RedirectToAction("InnerBanner");
                    //    }
                    //}

                    if (UpdateCMS(objCMS))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record updated successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CMS", new { Id });
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not updated successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CMS", new { Id });
                    }
                }
                return View("CMS", new { Id });
            }


            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                //return RedirectToAction("CMSPage", new { Id });
                return View("CMS", new { Id });
            }

        }

        //public bool SaveCMS(CMS objCMS)
        //{
        //    bool response;
        //    using (SqlCommand cmd = new SqlCommand("Proc_CMS"))
        //    {
        //        cmd.CommandType = System.Data.CommandType.StoredProcedure;
        //        cmd.Parameters.AddWithValue("@para", "add");
        //        cmd.Parameters.AddWithValue("@Heading", objCMS.Heading);
        //        cmd.Parameters.AddWithValue("@Description", objCMS.Description);
        //        if (util.Execute(cmd))
        //        {
        //            response = true;
        //        }
        //        else
        //        {
        //            response = false;
        //        }
        //    }
        //    return response;
        //}

        public bool UpdateCMS(CMS objCMS)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CMS"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@CmsId", objCMS.CmsId);
                cmd.Parameters.AddWithValue("@Heading", objCMS.Heading);
                cmd.Parameters.AddWithValue("@Description", objCMS.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }




        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Social Media --- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult SocialMedia(int? Id)
        {
            SocialMedia objSocialMedia = new SocialMedia();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_SocialMedia 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objSocialMedia.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objSocialMedia.opentime = dt.Rows[0]["opentime"].ToString();
                objSocialMedia.CallUs = dt.Rows[0]["CallUs"].ToString();
                objSocialMedia.MailUs = dt.Rows[0]["MailUs"].ToString();
                objSocialMedia.Sales = dt.Rows[0]["Sales"].ToString();
                objSocialMedia.Services = dt.Rows[0]["Services"].ToString();
                objSocialMedia.Purchasing = dt.Rows[0]["Purchasing"].ToString();
                objSocialMedia.Dispatches = dt.Rows[0]["Dispatches"].ToString();
                objSocialMedia.Sales1 = dt.Rows[0]["Sales1"].ToString();

                ViewBag.ActionType = "Update";
                return View("SocialMedia", objSocialMedia);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("SocialMedia", objSocialMedia);
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult SocialMedia(SocialMedia objSocialMedia)
        {
            try
            {
                if (UpdateSocialMedia(objSocialMedia))
                {
                    ViewBag.Message = "success";
                    ViewBag.Message1 = "Record updated successfully.";

                    TempData["Message"] = ViewBag.Message;
                    TempData["Message1"] = ViewBag.Message1;

                    return RedirectToAction("SocialMedia");
                }
                else
                {
                    ViewBag.Message = "warning";
                    ViewBag.Message1 = "Record could not updated successfully.";

                    TempData["Message"] = ViewBag.Message;
                    TempData["Message1"] = ViewBag.Message1;

                    return RedirectToAction("SocialMedia");
                }

                //return View("SocialMedia", objSocialMedia);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("SocialMedia");
            }

        }

        public bool UpdateSocialMedia(SocialMedia objSocialMedia)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_SocialMedia"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objSocialMedia.Id);
                cmd.Parameters.AddWithValue("@opentime", objSocialMedia.opentime);
                cmd.Parameters.AddWithValue("@CallUs", objSocialMedia.CallUs);
                cmd.Parameters.AddWithValue("@MailUs", objSocialMedia.MailUs);
                cmd.Parameters.AddWithValue("@Sales", objSocialMedia.Sales);
                cmd.Parameters.AddWithValue("@Services", objSocialMedia.Services);
                cmd.Parameters.AddWithValue("@Purchasing", objSocialMedia.Purchasing);
                cmd.Parameters.AddWithValue("@Dispatches", objSocialMedia.Dispatches);
                cmd.Parameters.AddWithValue("@Sales1", objSocialMedia.Sales1);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Career Current Openings -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult CurrentOpenings()
        {
            CurrentOpenings obj = new CurrentOpenings();
            obj.CurrentOpeningsList = GetCurrentOpeningList();
            return View(obj);
        }

        public List<CurrentOpenings> GetCurrentOpeningList()
        {
            try
            {
                List<CurrentOpenings> objCurrentOpening = new List<CurrentOpenings>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_CurrentOpening 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CurrentOpenings current = new CurrentOpenings();
                        current.Sr = dr["Sr"].ToString();
                        current.Id = int.Parse(dr["Id"].ToString());
                        current.Title = dr["Title"].ToString();
                        current.Location = dr["Location"].ToString();
                        current.Post = int.Parse(dr["Post"].ToString());
                        current.Qualification = dr["Qualification"].ToString();
                        current.Experience = dr["Experience"].ToString();
                        current.Description = dr["Description"].ToString();

                        objCurrentOpening.Add(current);
                    }
                }
                return objCurrentOpening;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        public ActionResult CurrentOpenings(CurrentOpenings objCurrentOpening)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    if (objCurrentOpening.Id == 0)
                    {
                        if (SaveCurrentOpening(objCurrentOpening))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Current Opening added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentOpenings");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Current Opening could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentOpenings");
                        }
                    }
                    else
                    {
                        if (UpdateCurrentOpening(objCurrentOpening))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Current Opening updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentOpenings");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Current Opening could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CurrentOpenings");
                        }
                    }
                }
                return View("CurrentOpenings", objCurrentOpening);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("CurrentOpenings");
            }

        }

        public bool SaveCurrentOpening(CurrentOpenings objCurrentOpening)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Title", objCurrentOpening.Title);
                cmd.Parameters.AddWithValue("@Location", objCurrentOpening.Location);
                cmd.Parameters.AddWithValue("@Qualification", objCurrentOpening.Qualification);
                cmd.Parameters.AddWithValue("@Post", objCurrentOpening.Post);
                cmd.Parameters.AddWithValue("@Experience", objCurrentOpening.Experience);
                cmd.Parameters.AddWithValue("@Description", objCurrentOpening.Description);
                
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateCurrentOpening(CurrentOpenings objCurrentOpening)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objCurrentOpening.Id);
                cmd.Parameters.AddWithValue("@Title", objCurrentOpening.Title);
                cmd.Parameters.AddWithValue("@Location", objCurrentOpening.Location);
                cmd.Parameters.AddWithValue("@Qualification", objCurrentOpening.Qualification);
                cmd.Parameters.AddWithValue("@Post", objCurrentOpening.Post);
                cmd.Parameters.AddWithValue("@Experience", objCurrentOpening.Experience);
                cmd.Parameters.AddWithValue("@Description", objCurrentOpening.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditCurrentOpening(int? Id)
        {
            CurrentOpenings objCurrentOpening = new CurrentOpenings();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CurrentOpening 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objCurrentOpening.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objCurrentOpening.Title = dt.Rows[0]["Title"].ToString();
                objCurrentOpening.Location = dt.Rows[0]["Location"].ToString();
                objCurrentOpening.Post = int.Parse(dt.Rows[0]["Post"].ToString());
                objCurrentOpening.Qualification = dt.Rows[0]["Qualification"].ToString();
                objCurrentOpening.Experience = dt.Rows[0]["Experience"].ToString();
                objCurrentOpening.Description = dt.Rows[0]["Description"].ToString();
           

                objCurrentOpening.CurrentOpeningsList = GetCurrentOpeningList();

                ViewBag.ActionType = "Update";
                return View("CurrentOpenings", objCurrentOpening);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CurrentOpenings", objCurrentOpening);
            }
        }

        public ActionResult DeleteCurrentOpening(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CurrentOpening"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Current Opening deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CurrentOpenings");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Current Opening could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CurrentOpenings");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CurrentOpenings");
                //throw;
            }

        }



        //Apply For Job

        public ActionResult ApplyForJob()
        {
            ApplyForJob objApplyForJob = new ApplyForJob();
            objApplyForJob.ApplyForJobList = GetApplyForJobList();
            return View(objApplyForJob);
        }

        public List<ApplyForJob> GetApplyForJobList()
        {
            try
            {
                List<ApplyForJob> objApplyForJob = new List<ApplyForJob>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_ApplyForJob 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        ApplyForJob apply = new ApplyForJob();
                        apply.Sr = dr["Sr"].ToString();
                        apply.Id = int.Parse(dr["Id"].ToString());
                        apply.Role = dr["Role"].ToString();
                        apply.Name = dr["Name"].ToString();
                        apply.Email = dr["Email"].ToString();
                        apply.ContactNo = dr["ContactNo"].ToString();
                        apply.Resume = dr["Resume"].ToString();
                        apply.ApplyDate = dr["ApplyDate"].ToString();

                        objApplyForJob.Add(apply);
                    }
                }
                return objApplyForJob;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult DeleteApplyForJob(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ApplyForJob"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ApplyForJob");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ApplyForJob");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("ApplyForJob");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxx -- Career Meet Our People -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx


        public ActionResult CareerOurPeople()
        {
            CareerOurPeople objCareerOurPeople = new CareerOurPeople();
            objCareerOurPeople.CareerOurPeopleList = GetCareerOurPeopleList();
            return View(objCareerOurPeople);
        }

        public List<CareerOurPeople> GetCareerOurPeopleList()
        {
            try
            {
                List<CareerOurPeople> objCareerOurPeople = new List<CareerOurPeople>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_CareerOurPeople 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CareerOurPeople obj = new CareerOurPeople();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Name = dr["Name"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Position = dr["Position"].ToString();
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objCareerOurPeople.Add(obj);
                    }
                }
                return objCareerOurPeople;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CareerOurPeople(CareerOurPeople objCareerOurPeople, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objCareerOurPeople.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/OurPeople/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("CareerOurPeople");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "CareerOurPeople-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objCareerOurPeople.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objCareerOurPeople.Id == 0)
                    {
                        if (SaveCareerOurPeople(objCareerOurPeople))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOurPeople");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOurPeople");
                        }
                    }
                    else
                    {
                        if (UpdateCareerOurPeople(objCareerOurPeople))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOurPeople");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOurPeople");
                        }
                    }
                }
                return View("CareerOurPeople", objCareerOurPeople);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("CareerOurPeople");
            }

        }

        public bool SaveCareerOurPeople(CareerOurPeople objCareerOurPeople)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CareerOurPeople"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objCareerOurPeople.Image);
                cmd.Parameters.AddWithValue("@Name", objCareerOurPeople.Name);
                cmd.Parameters.AddWithValue("@Position", objCareerOurPeople.Position);
                cmd.Parameters.AddWithValue("@Description", objCareerOurPeople.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCareerOurPeople.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objCareerOurPeople.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateCareerOurPeople(CareerOurPeople objCareerOurPeople)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CareerOurPeople"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objCareerOurPeople.Id);
                cmd.Parameters.AddWithValue("@Image", objCareerOurPeople.Image);
                cmd.Parameters.AddWithValue("@Name", objCareerOurPeople.Name);
                cmd.Parameters.AddWithValue("@Position", objCareerOurPeople.Position);
                cmd.Parameters.AddWithValue("@Description", objCareerOurPeople.Description);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCareerOurPeople.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objCareerOurPeople.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditCareerOurPeople(int? Id)
        {
            CareerOurPeople objCareerOurPeople = new CareerOurPeople();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CareerOurPeople 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objCareerOurPeople.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objCareerOurPeople.Image = dt.Rows[0]["Image"].ToString();
                objCareerOurPeople.imgPreview = dt.Rows[0]["Image"].ToString();
                objCareerOurPeople.Name = dt.Rows[0]["Name"].ToString();
                objCareerOurPeople.Position = dt.Rows[0]["Position"].ToString();
                objCareerOurPeople.Description = dt.Rows[0]["Description"].ToString();
                objCareerOurPeople.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objCareerOurPeople.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objCareerOurPeople.CareerOurPeopleList = GetCareerOurPeopleList();

                ViewBag.ActionType = "Update";
                return View("CareerOurPeople", objCareerOurPeople);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CareerOurPeople", objCareerOurPeople);
            }
        }

        public ActionResult DeleteCareerOurPeople(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CareerOurPeople"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CareerOurPeople");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CareerOurPeople");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CareerOurPeople");
                //throw;
            }

        }



        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx --Career OverView Images  -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
       
        public ActionResult CareerOverViewImages()
        {
            CareerOverViewImages objCareerOverViewImages = new CareerOverViewImages();
            objCareerOverViewImages.CareerOverViewImagesList = GetCareerOverViewImagesList();
            return View(objCareerOverViewImages);
           
        }


        public List<CareerOverViewImages> GetCareerOverViewImagesList()
        {
            try
            {
                List<CareerOverViewImages> objCareerOverViewImages = new List<CareerOverViewImages>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_CareerOverViewImages 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CareerOverViewImages obj = new CareerOverViewImages();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.DisplayOrder = int.Parse(dr["DisplayOrder"].ToString());
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();


                        objCareerOverViewImages.Add(obj);
                    }
                }
                return objCareerOverViewImages;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult CareerOverViewImages(CareerOverViewImages objCareerOverViewImages, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objCareerOverViewImages.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/OurPeople/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("CareerOverViewImages");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "CareerOverViewImages-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objCareerOverViewImages.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objCareerOverViewImages.Id == 0)
                    {
                        if (SaveCareerOverViewImages(objCareerOverViewImages))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOverViewImages");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOverViewImages");
                        }
                    }
                    else
                    {
                        if (UpdateCareerOverViewImages(objCareerOverViewImages))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOverViewImages");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerOverViewImages");
                        }
                    }
                }
                return View("CareerOverViewImages", objCareerOverViewImages);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("CareerOverViewImages");
            }

        }

        public bool SaveCareerOverViewImages(CareerOverViewImages objCareerOverViewImages)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CareerOverViewImages"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objCareerOverViewImages.Image);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCareerOverViewImages.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objCareerOverViewImages.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateCareerOverViewImages(CareerOverViewImages objCareerOverViewImages)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CareerOverViewImages"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objCareerOverViewImages.Id);
                cmd.Parameters.AddWithValue("@Image", objCareerOverViewImages.Image);
                cmd.Parameters.AddWithValue("@DisplayOrder", objCareerOverViewImages.DisplayOrder);
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objCareerOverViewImages.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditCareerOverViewImages(int? Id)
        {
            CareerOverViewImages objCareerOverViewImages = new CareerOverViewImages();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CareerOverViewImages'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objCareerOverViewImages.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objCareerOverViewImages.Image = dt.Rows[0]["Image"].ToString();
                objCareerOverViewImages.imgPreview = dt.Rows[0]["Image"].ToString(); 
                objCareerOverViewImages.DisplayOrder = Convert.ToInt32(dt.Rows[0]["DisplayOrder"].ToString());
                objCareerOverViewImages.Img_Alt_Tag= dt.Rows[0]["Img_Alt_Tag"].ToString();

                objCareerOverViewImages.CareerOverViewImagesList = GetCareerOverViewImagesList();

                ViewBag.ActionType = "Update";
                return View("CareerOverViewImages", objCareerOverViewImages);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CareerOverViewImages", objCareerOverViewImages);
            }
        }

        public ActionResult DeleteCareerOverViewImages(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CareerOverViewImages"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CareerOverViewImages");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CareerOverViewImages");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CareerOverViewImages");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx -- Career We Believe -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult CareerWeBelieve()
        {
            CareerWeBelieve obj = new CareerWeBelieve();
            obj.CareerWeBelieveList = GetCareerWeBelieveList();
            return View(obj);
        }

        public List<CareerWeBelieve> GetCareerWeBelieveList()
        {
            try
            {
                List<CareerWeBelieve> objCareerWeBelieve = new List<CareerWeBelieve>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_CareerWeBelieve 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        CareerWeBelieve obj = new CareerWeBelieve();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());

                        obj.Description = dr["Description"].ToString();

                        objCareerWeBelieve.Add(obj);
                    }
                }
                return objCareerWeBelieve;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        public ActionResult CareerWeBelieve(CareerWeBelieve objCareerWeBelieve)
        {
            try
            {

                if (ModelState.IsValid)
                {

                    if (objCareerWeBelieve.Id == 0)
                    {
                        if (SaveCareerWeBelieve(objCareerWeBelieve))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Current Opening added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerWeBelieve");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Current Opening could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerWeBelieve");
                        }
                    }
                    else
                    {
                        if (UpdateCareerWeBelieve(objCareerWeBelieve))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Current Opening updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerWeBelieve");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Current Opening could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CareerWeBelieve");
                        }
                    }
                }
                return View("CareerWeBelieve", objCareerWeBelieve);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("CareerWeBelieve");
            }

        }

        public bool SaveCareerWeBelieve(CareerWeBelieve objCareerWeBelieve)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CareerWeBelieve"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Description", objCareerWeBelieve.Description);

                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateCareerWeBelieve(CareerWeBelieve objCareerWeBelieve)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_CareerWeBelieve"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objCareerWeBelieve.Id);
                cmd.Parameters.AddWithValue("@Description", objCareerWeBelieve.Description);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditCareerWeBelieve(int? Id)
        {
            CareerWeBelieve objCareerWeBelieve = new CareerWeBelieve();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CareerWeBelieve 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objCareerWeBelieve.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objCareerWeBelieve.Description = dt.Rows[0]["Description"].ToString();


                objCareerWeBelieve.CareerWeBelieveList = GetCareerWeBelieveList();

                ViewBag.ActionType = "Update";
                return View("CareerWeBelieve", objCareerWeBelieve);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("CareerWeBelieve", objCareerWeBelieve);
            }
        }

        public ActionResult DeleteCareerWeBelieve(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_CareerWeBelieve"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Current Opening deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CareerWeBelieve");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Current Opening could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("CareerWeBelieve");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("CareerWeBelieve");
                //throw;
            }

        }


        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxx-- Contact Us -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult ContactUs()
        {
            ContactUs objContactUs = new ContactUs();
            objContactUs.ContactUsList = GetContactUsList();
            return View(objContactUs);
        }

        public List<ContactUs> GetContactUsList()
        {
            try
            {
                List<ContactUs> objContactUs = new List<ContactUs>();
                DataSet ds = new DataSet();
                ds = util.Display1("Execute Proc_ContactUs 'get'");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ContactUs contact = new ContactUs();
                        contact.Sr = dr["Sr"].ToString();
                        contact.Id = int.Parse(dr["Id"].ToString());
                        contact.FullName = dr["Name"].ToString();
                        contact.Email = dr["Email"].ToString();
                        contact.ContactNo = dr["ContactNo"].ToString();
                        contact.Subject = dr["Subject"].ToString();               
                        contact.Comment = dr["Comment"].ToString();
                       contact.ApplyDate = dr["ApplyDate"].ToString();                   
                        contact.Id = int.Parse(dr["Id"].ToString());

                        objContactUs.Add(contact);

                    }
                }
                return objContactUs;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        public ActionResult DeleteContactUs(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ContactUs");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ContactUs");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("ContactUs");
                //throw;
            }

        }

        #endregion

        #region xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx  -- Contact Us Reach Us -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult ReachUs()
        {
            ReachUs objReachUs = new ReachUs();
            objReachUs.ReachUsList = GetReachUsList();
            return View(objReachUs);
        }

        public List<ReachUs> GetReachUsList()
        {
            try
            {
                List<ReachUs> objReachUs = new List<ReachUs>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_ReachUs 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        ReachUs obj = new ReachUs();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.Image = dr["Image"].ToString();
                        obj.Heading = dr["Heading"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.Contact = dr["Contact"].ToString();
                        obj.Fax = dr["Fax"].ToString();
                        obj.Latitude = dr["Latitude"].ToString();
                        obj.Longitude = dr["Longitude"].ToString();
                        obj.Title = dr["Title"].ToString();


                        objReachUs.Add(obj);
                    }
                }
                return objReachUs;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult ReachUs(ReachUs objReachUs, HttpPostedFileBase[] Image)
        {
            try
            {
                if (objReachUs.Id != 0)
                {
                    ModelState.Remove("Image");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Images/";
                    string Img = string.Empty;

                    if (Image != null)
                    {
                        foreach (HttpPostedFileBase file in Image)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("ReachUs");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "Pointer-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objReachUs.Image = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objReachUs.Id == 0)
                    {
                        if (SaveReachUs(objReachUs))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ReachUs");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ReachUs");
                        }
                    }
                    else
                    {
                        if (UpdateReachUs(objReachUs))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ReachUs");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ReachUs");
                        }
                    }
                }
                return View("ReachUs", objReachUs);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("ReachUs");
            }

        }

        public bool SaveReachUs(ReachUs objReachUs)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ReachUs"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Image", objReachUs.Image);
                cmd.Parameters.AddWithValue("@Heading", objReachUs.Heading);   
                cmd.Parameters.AddWithValue("@Description", objReachUs.Description);
                cmd.Parameters.AddWithValue("@Contact", objReachUs.Contact);
                cmd.Parameters.AddWithValue("@Fax", objReachUs.Fax);
                cmd.Parameters.AddWithValue("@Latitude", objReachUs.Latitude);
                cmd.Parameters.AddWithValue("@Longitude", objReachUs.Longitude);
                cmd.Parameters.AddWithValue("@Title", objReachUs.Title);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateReachUs(ReachUs objReachUs)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ReachUs"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objReachUs.Id);
                cmd.Parameters.AddWithValue("@Image", objReachUs.Image);
                cmd.Parameters.AddWithValue("@Heading", objReachUs.Heading);
                cmd.Parameters.AddWithValue("@Description", objReachUs.Description);
                cmd.Parameters.AddWithValue("@Contact", objReachUs.Contact);
                cmd.Parameters.AddWithValue("@Fax", objReachUs.Fax);
                cmd.Parameters.AddWithValue("@Latitude", objReachUs.Latitude);
                cmd.Parameters.AddWithValue("@Longitude", objReachUs.Longitude);
                cmd.Parameters.AddWithValue("@Title", objReachUs.Title);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditReachUs(int? Id)
        {
            ReachUs objReachUs = new ReachUs();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ReachUs 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objReachUs.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objReachUs.Image = dt.Rows[0]["Image"].ToString();
                objReachUs.imgPreview = dt.Rows[0]["Image"].ToString();
                objReachUs.Heading = dt.Rows[0]["Heading"].ToString();
                objReachUs.Description = dt.Rows[0]["Description"].ToString();
                objReachUs.Contact = dt.Rows[0]["Contact"].ToString();
                objReachUs.Fax = dt.Rows[0]["Fax"].ToString();
                objReachUs.Latitude = dt.Rows[0]["Latitude"].ToString();
                objReachUs.Longitude = dt.Rows[0]["Longitude"].ToString();
                objReachUs.Title= dt.Rows[0]["Title"].ToString();

                objReachUs.ReachUsList = GetReachUsList();

                ViewBag.ActionType = "Update";
                return View("ReachUs", objReachUs);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("ReachUs", objReachUs);
            }
        }

        public ActionResult DeleteReachUs(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_ReachUs"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ReachUs");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("ReachUs");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("ReachUs");
                //throw;
            }

        }


        #endregion


        #region xxxxxxxxxxxxxxxxxxxxxxxxxxx -- Add New Page -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx
        public ActionResult AddNewPage()
        {
            AddNewPage objAddNewPage = new AddNewPage();
            objAddNewPage.NewPageList = GetNewPageList();
            return View(objAddNewPage);

        }
        public List<AddNewPage> GetNewPageList()
        {
            try
            {
                List<AddNewPage> objAddNewPage = new List<AddNewPage>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_AddNewPage 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        AddNewPage obj = new AddNewPage();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());
                        obj.BannerImage = dr["BannerImage"].ToString();
                        obj.PageName = dr["PageName"].ToString();
                        obj.Description = dr["Description"].ToString();
                        obj.Slug = dr["Slug"].ToString();
                        obj.Img_Alt_Tag = dr["Img_Alt_Tag"].ToString();

                        objAddNewPage.Add(obj);
                    }
                }
                return objAddNewPage;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult AddNewPage(AddNewPage objAddNewPage, HttpPostedFileBase[] BannerImage)
        {
            try
            {
                if (objAddNewPage.Id != 0)
                {
                    ModelState.Remove("BannerImage");
                }

                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/InnerBanner/";
                    string Img = string.Empty;

                    if (BannerImage != null)
                    {
                        foreach (HttpPostedFileBase file in BannerImage)
                        {
                            //string MainFile = string.Empty;
                            //Checking file is available to save.  
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidImageFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only jpg/jpeg and png files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;
                                    return RedirectToAction("AddNewPage");
                                }
                                else
                                {
                                    //MainFile = util.GetUniqueName(VirtualFile, "BlogImg", ".jpg", null, false); //Get Unique Name  
                                    //string  MainFile = "BlogImg-" + DateTime.Now.ToString("yyyymmddMMss") + System.IO.Path.GetExtension(file.FileName);
                                    var fileName = "AddNewPage-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    Img += fileName + ",";
                                }
                            }
                        }

                        if (Img != "")
                        {
                            objAddNewPage.BannerImage = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objAddNewPage.Id == 0)
                    {
                        if (SaveAddNewPage(objAddNewPage))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AddNewPage");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AddNewPage");
                        }
                    }
                    else
                    {
                        if (UpdateAddNewPage(objAddNewPage))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AddNewPage");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("AddNewPage");
                        }
                    }
                }
                return View("AddNewPage", objAddNewPage);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("AddNewPage");
            }

        }

        public bool SaveAddNewPage(AddNewPage objAddNewPage)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_AddNewPage"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@PageName", objAddNewPage.PageName);
                cmd.Parameters.AddWithValue("@BannerImage", objAddNewPage.BannerImage);
                cmd.Parameters.AddWithValue("@Description", objAddNewPage.Description);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objAddNewPage.PageName));
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objAddNewPage.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateAddNewPage(AddNewPage objAddNewPage)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_AddNewPage"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objAddNewPage.Id);
                cmd.Parameters.AddWithValue("@PageName", objAddNewPage.PageName);
                cmd.Parameters.AddWithValue("@BannerImage", objAddNewPage.BannerImage);
                cmd.Parameters.AddWithValue("@Description", objAddNewPage.Description);
                cmd.Parameters.AddWithValue("@Slug", Generateslg(objAddNewPage.PageName));
                cmd.Parameters.AddWithValue("@Img_Alt_Tag", objAddNewPage.Img_Alt_Tag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditAddNewPage(int? Id)
        {
            AddNewPage objAddNewPage = new AddNewPage();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_AddNewPage 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objAddNewPage.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objAddNewPage.BannerImage = dt.Rows[0]["BannerImage"].ToString();
                objAddNewPage.imgPreview = dt.Rows[0]["BannerImage"].ToString();
                objAddNewPage.PageName = dt.Rows[0]["PageName"].ToString();
                objAddNewPage.Description = dt.Rows[0]["Description"].ToString();
                objAddNewPage.Slug = dt.Rows[0]["Slug"].ToString();
                objAddNewPage.Img_Alt_Tag = dt.Rows[0]["Img_Alt_Tag"].ToString();

                objAddNewPage.NewPageList = GetNewPageList();

                ViewBag.ActionType = "Update";
                return View("AddNewPage", objAddNewPage);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("AddNewPage", objAddNewPage);
            }
        }

        public ActionResult DeleteAddNewPage(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_AddNewPage"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("AddNewPage");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("AddNewPage");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("AddNewPage");
                //throw;
            }

        }


        #endregion


        #region xxxxxxxxxxxxxxxxxxxxx -- Add MetaTag -- xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx

        public ActionResult MetaTag()
        {
            MetaTag objMetaTag = new MetaTag();
            objMetaTag.MetaTagList = GetMetaTagList();
            return View(objMetaTag);

        }
        public List<MetaTag> GetMetaTagList()
        {
            try
            {
                List<MetaTag> objMetaTag = new List<MetaTag>();
                DataTable dt = new DataTable();
                dt = util.Display("Execute Proc_MetaTag 'get'");
                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                        MetaTag obj = new MetaTag();
                        obj.Sr = dr["Sr"].ToString();
                        obj.Id = int.Parse(dr["Id"].ToString());            
                        obj.Title = dr["Title"].ToString();
                        obj.PageUrl = dr["PageUrl"].ToString();
                        obj.MetaKeyword = dr["MetaKeywords"].ToString();
                        obj.MetaDescription = dr["MetaDescription"].ToString();
                        obj.CanonicalTag = dr["CanonicalTag"].ToString();

                        objMetaTag.Add(obj);
                    }
                }
                return objMetaTag;
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return null;
            }
        }

        [HttpPost]
        [ValidateInput(false)]
        public ActionResult MetaTag(MetaTag objMetaTag)
        {
            try
            {
               
                if (ModelState.IsValid)
                {                  
                    if (objMetaTag.Id == 0)
                    {
                        if (SaveMetaTag(objMetaTag))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                    }
                    else
                    {
                        if (UpdateMetaTag(objMetaTag))
                        {
                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not updated successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("MetaTag");
                        }
                    }
                }
                return View("MetaTag", objMetaTag);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("MetaTag");
            }

        }

        public bool SaveMetaTag(MetaTag objMetaTag)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_MetaTag"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@PageUrl", objMetaTag.PageUrl);
                cmd.Parameters.AddWithValue("@Title", objMetaTag.Title);
                cmd.Parameters.AddWithValue("@MetaKeywords", objMetaTag.MetaKeyword);
                cmd.Parameters.AddWithValue("@MetaDescription", objMetaTag.MetaDescription);
                cmd.Parameters.AddWithValue("@CanonicalTag", objMetaTag.CanonicalTag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public bool UpdateMetaTag(MetaTag objMetaTag)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_MetaTag"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "update");
                cmd.Parameters.AddWithValue("@Id", objMetaTag.Id);
                cmd.Parameters.AddWithValue("@PageUrl", objMetaTag.PageUrl);
                cmd.Parameters.AddWithValue("@Title", objMetaTag.Title);
                cmd.Parameters.AddWithValue("@MetaKeywords", objMetaTag.MetaKeyword);
                cmd.Parameters.AddWithValue("@MetaDescription", objMetaTag.MetaDescription);
                cmd.Parameters.AddWithValue("@CanonicalTag", objMetaTag.CanonicalTag);
                if (util.Execute(cmd))
                {
                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }

        public ActionResult EditMetaTag(int? Id)
        {
            MetaTag objMetaTag = new MetaTag();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_MetaTag 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                objMetaTag.Id = Convert.ToInt32(dt.Rows[0]["Id"].ToString());
                objMetaTag.PageUrl = dt.Rows[0]["PageUrl"].ToString();
                objMetaTag.Title = dt.Rows[0]["Title"].ToString();
                objMetaTag.MetaKeyword = dt.Rows[0]["MetaKeywords"].ToString();
                objMetaTag.MetaDescription = dt.Rows[0]["MetaDescription"].ToString();
                objMetaTag.CanonicalTag = dt.Rows[0]["CanonicalTag"].ToString();


                objMetaTag.MetaTagList = GetMetaTagList();

                ViewBag.ActionType = "Update";
                return View("MetaTag", objMetaTag);
            }
            else
            {
                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return View("MetaTag", objMetaTag);
            }
        }

        public ActionResult DeleteMetaTag(int? Id)
        {
            try
            {
                using (SqlCommand cmd = new SqlCommand("Proc_MetaTag"))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.AddWithValue("@para", "delete");
                    cmd.Parameters.AddWithValue("@Id", Id);
                    if (util.Execute(cmd))
                    {
                        ViewBag.Message = "success";
                        ViewBag.Message1 = "Record deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("MetaTag");
                    }
                    else
                    {
                        ViewBag.Message = "warning";
                        ViewBag.Message1 = "Record could not deleted successfully.";

                        TempData["Message"] = ViewBag.Message;
                        TempData["Message1"] = ViewBag.Message1;

                        return RedirectToAction("MetaTag");
                    }
                }
            }
            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;

                return RedirectToAction("MetaTag");
                //throw;
            }

        }

        #endregion


        public string Generateslg(string strtext)
        {
            string str = strtext.ToString().ToLower();
            str = Regex.Replace(str, @"[^0-9a-zA-Z]+", "-");
            return str;
        }
    }
}
