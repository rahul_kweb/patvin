﻿using Patvin.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Mvc;

namespace Patvin.Controllers
{
    public class WebsiteController : Controller
    {
        //
        // GET: /Website/
        Utility util = new Utility();

        #region Home Page
        public ActionResult Index()
        {
            TempData["Menu"] = "Home";
            string title = "/";
            ViewBag.Title = UpdateMetaDetails(title);

            ViewBag.Banner = HomeBanner();
            ViewBag.WorldClassSolutions = CMS(1);
            ViewBag.WorldClassSolutionspartners = WorldClassSolutionspartners();
            ViewBag.AboutPatvin = CMS(2);
            ViewBag.IndustrialSolutions = CMS(3);
            ViewBag.HomeStatistics = HomeStatistics();
            ViewBag.HomeIndustries = HomeIndustries();
            ViewBag.GreenWorld = GreenWorld();
            ViewBag.ProductSolutions = ProductSolutions();
            ViewBag.HomeServices = HomeServices();


            return View();
        }

        public string HomeBanner()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Banner 'bindBanner'");
            if (dt.Rows.Count > 0)
            {
                //int count = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    //if (count == 1)
                    //{
                    strBuild.Append("<div class='slide' style='background-image:url(/Content/uploads/HomeBanner/" + dr["Image"].ToString() + ");'>");
                    strBuild.Append("<div class='maxWidth textSlide'>");
                    strBuild.Append("<div class='text_1'>" + dr["BannerText1"].ToString() + "</div>");
                    strBuild.Append("<div class='text_2'>" + dr["BannerText2"].ToString() + "</div>");
                    strBuild.Append("<p class='text_3'>" + dr["BannerText3"].ToString() + "</p>");
                    strBuild.Append("<div class='slideBtn'>");
                    strBuild.Append("<a href='" + dr["Url"].ToString() + "' class='orangeBtn'>Learn more</a>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    //}
                    //else
                    //{
                    //    strBuild.Append("<div class='slide' style='background-image:url(/Content/uploads/HomeBanner/" + dr["Image"].ToString() + ");'>");
                    //    strBuild.Append("<div class='maxWidth textSlide'>");
                    //    strBuild.Append("<div class='text_1'>" + dr["BannerText1"].ToString() + "</div>");
                    //    strBuild.Append("<div class='text_2'>" + dr["BannerText2"].ToString() + "</div>");
                    //    strBuild.Append("<p class='text_3'>" + dr["BannerText2"].ToString() + "</p>");
                    //    strBuild.Append("<div class='slideBtn'>");
                    //    strBuild.Append("<a href = 'class='orangeBtn''>Learn more</a>");
                    //    strBuild.Append("</div>");
                    //    strBuild.Append("</div>");
                    //    strBuild.Append("</div>");
                    //}
                    //count++;
                }
                response = strBuild.ToString();
            }
            return response;
        }

        public string WorldClassSolutionspartners()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeSolutions 'bindHomeSolutions'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class=''>");
                    strBuild.Append("<div class='clogoBox fade_anim'>");
                    strBuild.Append("<img src = '/Content/uploads/HomeSolutions/" + dr["Image"].ToString() + "' alt='"+ dr["Img_Alt_Tag"].ToString() + "' > ");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string HomeStatistics()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeStatistic 'bindHomeStatistics'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='statisticHolder' id='counter' >");
                    strBuild.Append("<div class='iconBox'>");
                    strBuild.Append("<span><i class='" + dr["Icon"].ToString() + "'></i></span>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='iconTxt'><strong class='titillium'><span class='counter' data-count=" + dr["Counting"].ToString() + ">0</span>+</strong>" + dr["Description"].ToString() + "</div> ");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string HomeIndustries()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeIndustries 'bindHomeIndustries'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<a href ='" + dr["Url"].ToString() + "' class='col-lg-3 col-sm-4 col-6'>");
                    strBuild.Append("<img src ='/Content/uploads/HomeIndustries/" + dr["Icon"].ToString() + "' alt='"+ dr["Img_Alt_Tag"].ToString() + "'>");
                    strBuild.Append("<h3>" + dr["Heading"].ToString() + "</h3>");
                    strBuild.Append("</a>");

                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string GreenWorld()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeGreenWorld 'bindHomeGreenWorld'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='row'>");
                    strBuild.Append("<div class='col-sm-6 col-md-7 wc_text whiteColor'>");
                    strBuild.Append("<div class='title whiteColor'>");
                    strBuild.Append("<h4>We believe in a</h4>");
                    strBuild.Append("<h2 class='titillium'><strong>Green World</strong></h2>");
                    strBuild.Append("</div>");
                    strBuild.Append("<p>" + dr["Descrption"].ToString() + "</p><a href='/green-initiatives'>Read More <i class='fas fa-chevron-circle-right'></i></a>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='col-sm-6 col-md-5 wc_img'>");
                    strBuild.Append("<div class='wcImg_holder' style='background-image:url(/Content/uploads/Images/" + dr["Image"].ToString() + ")'>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string ProductSolutions()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeProductSolutions 'bindHomeProductSolutions'");
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {

                    //strBuild.Append("<div class='col-lg-3 col-sm-6 col-12'>");
                    strBuild.Append("<div>");
                    strBuild.Append("<div class='prod_box fade_anim'>");
                    strBuild.Append("<div class='prod_img'><img src='/Content/Uploads/Images/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "'></div>");
                    strBuild.Append("<div class='prod_info fade_anim'>");
                    strBuild.Append(" <a href=" + dr["Url"].ToString() + "><h2 class='titillium prodTitle'>" + dr["title"].ToString() + "</h2></a>");
                    strBuild.Append(" <p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("<a href=" + dr["Url"].ToString() + " class='readmore'>Read more <i class='fas fa-chevron-circle-right'></i></a>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    //strBuild.Append("</div>");
                }
                response = strBuild.ToString();
            }
            return response;
        }

        public string HomeServices()
        {

            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Services 'bindHomeServices'");
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    string sluglink = dr["Title"].ToString();

                    strBuild.Append("<div class='hs_column fade_anim'>");
                    strBuild.Append("<img src='/Content/Uploads/Services/" + dr["Icon"].ToString() + "' alt='"+ dr["Icon_Alt_Tag"].ToString() + "'>");
                    strBuild.Append("<h2 class='titillium'>" + dr["Title"].ToString() + "</h2>");
                    strBuild.Append("" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("<a href='/Services#" + Generateslgforservice(sluglink) + "'>Read More <i class='fas fa-chevron-circle-right'></i></a>");
                    strBuild.Append("</div>");
                }
                response = strBuild.ToString();
            }
            return response;
        }

        #endregion

        #region About Us Who We Are

        public ActionResult WhoWeAre()
        {
            TempData["Menu"] = "WhoWeAre";
            string title = "WhoWeAre";
            ViewBag.Title = UpdateMetaDetails(title);


            ViewBag.Founded = CMS(4);
            ViewBag.FoundedImageText = CMS(5);

            var obj = GetQualityMissionVision();
            if (obj.Count > 0)
            {
                ViewBag.Heading = obj["Title"].ToString();
                ViewBag.Description = obj["Desc"].ToString();
            }

            ViewBag.Milestone = BindWhatWeAreMilestone();
            ViewBag.InfrastructureFacilities = BindInfrastructureFacilities();
            ViewBag.InnerBanner = BindInnerBanner(1);

            return View();
        }


        public Dictionary<string, string> GetQualityMissionVision()
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();

            StringBuilder strTitle = new StringBuilder();
            StringBuilder strDesc = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_QualityMissionVison 'bindQualityMissionVison'");
            if (dt.Rows.Count > 0)
            {
                int count = 1;
                foreach (DataRow dr in dt.Rows)
                {
                    if (count == 1)
                    {
                        strTitle.Append("<li class='nav-item'>");
                        strTitle.Append("<a class='nav-link active' role='tab' data-toggle='pill' href='#cf-quality' id='tab-quality' aria-controls='cf-quality' aria-selected='true' >");
                        strTitle.Append("<i class='" + dr["Icon"].ToString() + "'></i> " + dr["Title"].ToString() + "</a>");
                        strTitle.Append("</li>");

                        strDesc.Append("<div class='tab-pane fade show active' role='tabpanel' id='cf-quality' aria-labelledby='tab-quality'>");
                        strDesc.Append(dr["Description"].ToString());
                        strDesc.Append("</div>");

                        count++;
                    }
                    else if (count == 2)
                    {
                        strTitle.Append("<li class='nav-item'>");
                        strTitle.Append("<a class='nav-link' role='tab' data-toggle='pill' href='#cf-mission' id='tab-mission' aria-controls='cf-mission' aria-selected='true' >");
                        strTitle.Append("<i class='" + dr["Icon"].ToString() + "'></i> " + dr["Title"].ToString() + "</a>");
                        strTitle.Append("</li>");

                        strDesc.Append("<div class='tab-pane fade' role='tabpanel' id='cf-mission' aria-labelledby='tab-mission'>");
                        strDesc.Append(dr["Description"].ToString());
                        strDesc.Append("</div>");
                        count++;
                    }
                    else
                    {
                        strTitle.Append("<li class='nav-item'>");
                        strTitle.Append("<a class='nav-link' role='tab' data-toggle='pill' href='#cf-vision' id='tab-vision' aria-controls='cf-vision' aria-selected='true' >");
                        strTitle.Append("<i class='" + dr["Icon"].ToString() + "'></i> " + dr["Title"].ToString() + "</a>");
                        strTitle.Append("</li>");

                        strDesc.Append("<div class='tab-pane fade' role='tabpanel' id='cf-vision' aria-labelledby='tab-vision'>");
                        strDesc.Append(dr["Description"].ToString());
                        strDesc.Append("</div>");
                    }



                }
                objDis.Add("Title", strTitle.ToString());
                objDis.Add("Desc", strDesc.ToString());

            }

            return objDis;
        }

        public string BindWhatWeAreMilestone()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Milestones 'bindWhatWeAreMilestone'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='cd-timeline-block'>");
                    strBuild.Append("<div class='cd-timeline-img'>");
                    strBuild.Append(dr["Year"].ToString());
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='cd-timeline-content'>");
                    strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                }
                response = strBuild.ToString();
            }

            return response;
        }

        public string BindInfrastructureFacilities()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_InfrastructureFacilities 'bindInfrastructureFacilities'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='col-lg-4 col-sm-6'>");
                    strBuild.Append("<a href ='/Content/uploads/Images/" + dr["Image"] + "' data-fancybox='img_gallery' data-caption=" + dr["Heading"] + " class='infra_thumb'>");
                    strBuild.Append("<img src ='/Content/uploads/Images/" + dr["Image"] + "' alt='"+ dr["Img_Alt_Tag"] +"'>");
                    strBuild.Append("<strong>" + dr["Heading"] + "</strong>");
                    strBuild.Append("</a>");
                    strBuild.Append("</div>");
                }
                response = strBuild.ToString();
            }

            return response;
        }

        #endregion

        #region About Us Management
        public ActionResult Management()
        {
            TempData["Menu"] = "Management";

            string title = "Management";
            ViewBag.Title = UpdateMetaDetails(title);

            ViewBag.Management = BindManagement();
            ViewBag.InnerBanner = BindInnerBanner(12);
            ViewBag.CMS = CMS(10);
            return View();
        }

        public string BindManagement()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Management 'bindManagement'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='row teamBox mb-5 p-0'>");
                    strBuild.Append("<div class='col-md-5 p-3'>");
                    strBuild.Append("<img src='/Content/uploads/Images/" + dr["Image"].ToString() + "' class='img-fluid' alt='"+ dr["Img_Alt_Tag"].ToString() + "'>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='col px-5 py-3'>");
                    strBuild.Append("<h2>" + dr["Heading"].ToString() + ",</h2><h3> " + dr["Designation"].ToString() + "</h3>");
                    strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }


        #endregion

        #region About Us Our Values

        public ActionResult OurValues()
        {
            string title = "OurValues";
            ViewBag.Title = UpdateMetaDetails(title);

            TempData["Menu"] = "OurValues";
            ViewBag.OurValues = BindOurValues();
            ViewBag.InnerBanner = BindInnerBanner(2);
            ViewBag.CMS = CMS(6);

            return View();

        }

        public string BindOurValues()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_OurValues 'bindOurValues'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='w-100 text-center valueContainer' id='a'>");
                    strBuild.Append("<img src='/Content/uploads/Images/" + dr["Image"].ToString() + "' alt='"+ dr["Img_Alt_Tag"].ToString()+"'>");
                    strBuild.Append("<h2 class='font-weight-bold text-uppercase flexFont'>" + dr["Heading"].ToString() + "</h2>");
                    strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }

        #endregion


        #region About Health & Safety
        public ActionResult HealthSafety()
        {
            string title = "HealthSafety";
            ViewBag.Title = UpdateMetaDetails(title);

            TempData["Menu"] = "HealthSafety";

            ViewBag.InnerBanner = BindInnerBanner(3);
            ViewBag.CMS = CMS(7);

            return View();
        }

        #endregion

        #region Product

        public ActionResult products()
        {
            string title = "products";
            ViewBag.Title = UpdateMetaDetails(title);

            TempData["Menu"] = "products";

            ViewBag.CMS = CMS(8);
            ViewBag.BindPartners = BindPartners();
            ViewBag.BindRangeOfProducts = BindRangeOfProducts();
            ViewBag.ServiceSpareParts = ServiceSpareParts();
            ViewBag.HomeIndustries = HomeIndustries();
            ViewBag.BindRandomTestimonials = BindRandomTestimonials();

            ViewBag.InnerBanner = BindInnerBanner(4);

            return View();
        }

        public string BindPartners()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ProductPartners 'bindProductPartners'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='col-lg-3 col-sm-4 col-6 pplogo'>");
                    strBuild.Append("<a data-fancybox data-src='#" + dr["Id"].ToString() + "' href='javascript:;'>");
                    strBuild.Append("<div class='ppimg'><img src='/Content/uploads/Images/" + dr["Image"].ToString() + "' alt='"+ dr["Img_Alt_Tag"].ToString() + "'></div>");
                    strBuild.Append("<span> " + dr["Heading"].ToString() + " years </span>");
                    strBuild.Append("</a>");
                    strBuild.Append("<div id=" + dr["Id"].ToString() + " class='partnersDetails' style='display: none'>");
                    strBuild.Append("<div class='partnerLogo'><img src='/Content/uploads/Images/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "' ></div>");
                    strBuild.Append(dr["Description"].ToString());
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");



                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string BindRangeOfProducts()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_RangeOfProduct 'bindRangeOfProduct'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='col-md-4 mb-3'>");
                    strBuild.Append("<h3 class='orangeColor font-weight-bold border-bottom pb-2'>Finishing Equipment</h3>");
                    strBuild.Append(dr["Equipment"].ToString());
                    strBuild.Append("</div>");

                    strBuild.Append("<div class='col-md-4 mb-3'>");
                    strBuild.Append("<h3 class='orangeColor font-weight-bold border-bottom pb-2'>Sealant and Adhesive Applications</h3>");
                    strBuild.Append(dr["Applications"].ToString());

                    strBuild.Append("</div>");

                    strBuild.Append("<div class='col-md-4 mb-3'>");
                    strBuild.Append("<h3 class='orangeColor font-weight-bold border-bottom pb-2'>Pumps</h3>");
                    strBuild.Append(dr["Pumps"].ToString());

                    strBuild.Append("</div>");


                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string ServiceSpareParts()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ServiceSpareParts 'bindServiceSpareParts'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='row'>");
                    strBuild.Append("<div class='col-auto'><img src='/Content/uploads/Images/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "'></div>");
                    strBuild.Append("<div class='col'>");
                    strBuild.Append("<h3 class='titillium font-weight-bold blueColor mb-1'>" + dr["Heading"].ToString() + "</h3>");
                    strBuild.Append("<P>" + dr["Description"].ToString() + "</p>");

                    strBuild.Append("</div>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }


        public string BindRandomTestimonials()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Testimonials 'bindRandomTestimonials'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='csTestimonial'>");
                    strBuild.Append("<div class='container-fluid cstitle'><strong>Testimonials</strong></div>");
                    strBuild.Append("<div class='cs_testim'>");
                    strBuild.Append(dr["Description"].ToString());
                    strBuild.Append("<p class='cstestimName'><strong>" + dr["Name"].ToString() + "</strong><br>" + dr["Position"].ToString() + "<br>" + dr["CompanyName"].ToString() + "</p>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");



                }
                response = strBuild.ToString();

            }
            return response;
        }


        [HttpPost]
        public ActionResult products(GetInTouchForm objGetInTouchForm)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (objGetInTouchForm.Id == 0)
                    {
                        if (SaveGetInTouchForm(objGetInTouchForm))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("products");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("products");
                        }
                    }
                }
                return View("products", objGetInTouchForm);

            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return View("products", objGetInTouchForm);
            }
        }

        public bool SaveGetInTouchForm(GetInTouchForm objGetInTouchForm)
        {
            bool response;

            using (SqlCommand cmd = new SqlCommand("Proc_GetInTouchForm"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", objGetInTouchForm.FullName);
                cmd.Parameters.AddWithValue("@Email", objGetInTouchForm.Email);
                cmd.Parameters.AddWithValue("@ContactNo", objGetInTouchForm.ContactNo);
                cmd.Parameters.AddWithValue("@Comment", objGetInTouchForm.Comment);

                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact us enquiery </font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Full Name : </strong></td><td> " + objGetInTouchForm.FullName + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + objGetInTouchForm.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Mobile No : </strong></td> <td> " + objGetInTouchForm.ContactNo + " </td> </tr>");
                    sb.AppendFormat("<tr> <td><strong>Message : </strong></td> <td> " + objGetInTouchForm.Comment + " </td></tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["GetInTouchForm"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    util.SendEmail(sb.ToString(), emailid, "Get In Touch", "", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        #endregion


        #region CaseStudyListing

        public ActionResult CaseStudyListing()
        {
            string title = "CaseStudyListing";
            ViewBag.Title = UpdateMetaDetails(title);

            TempData["Menu"] = "CaseStudyListing";

            ViewBag.BindCaseStudyListing = BindCaseStudyListing();
            ViewBag.InnerBanner = BindInnerBanner(5);

            return View();
        }

        public string BindCaseStudyListing()
        {

            string response = "";
            StringBuilder strBuild = new StringBuilder();
            StringBuilder strTitle = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CaseStudies 'bindCaseStudyListing'");
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    strBuild.Append("<div class='col-lg-3 col-md-4 col-sm-6 col-12'>");
                    strBuild.Append("<div style='height:100%;padding-bottom:30px'>");
                    strBuild.Append("<a data-fancybox  data-src='' href='#case_study_form' data-touch='false' class='csBox' onclick=\"RoleSelected('" + dr["Heading"].ToString() + "')\">");
                    strBuild.Append("<div class='cs_img'><img src='/Content/Uploads/CaseStudies/" + dr["Image"].ToString() + "' alt='"+ dr["Img_Alt_Tag"].ToString() + "'></div>");
                    strBuild.Append("<div class='cs_title fade_anim'>");
                    strBuild.Append("<div class='industryName'>" + dr["Category"].ToString() + "</div>");
                    strBuild.Append("<h1 class='titillium'>" + dr["Heading"].ToString() + "</h1>");
                    strBuild.Append("</div>");
                    strBuild.Append("</a>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                }
                response = strBuild.ToString();
                ViewBag.Title = strTitle.ToString();
            }
            return response;
        }

        [HttpPost]
        public ActionResult CaseStudyListing(Casestudyform model)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (model.caseId == 0)
                    {
                        if (SaveCasestudyform(model))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CaseStudyListing");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("CaseStudyListing");
                        }
                    }
                }
                return View("CaseStudyListing", model);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return View("CaseStudyListing", model);
            }
        }

        public bool SaveCasestudyform(Casestudyform model)
        {
            bool response;

            using (SqlCommand cmd = new SqlCommand("Proc_Casestudyform"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Title", model.Title);
                cmd.Parameters.AddWithValue("@Name", model.Name);
                cmd.Parameters.AddWithValue("@Email", model.Email);
                cmd.Parameters.AddWithValue("@CompanyName", model.CompanyName);

                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Case Studies</font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Title : </strong></td><td> " + model.Title + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Name: </strong></td> <td> " + model.Name + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Company Name : </strong></td> <td> " + model.CompanyName + " </td> </tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + model.Email + " </td></tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["Casestudy"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    util.SendEmail(sb.ToString(), emailid, "Case study request from website", "", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        #endregion

        #region Testimonials
        public ActionResult Testimonials()
        {
            string title = "Testimonials";
            ViewBag.Title = UpdateMetaDetails(title);

            TempData["Menu"] = "Testimonials";

            ViewBag.Testimonials = BindTestimonials();
            ViewBag.InnerBanner = BindInnerBanner(14);
            return View();
        }
        public string BindTestimonials()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Testimonials 'bindTestimonials'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='col-md-10 mx-auto my-4 p-0 testimBox'>");
                    strBuild.Append("<div class='testimonial pb-4 pt-5 px-5'>");
                    strBuild.Append(dr["Description"].ToString());
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='testim_author'>");
                    strBuild.Append("<strong>" + dr["Name"].ToString() + "</strong>");
                    strBuild.Append("<br>");
                    strBuild.Append(dr["Position"].ToString());
                    strBuild.Append("<br>");
                    strBuild.Append(dr["CompanyName"].ToString());
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;

        }
        #endregion

        #region Services
        public ActionResult Services()
        {
            string title = "Services";
            ViewBag.Title = UpdateMetaDetails(title);

            TempData["Menu"] = "Services";

            ViewBag.BindServices = BindServices();
            ViewBag.InnerBanner = BindInnerBanner(6);
            return View();
        }

        public string BindServices()
        {

            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_Services 'bindservicesmenu'");
            if (dt.Rows.Count > 0)
            {

                foreach (DataRow dr in dt.Rows)
                {
                    string sluglink = dr["Title"].ToString();

                    strBuild.Append("<div class='row my-5 serviceSection' Id=" + Generateslgforservice(sluglink) + ">");
                    strBuild.Append("<div class='col-lg-7 p-0 serviceTextBox'>");
                    strBuild.Append("<div class='px-5 py-4'>");
                    strBuild.Append("<img src='/Content/uploads/Services/" + dr["Icon"].ToString() + "' alt='" + dr["Icon_Alt_Tag"].ToString() + "'>");
                    strBuild.Append("<h2 class='titillium'>" + dr["Title"].ToString() + "</h2>");
                    strBuild.Append(dr["Description"].ToString());
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='col-lg-5 p-0 serviceImgBox'>");
                    strBuild.Append("<img src='/Content/uploads/Services/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "'>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                }
                response = strBuild.ToString();
            }
            return response;
        }

        public string Generateslgforservice(string strtext)
        {
            string str = strtext.ToString().ToLower();
            str = Regex.Replace(str, @"[^a-z0-9\s-]", "-");
            str = Regex.Replace(str, " ", "-").Trim();

            return str;
        }

        #endregion



        #region Newsroom
        public ActionResult Newsroom()
        {
            TempData["Menu"] = "Newsroom";

            string title = "Newsroom";
            ViewBag.Title = UpdateMetaDetails(title);

            var obj = GetNewsroom();
            ViewBag.Type = obj["Type"].ToString();
            ViewBag.News = obj["News"].ToString();
            ViewBag.Event = obj["Event"].ToString();
            ViewBag.Articles = obj["Articles"].ToString();


            ViewBag.InnerBanner = BindInnerBanner(7);
            return View();
        }

        public Dictionary<string, string> GetNewsroom()
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();

            StringBuilder strType = new StringBuilder();
            StringBuilder strDescNews = new StringBuilder();
            StringBuilder strDescEvent = new StringBuilder();
            StringBuilder strDescArticles = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_NewsRoom 'bindNewsroom'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        if (dr["Type"].ToString() == "News")
                        {
                            strType.Append("<li class='col p-0 nav-item'><a class='nav-link active' role='tab' data-toggle='pill' href='#news' id='tab-news' aria-controls='news' aria-selected='true'>" + dr["Type"].ToString() + "</a></li>");
                        }
                        else if (dr["Type"].ToString() == "Events")
                        {
                            strType.Append("<li class='col p-0 nav-item'><a class='nav-link ' role='tab' data-toggle='pill' href='#events' id='tab-events' aria-controls='events' aria-selected='false'>" + dr["Type"].ToString() + "</a></li>");

                        }
                        else
                        {
                            strType.Append("<li class='col p-0 nav-item'><a class='nav-link' role='tab' data-toggle='pill' href='#techarticles' id='tab-techarticles' aria-controls='techarticles' aria-selected='false'>" + dr["Type"].ToString() + "</a></li>");
                        }

                    }

                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {
                        if (dr["Type"].ToString() == "News")
                        {
                            string dateyear = dr["Date"].ToString();
                            string[] date = dateyear.Split(' ');


                            strDescNews.Append("<div class='articleBox item-news'>");
                            strDescNews.Append("<div class='articleHolder rounded fade_anim'>");
                            strDescNews.Append("<div class='tag'><strong > " + date[0] + " </strong > " + date[1] + " " + date[2] + " </div>");
                            strDescNews.Append("<div class='imgbox rounded-top'><img src='/Content/uploads/NewsRoom/" + dr["Image"].ToString() + "' alt='"+ dr["Img_Alt_Tag"].ToString() + "'></div>");
                            strDescNews.Append("<h2 class='titillium'>" + dr["Title"].ToString() + "</h2>");
                            strDescNews.Append(dr["Description"].ToString());
                            strDescNews.Append("<a href='/NewsRoomDetail/" + dr["Slug"].ToString() + "'>Read More</a>");
                            strDescNews.Append("</div>");
                            strDescNews.Append("</div>");
                        }
                        else if (dr["Type"].ToString() == "Events")
                        {
                            string dateyear = dr["Date"].ToString();
                            string[] date = dateyear.Split(' ');

                            strDescEvent.Append("<div class='articleBox item-events'>");
                            strDescEvent.Append("<div class='articleHolder rounded fade_anim'>");
                            strDescEvent.Append("<div class='tag'><strong > " + date[0] + " </strong > " + date[1] + " " + date[2] + " </div>");
                            strDescEvent.Append("<div class='imgbox rounded-top'><img src='/Content/uploads/NewsRoom/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "'></div>");
                            strDescEvent.Append("<h2 class='titillium'>" + dr["Title"].ToString() + "</h2>");
                            strDescEvent.Append(dr["Description"].ToString());
                            strDescEvent.Append("<a href='/NewsRoomDetail/" + dr["Slug"].ToString() + "'>Read More</a>");
                            strDescEvent.Append("</div>");
                            strDescEvent.Append("</div>");
                        }
                        else
                        {
                            string dateyear = dr["Date"].ToString();
                            string[] date = dateyear.Split(' ');

                            strDescArticles.Append("<div class='articleBox item-news'>");
                            strDescArticles.Append("<div class='articleHolder rounded fade_anim'>");
                            strDescArticles.Append("<div class='tag'><strong > " + date[0] + " </strong > " + date[1] + " " + date[2] + " </div>");
                            strDescArticles.Append("<div class='imgbox rounded-top'><img src='/Content/uploads/NewsRoom/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "'></div>");
                            strDescArticles.Append("<h2 class='titillium'>" + dr["Title"].ToString() + "</h2>");
                            strDescArticles.Append(dr["Description"].ToString());
                            strDescArticles.Append("<a href='/NewsRoomDetail/" + dr["Slug"].ToString() + "'>Read More</a>");
                            strDescArticles.Append("</div>");
                            strDescArticles.Append("</div>");
                        }
                    }

                }

            }

            objDis.Add("Type", strType.ToString());
            objDis.Add("News", strDescNews.ToString());
            objDis.Add("Event", strDescEvent.ToString());
            objDis.Add("Articles", strDescArticles.ToString());

            return objDis;
        }




        #endregion

        #region NewsRoom Detail Page
        public ActionResult NewsRoomDetail(string strNews)
        {
           

            TempData["Menu"] = "Newsroom";

            var objNews = BindNewsRoomDetail(strNews);
            ViewBag.Type = objNews["Type"].ToString();
            ViewBag.BindNewsDetails = objNews["NewsDetail"].ToString();

            string title = "NewsRoomDetail/"+ objNews["MetatTitle"].ToString();
            ViewBag.Title = UpdateMetaDetails(title);


            ViewBag.HomeServices = HomeServices();
            ViewBag.HomeIndustries = HomeIndustries();

            if (objNews["BlogBanner"].ToString() == "")
            {
                ViewBag.InnerBanner = BindInnerBanner(7);
            }
            else
            {
                ViewBag.InnerBanner = objNews["BlogBanner"].ToString();
            }

            ViewBag.BlogBannerAlt = objNews["BlogBannerAlt"].ToString();



            return View();

        }

        public Dictionary<string, string> BindNewsRoomDetail(string strNews)
        {
            Dictionary<string, string> objNews = new Dictionary<string, string>();

            StringBuilder strBuild = new StringBuilder();
            StringBuilder strType = new StringBuilder();

            StringBuilder strMetaTitle = new StringBuilder();

            StringBuilder strBlogNewsRoomBanner = new StringBuilder();

            StringBuilder strBlogNewsRoomBannerAltTag = new StringBuilder();




            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_NewsRoom 'binNewsRoomDeatils',0,'','','','','','" + strNews + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strMetaTitle.Append(dr["Slug"].ToString());

                    //strType.Append(dr["Type"].ToString());

                    strType.Append(dr["Title"].ToString());

                    string dateyear = dr["Date"].ToString();
                    string[] date = dateyear.Split(' ');

                    strBuild.Append("<div class='newsTitle mb-4 pb-3 border-bottom'>");
                    strBuild.Append("<div class='date'><strong>" + date[0] + "</strong>" + date[1] + " " + date[2] + "</div>");
                    //strBuild.Append("<h1>" + dr["Title"].ToString() + "</h1>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='newsImg w-100 mb-4'><img src='/Content/uploads/NewsRoom/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "' class='w-100'></div>");
                    strBuild.Append(dr["Description"].ToString());


                    DataTable dtBanner = new DataTable();

                    //remove single quotes if title contain
                    String BlogTitle = dr["Title"].ToString();
                    String Title = BlogTitle.Replace("'", "");

                    dtBanner = util.Display("Exec Proc_NewsRoom 'bindNewsBannerByTitle',0,'','','','"+ Title + "'");
                    if (dtBanner.Rows.Count > 0)
                    {
                        foreach (DataRow drBanner in dtBanner.Rows)
                        {
                            strBlogNewsRoomBanner.Append(drBanner["Image"].ToString());
                            strBlogNewsRoomBannerAltTag.Append(drBanner["Img_Alt_Tag"].ToString());
                        }
                    }

                 }

            }
            objNews.Add("Type", strType.ToString());
            objNews.Add("NewsDetail", strBuild.ToString());

            objNews.Add("MetatTitle", strMetaTitle.ToString());

            objNews.Add("BlogBanner", strBlogNewsRoomBanner.ToString());

            objNews.Add("BlogBannerAlt", strBlogNewsRoomBannerAltTag.ToString());

            return objNews;
        }

        [HttpPost]
        public ActionResult NewsRoomDetail(GetInTouchForm objGetInTouchForm)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (objGetInTouchForm.Id == 0)
                    {
                        if (SaveNewsRoomDetail(objGetInTouchForm))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("NewsRoomDetail");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("NewsRoomDetail");
                        }
                    }
                }
                return View("NewsRoomDetail", objGetInTouchForm);

            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return View("NewsRoomDetail", objGetInTouchForm);
            }
        }


        public bool SaveNewsRoomDetail(GetInTouchForm objGetInTouchForm)
        {
            bool response;

            using (SqlCommand cmd = new SqlCommand("Proc_GetInTouchForm"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", objGetInTouchForm.FullName);
                cmd.Parameters.AddWithValue("@Email", objGetInTouchForm.Email);
                cmd.Parameters.AddWithValue("@ContactNo", objGetInTouchForm.ContactNo);
                cmd.Parameters.AddWithValue("@Comment", objGetInTouchForm.Comment);

                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact us enquiery </font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Full Name : </strong></td><td> " + objGetInTouchForm.FullName + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + objGetInTouchForm.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Mobile No : </strong></td> <td> " + objGetInTouchForm.ContactNo + " </td> </tr>");
                    sb.AppendFormat("<tr> <td><strong>Message : </strong></td> <td> " + objGetInTouchForm.Comment + " </td></tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["GetInTouchForm"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    util.SendEmail(sb.ToString(), emailid, "Get In Touch", "", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        #endregion


        #region Careers
        public ActionResult Careers()
        {
        
            TempData["Menu"] = "Careers";

            string title = "Careers";
            ViewBag.Title = UpdateMetaDetails(title);

            ViewBag.CareerOverview = CMS(9);
            ViewBag.BindCareerOverviewImages = BindCareerOverviewImages();
            ViewBag.BindWeBelive = BindWeBelive();
            ViewBag.BindOurPeople = BindOurPeople();
            ViewBag.BindCurrentOpening = BindCurrentOpening();

            ViewBag.InnerBanner = BindInnerBanner(8);
            return View();
        }

        public string BindCareerOverviewImages()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CareerOverViewImages 'bindCareerOverViewImages'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div style='background-image:url(/Content/uploads/OurPeople/" + dr["Image"].ToString() + "); '>");
                    strBuild.Append("<div class='cisImgbox'><img src='/Content/uploads/OurPeople/" + dr["Image"].ToString() + "' alt='"+ dr["Img_Alt_Tag"].ToString() + "'></div>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string BindWeBelive()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CareerWeBelieve 'bindCareerWeBelieve'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='col-md-3 col-sm-4 wbBox'>");
                    strBuild.Append("<p><strong class='no'>" + dr["Id"].ToString() + ".</strong><br>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }


        public string BindOurPeople()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CareerOurPeople 'bindCareerOurPeople'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div>");
                    strBuild.Append("<div class='employeeTestim'>");
                    strBuild.Append("<div class='employeeImg'><img src='/Content/uploads/OurPeople/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "'></div>");
                    strBuild.Append(" <div class='employeeContent'>");
                    strBuild.Append("<p class='employeeName'><strong>" + dr["Name"].ToString() + "</strong> (" + dr["Position"].ToString() + ")</p>");
                    strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }

        public string BindCurrentOpening()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CurrentOpening 'bindCurrentOpening'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='container-fluid vacancyBox'>");
                    strBuild.Append("<div class='row'>");
                    strBuild.Append("<div class='col-md-9 col-lg-10'>");
                    strBuild.Append("<h2 class='titillium'>" + dr["Title"].ToString() + "</h2>");
                    strBuild.Append("<ul class='vacancyTags clearfix'>");
                    strBuild.Append("<li title='Location'><i class='fas fa-map-marker-alt'></i> " + dr["Location"].ToString() + " </li>");
                    strBuild.Append("<li title='No. of Post'><i class='fas fa-check-circle'></i> " + dr["Post"].ToString() + " Post </li>");
                    strBuild.Append(" <li title='Qualification'><i class='fas fa-graduation-cap'></i> " + dr["Qualification"].ToString() + " </li>");
                    strBuild.Append("<li title='Experience Required'><i class='fas fa-briefcase'></i> " + dr["Experience"].ToString() + " </li>");
                    strBuild.Append("</ul>");
                    strBuild.Append("<p>" + dr["Description"].ToString() + "</p>");
                    strBuild.Append("</div>");
                    strBuild.Append("<div class='col-md-3 col-lg-2 d-flex align-items-center justify-content-md-center'>");
                    strBuild.Append("<button type='button' class='orangeBtn' data-fancybox  data-src='#Career_form'  data-touch='false' style='display: block' onclick=\"RoleSelected('" + dr["Title"].ToString() + "','" + dr["Location"].ToString() + "')\">Apply Now</button>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");
                    strBuild.Append("</div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }


        [HttpPost]
        public ActionResult Careers(ApplyForJob objApplyForJob, HttpPostedFileBase[] Resume)
        {
            try
            {
                ModelState.Remove("Resume");
                if (ModelState.IsValid)
                {
                    string VirtualFile = "~/Content/uploads/Resume/";
                    string Img = string.Empty;
                    Stream inputStr = null;
                    if (Resume != null)
                    {
                        foreach (HttpPostedFileBase file in Resume)
                        {
                            if (file != null)
                            {
                                string ext = Path.GetExtension(file.FileName).ToLower();
                                if (!util.IsValidResumeFileExtension(ext))
                                {
                                    ViewBag.Message = "warning";
                                    ViewBag.Message1 = "Only doc/docx and png pdf files are allowed!";

                                    TempData["Message"] = ViewBag.Message;
                                    TempData["Message1"] = ViewBag.Message1;

                                    return View("Careers", objApplyForJob);
                                }
                                else
                                {
                                    var fileName = "Resume-" + Guid.NewGuid().ToString() + System.IO.Path.GetExtension(file.FileName);
                                    file.SaveAs(Server.MapPath(VirtualFile + fileName));
                                    inputStr = file.InputStream;
                                    Img += fileName + ",";

                                }
                            }
                        }

                        if (Img != "")
                        {
                            objApplyForJob.Resume = Img.Substring(0, Img.Length - 1);
                        }
                    }

                    if (objApplyForJob.Id == 0)
                    {
                        if (SaveCareer(objApplyForJob, inputStr))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Careers");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Careers");
                        }
                    }
                }
                return View("Career", objApplyForJob);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("Careers");
            }
        }

        public bool SaveCareer(ApplyForJob objApplyForJob, Stream input)
        {
            bool response;
            using (SqlCommand cmd = new SqlCommand("Proc_ApplyForJob"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", objApplyForJob.Name);
                cmd.Parameters.AddWithValue("@Email", objApplyForJob.Email);
                cmd.Parameters.AddWithValue("@Role", objApplyForJob.Role);
                cmd.Parameters.AddWithValue("@Location", objApplyForJob.Location);
                cmd.Parameters.AddWithValue("@ContactNo", objApplyForJob.ContactNo);
                cmd.Parameters.AddWithValue("@Resume", objApplyForJob.Resume);
                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Apply For Job </font></strong></td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Role : </strong></td> <td> " + objApplyForJob.Role + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Location : </strong></td> <td> " + objApplyForJob.Location + " </td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Name : </strong></td><td> " + objApplyForJob.Name + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + objApplyForJob.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Mobile No : </strong></td> <td> " + objApplyForJob.ContactNo + " </td> </tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["CareerEmail"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    //var path1 = "~/Content/uploads/Resume/" + objApplyForJob.Resume;
                    //var path = Server.MapPath(path1);
                    util.SendEmail(sb.ToString(), emailid, "Career application from website", objApplyForJob.Resume, input);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        #endregion


        #region Contact Us
        public ActionResult ContactUs()
        {
            TempData["Menu"] = "ContactUs";

            string title = "ContactUs";
            ViewBag.Title = UpdateMetaDetails(title);

            var social = SocialMedia(2);
            if (social.Count > 0)
            {
                TempData["Sales"] = social["Sales"].ToString();
                TempData["Services"] = social["Services"].ToString();
                TempData["Purchasing"] = social["Purchasing"].ToString();
                TempData["Dispatches"] = social["Dispatches"].ToString();
                TempData["Sales1"] = social["Sales1"].ToString();
            }

            var obj = BindReachUs();

            ViewBag.Address = obj["Address"].ToString();
            ViewBag.MapAddress = obj["MapAddress"].ToString();
            ViewBag.Function = obj["Function"].ToString();

            //ViewBag.BindReachUs = BindReachUs();

            ViewBag.InnerBanner = BindInnerBanner(9);

            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(ContactUs objContact)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (objContact.Id == 0)
                    {
                        if (SaveContact(objContact))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ContactUs");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("ContactUs");
                        }
                    }
                }
                return View("ContactUs", objContact);
            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return RedirectToAction("ContactUs");
            }
        }

        public bool SaveContact(ContactUs objContact)
        {
            bool response;

            using (SqlCommand cmd = new SqlCommand("Proc_ContactUs"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", objContact.FullName);
                cmd.Parameters.AddWithValue("@Email", objContact.Email);
                cmd.Parameters.AddWithValue("@ContactNo", objContact.ContactNo);
                cmd.Parameters.AddWithValue("@Subject", objContact.Subject);
                cmd.Parameters.AddWithValue("@Comment", objContact.Comment);

                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact us enquiery </font></strong></td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Subject: </strong></td> <td> " + objContact.Subject + " </td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Full Name : </strong></td><td> " + objContact.FullName + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + objContact.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Mobile No : </strong></td> <td> " + objContact.ContactNo + " </td> </tr>");
                    sb.AppendFormat("<tr> <td><strong>Comments : </strong></td> <td> " + objContact.Comment + " </td></tr>");

                    sb.Append("</table>");


                    if (objContact.Subject == "Sales Inquiry")
                    {
                        string ToEmailid = ConfigurationManager.AppSettings["ContactUsSales"].ToString();
                        String[] emailid = new String[] { ToEmailid };
                        util.SendEmail(sb.ToString(), emailid, "Contact US", "", null);
                    }
                    else if (objContact.Subject == "Supplier")
                    {
                        string ToEmailid = ConfigurationManager.AppSettings["ContactUsSupplier"].ToString();
                        String[] emailid = new String[] { ToEmailid };
                        util.SendEmail(sb.ToString(), emailid, "Contact US", "", null);
                    }
                    else if (objContact.Subject == "Dispatch related")
                    {
                        string ToEmailid = ConfigurationManager.AppSettings["ContactUsDispatch"].ToString();
                        String[] emailid = new String[] { ToEmailid };
                        util.SendEmail(sb.ToString(), emailid, "Contact US", "", null);
                    }
                    else
                    {
                        string ToEmailid = ConfigurationManager.AppSettings["ContactUsService"].ToString();
                        String[] emailid = new String[] { ToEmailid };
                        util.SendEmail(sb.ToString(), emailid, "Contact US", "", null);
                    }




                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        public Dictionary<string, string> BindReachUs()
        {
            Dictionary<string, string> response = new Dictionary<string, string>();
            StringBuilder strBuild = new StringBuilder();

            StringBuilder strMapAddress = new StringBuilder();
            StringBuilder strFuction = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ReachUs 'bindReachUs'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {

                    strBuild.Append("<div class='col-sm-6 col-lg-3 addressBox'>");
                    strBuild.Append("<h3><img src='/Content/uploads/Images/" + dr["Image"].ToString() + "' style='height:25px;margin-right:5px;'>" + dr["Heading"].ToString() + "</h3>");
                    strBuild.Append("<p>");
                    strBuild.Append(dr["Title"].ToString() + "<br/>");
                    strBuild.Append(dr["description"].ToString());
                    strBuild.Append("</p>");
                    if (dr["Contact"].ToString() != "")
                    {
                        strBuild.Append("<p class='contNo'><i class='fas fa-phone' title='Telephone'></i>" + dr["Contact"].ToString() + "</p>");
                    }
                    if (dr["Fax"].ToString() != "")
                    {
                        strBuild.Append("<p class='contNo'><i class='fas fa-fax' title='Fax'></i>" + dr["Fax"].ToString() + "</p>");
                    }
                    strBuild.Append("<a class='locateBtn orangeBtn' data-title='" + dr["Id"].ToString() + "'>Locate</a>");
                    strBuild.Append("</div>");


                    //Map Address
                    strMapAddress.Append("{ lat: " + dr["Latitude"].ToString() + ", lon: " + dr["Longitude"].ToString() + ", icon: '/Content/uploads/Images/" + dr["Image"].ToString() + "', title: '" + dr["Title"].ToString() + "', html: '<h1>" + dr["Title"].ToString() + "</h1><h2>(" + dr["Heading"].ToString() + ")</h2>" + dr["description"].ToString() + ", animation: google.maps.Animation.DROP, zoom: 14 },");

                    //function

                    strFuction.Append("$('.locateBtn[data-title=" + dr["Id"].ToString() + "]').click(function() {$('.ullist').find('#ullist_a_" + dr["Id"].ToString() + "').trigger('click')}); ");

                }
                //response = strBuild.ToString();
                response.Add("Address", strBuild.ToString());
                response.Add("MapAddress", strMapAddress.ToString());
                response.Add("Function", strFuction.ToString());

            }
            return response;
        }


        public JsonResult SaveEmployeeRecord()
        {
            List<ReachUs> list = new List<Models.ReachUs>();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_ReachUs 'bindReachUs'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    ReachUs objReachUs = new ReachUs();
                    objReachUs.Title = dr["Title"].ToString();
                    objReachUs.Heading = dr["Heading"].ToString();
                    objReachUs.Description = dr["Description"].ToString();
                    objReachUs.Latitude = dr["Latitude"].ToString();
                    objReachUs.Longitude = dr["Longitude"].ToString();
                    objReachUs.Image = dr["Image"].ToString();
                    objReachUs.Contact = dr["Contact"].ToString();
                    objReachUs.Fax = dr["Fax"].ToString();

                    list.Add(objReachUs);
                }

            }


            return Json(list, JsonRequestBehavior.AllowGet);
        }


        #endregion

        #region Social Media


        public Dictionary<string, string> SocialMedia(int Id)
        {
            DataTable dt = new DataTable();
            Dictionary<string, string> obj = new Dictionary<string, string>();
            dt = util.Display("Exec Proc_SocialMedia 'getbyId','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                obj.Add("opentime", dt.Rows[0]["opentime"].ToString());
                obj.Add("CallUs", dt.Rows[0]["CallUs"].ToString());
                obj.Add("MailUs", dt.Rows[0]["MailUs"].ToString());
                obj.Add("Sales", dt.Rows[0]["Sales"].ToString());
                obj.Add("Services", dt.Rows[0]["Services"].ToString());
                obj.Add("Purchasing", dt.Rows[0]["Purchasing"].ToString());
                obj.Add("Dispatches", dt.Rows[0]["Dispatches"].ToString());
                obj.Add("Sales1", dt.Rows[0]["Sales1"].ToString());

            }

            return obj;
        }

        public void ReachUs()
        {
            var social = SocialMedia(1);
            if (social.Count > 0)
            {
                TempData["opentime"] = social["opentime"].ToString();
                TempData["CallUs"] = social["CallUs"].ToString();
                TempData["MailUs"] = social["MailUs"].ToString();
            }
        }

        #endregion

        #region Bind Inner Banner
        public string BindInnerBanner(int Id)
        {
            string response = "";
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_InnerBanner 'bindInnerBanner','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                response = dt.Rows[0]["Image"].ToString();
            }
            return response;
        }

        #endregion

        #region CMS Page Content
        public string CMS(int Id)
        {
            string response = "";
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_CMS 'bindCMS','" + Id + "'");
            if (dt.Rows.Count > 0)
            {
                response = dt.Rows[0]["Description"].ToString();
            }
            return response;
        }

        #endregion

        #region HeaderMenu

        public ActionResult HeaderMenu()
        {
            if (TempData["Menu"] != null)
            {
                ViewBag.ActiveMenu = TempData["Menu"].ToString();
            }
            else
            {
                ViewBag.ActiveMenu = "";
            }
            var obj = GetHederMenu();
            ViewBag.Solutions = obj["Solutions"].ToString();
            ViewBag.Technology = obj["Technology"].ToString();
            ViewBag.Industries = obj["Industries"].ToString();
            ViewBag.CollaborativeRobots = obj["CollaborativeRobots"].ToString();

            var objser = GetHederMenuForServices();
            ViewBag.ServicesMenu = objser["ServicesMenu"].ToString();
            return View();
        }


        public Dictionary<string, string> GetHederMenu()
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();

            StringBuilder strTitle = new StringBuilder();
            StringBuilder strDesc = new StringBuilder();
            StringBuilder strbuild1 = new StringBuilder();

            StringBuilder strbuild2 = new StringBuilder();
            StringBuilder strbuild3 = new StringBuilder();

            StringBuilder strbuild4 = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_SolAndTechSubTable 'bindMenu'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        strbuild1.Append("<li><a href=/Solutions/" + dr["Slug"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>");
                    }
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {

                        strbuild2.Append("<li><a href=/Technology/" + dr["Slug"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>");
                    }
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {

                        strbuild3.Append("<li><a href=/Industries/" + dr["Slug"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>");
                    }
                }

                    if (ds.Tables[3].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[3].Rows)
                    {

                        strbuild4.Append("<li><a href=/CollaborativeRobots/" + dr["Slug"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>");
                    }
                }
            }

            objDis.Add("Solutions", strbuild1.ToString());
            objDis.Add("Technology", strbuild2.ToString());
            objDis.Add("Industries", strbuild3.ToString());
            objDis.Add("CollaborativeRobots", strbuild4.ToString());


            return objDis;
        }

        public Dictionary<string, string> GetHederMenuForServices()
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();

            StringBuilder strTitle = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_Services 'bindservicesmenu'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        string sluglink = dr["Title"].ToString();

                        strTitle.Append("<li><a href='/Services#" + Generateslgforservice(sluglink) + "'>" + dr["Title"].ToString() + "</a></li>");
                    }
                }

            }

            objDis.Add("ServicesMenu", strTitle.ToString());


            return objDis;
        }

        #endregion

        #region Solutions
        public ActionResult Solutions(string slug)
        {
            TempData["Menu"] = "Solutions";

            string title = "Solutions/"+slug;
            ViewBag.Title = UpdateMetaDetails(title);

            var obj = GetSolutiondetails("Solutions", slug);
            ViewBag.Heading = obj["Heading"].ToString();
            ViewBag.Description = obj["Description"].ToString();
            ViewBag.Gallary = obj["Gallary"].ToString();
            ViewBag.NewDiv = obj["NewDiv"].ToString();
            ViewBag.NewDiv2 = obj["NewDiv2"].ToString();

            ViewBag.InnerBanner = obj["InnerBanner"].ToString();
            ViewBag.PageName = obj["PageName"].ToString();

            ViewBag.HomeServices = HomeServices();
            ViewBag.HomeIndustries = HomeIndustries();


            return View();
        }

        public Dictionary<string, string> GetSolutiondetails(string pagevalue, string slugvalue)
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();

            StringBuilder strbuildHeading = new StringBuilder();
            StringBuilder strbuildDes = new StringBuilder();
            StringBuilder strbuildGal = new StringBuilder();
            StringBuilder strnewdiv = new StringBuilder();
            StringBuilder strnewdiv2 = new StringBuilder();

            StringBuilder strInnerBanner = new StringBuilder();
            StringBuilder strPageName = new StringBuilder();

            StringBuilder strsolnList = new StringBuilder();



            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_SolAndTechSubTable 'bindMenuPageDetail',0,0,'" + pagevalue + "','" + slugvalue + "'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        strInnerBanner.Append(dr["Image"].ToString());
                        strPageName.Append(dr["Title"].ToString());

                        strbuildHeading.Append("<div class='maxWidth'>");
                        strbuildHeading.Append(dr["Heading"].ToString());
                        strbuildHeading.Append("</div>");
                    }
                }
                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr1 in ds.Tables[1].Rows)
                    {
                        strbuildDes.Append(dr1["Description"].ToString());
                    }
                }
                if (ds.Tables[2].Rows.Count > 0)
                {
                    if (ds.Tables[2].Rows[0]["Image"].ToString() != "" || ds.Tables[2].Rows[0]["Image"].ToString() != string.Empty || ds.Tables[2].Rows[0]["Url"].ToString() != "" || ds.Tables[2].Rows[0]["Url"].ToString() != string.Empty)
                    {
                        strbuildGal.Append("<div class='productCarousel mb-5'>");
                        strbuildGal.Append("<div class='productImgSlider'>");

                        foreach (DataRow dr1 in ds.Tables[2].Rows)
                        {
                            strbuildGal.Append("<div>");
                            strbuildGal.Append("<div class='pc_img_slide'>");
                            if (dr1["Type"].ToString() == "Image")
                            {

                                strbuildGal.Append("<a href='/Content/uploads/Images/" + dr1["Image"].ToString() + "' data-fancybox='img_gallery' data-caption='" + dr1["Title"].ToString() + "' class='fancyimg' alt='" + dr1["Img_Alt_Tag"].ToString() + "'><img src='/Content/uploads/Images/" + dr1["Image"].ToString() + "' alt='" + dr1["Img_Alt_Tag"].ToString() + "'></a>");

                            }
                            else
                            {
                                strbuildGal.Append("<a href='" + dr1["Url"].ToString() + "' data-fancybox='img_gallery' data-fancybox='img_gallery' data-caption='" + dr1["Title"].ToString() + "' class='fancyvideo'>");
                                strbuildGal.Append("<img src = '/Content/uploads/Images/" + dr1["ThumbImg"].ToString() + "'  alt='" + dr1["Img_Alt_Tag"].ToString() + "'> ");
                                strbuildGal.Append("</a>");

                            }
                            strbuildGal.Append("</div>");
                            strbuildGal.Append("</div>");


                        }
                        strbuildGal.Append("</div>");
                        strbuildGal.Append("</div>");
                    }

                }

                if (pagevalue == "technology" || pagevalue == "Industries")
                {
                    if (ds.Tables[3].Rows.Count > 0)
                    {


                        strsolnList.Append("<div class='container-fluid solutionLinks fade_anim clearfix mb-5'>");
                        strsolnList.Append("<div class='row sl_title'><i class='fas fa-cogs whiteColor' style='font-size:20px;vertical-align:middle;margin-right:10px;line-height:1.5;opacity:.7'></i> <strong>SOLUTIONS</strong></div>");
                        strsolnList.Append("<ul class='clearfix'>");
                        foreach (DataRow dr1 in ds.Tables[3].Rows)
                        {


                            strsolnList.Append("<li><a href='/Solutions/" + dr1["Slug"].ToString() + "'>" + dr1["PageName"].ToString() + "</a></li>");

                        }
                        strsolnList.Append("</ul>");
                        strsolnList.Append("</div>");
                    }


                }


                if (pagevalue != "technology")
                {
                    strnewdiv2.Append("<div class='row'>");
                    strnewdiv2.Append("<div class='container-fluid casestudiesSection'>");
                    strnewdiv2.Append("<div class='maxWidth'>");
                    strnewdiv2.Append("<div class='title_table'>");
                    strnewdiv2.Append("<div class='title_left'>");
                    strnewdiv2.Append("<div class='title titillium' style='color:#fff;'>");
                    strnewdiv2.Append("<h4>Our Projects</h4>");
                    strnewdiv2.Append("<h2><strong>Case Studies</strong></h2>");
                    strnewdiv2.Append("</div>");
                    strnewdiv2.Append("</div>");
                    strnewdiv2.Append("<div class='title_right' style='color:#fff;'>");
                    strnewdiv2.Append("<p>The experience gained over the years and the consistent drive for technical innovations and improvements have helped to attain the apex in offering innovative solutions to diverse global markets.</p>");
                    strnewdiv2.Append("<a href='/CaseStudyListing' class='whiteBtn'>View All Projects</a>");
                    strnewdiv2.Append("</div>");
                    strnewdiv2.Append("</div>");
                    strnewdiv2.Append("</div>");
                    strnewdiv2.Append("</div>");
                    strnewdiv2.Append("</div>");
                }


            }

            objDis.Add("Heading", strbuildHeading.ToString());
            objDis.Add("Description", strbuildDes.ToString());
            objDis.Add("Gallary", strbuildGal.ToString());
            objDis.Add("NewDiv", strnewdiv.ToString());
            objDis.Add("NewDiv2", strnewdiv2.ToString());
            objDis.Add("SolutionList", strsolnList.ToString());

            objDis.Add("InnerBanner", strInnerBanner.ToString());
            objDis.Add("PageName", strPageName.ToString());



            return objDis;
        }


        [HttpPost]
        public ActionResult Solutions(GetInTouchForm objGetInTouchForm)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (objGetInTouchForm.Id == 0)
                    {
                        if (SaveSolutions(objGetInTouchForm))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Solutions");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Solutions");
                        }
                    }
                }
                return View("Solutions", objGetInTouchForm);

            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return View("Solutions", objGetInTouchForm);
            }
        }

        public bool SaveSolutions(GetInTouchForm objGetInTouchForm)
        {
            bool response;

            using (SqlCommand cmd = new SqlCommand("Proc_GetInTouchForm"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", objGetInTouchForm.FullName);
                cmd.Parameters.AddWithValue("@Email", objGetInTouchForm.Email);
                cmd.Parameters.AddWithValue("@ContactNo", objGetInTouchForm.ContactNo);
                cmd.Parameters.AddWithValue("@Comment", objGetInTouchForm.Comment);

                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact us enquiery </font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Full Name : </strong></td><td> " + objGetInTouchForm.FullName + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + objGetInTouchForm.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Mobile No : </strong></td> <td> " + objGetInTouchForm.ContactNo + " </td> </tr>");
                    sb.AppendFormat("<tr> <td><strong>Message : </strong></td> <td> " + objGetInTouchForm.Comment + " </td></tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["GetInTouchForm"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    util.SendEmail(sb.ToString(), emailid, "Get In Touch", "", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }


        #endregion


        #region Technology
        public ActionResult Technology(string slug)
        {
            TempData["Menu"] = "Technology";

            string title = "Technology/" + slug;
            ViewBag.Title = UpdateMetaDetails(title);

            var obj = GetSolutiondetails("technology", slug);
            ViewBag.Heading = obj["Heading"].ToString();
            ViewBag.Description = obj["Description"].ToString();
            ViewBag.Gallary = obj["Gallary"].ToString();
            ViewBag.NewDiv = obj["NewDiv"].ToString();
            ViewBag.NewDiv2 = obj["NewDiv2"].ToString();

            ViewBag.SolutionList = obj["SolutionList"].ToString();

            ViewBag.InnerBanner = obj["InnerBanner"].ToString();
            ViewBag.PageName = obj["PageName"].ToString();

            ViewBag.HomeServices = HomeServices();
            ViewBag.HomeIndustries = HomeIndustries();

            return View();
        }

        #endregion

        #region Industries
        public ActionResult Industries(string slug)
        {
            TempData["Menu"] = "Industries";

            string title = "Industries/" + slug;
            ViewBag.Title = UpdateMetaDetails(title);

            var obj = GetSolutiondetails("Industries", slug);
            ViewBag.Heading = obj["Heading"].ToString();
            ViewBag.Description = obj["Description"].ToString();
            ViewBag.Gallary = obj["Gallary"].ToString();
            ViewBag.NewDiv = obj["NewDiv"].ToString();
            ViewBag.NewDiv2 = obj["NewDiv2"].ToString();

            ViewBag.InnerBanner = obj["InnerBanner"].ToString();
            ViewBag.PageName = obj["PageName"].ToString();

            ViewBag.SolutionList = obj["SolutionList"].ToString();

            ViewBag.HomeServices = HomeServices();
            ViewBag.HomeIndustries = HomeIndustries();
            ViewBag.BindPartnerLogos = BindPartnerLogos();

            return View();
        }
 
        public string BindPartnerLogos()
        {
            string response = "";
            StringBuilder strBuild = new StringBuilder();
            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_HomeSolutions 'bindHomeSolutions'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {


                    strBuild.Append("<div><div class='plBox'><img src = '/Content/uploads/HomeSolutions/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "' ></div></div>");

                }
                response = strBuild.ToString();

            }
            return response;
        }


        [HttpPost]
        public ActionResult Industries(GetInTouchForm objGetInTouchForm)
        {
            try
            {
                if (ModelState.IsValid)
                {

                    if (objGetInTouchForm.Id == 0)
                    {
                        if (SaveIndustries(objGetInTouchForm))
                        {
                            ModelState.Clear();

                            ViewBag.Message = "success";
                            ViewBag.Message1 = "Record added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Industries");
                        }
                        else
                        {
                            ViewBag.Message = "warning";
                            ViewBag.Message1 = "Record could not added successfully.";

                            TempData["Message"] = ViewBag.Message;
                            TempData["Message1"] = ViewBag.Message1;

                            return RedirectToAction("Industries");
                        }
                    }
                }
                return View("products", objGetInTouchForm);

            }

            catch (Exception ex)
            {
                LogError log = new LogError();
                log.HandleException(ex);

                ViewBag.Message = "error";
                ViewBag.Message1 = "Try again after some time.";

                TempData["Message"] = ViewBag.Message;
                TempData["Message1"] = ViewBag.Message1;
                return View("products", objGetInTouchForm);
            }
        }

        public bool SaveIndustries(GetInTouchForm objGetInTouchForm)
        {
            bool response;

            using (SqlCommand cmd = new SqlCommand("Proc_GetInTouchForm"))
            {
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@para", "add");
                cmd.Parameters.AddWithValue("@Name", objGetInTouchForm.FullName);
                cmd.Parameters.AddWithValue("@Email", objGetInTouchForm.Email);
                cmd.Parameters.AddWithValue("@ContactNo", objGetInTouchForm.ContactNo);
                cmd.Parameters.AddWithValue("@Comment", objGetInTouchForm.Comment);

                if (util.Execute(cmd))
                {
                    StringBuilder sb = new StringBuilder();

                    sb.Append("<table border=\"1\" cellspacing=\"0\" cellpadding=\"10\" width=\"100%\" style=\"border-color: #ddd; font-family: Arial, Helvetica, sans-serif; font-size: 12px; border-collapse: collapse;\">");
                    sb.AppendFormat("<tr> <td colspan=\"2\" align=\"center\" bgcolor=\"#2f72b4\"><strong><font color=\"#FFFFFF\">Contact us enquiery </font></strong></td></tr>");
                    sb.AppendFormat("<tr><td width=\"200px\"><strong>Full Name : </strong></td><td> " + objGetInTouchForm.FullName + " </td></tr>");
                    sb.AppendFormat("<tr> <td><strong>Email : </strong></td> <td> " + objGetInTouchForm.Email + " </td></tr>");
                    sb.AppendFormat("<tr bgcolor=\"#fafafa\"> <td><strong>Mobile No : </strong></td> <td> " + objGetInTouchForm.ContactNo + " </td> </tr>");
                    sb.AppendFormat("<tr> <td><strong>Message : </strong></td> <td> " + objGetInTouchForm.Comment + " </td></tr>");

                    sb.Append("</table>");

                    string ToEmailid = ConfigurationManager.AppSettings["GetInTouchForm"].ToString();
                    String[] emailid = new String[] { ToEmailid };

                    util.SendEmail(sb.ToString(), emailid, "Get In Touch", "", null);

                    response = true;
                }
                else
                {
                    response = false;
                }
            }
            return response;
        }



        #endregion


        #region CollaborativeRobots

        public ActionResult CollaborativeRobots(string slug)
        {
            TempData["Menu"] = "Collaborative Robots";

            string title = "Collaborative Robots/" + slug;
            //ViewBag.Title = UpdateMetaDetails(title);

            var obj = GetSolutiondetails("Collaborative Robots", slug);
            ViewBag.Heading = obj["Heading"].ToString();
            ViewBag.Description = obj["Description"].ToString();
            ViewBag.Gallary = obj["Gallary"].ToString();
            ViewBag.NewDiv = obj["NewDiv"].ToString();
            ViewBag.NewDiv2 = obj["NewDiv2"].ToString();

            ViewBag.InnerBanner = obj["InnerBanner"].ToString();
            ViewBag.PageName = obj["PageName"].ToString();

            ViewBag.SolutionList = obj["SolutionList"].ToString();

            ViewBag.HomeServices = HomeServices();
            ViewBag.HomeIndustries = HomeIndustries();
            ViewBag.BindPartnerLogos = BindPartnerLogos();

            return View();
        }

    

        #endregion

        #region Footer List
        public ActionResult FooterList()
        {
            var obj = GetFooterList();

            ViewBag.SolutionsFooter = obj["Solutions"].ToString();
            ViewBag.TechnologyFooter = obj["Technology"].ToString();
            ViewBag.IndustriesFooter = obj["Industries"].ToString();
            return View();
        }

        public Dictionary<string, string> GetFooterList()
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();

            StringBuilder strTitle = new StringBuilder();
            StringBuilder strDesc = new StringBuilder();
            StringBuilder strbuild1 = new StringBuilder();

            StringBuilder strbuild2 = new StringBuilder();
            StringBuilder strbuild3 = new StringBuilder();

            DataSet ds = new DataSet();
            ds = util.Display1("Exec Proc_SolAndTechSubTable 'bindMenu'");
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        strbuild1.Append("<li class='col-6'><a href=/Solutions/" + dr["Slug"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>");
                    }
                }

                if (ds.Tables[1].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {

                        strbuild2.Append("<li class='col-6'><a href=/Technology/" + dr["Slug"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>");
                    }
                }

                if (ds.Tables[2].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[2].Rows)
                    {

                        strbuild3.Append("<li class='col-6'><a href=/Industries/" + dr["Slug"].ToString() + ">" + dr["PageName"].ToString() + "</a></li>");
                    }
                }
            }

            objDis.Add("Solutions", strbuild1.ToString());
            objDis.Add("Technology", strbuild2.ToString());
            objDis.Add("Industries", strbuild3.ToString());
            return objDis;
        }
        #endregion


        #region New Page
        public ActionResult NewPage(string slug)
        {
            var obj = GetNewPageDetail(slug);
            string title = slug;
            ViewBag.Title = UpdateMetaDetails(title);

            ViewBag.InnerBanner = obj["BannerImage"].ToString();
            ViewBag.PageName = obj["PageName"].ToString();
            ViewBag.Description = obj["Description"].ToString();
            return View();
        }

        public Dictionary<string, string> GetNewPageDetail(string SlugUrl)
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();
            StringBuilder strPageName = new StringBuilder();
            StringBuilder strBannerImage = new StringBuilder();
            StringBuilder StrDesc = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_AddNewPage 'bindNewPage','','','','','" + SlugUrl + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strBannerImage.Append(dr["BannerImage"].ToString());

                    strPageName.Append(dr["Pagename"].ToString());

                    StrDesc.Append(dr["Description"].ToString());
                }

            }

            objDis.Add("BannerImage", strBannerImage.ToString());
            objDis.Add("PageName", strPageName.ToString());
            objDis.Add("Description", StrDesc.ToString());

            return objDis;

        }

        #endregion

        public ActionResult GreenSolutions()
        {
            ViewBag.InnerBanner = BindInnerBanner(11);
            return View();
        }
        public ActionResult Search_Result(string searchkey)
        {

            string searchvalue = searchkey;
            ViewBag.SearchTitle = searchkey;
            var Search = BindSearch_Result(searchvalue);
            ViewBag.Heading = Search["Heading"].ToString();
            ViewBag.InnerBanner = BindInnerBanner(10);

            return View();
        }

        public Dictionary<string, string> BindSearch_Result(string value1)
        {
            Dictionary<string, string> objDictionary = new Dictionary<string, string>();
            StringBuilder strHeading = new StringBuilder();
            StringBuilder strDesc = new StringBuilder();


            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_SearchResult 'GET_SEARCH','" + value1 + "'");
            if (dt.Rows.Count > 0)
            {
                foreach (DataRow dr in dt.Rows)
                {
                    strHeading.Append("<a href=/" + dr["PageName"].ToString() + " class='container-fluid searchresultBox'>");
                    strHeading.Append("<h2 class='titillium'>" + dr["Heading"].ToString() + "</h2>");

                    string your_String = dr["Description"].ToString();
                    //string resultString = Regex.Replace(your_String, "[(<.+?)\s+style\s*=\s*(["']).*?\2(.*?>)]", "");

                    var pattern = @"</?\w+((\s+\w+(\s*=\s*(?:"".*?""|'.*?'|[^'"">\s]+))?)+\s*|\s*)/?>";
                    string Output = Regex.Replace(your_String, pattern, string.Empty).Replace("\r\n\t", "");

                    strHeading.Append("<p>" + Output + "</p>");
                    strHeading.Append("</a>");

                }
            }
            objDictionary.Add("Heading", strHeading.ToString());
            return objDictionary;
        }


        public static string UpdateMetaDetails(string pageUrl)
        {
            Utility util = new Utility();
            //--- StringBuilder object to store MetaTags information.
            StringBuilder sbMetaTags = new StringBuilder();

            //--Step1 Get data from database.
            DataTable dt = new DataTable();
            dt = util.Display("exec Proc_MetaTag 'bindMetatTag',0,'" + pageUrl.ToString() + "'");
            if (dt.Rows.Count > 0)
            {

                //---- Step2 In this step we will add <title> tag to our StringBuilder Object.
                sbMetaTags.Append("<title>" + dt.Rows[0]["Title"].ToString() + "</title>");

                //---- Step3 In this step we will add "Meta Description" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='description' content='" + dt.Rows[0]["MetaDescription"].ToString() + "' >");

                //---- Step4 In this step we will add "Meta Keywords" to our StringBuilder Object.
                sbMetaTags.Append("<meta name='keywords' content ='" + dt.Rows[0]["MetaKeywords"].ToString() + "'>");

                sbMetaTags.Append("<link rel='canonical' href='" + dt.Rows[0]["CanonicalTag"].ToString() + "'/>");
            }
            return sbMetaTags.ToString();
        }


        public ActionResult CollaborativeRobotsBlogs()
        {
            var obj = CollaborativeRobotsBlogsBind();

            ViewBag.Blogs = obj["Blogs"].ToString();
            ViewBag.InnerBanner = BindInnerBanner(7);
            return View();
        }


        public Dictionary<string, string> CollaborativeRobotsBlogsBind()
        {
            Dictionary<string, string> objDis = new Dictionary<string, string>();

            StringBuilder strDescArticles = new StringBuilder();

            DataTable dt = new DataTable();
            dt = util.Display("Exec Proc_NewsRoom 'bindCollaborativeRobotsBlogs'");
         

                if (dt.Rows.Count > 0)
                {
                    foreach (DataRow dr in dt.Rows)
                    {
                      
                  
                       
                            string dateyear = dr["Date"].ToString();
                            string[] date = dateyear.Split(' ');

                            strDescArticles.Append("<div class='articleBox item-news'>");
                            strDescArticles.Append("<div class='articleHolder rounded fade_anim'>");
                            strDescArticles.Append("<div class='tag'><strong > " + date[0] + " </strong > " + date[1] + " " + date[2] + " </div>");
                            strDescArticles.Append("<div class='imgbox rounded-top'><img src='/Content/uploads/NewsRoom/" + dr["Image"].ToString() + "' alt='" + dr["Img_Alt_Tag"].ToString() + "'></div>");
                            strDescArticles.Append("<h2 class='titillium'>" + dr["Title"].ToString() + "</h2>");
                            strDescArticles.Append(dr["Description"].ToString());
                            strDescArticles.Append("<a href='/NewsRoomDetail/" + dr["Slug"].ToString() + "'>Read More</a>");
                            strDescArticles.Append("</div>");
                            strDescArticles.Append("</div>");
                        
                    }

                }


            objDis.Add("Blogs", strDescArticles.ToString());

            return objDis;
        }


    }
}