﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Patvin
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
            name: "SuperAdmin",
            url: "SuperAdmin",
            defaults: new { controller = "Login", action = "Index", Usertype = "SuperAdmin" }

            );



            routes.MapRoute(
             name: "Index",
             url: "",
             defaults: new { controller = "Website", action = "Index" }

            );

            //routes.MapRoute(
            // name: "Solution",
            // url: "Pages/{pageName}/{slug}",
            // defaults: new { controller = "Website", action = "Solutions", id = UrlParameter.Optional }

            //);

            routes.MapRoute(
       name: "Solution",
       url: "Solutions/{slug}",
       defaults: new { controller = "Website", action = "Solutions", id = UrlParameter.Optional }

      );
            routes.MapRoute(
       name: "Technology",
       url: "Technology/{slug}",
       defaults: new { controller = "Website", action = "Technology", id = UrlParameter.Optional }

      );
            routes.MapRoute(
       name: "Industries",
       url: "Industries/{slug}",
       defaults: new { controller = "Website", action = "Industries", id = UrlParameter.Optional }

      );

            routes.MapRoute(
    name: "Collaborative Robots",
    url: "CollaborativeRobots/{slug}",
    defaults: new { controller = "Website", action = "CollaborativeRobots", id = UrlParameter.Optional }

   );

            routes.MapRoute(
             name: "WhoWeAre",
             url: "WhoWeAre",
             defaults: new { controller = "Website", action = "WhoWeAre" }

            );


            routes.MapRoute(
    name: "Management",
    url: "LeadershipTeam",
    defaults: new { controller = "Website", action = "Management" }

   );

            routes.MapRoute(
           name: "OurValues",
           url: "OurValues",
           defaults: new { controller = "Website", action = "OurValues" }

          );

            routes.MapRoute(
         name: "HealthSafety",
         url: "HealthSafety",
         defaults: new { controller = "Website", action = "HealthSafety" }

        );

            routes.MapRoute(
      name: "products",
      url: "products",
      defaults: new { controller = "Website", action = "products" }

     );


            routes.MapRoute(
    name: "Search_Result",
    url: "Search_Result",
    defaults: new { controller = "Website", action = "Search_Result" }

);




            routes.MapRoute(
            name: "CaseStudyListing",
            url: "CaseStudyListing",
            defaults: new { controller = "Website", action = "CaseStudyListing" }

            );


            routes.MapRoute(
           name: "Testimonials",
           url: "Testimonials",
           defaults: new { controller = "Website", action = "Testimonials" }

           );


            routes.MapRoute(
           name: "Services",
           url: "Services",
           defaults: new { controller = "Website", action = "Services" }

           );

            routes.MapRoute(
            name: "Newsroom",
            url: "Newsroom",
            defaults: new { controller = "Website", action = "Newsroom" }

            );

            routes.MapRoute(
         name: "Blogs",
         url: "Blogs",
         defaults: new { controller = "Website", action = "CollaborativeRobotsBlogs" }

         );


            routes.MapRoute(
       name: "NewsRoomDetail",
       url: "NewsRoomDetail/{strNews}",
       defaults: new { controller = "Website", action = "NewsRoomDetail", id = UrlParameter.Optional }

      );

            routes.MapRoute(
            name: "Careers",
            url: "Careers",
            defaults: new { controller = "Website", action = "Careers" }

        );
            routes.MapRoute(
            name: "ContactUs",
            url: "ContactUs",
            defaults: new { controller = "Website", action = "ContactUs" }

        );

            routes.MapRoute(
                    name: "GreenSolutions",
                    url: "GreenSolutions",
                    defaults: new { controller = "Website", action = "GreenSolutions" }

                );



            routes.MapRoute(
       name: "NewPage",
       url: "{slug}",
       defaults: new { controller = "Website", action = "NewPage", id = UrlParameter.Optional }

      );

          


            routes.MapRoute(
               name: "Default",
               url: "{controller}/{action}/{id}",
               defaults: new { controller = " ", action = "Index", id = UrlParameter.Optional }
           );

        }
    }
}