USE [Patvin]
GO
/****** Object:  StoredProcedure [dbo].[Proc_AddNewPage]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_AddNewPage]
(
	@Para			nvarchar(100)=null,
	@Id				int=0,
	@PageName		nvarchar(500)=null,
	@BannerImage	nvarchar(3000)=null,
	@Description	nvarchar(3000)=null,
	@Slug			nvarchar(500)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into AddNewPage
		(
		
			PageName,
			BannerImage,
			Description,
			IsActive,
			AddedOn,
			Slug
		)
		values
		(
			@PageName,
			@BannerImage,
			@Description,
			1,
			GETDATE(),
			@Slug
		)
	end

	else if @Para = 'delete'
	begin
		update AddNewPage set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'update'
	begin
		if @BannerImage<>''
			Begin
			update AddNewPage set
			BannerImage = @BannerImage
			where
			Id = @Id
			End

		update AddNewPage set
		PageName=@PageName,
		Description = @Description,
		UpdatedOn = getdate(),
		Slug=@Slug
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by Id) as Sr,
			Id,
			PageName,
			BannerImage,
			Description,
			 Slug
		from AddNewPage
		where IsActive = 1
	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			PageName,
			BannerImage,
			Description,
			Slug
		from AddNewPage
		where IsActive = 1
		and Id = @Id
	end

	else if @Para='bindNewPage'
	Begin
		select 
			Id,
			PageName,
			BannerImage,
			Description,
			Slug
		from AddNewPage
		where IsActive = 1
		and Slug = @Slug
	End

END









GO
/****** Object:  StoredProcedure [dbo].[Proc_ApplyForJob]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[Proc_ApplyForJob]
(
	@Para		nvarchar(100)=null,
	@Id			int=0,
	@Name		varchar(200)=null,
	@Email		varchar(50)=null,
	@Role		varchar(100)=null,
	@Location	varchar(100)=null,
	@ContactNo	varchar(50)=null,
	@Resume		varchar(100)=null	
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into ApplyForJob
		(
			Name,
			Email,
			Role,
			Location,
			ContactNo,
			Resume,
			IsActive,
			AddedOn
		)
		values
		(
			@Name,
			@Email,
			@Role,
			@Location,
			@ContactNo,
			@Resume,
			1,
			getdate()
		)
	end

	else if @Para = 'delete'
	begin
		update ApplyForJob set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select
			ROW_NUMBER() over(order by AddedOn desc) as Sr, 
			Id,
			Name,
			Email,
			Role,
			Location,
			ContactNo,
			Resume,
			CONVERT(varchar,AddedOn,105)as ApplyDate
		 from ApplyForJob
		where IsActive = 1
		order by AddedOn desc
	end


		Else if @para = 'GetForExportToExcel'
		Begin 
			select
			ROW_NUMBER() over(order by AddedOn desc) as Sr, 
			Name,
			Email,
			Role,
			Location,
			ContactNo,
			Resume,
			CONVERT(varchar,AddedOn,105)as ApplyDate
		 from ApplyForJob
		  where IsActive=1 
		 order by Id desc
		End
END




GO
/****** Object:  StoredProcedure [dbo].[Proc_Banner]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

-- =============================================
CREATE PROCEDURE [dbo].[Proc_Banner]
(
		@Para			nvarchar(100)=null,
		@BannerId		int=0,
		@Image			varchar(100)=null,
		@BannerText1	varchar(500)=null,
		@BannerText2	varchar(500)=null,
		@BannerText3	varchar(500)=null,
		@DisplayOrder	int=0,
		@Url			nvarchar(100)=null
)
AS
BEGIN

	if @Para = 'add'
	begin
		insert into Banner
		(
			Image,
			BannerText1,
			BannerText2,
			BannerText3,
			DisplayOrder,
			IsActive,
			AddedOn,
			Url
		)
		values
		(
			@Image,
			@BannerText1,
			@BannerText2,
			@BannerText3,
			@DisplayOrder,
			1,
			getdate(),
			@Url
		)
	end

		else if @Para = 'delete'
	begin
		update Banner set
		IsActive = 0,
		DeletedOn = getdate()
		where
		BannerId = @BannerId
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update Banner set
			Image = @Image,
			UpdatedOn = getdate()
			where
			BannerId = @BannerId
		end

		update Banner set
			BannerText1 = @BannerText1,
			BannerText2 = @BannerText2,
			BannerText3 = @BannerText3,
			DisplayOrder = @DisplayOrder,
			UpdatedOn = getdate(),
			Url=@Url
			where
			BannerId = @BannerId
	end

		else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,
			BannerId,
			Image,
			BannerText1,
			BannerText2,
			BannerText3,
			DisplayOrder,
			Url
		from Banner
		where IsActive = 1
		order by DisplayOrder asc
	end

	else if @Para = 'getbyId'
	begin
		select * from Banner
		where IsActive = 1
		and BannerId = @BannerId
	end

	else if @Para = 'bindBanner'
	begin
		select 
			BannerId,
			Image,
			BannerText1,
			BannerText2,
			BannerText3,
			DisplayOrder,
			Url
		from Banner
		where IsActive = 1
		order by DisplayOrder asc
	end

END





GO
/****** Object:  StoredProcedure [dbo].[Proc_CareerOurPeople]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_CareerOurPeople]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Image				nvarchar(1000)=null,
	@Name				nvarchar(200)=null,
	@Description		nvarchar(3000)=null,
	@DisplayOrder		int=0,
	@Position			nvarchar(200)=null
AS
BEGIN
	if @Para = 'add'
	begin
		insert into CareerOurPeople
		(
		    Name,			
			Description,
			DisplayOrder,
			IsActive,
			AddedOn,
			Image,
			Position
			
		)
		values
		(
			@Name,
			@Description,
			@DisplayOrder,
			1,
			getdate(),
			@Image,
			@Position
		)
	end

	else if @Para = 'delete'
	begin
		update CareerOurPeople set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update CareerOurPeople set
			Image = @Image
			where
			Id = @Id
		end

		update CareerOurPeople set
		Name = @Name,	
		Description = @Description,
		DisplayOrder = @DisplayOrder,
		Position=@Position,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,			
			Id,
			Name,
			Position,	
			Description,
			Image,
			DisplayOrder
			from CareerOurPeople 			
		where IsActive = 1
		order by DisplayOrder asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Name,	
			Position,		
			Description,
			Image,
			DisplayOrder	
			from CareerOurPeople
		where IsActive = 1
		and Id = @Id
		order by DisplayOrder asc
	end

	else if @Para = 'bindCareerOurPeople'
	begin
        select * from CareerOurPeople where IsActive = 1 
	 order by DisplayOrder asc
	end
	End



GO
/****** Object:  StoredProcedure [dbo].[Proc_CareerOverViewImages]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Proc_CareerOverViewImages]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Image				nvarchar(1000)=null,
	@DisplayOrder		int=0
AS
BEGIN
	if @Para = 'add'
	begin
		insert into CareerOverViewImages
		(
		   Image,
			DisplayOrder,
			IsActive,
			AddedOn
			

			
		)
		values
		(
			@Image,
			@DisplayOrder,
			1,
			getdate()
			
			
		)
	end

	else if @Para = 'delete'
	begin
		update CareerOverViewImages set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update CareerOverViewImages set
			Image = @Image
			where
			Id = @Id
		end

		update CareerOverViewImages set	
		DisplayOrder = @DisplayOrder,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,			
			Id,
			Image,
			DisplayOrder
			from CareerOverViewImages 			
		where IsActive = 1
		order by DisplayOrder asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Image,
			DisplayOrder	
			from CareerOverViewImages
		where IsActive = 1
		and Id = @Id
		order by DisplayOrder asc
	end

	else if @Para = 'bindCareerOverViewImages'
	begin
     select * from CareerOverViewImages where IsActive = 1 
	end
	End



GO
/****** Object:  StoredProcedure [dbo].[Proc_CareerWeBelieve]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create PROCEDURE [dbo].[Proc_CareerWeBelieve]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Description		nvarchar(1000)=null,
	@DisplayOrder		int=0
AS
BEGIN
	if @Para = 'add'
	begin
		insert into CareerWeBelieve
		(
		   Description,
			IsActive,
			AddedOn
			

			
		)
		values
		(
			@Description,
			1,
			getdate()
			
			
		)
	end

	else if @Para = 'delete'
	begin
		update CareerWeBelieve set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		
		update CareerWeBelieve set	
		Description=@Description,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by Id asc) as Sr,			
			Id,
			Description
			from CareerWeBelieve 			
		where IsActive = 1
		order by Id asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Description	
			from CareerWeBelieve
		where IsActive = 1
		and Id = @Id
		order by Id asc
	end

	else if @Para = 'bindCareerWeBelieve'
	begin
     select * from CareerWeBelieve where IsActive = 1 
	end
	End



GO
/****** Object:  StoredProcedure [dbo].[Proc_CaseStudies]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_CaseStudies] 
		(
			@Para			nvarchar(50)='',
			@Id				int=0,
			@Image			nvarchar(3000)='',
			@Heading		nvarchar(200)='',
			@Category		nvarchar(200)='',
			@DisplayOrder	int=0

		)

AS
BEGIN
			If @Para='Add'
				Begin
					insert into CaseStudies
						(
							Image,
							Heading,
							Category,
							IsActive,
							AddedOn,
							DisplayOrder
						)
				values
						(
							@Image,
							@Heading,
							@Category,
							1,
							GETDATE(),
							@DisplayOrder
						)
				End

				Else If @Para='update'
			Begin
				if @Image <>''
				Begin
				Update CaseStudies
				set 
				Image=@Image
				where
				Id = @Id
				End

				Update CaseStudies
				set 				
				Heading=@Heading,
				Category=@Category,
				UpdatedOn=Getdate(),
				DisplayOrder=@DisplayOrder
				where
				Id = @Id
				End


			Else If @Para='delete'
			Begin
				Update CaseStudies
				set 
				IsActive=0
				where
				Id = @Id
			End


			Else If @Para='get'
			Begin
				select
			  ROW_NUMBER() over (order by DisplayOrder asc) as Sr,
				Id,
				Image,
				Heading,
				Category,
				IsActive,
				DisplayOrder
				from 
				CaseStudies 
				Where
				IsActive = 1

			End

			Else If @Para='getbyId'
			Begin
				select
				Id,
				Image,
				Heading,
				Category,
				IsActive,
				DisplayOrder
				from 
				CaseStudies 
				Where
				IsActive = 1
				and Id=@Id

			End

					Else If @Para='bindCaseStudyListing'
			Begin
				select
			  ROW_NUMBER() over (order by DisplayOrder asc) as Sr,
				Id,
				Image,
				Heading,
				Category,
				IsActive,
				DisplayOrder
				from 
				CaseStudies 
				Where
				IsActive = 1

			End

END





GO
/****** Object:  StoredProcedure [dbo].[Proc_Casestudyform]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Proc_Casestudyform]
(
	@Para				nvarchar(100)=null,
	@CaseId				int=0,
	@Title				nvarchar(100)=null,
	@Name				nvarchar(100)=null,
	@CompanyName		nvarchar(100)=null,
	@Email				nvarchar(50)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into Casestudyform
		(
			Title,
			Name,
			CompanyName,
			Email,
			IsActive,
			AddedOn
		)
		values
		(
			@Title,
		    @Name,
			@CompanyName,
			@Email,
			1,
			GETDATE()
		)
	end

	else if @Para = 'delete'
	begin
		update Casestudyform set
		IsActive = 0,
		DeletedOn = getdate()
		where
		CaseId	 = @CaseId	
	end

		else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by AddedOn desc)as Sr,
			CaseId,
			Name,
			Title,
			CompanyName,
			Email,
			IsActive,		
			CONVERT(varchar,AddedOn,106) as ApplyDate		
		 from Casestudyform
		 where IsActive = 1
		 order by AddedOn desc

	end
	End


GO
/****** Object:  StoredProcedure [dbo].[Proc_CMS]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_CMS]
(
	@Para			nvarchar(100)=null,
	@CmsId			int=0,
	@Heading		varchar(200)=null,
	@Description	nvarchar(max)=null
)
AS
BEGIN

	 if @Para = 'update'
	begin
		update CMS set
		Description = @Description,
		UpdatedOn = getdate()
		where
		CmsId = @CmsId
	end

	else if @Para = 'get'
	begin
		select * from CMS
		where IsActive = 1
		and CmsId = @CmsId
	end

	else if @Para = 'bindCMS'
	begin
		select * from CMS
		where IsActive = 1
		and CmsId = @CmsId
	end

	else if @Para = 'adminIndexCount'
	begin
		select COUNT(*) as Count,'CaseStudy' as Tbl from Casestudyform where IsActive = 1

		select COUNT(*) as Count,'ApplyForJob' as Tbl from ApplyForJob where IsActive = 1

		select COUNT(*) as Count,'ContactUs' as Tbl from ContactUs where IsActive = 1

		select COUNT(*) as Count,'GetInTouch' as Tbl from GetInTouchForm where IsActive = 1
	end
END







GO
/****** Object:  StoredProcedure [dbo].[Proc_ContactUs]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


Create PROCEDURE [dbo].[Proc_ContactUs]
(
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Name			varchar(100)=null,
	@ContactNo			varchar(50)=null,
	@Email				varchar(50)=null,
	@Subject			varchar(50)=null,
	@Comment			nvarchar(500)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into ContactUs
		(
			Name,
			ContactNo,
			Email,
			Subject,
			Comment,
			IsActive,
			AddedOn
		)
		values
		(
			@Name,
			@ContactNo,
			@Email,
			@Subject,
			@Comment,		
			1,
			getdate()
	
		)
	end

	else if @Para = 'delete'
	begin
		update ContactUs set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by AddedOn desc)as Sr,
			Id,
			Name,
			ContactNo,
			Email,
			Subject,
			Comment,		
			CONVERT(varchar,AddedOn,106) as ApplyDate		
		 from ContactUs
		 where IsActive = 1
		 order by AddedOn desc

	end

	Else if @para = 'GetForExportToExcel'
		Begin 
		select 
			Id,
			Name,
			ContactNo,
			Email,
			Subject,
			Comment,			
			CONVERT(varchar,AddedOn,106) as ApplyDate			
		   from ContactUs
		  where IsActive=1 
		 order by Id desc
		End
END





GO
/****** Object:  StoredProcedure [dbo].[Proc_ContactUsMap]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[Proc_ContactUsMap]
	( 
			@Para			nvarchar(100)=null,
			@Id				int=0,			
			@Description	nvarchar(1000)=null,
			@Latitude		numeric(12,6)= Null,
			@Longitude		numeric(12,6)= Null,
			@Icon			nvarchar(3000)=null,
			@Title			nvarchar(200)=null
			
	)
AS
BEGIN
if @Para = 'add'
	begin

	insert into ContactUsMap
	(
		Title,
		Description,
		Latitude,
		Longitude,
		Icon,
		AddedOn,
		IsActive
				
	)
	values
	(
		@Title,
		@Description,
		@Latitude,
		@Longitude,
		@Icon,
		GETDATE(),
		1
		
		

	)
	
	end


	if @Para='delete'
	Begin
	 update ContactUsMap
	 set
	 IsActive=0
	 where Id=@Id
	End

	if @Para = 'update'
	begin
	 if @Icon<>''
	 Begin
		update ContactUsMap set
		Icon = @Icon,
		UpdatedOn = getdate()
	 End

		update ContactUsMap set
		Title = @Title,
		Description = @Description,
		Latitude = @Latitude,
		Longitude=@Longitude,
		UpdatedOn = getdate()
		
		where
		Id = @Id
	end


	else if @Para='get'
	begin
		select 
		ROW_NUMBER() over (order by Id asc) as Sr,
		Id,
		Icon,
		Title,
		Description,
		Latitude,
		Longitude
		from ContactUsMap
		where IsActive=1
	end


	else if @Para = 'getbyId'
	begin
		select * from ContactUsMap
		where IsActive = 1
		and Id = @Id
	end

END



GO
/****** Object:  StoredProcedure [dbo].[Proc_CurrentOpening]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_CurrentOpening]
(
	@Para			nvarchar(100)=null,
	@Id				int=0,
	@Title			nvarchar(200)=null,
	@Location		nvarchar(100)=null,
	@Post			nvarchar(100)=null,
	@Qualification	nvarchar(200)=null,
	@Experience		nvarchar(200)=null,
	@Description	nvarchar(2000)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into CurrentOpening
		(
			Title,
			Location,
			Post,
			Qualification,
			Experience,
			Description,
			IsActive,
			AddedOn
		)
		values
		(
			@Title	,
			@Location,
			@Post,
			@Qualification,
			@Experience,
			@Description,
			1,
			getdate()
		)
	end

	else if @Para = 'delete'
	begin
		update CurrentOpening set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'update'
	begin
		update CurrentOpening set
		Title = @Title,
		Location = @Location,
		Post=@Post,
		Qualification=@Qualification,
		Experience=@Experience,
		Description=@Description,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over(order by Id asc) as Sr,
			Id,
			Title,
			Location,
			Post,
			Qualification,
			Experience,
			Description
		 from CurrentOpening
		where
		IsActive = 1
		order by Id asc
		
	end

	else if @Para = 'getbyId'
	begin
		select * from CurrentOpening
		where
		Id = @Id
	end

	else if @Para='bindCurrentOpening'
	Begin
	select 
			ROW_NUMBER() over(order by Id asc) as Sr,
			Id,
			Title,
			Location,
			Post,
			Qualification,
			Experience,
			Description
		 from CurrentOpening
		where
		IsActive = 1
		order by Id asc
end

END






GO
/****** Object:  StoredProcedure [dbo].[Proc_GetInTouchForm]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

Create PROCEDURE [dbo].[Proc_GetInTouchForm]
(
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Name			varchar(100)=null,
	@ContactNo			varchar(50)=null,
	@Email				varchar(50)=null,
	@Comment			nvarchar(500)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into GetInTouchForm
		(
			Name,
			ContactNo,
			Email,
			Comment,
			IsActive,
			AddedOn
		)
		values
		(
			@Name,
			@ContactNo,
			@Email,
			@Comment,		
			1,
			getdate()
	
		)
	end

	else if @Para = 'delete'
	begin
		update GetInTouchForm set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by AddedOn desc)as Sr,
			Id,
			Name,
			ContactNo,
			Email,
			Comment,		
			CONVERT(varchar,AddedOn,106) as ApplyDate		
		 from GetInTouchForm
		 where IsActive = 1
		 order by AddedOn desc

	end

	Else if @para = 'GetForExportToExcel'
		Begin 
		select 
			Id,
			Name,
			ContactNo,
			Email,
			Comment,			
			CONVERT(varchar,AddedOn,106) as ApplyDate			
		   from GetInTouchForm
		  where IsActive=1 
		 order by Id desc
		End
END






GO
/****** Object:  StoredProcedure [dbo].[Proc_HomeGreenWorld]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_HomeGreenWorld]
(
	@Para		nvarchar(100)=null,
	@Id			int=0,
	@Descrption	varchar(200)=null,
	@Image		varchar(100)=null

)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into HomeGreenWorld
		(
			Descrption,
			Image,
			IsActive,
			AddedOn
		)
		values
		(
			@Descrption,
			@Image,
			1,
			getdate()
		)
	end

	else if @Para = 'update'
	begin

		if @Image<>''
		begin
			update HomeGreenWorld set
			Image = @Image
			where
			Id = @Id
		end

		update HomeGreenWorld set
		@Descrption = @Descrption
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over(order by Id desc)as Sr,
			Id,
			Descrption,
			Image
		 from HomeGreenWorld
		 where IsActive = 1
		 order by Id desc
	end

	else if @Para = 'getbyId'
	begin
		select * from HomeGreenWorld
		where Id = @Id
	end

	else if @Para = 'bindHomeGreenWorld'
	begin
		select 
			ROW_NUMBER() over(order by Id desc)as Sr,
			Id,
			Descrption,
			Image
		 from HomeGreenWorld
		 where IsActive = 1
	end
	end




GO
/****** Object:  StoredProcedure [dbo].[Proc_HomeIndustries]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_HomeIndustries]
(
	@Para		nvarchar(100)=null,
	@Id			int=0,
	@Heading	varchar(200)=null,
	@Icon		varchar(100)=null,
	@Url		varchar(100)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into HomeIndustries
		(
			Heading,
			Icon,
			Url,
			IsActive,
			AddedOn
		)
		values
		(
			@Heading,
			@Icon,
			@Url,
			1,
			getdate()
		)
	end

	else if @Para = 'update'
	begin

		if @Icon<>''
		begin
			update HomeIndustries set
			Icon = @Icon
			where
			Id = @Id
		end

		update HomeIndustries set
		Heading = @Heading,
		Url = @Url
		where
		Id = @Id
	end

	else if @Para = 'delete'
	begin
	update HomeIndustries set
		IsActive=0
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over(order by Id Asc)as Sr,
			Id,
			Heading,
			Icon,
			Url
		 from HomeIndustries
		 where IsActive = 1
		 order by Id Asc
	end

	else if @Para = 'getbyId'
	begin
		select * from HomeIndustries
		where Id = @Id
	end

	else if @Para = 'bindHomeIndustries'
	begin
		select 
			ROW_NUMBER() over(order by Id desc)as Sr,
			Id,
			Heading,
			Icon,
			Url
		 from HomeIndustries
		 where IsActive = 1
		 order by Id asc
	end

END





GO
/****** Object:  StoredProcedure [dbo].[Proc_HomeProductSolutions]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_HomeProductSolutions]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Image				nvarchar(1000)=null,
	@Title				nvarchar(200)=null,
	@Description		nvarchar(3000)=null,
	@Url				nvarchar(200)=null
AS
BEGIN
	if @Para = 'add'
	begin
		insert into HomeProductSolutions
		(
		    Title,			
			Description,
			IsActive,
			AddedOn,
			Image,
			Url
			
		)
		values
		(
			@Title,
			@Description,
			1,
			getdate(),
			@Image,
			@Url
		)
	end

	else if @Para = 'delete'
	begin
		update HomeProductSolutions set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update HomeProductSolutions set
			Image = @Image
			where
			Id = @Id
		end

		update HomeProductSolutions set
		Title = @Title,	
		Description = @Description,
		Url=@Url,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by Id asc) as Sr,			
			Id,
			Title,
			Url,	
			Description,
			Image
			from HomeProductSolutions 			
		where IsActive = 1
		order by Id asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Title,	
			Url,		
			Description,
			Image	
			from HomeProductSolutions
		where IsActive = 1
		and Id = @Id
	end

	else if @Para = 'bindHomeProductSolutions'
	begin
	
     select 
			Id,
			Title,	
			Url,	
			CONVERT(VARCHAR(150),Description)+'...'[DESCRIPTION],	
			Image	
			from HomeProductSolutions 
			where
			IsActive = 1 
	end
	End

	



GO
/****** Object:  StoredProcedure [dbo].[Proc_HomeSolutions]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_HomeSolutions]
			(	
				@Para			nvarchar(50)=null,
				@Id				int=0,
				@Image			varchar(500)	=null,
				@DisplayOrder	int=0
			

			)
AS
BEGIN
			If @Para ='add'
			Begin
				insert into HomeSolutions
					(
						Image,
						DisplayOrder,
						IsActive,
						AddedOn
					)
				values
					(
						@Image,
						@DisplayOrder,
						1,
						GETDATE()
					)
			End

			Else If @Para='delete'
				Begin
					Update HomeSolutions
					set
					IsActive=0
					where Id=@Id
				End

			Else If @Para = 'update'
				Begin
					if @Image <> ''
					begin
						update HomeSolutions
						set
						Image=@Image
						where Id=@Id
					end

					update HomeSolutions
					set
					DisplayOrder=@DisplayOrder,
					UpdatedOn=GETDATE()
					where Id=@Id
				End

			Else If @Para = 'Get'
			Begin
			Select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,
			Id,
			Image,
			DisplayOrder
			from HomeSolutions
			where IsActive = 1
			order by DisplayOrder asc
			End

			Else If @Para='getbyId'
			Begin
					select * from HomeSolutions
					where IsActive = 1
					and Id = @Id
			End


		else if @Para = 'bindHomeSolutions'
		begin
		select * from HomeSolutions
		where IsActive = 1
		order by DisplayOrder
	end
END





GO
/****** Object:  StoredProcedure [dbo].[Proc_HomeStatistic]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_HomeStatistic]
		(
			@Para			nvarchar(100)=null,
			@Id				int=0,
			@Icon			nvarchar(200)=null,
			@Counting		nvarchar(100)=null,
			@Description	nvarchar(200)=null

		)
AS
BEGIN
		
		if @Para='Add'
			Begin
				insert into HomeStatistic
					(
						Icon,
						Counting,
						Description,					
						IsActive,
						AddedOn
					)
				Values
					(
						@Icon,
						@Counting,
						@Description,
						1,
						getdate()
					)

			End

		else if @Para='Update'
			Begin
			if @Icon<>''
			 Begin
					Update HomeStatistic
					set Icon=@Icon,
					UpdatedOn = getdate()
					where 
					Id=@Id and
					IsActive=1
			 End

					Update HomeStatistic
					set Counting=@Counting,
					Description=@Description,
					UpdatedOn=GETDATE()
					where	
					Id=@Id and
					IsActive=1
			End

	     else if @Para='Delete'
		 Begin
				update HomeStatistic
				set IsActive=0
				where Id=@Id
        End
 
		else if @Para='get'
		Begin
			select
			ROW_NUMBER() over (order by Id asc) as Sr,
			Id,
			Counting,
			Icon,
			Description
		from HomeStatistic
		where IsActive = 1
		End
			
		else if @Para = 'getbyId'
		begin
			select * from HomeStatistic
			where IsActive = 1
			and Id=@Id
		end

		else if @Para = 'bindHomeStatistics'
		begin
			select * from HomeStatistic
			where IsActive = 1
		end
END





GO
/****** Object:  StoredProcedure [dbo].[Proc_InfrastructureFacilities]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_InfrastructureFacilities]
(
	@Para		nvarchar(100)=null,
	@Id	int=0,
	@Image		varchar(100)=null,
	@Heading varchar(500)=null,
	@DisplayOrder int=0
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into InfrastructureFacilities
		(
			Image,
			Heading,
			DisplayOrder,
			IsActive,
			AddedOn
		)
		values
		(
			@Image,
			@Heading,
			@DisplayOrder,
			1,
			getdate()
		)
	end

	else if @Para = 'delete'
	begin
		update InfrastructureFacilities set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update InfrastructureFacilities set
			Image = @Image,
			UpdatedOn = getdate()
			where
			Id = @Id
		end

		update InfrastructureFacilities set
			Heading = @Heading,
			DisplayOrder = @DisplayOrder,
			UpdatedOn = getdate()
			where
			Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,
			Id,
			Image,
			Heading,
			DisplayOrder
		from InfrastructureFacilities
		where IsActive = 1
		order by DisplayOrder asc
	end

	else if @Para = 'getbyId'
	begin
		select * from InfrastructureFacilities
		where IsActive = 1
		and Id = @Id
	end

	else if @Para = 'bindInfrastructureFacilities'
	begin
		select 
            Id,
			Image,
			heading,
			DisplayOrder
		from InfrastructureFacilities
		where IsActive = 1
		order by DisplayOrder asc
	end
END





GO
/****** Object:  StoredProcedure [dbo].[Proc_InnerBanner]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_InnerBanner]
(
	@Para		nvarchar(100)=null,
	@Id			int=0,
	@Image		varchar(100)=null,
	@PageName	varchar(100)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into InnerBanner
		(
			Image,
			PageName,
			IsActive,
			AddedOn
		)
		values
		(
			@Image,
			@PageName,
			1,
			getdate()
		)
	end

	else if @Para = 'delete'
	begin
		update InnerBanner set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update InnerBanner set
			Image = @Image			
			where
			Id = @Id
		end

		Update InnerBanner
		set
		PageName=@PageName,
		UpdatedOn = getdate()
		where 
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by Id asc) as Sr,
			Id,
			Image,
			PageName
		 from InnerBanner
		where IsActive = 1
		order by Id asc
	end

	else if @Para = 'getbyId'
	begin
		select * from InnerBanner
		where IsActive = 1
		and Id = @Id
	end

	else if @Para = 'bindInnerBanner'
	begin
		select * from InnerBanner
		where IsActive = 1
		and Id = @Id
	end

END







GO
/****** Object:  StoredProcedure [dbo].[Proc_Management]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_Management]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Image				varchar(1000)=null,
	@Heading			varchar(100)=null,
	@Description		varchar(3000)=null,
	@DisplayOrder		int=0,
	@Designation		nvarchar(100)=null
AS
BEGIN
	if @Para = 'add'
	begin
		insert into Management
		(
			Heading,
			Description,
			DisplayOrder,
			IsActive,
			AddedOn,
			Image,
			Designation
			
		)
		values
		(
			@Heading,
			@Description,
			@DisplayOrder,
			1,
			getdate(),
			@Image,
			@Designation
		)
	end

	else if @Para = 'delete'
	begin
		update Management set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update Management set
			Image = @Image
			where
			Id = @Id
		end

		update Management set
		Heading = @Heading,	
		Description = @Description,
		DisplayOrder = @DisplayOrder,
		Designation=@Designation,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,			
			Id,
			Heading,	
			Description,
			Image,
			DisplayOrder,
			Designation
			from Management 			
		where IsActive = 1
		order by DisplayOrder asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Heading,			
			Description,
			Image,
			DisplayOrder,
			Designation	
			from Management
		where IsActive = 1
		and Id = @Id
		order by DisplayOrder asc
	end

	else if @Para = 'bindManagement'
	begin
     select * from Management where IsActive = 1 order by DisplayOrder asc
	end
	End





GO
/****** Object:  StoredProcedure [dbo].[Proc_MetaTag]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_MetaTag]
		( 
			@Para				nvarchar(100)='',
			@Id					int=0,
			@PageUrl			nvarchar(100)='',
			@Title				nvarchar(500)='',
			@MetaKeywords		nvarchar(100)='',
			@MetaDescription	nvarchar(500)=''
		)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into MetaTag
		(
			PageUrl,
			Title,
			MetaKeywords,
			MetaDescription,
			IsActive,
			AddedOn		
		)
		values
		(
			@PageUrl,
			@Title,
			@MetaKeywords,
			@MetaDescription,
			1,
			getdate()
		
		)
	end

	else if @Para = 'delete'
	begin
		update MetaTag set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		update MetaTag set
		PageUrl = @PageUrl,	
		Title = @Title,
		MetaKeywords = @MetaKeywords,
		MetaDescription=@MetaDescription,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by Id asc) as Sr,			
			Id,
		PageUrl,
			Title,
			MetaKeywords,
			MetaDescription
			from MetaTag 			
		   where IsActive = 1

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
		PageUrl,
			Title,
			MetaKeywords,
			MetaDescription
			from MetaTag 			
		   where IsActive = 1
		and Id = @Id
	end

	else if @Para = 'bindMetatTag'
	begin
     select * from MetaTag where IsActive = 1 and PageUrl=@PageUrl
	end
	
	End



GO
/****** Object:  StoredProcedure [dbo].[Proc_Milestones]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_Milestones]
(
	@Para			nvarchar(100)=null,
	@MilesId		int=0,
	@Year			varchar(50)=null,
	@Description	varchar(1000)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into Milestones
		(
			Year,
			Description,
			IsActive,
			AddedOn
		)
		values
		(
			@Year,
			@Description,
			1,
			getdate()
		)
	end

	else if @Para = 'delete'
	begin
		update Milestones set
		IsActive = 0,
		DeletedOn = getdate()
		where
		MilesId = @MilesId
	end

	else if @Para = 'update'
	begin
		update Milestones set
		Year = @Year,
		Description = @Description,
		UpdatedOn = getdate()
		where
		MilesId = @MilesId
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by CAST(Year as int) desc ) as Sr,
			MilesId,
			Year,
			Description
		from Milestones
		where IsActive = 1
		order by CAST(Year as int) desc
	end

	else if @Para = 'getbyId'
	begin
		select * from Milestones
		where IsActive = 1
		and MilesId = @MilesId
	end

	else if @Para = 'bindWhatWeAreMilestone'
	begin
		select 
			MilesId,
			Year,
			Description
		from Milestones
		where IsActive = 1
		order by CAST(Year as int) asc
	end

END





GO
/****** Object:  StoredProcedure [dbo].[Proc_NewsRoom]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_NewsRoom]
(
	@Para			nvarchar(100)=null,
	@NewsId			int=0,
	@Type			nvarchar(100)=null,
	@Image			nvarchar(3000)=null,
	@Date			date=null,
	@Title			nvarchar(500)=null,
	@Description	varchar(4000)=null,
	@Slug			nvarchar(500)=null
)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into NewsRoom
		(
		
			Type,
			Title,
			Image,
			Description,
			Date,
			IsActive,
			AddedOn,
			Slug
		)
		values
		(
			@Type,
			@Title,
			@Image,
			@Description,
			GETDATE(),		
			1,
			GETDATE(),
			@Slug
		)
	end

	else if @Para = 'delete'
	begin
		update NewsRoom set
		IsActive = 0,
		DeletedOn = getdate()
		where
		NewsId = @NewsId
	end

	else if @Para = 'update'
	begin
		if @Image<>''
			Begin
			update NewsRoom set
			Image = @Image
			where
			NewsId = @NewsId
			End

		update NewsRoom set
		Type = @Type,
		Title=@Title,
		date=GETDATE(),
		Description = @Description,
		UpdatedOn = getdate(),
		Slug=@Slug
		where
		NewsId = @NewsId
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by Date  asc ) as Sr,
			NewsId,
			Type,
			Title,
			Image,
			Description,
			 CONVERT(varchar,Date,106) as Date,
			 Slug
		from NewsRoom
		where IsActive = 1
		--order by CAST(Date as int) desc
	end

	else if @Para = 'getbyId'
	begin
		select 
			--ROW_NUMBER() over (order by CAST(Date as int) desc ) as Sr,
			NewsId,
			Type,
			Title,
			Image,
			Description,
			 CONVERT(varchar,Date,105) as Date,
			 Slug
		from NewsRoom
		where IsActive = 1
		and NewsId = @NewsId
	end

	else if @Para='bindNewsroom'
	Begin

	select distinct type from NewsRoom 

				select 
			ROW_NUMBER() over (order by Date  asc ) as Sr,
			NewsId,
			Type,
			Title,
			Image,
			CONVERT(VARCHAR(95),Description)+'...'[DESCRIPTION],
			 CONVERT(varchar,Date,106) as Date,
			 Slug
		from NewsRoom
		where IsActive = 1

	

	End


else if @Para='binNewsRoomDeatils'
Begin
			select 
			ROW_NUMBER() over (order by Date  asc ) as Sr,
			NewsId,
			Type,
			Title,
			Image,
			Description,
			 CONVERT(varchar,Date,106) as Date,
			 Slug
		from NewsRoom
		where  Slug=@Slug  and IsActive = 1
End

END


GO
/****** Object:  StoredProcedure [dbo].[Proc_OurValues]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_OurValues]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Image				varchar(1000)=null,
	@Heading			varchar(100)=null,
	@Description		varchar(3000)=null,
	@DisplayOrder		int=0
AS
BEGIN
	if @Para = 'add'
	begin
		insert into OurValues
		(
			Heading,
			Description,
			DisplayOrder,
			IsActive,
			AddedOn,
			Image
			
		)
		values
		(
			@Heading,
			@Description,
			@DisplayOrder,
			1,
			getdate(),
			@Image
		)
	end

	else if @Para = 'delete'
	begin
		update OurValues set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update OurValues set
			Image = @Image
			where
			Id = @Id
		end

		update OurValues set
		Heading = @Heading,	
		Description = @Description,
		DisplayOrder = @DisplayOrder,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,			
			Id,
			Heading,	
			Description,
			Image,
			DisplayOrder
			from OurValues 			
		where IsActive = 1
		order by DisplayOrder asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Heading,			
			Description,
			Image,
			DisplayOrder	
			from OurValues
		where IsActive = 1
		and Id = @Id
		order by DisplayOrder asc
	end

	else if @Para = 'bindOurValues'
	begin
     select * from OurValues where IsActive = 1 
	end
	End




GO
/****** Object:  StoredProcedure [dbo].[Proc_ProductPartners]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_ProductPartners]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Image				varchar(1000)=null,
	@Heading			varchar(100)=null,
	@Description		varchar(3000)=null,
	@DisplayOrder		int=0
AS
BEGIN
	if @Para = 'add'
	begin
		insert into ProductPartners
		(
			Heading,
			Description,
			DisplayOrder,
			IsActive,
			AddedOn,
			Image
			
		)
		values
		(
			@Heading,
			@Description,
			@DisplayOrder,
			1,
			getdate(),
			@Image
		)
	end

	else if @Para = 'delete'
	begin
		update ProductPartners set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update ProductPartners set
			Image = @Image
			where
			Id = @Id
		end

		update ProductPartners set
		Heading = @Heading,	
		Description = @Description,
		DisplayOrder = @DisplayOrder,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,			
			Id,
			Heading,	
			Description,
			Image,
			DisplayOrder
			from ProductPartners 			
		where IsActive = 1
		order by DisplayOrder asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Heading,			
			Description,
			Image,
			DisplayOrder	
			from ProductPartners
		where IsActive = 1
		and Id = @Id
		order by DisplayOrder asc
	end

	else if @Para = 'bindProductPartners'
	begin
     select * from ProductPartners where IsActive = 1 
	end
	End





GO
/****** Object:  StoredProcedure [dbo].[Proc_QualityMissionVison]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_QualityMissionVison]
(
	@Para			nvarchar(100)=null,
	@Id				int=0,
	@Title			varchar(500)=null,
	@Icon			varchar(100)=null,
	@Description	nvarchar(max)=null,
	@DisplayOrder	int=0
	)
AS
BEGIN
     if @Para = 'add'
	begin
		insert into QualityMissionVison
		(
			Title,
			Icon,
			Description,
			DisplayOrder,
			IsActive,
			AddedOn
		)
		values
		(
			@Title,
			@Icon,
			@Description,
			@DisplayOrder,
			1,
			getdate()
		)
	end

	else if @Para = 'delete'
	begin
		update QualityMissionVison set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

		else if @Para = 'update'
	begin
		update QualityMissionVison set
		Title = @Title,
		Icon = @Icon,
		Description = @Description,
		DisplayOrder = @DisplayOrder,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over(order by DisplayOrder asc) Sr,
			Id,
			Title,
			Icon,
			Description,
			DisplayOrder
		from QualityMissionVison 	
		where
		IsActive = 1		
		order by DisplayOrder asc
	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Title,
			Icon,
			Description,
			DisplayOrder
		from QualityMissionVison
		where IsActive = 1
		and Id = @Id
	end


		else if @Para = 'bindQualityMissionVison'
	begin

	select 
		Id,
		Title,
		Icon,
		Description
	from QualityMissionVison 
	where
	IsActive=1
	order by DisplayOrder asc
	end
END






GO
/****** Object:  StoredProcedure [dbo].[Proc_RangeOfProduct]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_RangeOfProduct]
	(
			@Para				nvarchar(100)=null,
			@Id					int=0,
			@Equipment			nvarchar(3000)=null,
			@Applications		nvarchar(3000)=null,
			@Pumps				nvarchar(3000)=null

		)
AS
BEGIN
		
		if @Para='Add'
			Begin
				insert into RangeOfProduct
					(
						Equipment,
						Applications,
						Pumps,					
						IsActive
					)
				Values
					(
						@Equipment,
						@Applications,
						@Pumps,
						1
					)

			End

		else if @Para='Update'
			Begin

					Update RangeOfProduct
					set Equipment=@Equipment,
					Applications=@Applications,
					Pumps=@Pumps,
					UpdatedOn=GETDATE()
					where	
					Id=@Id and
					IsActive=1
			End

	     else if @Para='Delete'
		 Begin
				update RangeOfProduct
				set IsActive=0
				where Id=@Id
        End
 
		else if @Para='get'
		Begin
			select
			Id,
			Equipment,
			Applications,
			Pumps
		from RangeOfProduct
		where IsActive = 1
		and Id=@Id
		End
			
		else if @Para = 'getbyId'
		begin
			select * from RangeOfProduct
			where IsActive = 1
			and Id=@Id
		end

		else if @Para = 'bindRangeOfProduct'
		begin
			select * from RangeOfProduct
			where IsActive = 1
		end
END







GO
/****** Object:  StoredProcedure [dbo].[Proc_ReachUs]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_ReachUs]
(
	@Para			nvarchar(100)=null,
	@Id				int=0,
	@Heading		nvarchar(200)=null,
	@Description	nvarchar(1000)=null,
	@Contact		nvarchar(500)=null,
	@Image			nvarchar(3000)=null,
	@Fax			nvarchar(100)=null,
	@Latitude		numeric(12,6)= Null,
	@Longitude		numeric(12,6)= Null,
	@Title			nvarchar(100)=Null
	
)

AS
BEGIN
if @Para = 'add'
	begin

	insert into ReachUs
	(
		Heading,
		Description,
		Contact,
		AddedOn,
		IsActive,
		Image,
		Fax,
		Latitude,
		Longitude,
		Title

	)
	values
	(
		@Heading,
		@Description,
		@Contact,
		GETDATE(),
		1,
		@Image,
		@Fax,
		@Latitude,
		@Longitude,
		@Title

	)
	
	end


	if @Para='delete'
	Begin
	 update ReachUs
	 set
	 IsActive=0
	 where Id=@Id
	End

	if @Para = 'update'
	begin
	 if @Image<>''
	 Begin
		update ReachUs set
		Image = @Image,
		UpdatedOn = getdate()
	 End

		update ReachUs set
		Heading = @Heading,
		Description = @Description,
		Contact = @Contact,
		UpdatedOn = getdate(),
		Fax=@Fax,
		Latitude = @Latitude,
		Longitude=@Longitude,
		Title=@Title
		where
		Id = @Id
	end


	else if @Para='get'
	begin
		select 
		ROW_NUMBER() over (order by Id asc) as Sr,
		Id,
		Image,
		Heading,
		Description,
		Contact,
		Fax,
		Latitude,
		Longitude,
		Title
		from ReachUs
		where IsActive=1
	end


	else if @Para = 'getbyId'
	begin
		select * from ReachUs
		where IsActive = 1
		and Id = @Id
	end


	else if @Para='bindReachUs'
	begin
		select 
		ROW_NUMBER() over (order by Id asc) as Sr,
		Id,
		Image,
		Heading,
		Description,
		Contact,
		Fax,
		Latitude,
		Longitude,
		Title
		from ReachUs
		where IsActive=1
	end
END






GO
/****** Object:  StoredProcedure [dbo].[Proc_SearchResult]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_SearchResult]
		@PARA		nvarchar(50)='',
		@SEARCH			NVARCHAR(100)=''
AS
BEGIN
IF @PARA='GET_SEARCH'
	BEGIN
		SELECT Url as PageName,
	cm.Heading as Heading,
     CONVERT(VARCHAR(250),cm.Description)+'...'[DESCRIPTION]
	 FROM CMS cm
	 WHERE
	 ISACTIVE = 1
	 and
	     cm.Heading LIKE '%' + @SEARCH + '%' OR
		cm.Description LIKE '%' + @SEARCH + '%'
		
		Union
			SELECT 'Careers' as PageName,
	Name as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM CareerOurPeople
	 WHERE
	 ISACTIVE = 1
	 and
	     Name LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'

		Union
	SELECT 'Careers' as PageName,
	'Careers' as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM CareerWeBelieve
	 WHERE
	 ISACTIVE = 1
	 and
		Description LIKE '%' + @SEARCH + '%'

		Union
		SELECT 'CaseStudyListing' as PageName,
	'CaseStudyListing' as Heading,
     CONVERT(VARCHAR(250),Heading)+'...'[DESCRIPTION]
	 FROM CaseStudies
	 WHERE
	 ISACTIVE = 1
	 and
		Heading LIKE '%' + @SEARCH + '%'
		
		Union
		SELECT 'Newsroom' as PageName,
	Title as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM Newsroom
	 WHERE
	 ISACTIVE = 1
	 and
	     Title LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'

	Union
		SELECT 'OurValues' as PageName,
	Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM OurValues
	 WHERE
	 ISACTIVE = 1
	 and
	     Heading LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'

Union
		SELECT 'products' as PageName,
	'Products' as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM ProductPartners
	 WHERE
	 ISACTIVE = 1
	 and
	     Heading LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'

Union
		SELECT 'WhoWeAre' as PageName,
	Title as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM QualityMissionVison
	 WHERE
	 ISACTIVE = 1
	 and
	     Title LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'

Union
			SELECT 'products' as PageName,
	Heading as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM ServiceSpareParts
	 WHERE
	 ISACTIVE = 1
	 and
	     Heading LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'

Union
	SELECT 'Testimonials' as PageName,
	Name as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM Testimonials
	 WHERE
	 ISACTIVE = 1
	 and
	     Name LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'

Union
	SELECT 'Services' as PageName,
	Title as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM Services
	 WHERE
	     Title LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'
and
 ISACTIVE = 1

Union
  select Convert(nvarchar(50),sm.Title)+'/'+Convert(nvarchar(50),ss.Slug) as PageName, 
 Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM SolAndTechDetailsTable sd
	 inner join 
	 SolAndTechSubTable ss
	 on
	 sd.SubId=ss.SubId
	 inner join
	 SolAndTechMasterTable sm
	 on sm.MasterId=sd.MasterId
	 WHERE
	     sd.Heading LIKE '%' + @SEARCH + '%' OR
		sd.Description LIKE '%' + @SEARCH + '%'
and
 sd.ISACTIVE = 1


  Union
 SELECT Slug as PageName,
	PageName as Heading,
     CONVERT(VARCHAR(250),Description)+'...'[DESCRIPTION]
	 FROM AddNewPage
	 WHERE
	     PageName LIKE '%' + @SEARCH + '%' OR
		Description LIKE '%' + @SEARCH + '%'
and
 ISACTIVE = 1

END
END


GO
/****** Object:  StoredProcedure [dbo].[Proc_Services]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_Services]
	(		@Para			nvarchar(100)='',
			@ServId			int=0,
			@Title			nvarchar(500)='',
			@Description	nvarchar(4000)='',
			@Icon			nvarchar(3000)='',
			@Image			nvarchar(3000)=''
	)
AS
			
BEGIN
 	If @Para='Add'
				Begin
					insert into Services
						(
							Title,							
							Description,
							Icon,
							Image,
							IsActive,
							AddedOn
							
						)
				values
						(
							@Title,							
							@Description,
							@Icon,
							@Image,
							1,
							GETDATE()
						)
				End

				Else If @Para='update'
			Begin
				if @Image <>''
				Begin
				Update Services
				set 
				Image=@Image
				where
				ServId = @ServId
				End

				if @Icon <>''
				Begin
				Update Services
				set 
				Icon=@Icon
				where
				ServId = @ServId
				End

				Update Services
				set 				
				Title=@Title,
				Description=@Description,
				UpdatedOn=Getdate()
				where
				ServId = @ServId
				End


			Else If @Para='delete'
			Begin
				Update Services
				set 
				IsActive=0
				where
				ServId = @ServId
			End


			Else If @Para='get'
			Begin
				select
			  --ROW_NUMBER() over (order by DisplayOrder asc) as Sr,
				ServId,
				Title,							
				Description,
				Icon,
				Image
				from 
				Services 
				Where
				IsActive = 1

			End

			Else If @Para='getbyId'
			Begin
				select
			    ServId,
				Title,							
				Description,
				Icon,
				Image
				from 
				Services  
				Where
				IsActive = 1
				and ServId=@ServId

			End

			Else If @Para='bindHomeServices'
			Begin
				select
			  --ROW_NUMBER() over (order by DisplayOrder asc) as Sr,
				ServId,
				Title,							
				CONVERT(VARCHAR(95),Description)+'...'[DESCRIPTION],
				Icon,
				Image
				from 
				Services 
				Where
				IsActive = 1

			End

			Else if @Para='bindservicesmenu'
			Begin
			 select * from Services
			 where IsActive=1
			End
END






GO
/****** Object:  StoredProcedure [dbo].[Proc_ServiceSpareParts]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_ServiceSpareParts]
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Image				varchar(1000)=null,
	@Heading			varchar(100)=null,
	@Description		varchar(3000)=null,
	@DisplayOrder		int=0
AS
BEGIN
	if @Para = 'add'
	begin
		insert into ServiceSpareParts
		(
			Heading,
			Description,
			DisplayOrder,
			IsActive,
			AddedOn,
			Image
			
		)
		values
		(
			@Heading,
			@Description,
			@DisplayOrder,
			1,
			getdate(),
			@Image
		)
	end

	else if @Para = 'delete'
	begin
		update ServiceSpareParts set
		IsActive = 0,
		DeletedOn = getdate()
		where
	    Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update ServiceSpareParts set
			Image = @Image
			where
			Id = @Id
		end

		update ServiceSpareParts set
		Heading = @Heading,	
		Description = @Description,
		DisplayOrder = @DisplayOrder,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over (order by DisplayOrder asc) as Sr,			
			Id,
			Heading,	
			Description,
			Image,
			DisplayOrder
			from ServiceSpareParts 			
		where IsActive = 1
		order by DisplayOrder asc

	end

	else if @Para = 'getbyId'
	begin
		select 
			Id,
			Heading,			
			Description,
			Image,
			DisplayOrder	
			from ServiceSpareParts
		where IsActive = 1
		and Id = @Id
		order by DisplayOrder asc
	end

	else if @Para = 'bindServiceSpareParts'
	begin
     select * from ServiceSpareParts where IsActive = 1 
	end
	End






GO
/****** Object:  StoredProcedure [dbo].[Proc_SocialMedia]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_SocialMedia]
(
	@Para		nvarchar(100)=null,
	@Id			int=0,
	@opentime	 varchar(500)=null,
	@CallUS		varchar(500)=null,
	@MailUs		varchar(500)=null,
	@Sales		varchar(500)=null,
	@Services	varchar(500)=null,
	@Purchasing	varchar(500)=null,
	@Dispatches	varchar(500)=null,
	@Sales1		varchar(500)=null
)
AS
BEGIN
if @Para = 'add'
	begin

	insert into SocialMedia
	(
		opentime,
		CallUs,
		MailUs,
		Sales,
		Services,
		Purchasing,
		Dispatches,
		AddedOn,
		IsActive,
		Sales1

	)
	values
	(
		@opentime,
		@CallUS,
		@MailUs,
		@Sales,
		@Services,
		@Purchasing,
		@Dispatches,
		GETDATE(),
		1,
		@Sales1

	)
	
	end


	if @Para = 'update'
	begin
		update SocialMedia set
		opentime = @opentime,
		CallUs = @CallUS,
		MailUs = @MailUs,
		Sales = @Sales,
		Services = @Services,
		Purchasing = @Purchasing,
		Dispatches = @Dispatches,
		UpdatedOn = getdate(),
		Sales1 = @Sales1
		where
		Id = @Id
	end

	else if @Para = 'getbyId'
	begin
		select * from SocialMedia
		where IsActive = 1
		and Id = @Id
	end

END





GO
/****** Object:  StoredProcedure [dbo].[Proc_SolAndTechDetailsTable]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_SolAndTechDetailsTable]
	(
			@Para			varchar(100)='',
			@Id				int=0,
			@SubId			int=0,
			@MasterId		int=0,
			@Heading		varchar(200)='',
			@Description	nvarchar(3000)='',
			@Image			nvarchar(3000)=''
	)
AS
BEGIN
		If @Para='add'
		Begin
			insert into SolAndTechDetailsTable
				(
					SubId,
					MasterId,
					Heading,
					Description,
					IsActive,
					AddedOn,
					Image
				)
			values
				(
					@SubId,
					@MasterId,
					@Heading,
					@Description,
					1,
					GETDATE(),
					@Image
				)
		End

		Else If @Para='update'
			Begin

			if @Image<>''
			Begin
			Update SolAndTechDetailsTable
				set 
				Image=@Image
				where
				Id = @Id
			End

				Update SolAndTechDetailsTable
				set 
				SubId = @SubId,
				MasterId=@MasterId,
				Heading=@Heading,
				Description=@Description,
				UpdatedOn=Getdate()
				where
				Id = @Id
			End

			Else If @Para='delete'
			Begin
				Update SolAndTechDetailsTable
				set 
				IsActive=0
				where
				Id = @Id
			End

			Else If @Para='get'
			Begin
				select
				ROW_NUMBER() over (order by std.Id asc) as Sr,
				sm.Title as MasterTitle,
				sts.Title as PageName,
				std.Id,
				std.SubId,
				std.MasterId,
				std.Heading,
				std.Description,
				std.Image
				from 
				SolAndTechDetailsTable std
				inner join
				SolAndTechSubTable sts
				on 
				std.SubId=sts.SubId
				inner join
				SolAndTechMasterTable sm
				on
				std.MasterId=sm.MasterId
				Where
				std.IsActive = 1

			End

			Else If @Para='getbyId'
			Begin
				select
				sm.Title as MasterTitle,
				sts.Title as PageName,
				std.Id,
				std.SubId,
				std.MasterId,
				std.Heading,
				std.Description,
				std.Image
				from 
				SolAndTechDetailsTable std
				inner join
				SolAndTechSubTable sts
				on 
				std.SubId=sts.SubId
				inner join
				SolAndTechMasterTable sm
				on
				std.MasterId=sm.MasterId
				Where
				std.IsActive = 1
				and
				std.Id=@Id
			End


			Else If @Para='bindsubdropdown'
			Begin
				select SubId,
				sts.Title as PageName
				from SolAndTechSubTable sts
				inner join SolAndTechMasterTable sm
				on 
				sm.MasterId=sts.MasterId
				where sm.MasterId=@MasterId
				and sts.IsActive=1

			End


			Else if @Para='bindHomeProductSolution'
			Begin
				select 
				top 4
				sm.Title as MasterTitle,
				ss.Title as PageName,
				sd.Heading,
				slug
				from
				SolAndTechSubTable ss
				inner join
				SolAndTechDetailsTable sd
				on
				ss.SubId=sd.SubId
				inner join
				SolAndTechMasterTable sm
				on
				sm.MasterId=ss.MasterId
				where
				sm.MasterId=1
				and 
				sd.IsActive=1			
			End
END









GO
/****** Object:  StoredProcedure [dbo].[Proc_SolAndTechGalleryTable]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_SolAndTechGalleryTable]
		(
				@Para			varchar(100)='',
				@GalleryId		int=0,
				@Id				int=0,
				@Title			varchar(200)='',
				@Type			varchar(100)='',
				@Image			nvarchar(3000)='',
				@Url			nvarchar(200)='',
				@ThumbImg		varchar(3000)=''

		)

AS
BEGIN
	
		If @Para='add'
		Begin
			insert into SolAndTechGalleryTable
				(
					Id,
					Title,
					Type,
					Image,
					Url,
					ThumbImg,
					IsActive,
					AddedOn
				)
			values
				(
					@Id,
					@Title,
					@Type,
					@Image,
					@Url,
					@ThumbImg,
					1,
					GETDATE()
				)
		End

		Else If @Para='update'
			Begin
				if @Image <>''
				Begin
				Update SolAndTechGalleryTable
				set 
				Image=@Image
				where
				GalleryId = @GalleryId
				End


				else if @ThumbImg<>''
				Begin
				Update SolAndTechGalleryTable
				set 
				ThumbImg=@ThumbImg
				where
				GalleryId = @GalleryId
				End



				Update SolAndTechGalleryTable
				set 
				Id = @Id,
				Title=@Title,
				Type=@Type,
				Url=@Url,
				UpdatedOn=Getdate()
				where
				GalleryId = @GalleryId
			End

			Else If @Para='delete'
			Begin
				Update SolAndTechGalleryTable
				set 
				IsActive=0
				where
				GalleryId = @GalleryId
			End

			Else If @Para='get'
			Begin
				select
				ROW_NUMBER() over (order by GalleryId asc) as Sr,
				GalleryId,
				Id,
				stg.Title,
				Type,
				Image,
				Url,
				ThumbImg
				from 
				SolAndTechGalleryTable stg
				Where
				stg.IsActive = 1
				and stg.Id=@Id
			End

			Else If @Para='getbyId'
			Begin
			select GalleryId,
				stg.Id,				
				stg.Title,
				stg.Type,
				stg.Image,
				stg.Url,
				stg.ThumbImg
				from SolAndTechGalleryTable stg
				inner join
				SolAndTechDetailsTable std
				on std.Id=stg.Id 
				where 
				GalleryId = @GalleryId
				and stg.Id=@Id
				and stg.IsActive=1
			End

			Else If @Para='bindDropdownGallary'
			Begin
				select Title,SubId from SolAndTechSubTable 									
			End

END







GO
/****** Object:  StoredProcedure [dbo].[Proc_SolAndTechSubTable]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[Proc_SolAndTechSubTable]
		(
			@Para			varchar(100)='',
			@SubId			int=0,
			@MasterId		int=0,
			@Title			varchar(200)='',
			@Slug			nvarchar(100)='',
			@DisplayOrder	int=0

			
		)
AS
BEGIN
		If @Para='add'
		Begin
			insert into SolAndTechSubTable
				(
					MasterId,
					Title,
					Slug,
					IsActive,
					AddedOn,
					DisplayOrder
					
				)
			values
				(
					@MasterId,
					@Title,
					@Slug,
					1,
					GETDATE(),
					@DisplayOrder
				)
		End

		Else If @Para='update'
			Begin
				Update SolAndTechSubTable
				set 
				MasterId=@MasterId,
				Title=@Title,
				Slug=@Slug,
				UpdatedOn=Getdate(),
				DisplayOrder=@DisplayOrder
				where
				SubId = @SubId
			End

			Else If @Para='delete'
			Begin
				Update SolAndTechSubTable
				set 
				IsActive=0,
				DeletedOn=GETDATE()
				where
				SubId = @SubId
			End

			Else If @Para='get'
			Begin
				select
				ROW_NUMBER() over (order by sts.SubId asc) as Sr,
				sts.SubId,
				sts.MasterId,
				sm.Title as MasterTitle,
				sts.Title,
				sts.Slug,
				sts.DisplayOrder
				
				from 
				SolAndTechSubTable sts
				inner join 
				SolAndTechMasterTable sm
				on
				sts.MasterId=sm.MasterId
				Where
				sts.IsActive = 1

				select 
				MasterId,
				Title
				from 
				SolAndTechMasterTable sm
				where 
				MasterId=@MasterId
				and
				IsActive=1


			End

			Else If @Para='getbyId'
			Begin
				select 
				sts.SubId,
				sts.MasterId,
				sm.Title as MasterTitle,
				sts.Title,
				sts.Slug,
				sts.DisplayOrder
				 from SolAndTechSubTable sts
				 inner join
				 SolAndTechMasterTable sm
				 on sm.MasterId=sts.MasterId
				 where 
				 SubId=@SubId
				 and sts.IsActive=1
				
			End

			Else if @Para='binddropdown'
			Begin
			select 
				sm.MasterId,
				sm.Title
				from 
				SolAndTechMasterTable sm
				where 
				IsActive=1
			End



			Else if @Para='bindMenu'
			begin
			select				
				sts.MasterId,
				LOWER(sm.Title) as MasterTitle,
				sts.Title  as PageName,
				sts.Slug,
				LOWER(sm.Title)+'/'+sts.Slug as Url,
				sts.DisplayOrder
				from 
				SolAndTechSubTable sts
				inner join 
				SolAndTechMasterTable sm
				on
				sts.MasterId=sm.MasterId
				Where
				sts.IsActive = 1
				and sts.MasterId =1
				order by sts.DisplayOrder asc
				
				select				
				sts.MasterId,
				LOWER(sm.Title) as MasterTitle,
				sts.Title  as PageName,
				sts.Slug,
				LOWER(sm.Title)+'/'+sts.Slug as Url,
				sts.DisplayOrder
				from 
				SolAndTechSubTable sts
				inner join 
				SolAndTechMasterTable sm
				on
				sts.MasterId=sm.MasterId
				Where
				sts.IsActive = 1
				and sts.MasterId =2
				order by sts.DisplayOrder asc
				

				select				
				sts.MasterId,
				LOWER(sm.Title) as MasterTitle,
				sts.Title  as PageName,
				sts.Slug,
				LOWER(sm.Title)+'/'+sts.Slug as Url,
				sts.DisplayOrder
				from 
				SolAndTechSubTable sts
				inner join 
				SolAndTechMasterTable sm
				on
				sts.MasterId=sm.MasterId
				Where
				sts.IsActive = 1
				and sts.MasterId =3
				order by sts.DisplayOrder asc
			end
				
				else if @Para='bindMenuPageDetail'
				Begin
				  select 
				sd.Heading,
				sd.Image,
				ss.Title
				from SolAndTechDetailsTable sd
				inner join SolAndTechSubTable ss
				on 
				sd.MasterId=ss.MasterId
				and
				sd.SubId=ss.SubId
				inner join
				SolAndTechMasterTable sm
				on
				sm.MasterId=sd.MasterId			
				where 
				ss.IsActive=1
				and
				sm.IsActive=1
				and
				sd.IsActive=1
				and 
				sm.Title=@Title
				and
				ss.Slug=@Slug

			  select 
				sd.Heading,
				sd.Description					
				from SolAndTechDetailsTable sd
				inner join SolAndTechSubTable ss
				on 
				sd.MasterId=ss.MasterId
				and
				sd.SubId=ss.SubId
				inner join
				SolAndTechMasterTable sm
				on
				sm.MasterId=sd.MasterId			
				where 
				ss.IsActive=1
				and
				sm.IsActive=1
				and
				sd.IsActive=1
				and 
				sm.Title=@Title
				and
				ss.Slug=@Slug
		
			select
			sd.Id,
			sg.GalleryId,
			   sg.Title,
				sg.Type,
				sg.Image,
				sg.Url,
				sg.ThumbImg
				from
				SolAndTechGalleryTable sg
				inner join
				SolAndTechDetailsTable sd
				on 
				sg.Id=sd.Id
				inner join
				SolAndTechSubTable ss
				on
				ss.SubId=sd.SubId
				inner join
				SolAndTechMasterTable sm
				on
				sm.MasterId=sd.MasterId	
				where
				ss.IsActive=1
				and
				sm.IsActive=1
				and
				sd.IsActive=1
				and
				sg.IsActive=1
				and
			    sm.Title=@Title
				and
				ss.Slug=@Slug


					select				
				sts.MasterId,
				LOWER(sm.Title) as MasterTitle,
				sts.Title  as PageName,
				sts.Slug,
				LOWER(sm.Title)+'/'+sts.Slug as Url
				from 
				SolAndTechSubTable sts
				inner join 
				SolAndTechMasterTable sm
				on
				sts.MasterId=sm.MasterId
				Where
				sts.IsActive = 1
				and
				sm.IsActive=1
				and sts.MasterId =1

				End


END






GO
/****** Object:  StoredProcedure [dbo].[Proc_SuperAdminLogin]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[Proc_SuperAdminLogin]
(
	@PARA			NVARCHAR(100)=NULL,
	@NAME			NVARCHAR(100)=NULL,
	@USERNAME		NVARCHAR(100)=NULL,
	@PASSWORD		NVARCHAR(100)=NULL,
	@EMAILID		NVARCHAR(100)=NULL,
	@MOBILENO		VARCHAR(15)=NULL,
	@ID				INT=0,
	@NEWPASSWORD		NVARCHAR(100)=NULL
)
AS
BEGIN
	/*Note:- COLLATE SQL_Latin1_General_CP1_CS_AS is using for validating case sensitive*/
    IF @PARA='GET_LOGIN'
	BEGIN
	   IF EXISTS(SELECT * FROM SUPERADMINLOGIN 
				WHERE 
					USERNAME=@USERNAME COLLATE SQL_Latin1_General_CP1_CS_AS 
				AND [PASSWORD]=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS  
				AND ISACTIVE=1
				)
				BEGIN
					UPDATE SUPERADMINLOGIN SET LoginOn=getdate() --Update login time
					SELECT * FROM SUPERADMINLOGIN WHERE USERNAME=@USERNAME AND [PASSWORD]=@PASSWORD AND ISACTIVE=1 --fetch data after check valid uid & pwd
				END
		--ELSE
		--	BEGIN
		--		SELECT ''[incorrect]
		--	END
		
	END

	ELSE IF @PARA='CHANGE_PASSWORD'
	BEGIN
		IF EXISTS(SELECT * FROM SUPERADMINLOGIN 
				WHERE 
					USERNAME=@USERNAME COLLATE SQL_Latin1_General_CP1_CS_AS 
				AND [PASSWORD]=@PASSWORD COLLATE SQL_Latin1_General_CP1_CS_AS  
				AND ISACTIVE=1
			    )
				BEGIN
					UPDATE SUPERADMINLOGIN SET [Password]=@NEWPASSWORD,UpdatedOn=getdate() --Update New Pwd
					SELECT 'correct' [update]
				END		
			ELSE
			BEGIN
				SELECT 'incorrent'[update]
			END
	END

		ELSE IF @PARA='FORGOT_PASSWORD'	
		IF EXISTS(SELECT * FROM SUPERADMINLOGIN WHERE Emailid=@EMAILID AND ISACTIVE=1)
				BEGIN
					SELECT Name,Username,[Password],Emailid FROM SUPERADMINLOGIN WHERE Emailid=@EMAILID AND ISACTIVE=1 
				END		
			ELSE
			BEGIN
				SELECT ''[incorrect]
			END
		
		ELSE IF @PARA='GETDETAIL_FOR_UPDATE'	
			BEGIN
				SELECT Id,Name,Emailid,Mobileno FROM SUPERADMINLOGIN WHERE  ISACTIVE=1 
			END

		ELSE IF @PARA='UPDATEPROFILEDETAILS'
		begin
			UPDATE SUPERADMINLOGIN SET 
				UpdatedOn=getdate(),
				Name=@NAME,
				Emailid=@EMAILID,
				Mobileno=@MOBILENO
			where
				Id=@ID
		end

		ELSE IF @PARA='UPDATEOF_LOGOUT'
		begin
			UPDATE SUPERADMINLOGIN SET 
				LogoutOn=getdate()
			where
				Id=@ID
		end

		ELSE IF @PARA = 'ADMIN_TOTAL_COUNTS'
		BEGIN
			select '[' + STUFF((
				select 
					',{"Name":"Client","Count":' + CAST(COUNT(*) as varchar(max)) + ',"Url","/Admin/RegisteredUsers"},{"Name":"Category","Count":'+ cast((select top 1 COUNT(*) from Category where IsActive = 1) as varchar(max)) +',"Url":"/Admin/CategoryList"},{"Name":"SubCategory","Count":'+CAST((select top 1 COUNT(*) from SubCategory where IsActive = 1) as varchar(max))+',"Url":"/Admin/SubCategoryList"},{"Name":"Product","Count":'+ Cast((select top 1 COUNT(*) from Products where IsActive = 1) as varchar(max)) +',"Url":"/Admin/ProductList"}'
				from ClientRegistration
				for xml path(''), type
			).value('.', 'varchar(max)'), 1, 1, '') + ']' [TotalCounts]
		END
END










GO
/****** Object:  StoredProcedure [dbo].[Proc_TechnologyImageVideoCaption]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_TechnologyImageVideoCaption]
	(
	@Para		nvarchar(100)=null,
	@Id			int = 0,
	@Title		varchar(100)=null,
	@Image		varchar(500)=null,
	@Video		varchar(200)=null,
	@Type		varchar(50)=null
	)
AS
BEGIN
	if @Para = 'add'
	begin
		insert into TechnologyImageVideoCaption
		(
			Title,		
			Image,
			Video,
			IsActive,
			AddedOn,
			Type
		)
		values
		(
			@Title,
			@Image,
			@Video,
			1,
			getdate(),
			@Type
		)
	end

	else if @Para = 'delete'
	begin
		update TechnologyImageVideoCaption set
		IsActive = 0,
		DeletedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'update'
	begin
		if @Image<>''
		begin
			update TechnologyImageVideoCaption set
			Image = @Image
			where
			Id = @Id
		end

		update TechnologyImageVideoCaption set
		Title = @Title,		
		Video = @Video,
		Type = @Type,
		UpdatedOn = getdate()
		where
		Id = @Id
	end

	else if @Para = 'get'
	begin
		select 
			ROW_NUMBER() over(order by Id desc) as Sr,
			Id,
			Title,
			Image,
			Video,
			Type
		  from TechnologyImageVideoCaption
		  where IsActive = 1

	end

	else if @Para = 'getbyId'
	begin
		select 
			ROW_NUMBER() over(order by Id desc) as Sr,
			Id,
			Title,
			Image,
			Video,
			Type
		  from TechnologyImageVideoCaption
		  where IsActive = 1
		  and Id = @Id
	end



	else if @Para = 'bindTechnologyImageVideoCaption'
	begin
		select 
			Id,
			Title,
			Image,
			Video,
			Type
		  from TechnologyImageVideoCaption
		  where IsActive = 1
		  order by Id asc
	end


END





GO
/****** Object:  StoredProcedure [dbo].[Proc_Testimonials]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[Proc_Testimonials]
(
	@Para				nvarchar(100)=null,
	@Id					int=0,
	@Name				varchar(100)=null,
	@Position			varchar(100)=null,
	@CompanyName		varchar(100)=null,
	@Description		varchar(3000)=null,
	@DisplayOrder		int=0
)
AS
BEGIN
	If @Para='add'
	Begin
		Insert into Testimonials
			(
				Name,
				Position,
				CompanyName,
				Description,
				DisplayOrder,
				IsActive,
				AddedOn

			)
		Values
			(
				@Name,
				@Position,
				@CompanyName,
				@Description,
				@DisplayOrder,
				1,
				GETDATE()
			)

	End
	Else If @Para = 'delete'
		Begin
			Update Testimonials
			set 
			IsActive=0
			where
			 Id=@Id
		 End

	Else If @Para='update'
		Begin
			 Update Testimonials
			set 
				Name=@Name,
				Position=@Position,
				CompanyName = @CompanyName,
				Description = @Description,
				DisplayOrder = @DisplayOrder,
				UpdatedOn  = GETDATE()

			where
			 Id=@Id
		End	

	Else If @Para='get'
		Begin
			Select 
			ROW_NUMBER() over (order by Id asc) as Sr,
			Id,
			Name,
			Position,
			CompanyName,
			Description,
			DisplayOrder
			from Testimonials
			where 
			IsActive=1
		End

		Else If @Para='getbyId'
		Begin
			Select * from Testimonials
			where 
			Id=@Id
			and
			IsActive=1
		End


		Else If @Para='bindTestimonials'
		Begin
			Select * from Testimonials
			where 
			IsActive=1 order by DisplayOrder
		End

		Else if @Para='bindRandomTestimonials'
		Begin
		SELECT Top 1 Name, Position, CompanyName,Description
         FROM Testimonials where IsActive=1 order by CHECKSUM(NEWID())
		End
		
END






GO
/****** Object:  Table [dbo].[AddNewPage]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[AddNewPage](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageName] [nvarchar](500) NULL,
	[BannerImage] [nvarchar](3000) NULL,
	[Description] [nvarchar](3000) NULL,
	[Slug] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_AddNewPage] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ApplyForJob]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ApplyForJob](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](200) NULL,
	[Email] [varchar](100) NULL,
	[Role] [varchar](200) NULL,
	[Location] [varchar](100) NULL,
	[ContactNo] [varchar](100) NULL,
	[Resume] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_ApplyForJob] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Banner]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Banner](
	[BannerId] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](500) NULL,
	[BannerText1] [varchar](500) NULL,
	[BannerText2] [varchar](500) NULL,
	[BannerText3] [varchar](500) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Url] [nvarchar](100) NULL,
 CONSTRAINT [PK_Banner] PRIMARY KEY CLUSTERED 
(
	[BannerId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[CareerOurPeople]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerOurPeople](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](3000) NULL,
	[Name] [nvarchar](200) NULL,
	[Description] [nvarchar](3000) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Position] [nvarchar](200) NULL,
 CONSTRAINT [PK_CareerOurPeople] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerOverViewImages]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerOverViewImages](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](3000) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_CareerOverViewImages] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CareerWeBelieve]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CareerWeBelieve](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Description] [nvarchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_CareerWeBelieve] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CaseStudies]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CaseStudies](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [nvarchar](3000) NULL,
	[Heading] [nvarchar](200) NULL,
	[Category] [nvarchar](200) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[DisplayOrder] [int] NULL,
 CONSTRAINT [PK_CaseStudies] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Casestudyform]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Casestudyform](
	[CaseId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Name] [nvarchar](200) NULL,
	[CompanyName] [nvarchar](100) NULL,
	[Email] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_Casestudyform] PRIMARY KEY CLUSTERED 
(
	[CaseId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CMS]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[CMS](
	[CmsId] [int] IDENTITY(1,1) NOT NULL,
	[Heading] [varchar](200) NULL,
	[Description] [nvarchar](max) NULL,
	[Url] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_CMS] PRIMARY KEY CLUSTERED 
(
	[CmsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactUs]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ContactUs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[ContactNo] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Subject] [varchar](50) NULL,
	[Comment] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_ContactUs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ContactUsMap]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ContactUsMap](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Latitude] [numeric](12, 6) NULL,
	[Longitude] [numeric](12, 6) NULL,
	[Icon] [nvarchar](3000) NULL,
	[Description] [nvarchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Title] [nvarchar](200) NULL,
 CONSTRAINT [PK_ContactUsMap] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[CurrentOpening]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[CurrentOpening](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Location] [nvarchar](100) NULL,
	[Post] [nvarchar](500) NULL,
	[Qualification] [nvarchar](200) NULL,
	[Experience] [nvarchar](200) NULL,
	[Description] [nvarchar](2000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_CurrentOpening] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[GetInTouchForm]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[GetInTouchForm](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[ContactNo] [varchar](50) NULL,
	[Email] [varchar](50) NULL,
	[Comment] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_GetInTouchForm] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HomeGreenWorld]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomeGreenWorld](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Descrption] [varchar](1000) NULL,
	[Image] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_HomeGreenWorld] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HomeIndustries]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomeIndustries](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Heading] [varchar](200) NULL,
	[Icon] [varchar](100) NULL,
	[Url] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_HomeIndustries] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HomeProductSolutions]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeProductSolutions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](200) NULL,
	[Description] [nvarchar](1000) NULL,
	[Url] [nvarchar](200) NULL,
	[Image] [nvarchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_HomeProductSolutions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[HomeSolutions]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[HomeSolutions](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](500) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_HomeSolutions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[HomeStatistic]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[HomeStatistic](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Icon] [nvarchar](500) NULL,
	[Counting] [nvarchar](100) NULL,
	[Description] [nvarchar](200) NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
 CONSTRAINT [PK_HomeStatistic] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[InfrastructureFacilities]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InfrastructureFacilities](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](500) NULL,
	[Heading] [varchar](200) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_InfrastructureFacilities] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[InnerBanner]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[InnerBanner](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](500) NULL,
	[PageName] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_InnerBanner] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Management]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Management](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](1000) NULL,
	[Heading] [varchar](100) NULL,
	[Description] [varchar](3000) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Designation] [nvarchar](100) NULL,
 CONSTRAINT [PK_Management] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[MetaTag]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[MetaTag](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[PageUrl] [nvarchar](100) NULL,
	[Title] [nvarchar](500) NULL,
	[MetaKeywords] [nvarchar](200) NULL,
	[MetaDescription] [nvarchar](500) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_MetaTag] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Milestones]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Milestones](
	[MilesId] [int] IDENTITY(1,1) NOT NULL,
	[Year] [varchar](50) NULL,
	[Description] [varchar](1000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_Milestones] PRIMARY KEY CLUSTERED 
(
	[MilesId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[NewsRoom]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[NewsRoom](
	[NewsId] [int] IDENTITY(1,1) NOT NULL,
	[Type] [nvarchar](100) NULL,
	[Title] [nvarchar](500) NULL,
	[Image] [nvarchar](3000) NULL,
	[Date] [date] NULL,
	[Description] [nvarchar](4000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Slug] [nvarchar](500) NULL,
 CONSTRAINT [PK_NewsRoom] PRIMARY KEY CLUSTERED 
(
	[NewsId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[OurValues]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[OurValues](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](1000) NULL,
	[Heading] [varchar](100) NULL,
	[Description] [varchar](3000) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Url] [nvarchar](200) NULL,
 CONSTRAINT [PK_OurValues] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ProductPartners]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ProductPartners](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](1000) NULL,
	[Heading] [varchar](100) NULL,
	[Description] [varchar](3000) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_ProductPartners] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[QualityMissionVison]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[QualityMissionVison](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](100) NULL,
	[Icon] [nvarchar](100) NULL,
	[Description] [nvarchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[DisplayOrder] [int] NULL,
 CONSTRAINT [PK_QualityMissionVison] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[RangeOfProduct]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[RangeOfProduct](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Equipment] [varchar](3000) NULL,
	[Applications] [varchar](3000) NULL,
	[Pumps] [varchar](3000) NULL,
	[IsActive] [bit] NULL,
	[UpdatedOn] [datetime] NULL,
 CONSTRAINT [PK_RangeOfProduct] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[ReachUs]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ReachUs](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Heading] [nvarchar](100) NULL,
	[Description] [nvarchar](1000) NULL,
	[Contact] [nvarchar](200) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Image] [nvarchar](3000) NULL,
	[Fax] [nvarchar](100) NULL,
	[Latitude] [numeric](12, 6) NULL,
	[Longitude] [numeric](12, 6) NULL,
	[Title] [nvarchar](100) NULL,
 CONSTRAINT [PK_ReachUs] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[Services]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[Services](
	[ServId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [nvarchar](500) NULL,
	[Description] [nvarchar](4000) NULL,
	[Icon] [nvarchar](3000) NULL,
	[Image] [nvarchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_Services] PRIMARY KEY CLUSTERED 
(
	[ServId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[ServiceSpareParts]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[ServiceSpareParts](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Image] [varchar](1000) NULL,
	[Heading] [varchar](100) NULL,
	[Description] [varchar](3000) NULL,
	[DisplayOrder] [int] NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_ServiceSpareParts] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SocialMedia]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SocialMedia](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[opentime] [varchar](500) NULL,
	[CallUs] [varchar](500) NULL,
	[MailUs] [varchar](500) NULL,
	[Sales] [varchar](500) NULL,
	[Services] [varchar](500) NULL,
	[Purchasing] [varchar](500) NULL,
	[Dispatches] [varchar](500) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[Sales1] [varchar](200) NULL,
 CONSTRAINT [PK_SocialMedia] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SolAndTechDetailsTable]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolAndTechDetailsTable](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[MasterId] [int] NULL,
	[SubId] [int] NULL,
	[Heading] [varchar](200) NULL,
	[Description] [varchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Image] [nvarchar](3000) NULL,
 CONSTRAINT [PK_SolAndTechDetailsTable] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SolAndTechGalleryTable]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolAndTechGalleryTable](
	[GalleryId] [int] IDENTITY(1,1) NOT NULL,
	[Id] [int] NULL,
	[Title] [varchar](200) NULL,
	[Type] [varchar](100) NULL,
	[Image] [nvarchar](3000) NULL,
	[Url] [nvarchar](200) NULL,
	[ThumbImg] [varchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_SolAndTechGalleryTable] PRIMARY KEY CLUSTERED 
(
	[GalleryId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SolAndTechMasterTable]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolAndTechMasterTable](
	[MasterId] [int] IDENTITY(1,1) NOT NULL,
	[Title] [varchar](100) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
 CONSTRAINT [PK_SolAndTechMasterTable] PRIMARY KEY CLUSTERED 
(
	[MasterId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SolAndTechSubTable]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SolAndTechSubTable](
	[SubId] [int] IDENTITY(1,1) NOT NULL,
	[MasterId] [int] NULL,
	[Title] [varchar](100) NULL,
	[Slug] [nvarchar](100) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[DisplayOrder] [int] NULL,
 CONSTRAINT [PK_SolAndTechSubTable] PRIMARY KEY CLUSTERED 
(
	[SubId] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[SuperAdminLogin]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[SuperAdminLogin](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](50) NULL,
	[Username] [nvarchar](50) NULL,
	[Password] [nvarchar](50) NULL,
	[Emailid] [nvarchar](50) NULL,
	[Mobileno] [varchar](15) NULL,
	[AddedOn] [datetime] NULL,
	[IsActive] [bit] NULL,
	[Remark] [varchar](20) NULL,
	[UpdatedOn] [datetime] NULL,
	[LoginOn] [datetime] NULL,
	[LogoutOn] [datetime] NULL,
 CONSTRAINT [PK_SuperAdminLogin] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[TechnologyImageVideoCaption]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[TechnologyImageVideoCaption](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Type] [varchar](50) NULL,
	[Image] [varchar](500) NULL,
	[Video] [varchar](200) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[Title] [varchar](100) NULL,
 CONSTRAINT [PK_TechnologyImageVideoCaption] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Testimonials]    Script Date: 09/10/2019 1:03:08 PM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Testimonials](
	[Id] [int] IDENTITY(1,1) NOT NULL,
	[Name] [varchar](100) NULL,
	[Position] [varchar](100) NULL,
	[CompanyName] [varchar](100) NULL,
	[Description] [varchar](3000) NULL,
	[IsActive] [bit] NULL,
	[AddedOn] [datetime] NULL,
	[DeletedOn] [datetime] NULL,
	[UpdatedOn] [datetime] NULL,
	[DisplayOrder] [int] NULL,
 CONSTRAINT [PK_Testimonials] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
SET ANSI_PADDING OFF
GO
SET IDENTITY_INSERT [dbo].[AddNewPage] ON 

GO
INSERT [dbo].[AddNewPage] ([Id], [PageName], [BannerImage], [Description], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Green Solutions', N'AddNewPage-c35a6b1c-3846-4ff9-9b56-dce74f64daa7.jpg', N'<p>
	Every solution we provide is designed with a special impetus in controlling environmental emissions, increase productivity, improving quality, saving materials and at the same time consuming less energy.</p>
<h2 class="orangeColor">
	Eco-Friendly Solutions</h2>
<p>
	&quot;To reduce VOCs, manufacturers are constantly looking for solutions to minimize material wastage with more efficient equipment and automated processes. Patvin systems are equipped with advanced features that help you reduce your carbon footprints by controlling waste and environmental emissions.</p>
<p>
	For example in a Paint Circulation system, we provide precise flow and velocity at the lowest running energy costs.</p>
<p>
	With ecofriendly solutions, Patvin contributes to a more sustainable world.</p>
<h2 class="orangeColor">
	Resource Conservation</h2>
<p>
	One of the most important resources of nature is Energy. Every solution that we deliver counts high in energy efficiency as well as effective use and recovery of raw materials.</p>
<p>
	For example, our color change systems allow clever recovery of paint during the color change, saving paint and cleaning solvents/ Our pigging solutions have helped our customers recover significant amounts of the useable product and over 95% lower consumption of flushing solvent.</p>
<p>
	Not only does this contribute towards conserving resources but it also allows for substantial cost savings for our customers.</p>
<p>
	We can automate your industrial processes to achieve the above mentioned advantages. The accuracy of robots has substantially transformed the conventional fluid handling industry. Automation in fluid handling systems has proven to reduce material consumption by up to 30%. Increased control and access to amount of material used enable huge savings on material costs. Using plural component proportioners, that mix material close to the gun, and using automatic electrostatic guns that increase transfer efficiency, you can save material and reduce VOC emissions.</p>
<h2 class="orangeColor">
	Minimizing Scrap and Proper waste management</h2>
<p>
	With the use of innovative tools and automated processes, we reduce the amount of scrap produced in our manufacturing activities. We also follow an efficient waste management system that disposes waste in the most safe manner and focuses on maximum recycling of materials.</p>
', N'green-solutions', 1, CAST(0x0000AA1C000B4ADB AS DateTime), CAST(0x0000AA2501120F7A AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[AddNewPage] OFF
GO
SET IDENTITY_INSERT [dbo].[ApplyForJob] ON 

GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (1, N'test', N'test@mail.com', N'Resident Sales Engineer (Male)', N'Resident Sales Engineer (Male)', N'1542365866', N'ALL_ Analyst_Institutional_Investor_Meet_19_02_2019.pdf', 1, CAST(0x0000AA0800FEBAF7 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (2, N'test', N'test@mail.com', N'Resident Sales Engineer (Male)', N'Resident Sales Engineer (Male)', N'4512658985', N'ALL_ Analyst_Institutional_Investor_Meet_19_02_2019.pdf', 1, CAST(0x0000AA0800FF3423 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (3, N'test', N'test@mail.com', N'Resident Sales Engineer (Male)', N'Pune', N'4512655655', N'ALL_ Analyst_Institutional_Investor_Meet_19_02_2019.pdf', 1, CAST(0x0000AA0801002A60 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (4, N'test', N'test@mail.com', N'Resident Sales Engineer (Male)', N'Pune', N'4512356985', N'ALL_ Analyst_Institutional_Investor_Meet_19_02_2019.pdf', 1, CAST(0x0000AA080101E908 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (5, N'test', N'test@mail.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567895', N'ALL_ Analyst_Institutional_Investor_Meet_19_02_2019.pdf', 1, CAST(0x0000AA08010272E8 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (6, N'test', N'test@mail.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567892', N'Resume-932d3ac8-7345-4973-8642-2073a6cdf0b2.pdf', 1, CAST(0x0000AA080102E0D2 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (7, N'test', N'test@mail.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567895', N'Resume-bf741f01-d92e-41e5-bed0-0700f84f540d.pdf', 1, CAST(0x0000AA0900BCF6F1 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (8, N'Vinita Darole', N'vinita_darole@patvin.co.in', N'Resident Sales Engineer (Male)', N'Pune', N'9819085169', N'Resume-8b3bf525-76f0-4f6c-ba70-261883a78e1a.docx', 1, CAST(0x0000AA120050FCA0 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (9, N'Vinita Darole', N'vinita.ws@gmail.com', N'Resident Sales Engineer (Female)', N'Pune', N'9819085169', N'Resume-a63ef5f2-ff76-4019-bab2-5aeee14943ec.docx', 1, CAST(0x0000AA140007D548 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (10, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567895', N'Resume-12ca5d4f-d1e6-465e-b0c3-db72b7050069.pdf', 1, CAST(0x0000AA170176F9DE AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (11, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567895', N'Resume-c313f70e-e492-4ce0-95d5-4726055810b8.pdf', 1, CAST(0x0000AA170177AC6D AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (12, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Female)', N'Pune', N'1234567895', N'Resume-257e86da-580c-4eee-a5cc-d3ca05f3d9a4.pdf', 1, CAST(0x0000AA1701793211 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (13, N'rahul', N'rahul@kwebmaker.com', NULL, NULL, N'1234567895', N'Resume-116d76a8-a9ee-414e-904c-1c4d84f9df1c.pdf', 1, CAST(0x0000AA17017AB13D AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (14, N'rahul', N'rahul@kwebmaker.com', NULL, NULL, N'1234567895', N'Resume-850ea90c-2d61-4168-bbeb-76dcb587f7bf.pdf', 1, CAST(0x0000AA17017ADB3B AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (15, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567895', N'Resume-c2451802-4824-4e57-81bd-303546bcd52c.pdf', 1, CAST(0x0000AA17017B66DE AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (16, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Female)', N'Pune', N'1234567895', N'Resume-00de5daf-a05d-4b87-bb45-7e5969ee8d6e.pdf', 1, CAST(0x0000AA17017BD3B8 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (17, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567895', N'Resume-4d6727ea-50e7-4f65-8fd8-5e1ab6e737aa.pdf', 1, CAST(0x0000AA17017C1BBF AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (18, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Male)', N'Pune', N'1234567895', N'Resume-94264910-9bd9-4b58-b746-3b387b8906c1.pdf', 1, CAST(0x0000AA17017C2129 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (19, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Female)', N'Pune', N'1234567895', N'Resume-c77cb86e-ca98-4ed7-9b96-0316ab932fd3.pdf', 1, CAST(0x0000AA17017C75B2 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (20, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Female)', N'Pune', N'1234567895', N'Resume-35c7e159-2253-4e31-aa0e-dbce20ca56f3.pdf', 1, CAST(0x0000AA17017D566F AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (21, N'rahul', N'rahul@kwebmaker.com', N'Resident Sales Engineer (Female)', N'Pune', N'1234567895', N'Resume-2b17075d-ea25-48b2-b353-6c61d1c1e4b8.pdf', 1, CAST(0x0000AA17017D570E AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (22, N'Vinita Darole', N'vinita_darole@patvin.co.in', N'Resident Sales Engineer (Female)', N'Pune', N'9819085169', N'Resume-07d355a9-496d-4d5f-9c61-c3fdad0d13f9.docx', 1, CAST(0x0000AA170180BC52 AS DateTime), NULL)
GO
INSERT [dbo].[ApplyForJob] ([Id], [Name], [Email], [Role], [Location], [ContactNo], [Resume], [IsActive], [AddedOn], [DeletedOn]) VALUES (23, N'Vinita Darole', N'vinita_darole@patvin.co.in', N'Resident Sales Engineer (Female)', N'Pune', N'9819085169', N'Resume-c232111c-ede2-4ca7-b582-d2e38ecedeec.docx', 1, CAST(0x0000AA1E016A969C AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[ApplyForJob] OFF
GO
SET IDENTITY_INSERT [dbo].[Banner] ON 

GO
INSERT [dbo].[Banner] ([BannerId], [Image], [BannerText1], [BannerText2], [BannerText3], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (1, N'Banner-445dfd32-f27a-47f0-b316-faf2a7643da9.jpg', N'We provide Innovative', N'Industrial Solutions', N'Leadership position by innovation, delivering best value for customers.', 1, 1, CAST(0x0000A9EC0132A3D7 AS DateTime), CAST(0x0000AA11008765E2 AS DateTime), NULL, N'#industrialSolutions')
GO
INSERT [dbo].[Banner] ([BannerId], [Image], [BannerText1], [BannerText2], [BannerText3], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (2, N'Banner-40b1cb20-a04f-4ab9-b3c1-f30793037034.jpg', N'Reliable and competent', N'Customer Service', N'Rated high by customers on satisfaction there by receiving repeat business from all our customers.', 2, 1, CAST(0x0000A9EC01330C5C AS DateTime), CAST(0x0000AA11008667E9 AS DateTime), NULL, N'products#customerService')
GO
INSERT [dbo].[Banner] ([BannerId], [Image], [BannerText1], [BannerText2], [BannerText3], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (3, N'Banner-fab2ac75-e39b-49c5-855b-540bb0f3d1db.jpg', N'Customized Integrated ', N'Robotic Solutions', N'Automating your industrial processes with next generation technology.', 3, 1, CAST(0x0000A9EC0133D836 AS DateTime), CAST(0x0000AA250113343C AS DateTime), NULL, N'Technology/robotic-solutions')
GO
SET IDENTITY_INSERT [dbo].[Banner] OFF
GO
SET IDENTITY_INSERT [dbo].[CareerOurPeople] ON 

GO
INSERT [dbo].[CareerOurPeople] ([Id], [Image], [Name], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Position]) VALUES (1, N'CareerOurPeople-f1f50d97-4aaa-4e2b-b224-74ea720b22c8.jpg', N'Mahesh Kulkarni', N'My current role at Patvin is of a Sales Manager. I joined the company three years back. My typical day begins with a quick check on yesterday’s sales activities, active leads and numbers for the region to plan my actions for the day. The rest of my day includes meeting customers, understanding their requirements and sales reviews with my counterparts. Being able to challenge myself with targets, travel and meet interesting customers makes the job worthwhile. Patvin is a fast growing organization having immense growth opportunities for individuals under a visionary & caring leadership. A culture of innovation and constant improvement gives me new learning opportunities every day. ', 3, 1, CAST(0x0000AA0401467CDD AS DateTime), CAST(0x0000AA11017DFB8B AS DateTime), NULL, N'Sales Manager')
GO
INSERT [dbo].[CareerOurPeople] ([Id], [Image], [Name], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Position]) VALUES (2, N'CareerOurPeople-fcaf2caa-046d-4b1e-a221-06d96a44001d.jpg', N'Sachin Zope', N'As a Service engineer with Patvin, I am responsible for handling the Service and Spare requirements of existing customers, as well supporting refurbishments & modification projects.
I am also handling Product Demo’s at client end and Training & commissioning of newly installed systems. During these nine years of my service at Patvin, I have had the opportunity to deal with challenging tasks and situations. The unfailing support from my superiors and colleagues have made every challenging objective achievable. We have a strong service team and network that enables smooth service support. It is a great experience to be the front face to customers. In my role, I am happy to be contributing towards the most important objective of Customer satisfaction.', 5, 1, CAST(0x0000AA04014A35F7 AS DateTime), CAST(0x0000AA0F0182AC87 AS DateTime), NULL, N'Service Engineer')
GO
INSERT [dbo].[CareerOurPeople] ([Id], [Image], [Name], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Position]) VALUES (3, N'CareerOurPeople-b87cea50-a13a-4560-b750-98bcce1bd96e.jpg', N'Lata Nair', N'It''s been an enriching 11 years journey at Patvin. However, every day is a new learning day adding to your wealth of knowledge.
A good amount of hard work is put in to build a robust accounting system with proper accounting methods, policies and principles and high attention to accuracy.
Nevertheless, our constant endeavour is to keep improving ourselves. Patvin is a great place to be and has a fairly young crowd of professionals with high energy, enthusiasm and an open mind.
A good working environment, employee engagement, support from management, ownership of responsibilities and a dedicated team makes Patvin an ideal place to work. 
I love my job and feel positive about my contribution to the company at the highest level. It feels great to work at Patvin!!		
', 1, 1, CAST(0x0000AA0E0184BE30 AS DateTime), CAST(0x0000AA0E01870C8C AS DateTime), NULL, N'Accounts Manager')
GO
INSERT [dbo].[CareerOurPeople] ([Id], [Image], [Name], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Position]) VALUES (4, N'CareerOurPeople-8a8f2482-3a7f-474e-9329-011f2b048c81.jpg', N'Shashikant Katelia', N'I am working with Patvin since 1997 as the Head – Engineering & Design, Electrical and Automation Division. My role is a challenging one requiring effective leadership, designing effective engineering strategies, policies, processes and systems with a focus on adding value, reducing costs and making business improvements. I feel proud to have contributed to take Patvin to the leadership position by developing innovating solutions which are energy efficient and rate high in quality.

The best thing about my work is that every project comes in with a unique challenge and brings new learning. It is this diversity that makes working at Patvin really enjoyable. An outstanding team spirit further keeps the team bonded. We have a supportive team that’s works together to convert ideas into reality.
', 2, 1, CAST(0x0000AA0F0013B3BC AS DateTime), NULL, NULL, N'Head – Engineering & Design')
GO
INSERT [dbo].[CareerOurPeople] ([Id], [Image], [Name], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Position]) VALUES (5, N'CareerOurPeople-9eb1fda8-2347-4907-b2b7-a98845f3262c.JPG', N'Snehal Kamble', N'As an Inside Sales manager, my role allows me the opportunity to be in contact with people from different regions, not only within the organization, but also with our clients and partners from across the globe. I am surrounded by a diverse team; experts in their subjects, at the same time warm and approachable. It is an encouraging environment where good work and initiative is always appreciated.
From a humble beginning, Patvin has risen to a leadership position in its core area of business and has aspirations to soar higher as well in new areas. This is an exciting time for Patvin, when we are all geared for rapid growth and exciting new global initiatives. I look forward to contributing my best ahead with high spirit.
', 4, 1, CAST(0x0000AA0F018066D9 AS DateTime), CAST(0x0000AA1101808612 AS DateTime), NULL, N'Manager Inside Sales')
GO
SET IDENTITY_INSERT [dbo].[CareerOurPeople] OFF
GO
SET IDENTITY_INSERT [dbo].[CareerOverViewImages] ON 

GO
INSERT [dbo].[CareerOverViewImages] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'CareerOverViewImages-28b57523-1a8a-49d7-af41-769decf510ba.jpg', 1, 1, CAST(0x0000AA040155FAAB AS DateTime), CAST(0x0000AA0E002B2D4D AS DateTime), NULL)
GO
INSERT [dbo].[CareerOverViewImages] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'CareerOverViewImages-39202a53-c535-423b-bb4a-30535ddf436e.JPG', 2, 1, CAST(0x0000AA0401561356 AS DateTime), CAST(0x0000AA0E017A7081 AS DateTime), NULL)
GO
INSERT [dbo].[CareerOverViewImages] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'CareerOverViewImages-201435b6-a089-4b3f-bfb1-74f9cc72b37b.JPG', 3, 1, CAST(0x0000AA040156207E AS DateTime), CAST(0x0000AA1B004909AD AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[CareerOverViewImages] OFF
GO
SET IDENTITY_INSERT [dbo].[CareerWeBelieve] ON 

GO
INSERT [dbo].[CareerWeBelieve] ([Id], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Employee Development is not just about acquiring skills to solve specific problems, but also about addressing challenges and expanding perspectives.', 1, CAST(0x0000AA04016AC5DE AS DateTime), CAST(0x0000AA1C0182BF33 AS DateTime), NULL)
GO
INSERT [dbo].[CareerWeBelieve] ([Id], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'Continuing Personal Development is as much an obligation of all Employees as it is of Supervisors, Executives and Managers.', 1, CAST(0x0000AA04016B7947 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[CareerWeBelieve] ([Id], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'Building Human Competencies and Capacities is critical to our continued growth and success.', 1, CAST(0x0000AA04016B8952 AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[CareerWeBelieve] OFF
GO
SET IDENTITY_INSERT [dbo].[CaseStudies] ON 

GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (1, N'CaseStudies-9b725c3c-d992-4597-a142-abfa13bacb0a.jpg', N'Hydraulic Paint Circulation system', N'Four Wheeler OEM', 1, CAST(0x0000A9FF01149AB3 AS DateTime), CAST(0x0000AA0E016D5A27 AS DateTime), NULL, 1)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (2, N'CaseStudies-b7dc07c7-9c11-44f8-9db4-7503ebb94536.jpg', N'Wheel Strip Painting System ', N'Two Wheeler OEM', 1, CAST(0x0000AA06012883FD AS DateTime), CAST(0x0000AA0E00450D8C AS DateTime), NULL, 2)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (3, N'CaseStudies-57ad45b3-d292-4e82-828f-06f1fd84c039.jpg', N'Electrical Paint Circulation System', N'Four Wheeler OEM', 1, CAST(0x0000AA060128C80E AS DateTime), CAST(0x0000AA0E016D7A75 AS DateTime), NULL, 3)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (4, N'CaseStudies-7effe549-6c52-4023-83d6-6389b6975008.JPG', N'Two Component Paint System - 2K ', N'Heavy Earth Moving OEM', 1, CAST(0x0000AA0601291C4F AS DateTime), CAST(0x0000AA0E016EF328 AS DateTime), NULL, 4)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (5, N'CaseStudies-8d7d4941-0b68-4ce4-967b-5035ceca973a.jpg', N'Piggable Paint System', N'Two Wheeler OEM', 1, CAST(0x0000AA0E016D419A AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (6, N'CaseStudies-3771c6c8-df8e-4e7f-9974-33f264022691.jpg', N'Pneumatic Paint Circulation System', N'Two Wheeler OEM', 1, CAST(0x0000AA0E016E3EEA AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (7, N'CaseStudies-17d72a0f-e884-41f3-a997-5a5d9c7e9b6c.jpg', N'Bearing Hub Greasing System', N'Four Wheeler OEM', 1, CAST(0x0000AA0E016FB377 AS DateTime), CAST(0x0000AA0E016FCBB0 AS DateTime), NULL, 0)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (8, N'CaseStudies-2522e707-d573-4f67-929e-7082f536ebcb.JPG', N'Robotic Paint Application System', N'Two Wheeler OEM', 1, CAST(0x0000AA0E017030B2 AS DateTime), CAST(0x0000AA150160D79D AS DateTime), NULL, 0)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (9, N'CaseStudies-27272e34-a8cc-43cb-bb33-88b2ca4694bf.jpg', N'Robotic Glass Gluing Application', N'Commercial Vehicles OEM', 1, CAST(0x0000AA0E0171B285 AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (10, N'CaseStudies-bcca590c-73ef-4b8b-9b99-3f4b5bc0c6b3.jpg', N'Metered Oil Dispensing System', N'Four Wheeler OEM', 1, CAST(0x0000AA0E01725054 AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (11, N'CaseStudies-3e72be28-92a3-47b8-a403-c13248775596.JPG', N'Auto Color Change System', N'Four Wheeler OEM', 1, CAST(0x0000AA0E0172E748 AS DateTime), NULL, NULL, 0)
GO
INSERT [dbo].[CaseStudies] ([Id], [Image], [Heading], [Category], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (12, N'CaseStudies-15961474-3a69-43b4-a856-719973e4e420.jpg', N'Two component Paint System 2K', N'Automotive Tier 1 supplier', 1, CAST(0x0000AA0E017641EE AS DateTime), CAST(0x0000AA0E0177972A AS DateTime), NULL, 0)
GO
SET IDENTITY_INSERT [dbo].[CaseStudies] OFF
GO
SET IDENTITY_INSERT [dbo].[Casestudyform] ON 

GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (1, NULL, N'test', N'test', N'test@mail.com', 0, CAST(0x0000AA07012CD6CE AS DateTime), CAST(0x0000AA0D0019CC39 AS DateTime))
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (2, NULL, N'test', N'test', N'test@mail.com', 0, CAST(0x0000AA07012CECD8 AS DateTime), CAST(0x0000AA0D0019D04C AS DateTime))
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (3, NULL, N'test', N'test', N'test@mail.com', 0, CAST(0x0000AA0800DE9DB3 AS DateTime), CAST(0x0000AA0D0019C7BE AS DateTime))
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (4, NULL, N'test', N'test', N'test@mail.com', 0, CAST(0x0000AA0800DF2AEF AS DateTime), CAST(0x0000AA0D0019D46E AS DateTime))
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (5, NULL, N'test', N'test', N'test@mail.com', 0, CAST(0x0000AA0800E161B9 AS DateTime), CAST(0x0000AA0D0019C13C AS DateTime))
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (6, N'Paint Application System', N'test', N'test', N'test@mail.com', 0, CAST(0x0000AA0800E19411 AS DateTime), CAST(0x0000AA0D0019BBD7 AS DateTime))
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (7, N'Design and building of a Graco Pump Circulation Module for the Automotive Industry', N'test', N'test', N'test@mail.com', 1, CAST(0x0000AA08011E059C AS DateTime), NULL)
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (8, N'Paint Application System', N'test', N'test', N'test@mail.com', 1, CAST(0x0000AA08011E9683 AS DateTime), NULL)
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (9, N'Piggable Paint System', N'Vinita Darole', N'Patvin Engineering Private Limited', N'vinita_darole@patvin.co.in', 1, CAST(0x0000AA1800092119 AS DateTime), NULL)
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (10, N'Metered Oil Dispensing System', N'Vinita Darole', N'Patvin Engineering Private Limited', N'vinita.ws@gmail.com', 1, CAST(0x0000AA1E01694085 AS DateTime), NULL)
GO
INSERT [dbo].[Casestudyform] ([CaseId], [Title], [Name], [CompanyName], [Email], [IsActive], [AddedOn], [DeletedOn]) VALUES (11, N'Piggable Paint System', N'rahul', N'test', N'rahul@mail.com', 1, CAST(0x0000AA2A00F76521 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[Casestudyform] OFF
GO
SET IDENTITY_INSERT [dbo].[CMS] ON 

GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (1, N'World Class Solutions', N'<div class="container-fluid homeIntro">
	<div class="tagText titillium">
		Delivering <strong class="orangeColor">World Class Solutions</strong> Since 1987</div>
	<div class="homepartners_text">
		<p>
			Together with our partners, we&#39;re solving the most intricate challenges in our area of business. We bring out the value others fail to discover, we unlock innovation and simplify engineering.</p>
	</div>
</div>
', NULL, 1, CAST(0x0000A9ED011936BB AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (2, N'ABOUT PATVIN', N'<div class="title">
	<h4 class="blueColor">
		<strong>About Patvin</strong></h4>
	<h2 class="titillium">
		Your Partner For Quality.</h2>
</div>
<p>
	We deliver Turnkey systems for spray finishing, paint circulation, sealant, adhesives and lubricant dispensing to diverse markets including Automotives, Autocomponents, Aerospace, Pharmaceuticals, Industrials products, White goods and the FMCG industry.</p>
<p>
	Unwavering commitment to technical excellence and unparalleled service have been the decisive factors in elevating Patvin to the leading position that it enjoys today in core areas of business.</p>
<p>
	Continuing a history of innovation spanning more than three decades,&nbsp;and &nbsp;a future ready work force and organisation, Patvin continues to develop innovative and efficient solutions for complex applications within the operating domain. Patvin&#39;s vision is to innovate using frugal engineering to the best value and make complex manufacturing operations simple and effective.</p>
', NULL, 1, CAST(0x0000AA1F0055FF7E AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (3, N'Industrial Solutions', N'<p>
	The experience gained over the years and the consistent drive for technical innovations and improvements have helped to attain the apex in offering innovative solutions to diverse global markets.</p>
', NULL, 1, CAST(0x0000A9F10146468D AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (4, N'Founded', N'<p>
	Patvin is a leading supplier of Turnkey systems for spray finishing, paint circulation, sealant, adhesives and lubricant dispensing.</p>
<p>
	Whether small volume or large robotic systems, we are the first choice!</p>
<p>
	From design &amp; engineering to system integration, installation, training and service, Patvin offers complete solutions to customers from diverse industry segments. Our team includes expert engineering professionals, designers, programmers, service engineers, and technicians with broad experience in the field.</p>
<p>
	Founded in 1987, under the entrepreneurial vision of the Late Mr. Ivan D&rsquo;costa, Patvin&rsquo;s story has since then been a story of success, growth and dynamism. A highly committed, resourceful &amp; target oriented organisation, Patvin has grown to be a trusted name in the industry.&nbsp;</p>
<p>
	With a deep focus on innovation and investing 3% of our revenues into research and development, we develop advanced solutions of outstanding quality and efficiency.</p>
<p>
	We strive to bring out the value others fail to discover. We Ideate, Innovate and Accomplish!</p>
<p>
	We are Patvin. Your partner for Quality.</p>
', NULL, 1, CAST(0x0000AA15002EB742 AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (5, N'Founded Image Text', N'<div>
	Engineering company with 120 Employees.</div>
<div>
	Service Automotive companies &amp; tier 1 markets.</div>
<div>
	We specialize in Paint &amp; Sealant Application Equipment.</div>
<div>
	Inventory of parts &amp; spares.</div>
<div>
	Dedicated Service Network across the company.</div>
', NULL, 1, CAST(0x0000AA11003D4B5C AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (6, N'Our Values', N'<p>
	Our values form the roots of our company culture. They guide the way we treat our business partners, our communities and each other.</p>
<p>
	Our values place our customers at the heart of everything we do. They drive our commitment to constant improvement and innovation, efficiency, passion to deliver the best and be a reliable partner to our clients.</p>
', NULL, 1, CAST(0x0000A9F800D5C4B2 AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (7, N'Health & Safety', N'<h2 class="orangeColor">
	Patvin Health, Safety &amp; Environment (HSE) Policy</h2>
<p>
	It is the philosophy of Patvin that accidents/injuries are preventable and unacceptable. Patvin Engineering Private Limited has therefore integrated safety into its business and is vigorously pursuing all accident prevention programs through it&rsquo;s well-structured and effective HSE Systems.</p>
<p>
	Patvin activities have therefore been organized, planned and executed in such a manner as to:</p>
<ul>
	<li>
		Protect and promote the health of its work force as well as the conduct of its activities in such a manner not to adversely affect any third party (host Community inclusive).</li>
	<li>
		Avoid injury to any workers, sub-contractors, and third parties who are involved in Patvin activities.</li>
	<li>
		Ensure the personal security of the work force and third parties and the security of property.</li>
	<li>
		Minimize the impact on the environment in which Patvin operates.</li>
</ul>
<p>
	Every employee of Patvin is performing his/her work in accordance with this policy and work must be suspended when it is believed that essential safety systems are not in place. Management and Supervisors are accountable for the safety of the employees working under their supervision, and are expected to organize and conduct operations in a safe manner at all times.</p>
<p>
	Employees are reminded that they have a duty underlay to take reasonable care for their own safety and safety of others who may be affected by their acts or omissions and also to co-operate with statutory safety obligations which include adherence to company HSE Policy, rules and regulations. The implementation, of this policy is the responsibility of supervisors under delegated control and co-ordination of HSES Manager.</p>
<h2 class="orangeColor">
	HSE STRATEGIC PLAN AND OBJECTIVE</h2>
<p>
	Our goal is simple &ndash; to achieve HSE excellence by providing safe workplaces supported by a culture which ensures that the safety of people and protection of the environment is an absolute priority. We hold the principle that the best solution for the management of Health Safety Environment is also the best business solution for all stakeholders across Patvin Engineering Private Limited. We remain committed to measuring and constantly improving in our Health Safety Environment performance through the implementation of the Patvin&#39;s Health Safety Environment Strategic Plan.</p>
<p>
	The core objectives of Patvin&#39;s HSE Strategic Plan include:</p>
<p>
	<strong class="blueColor">PRIORITY AREA 1 : Systems and Processes</strong></p>
<p>
	Strategic Objective: Consolidation of systems and processes to drive operational excellence in the management of health and safety risks or environmental impacts.</p>
<p>
	<strong>Key Actions:</strong></p>
<ul>
	<li>
		Further implement the Patvin standardized HSE Management System (MS) in all spheres of the company</li>
	<li>
		Monitor compliance with the Patvin HSE MS, and related policies, procedures and initiatives.</li>
	<li>
		Continue random audit sampling of HSE MS implementation.</li>
	<li>
		Implement harmonized worker health and safety legislation across the Patvin HSE MS and related policies, procedures, forms and guidelines.</li>
	<li>
		Continue a program of review of the accuracy and currency of HSE MS information in accordance with existing document identification, retention and control procedures.</li>
	<li>
		Evaluate and further develop programs to enhance HSE awareness, involvement and compliance for office environments.</li>
</ul>
<p>
	<strong class="blueColor">PRIORITY AREA 2 : Train, Support and Motivate</strong></p>
<p>
	Strategic Objective: Train, support and motivate people to identify and manage workplace safety, risks or environmental impacts.</p>
<p>
	<strong>Key Actions:</strong></p>
<ul>
	<li>
		Implement HSE roles and responsibilities and accountabilities for all key positions charged with HSE responsibilities.</li>
	<li>
		Target training needs to roles and responsibilities at all levels.</li>
	<li>
		Maintain the HSE learning and development program to include:
		<ul class="decimal">
			<li>
				HSE induction for all new staff on Day 1.</li>
			<li>
				Implement due diligence training in the HSE MS for directors and officers.</li>
		</ul>
	</li>
</ul>
<p>
	<strong class="blueColor">PRIORITY AREA 3 : Performance Data &amp; Analysis</strong></p>
<p>
	Strategic Objective: Monitor the effectiveness of control strategies and the accuracy of HSE data to respond to significant incidents, detect recurring trends and learn from experience.</p>
<p>
	<strong>Key Actions:</strong></p>
<ul>
	<li>
		Continue implementation of the HSE incident reporting system within an overall framework of Risk Management to monitor the accuracy, timeliness and reporting of HSE incidents.</li>
	<li>
		Expand incident investigation training as required, to further stress root cause analysis.</li>
	<li>
		Expand the HSE incident reporting system to include proactive reporting of hazards and risk register tracking.</li>
	<li>
		Evaluate improvement outcomes against HSE objectives and targets, and seek continual improvement through annual review of objectives and targets benchmarked against industry sector performance.</li>
	<li>
		Monitor workplace hazard and incident reporting to identify trends and proactive interventions.</li>
</ul>
<h2 class="orangeColor">
	ENVIRONMENTAL PROTECTION POLICY</h2>
<p>
	All operations of Patvin Engineering Private Limited are planned and executed in such a way as to minimize any adverse effects or impact on the environment in which we work. Patvin Engineering Private Limited is promoting and encouraging plans, activities, and programes aimed at environmental preservation and protection. Staffs are encouraged to pay appropriate regard to the environment by protecting it from adverse effects. We are collecting and disposing all wastes generated in the course of our operations in an environmentally sound manner. Discharge of pollutants into the air, or in water bodies is strictly prohibited. All such disregard to environmental laws has been reported immediately. &quot;The Environment is our common home, we must protect it&quot;.</p>
<h2 class="orangeColor">
	SAFETY COMPLIANCE/ IMPLEMENTATION</h2>
<p>
	The foremost interest of Patvin Engineering Private Limited in all projects is Safety and Environmental protection; and we are demonstrating Safety in all our operations. We are already conversant with Client safety policy and standards, which has been adhered to in conjunction with our own safety policy in the execution of our contract services.</p>
<p>
	We have familiarized ourselves with the requirements for standards applicable to our Project Management and incorporating all standards into our operations. Patvin provides all necessary safety clothing and equipment for all personnel. We ensure that persons engaged in work and services in all contracts are medically fit throughout the duration of the contracts. We are prepared to present certificates of medical fitness on all personnel and ensure that all personnel are Client certified swimmers.</p>
<p>
	Patvin Engineering Privated Limited engages the services of a safety manager and a project manager who are ultimately responsible for safety in our operations. We have experienced safety officers who are well acquainted with Client&#39;s safety regulations and ensure that all proper work permits and complementary certificates are obtained prior to commencement of work and keeping them up to date daily and ensure that all safe working practices&nbsp; are observed during the execution of all contracts.</p>
<p>
	We strictly enforce the permit to work system before commencement of any work. Only Client&#39;s authorizations can action any night movement. Patvin Engineering Private Limited has a Workmen&#39;s compensation insurance policy for their workers against any occupational disease or any other impairment to health or injury by accident.</p>
', NULL, 1, CAST(0x0000AA18001D8EFF AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (8, N'Patvin represents', N'<p>
	Patvin represents <strong class="blueColor text-uppercase d-block">leading global manufacturers as their official distributor in India</strong> for supplying Industrial equipment including Paint applicators, Pumps, Spray packages, Plural component equipment, Valves, Speciality Adapters, Industrial Cobots and related accessories.</p>
', NULL, 1, CAST(0x0000AA24002498DE AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (9, N'Career Overview', N'<div class="col-lg-6">
	<div class="careerFirst">
		<div class="title">
			<h4>
				Life at Patvin is</h4>
			<h2 class="titillium orangeColor">
				Challenging, Fulfilling and Exciting!</h2>
		</div>
		<p>
			Patvin offers access to world-class resources for personal and professional growth.</p>
		<p>
			At Patvin, you&#39;ll have the chance to take on challenging responsibilities, working with top-notch, professionals from around the country. You will be part of a culture of excellence. You would not be just working for a living; you will be part of a global team that&#39;s focused on making a difference in the everyday lives of people.</p>
		<p>
			People are central to Patvin&#39;s growth strategy. <strong class="blueColor">People are our strength and pride.</strong></p>
		<p>
			Fundamental to the work at Patvin and to its competitive position is respect for development of employees.</p>
	</div>
</div>
', NULL, 1, CAST(0x0000AA06012D6DEB AS DateTime))
GO
INSERT [dbo].[CMS] ([CmsId], [Heading], [Description], [Url], [IsActive], [UpdatedOn]) VALUES (10, N'Management Overview', N'<p>
	Patvin engineering Private Limited is engaged in a diverse portfolio of businesses.</p>
<p>
	The company was founded in 1987 by Mr. Ivan D&rsquo;Costa, as a distributor of Fluid handling &amp; dispensing products and components for Graco Ms. Since then, the business has diversified into integration of systems for Paint circulation, Sealant systems, Robotics/Manual systems for Sealant/Paint application catering to the automotive and other industries. The company is now foraying into allied as well as diverse segments and markets that will further fortify its offering. The company is managed by a Team of highly-motivated, high performing professionals.&nbsp;</p>
<p>
	The management team operates on a clear philosophy - creating a culture of technical excellence within the company and delivering best value for customers. We aim for a leaner, flatter and adaptable enterprise, geared to work smarter and respond quickly to changing market dynamics. Passion and commitment towards excellence are rewarded. We foster long standing relations with our customers, stakeholders and employees.</p>
', N'Management', 1, CAST(0x0000AA1701889DA1 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[CMS] OFF
GO
SET IDENTITY_INSERT [dbo].[ContactUs] ON 

GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (1, N'test', N'1234567894', N'test@mail.com', NULL, N'test', 1, CAST(0x0000AA08010DD24F AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (2, N'test', N'1234567895', N'test@mail.com', N'Supplier', N'test', 1, CAST(0x0000AA0900C3D0EE AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (3, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Sales Inquiry', N'Test', 1, CAST(0x0000AA100048B628 AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (4, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Supplier', N'Test', 1, CAST(0x0000AA1400429380 AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (5, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Service related', N'Testing the new website,  pls let me know when this email is received.', 1, CAST(0x0000AA1400430D2B AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (6, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Service related', N'Testing the new website,  pls let me know when this email is received.', 1, CAST(0x0000AA1400431929 AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (7, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Supplier', N'Testing the website, please let me know when this email reaches you.', 1, CAST(0x0000AA14018A96CB AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (8, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Service related', N'Testing the website, please let me know when this email reaches you.', 1, CAST(0x0000AA14018AC3DB AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (9, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Dispatch related', N'Testing the new website, please forward mail to vinita_darole@patvin.co.in when received ', 1, CAST(0x0000AA18001FFF69 AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (10, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Service related', N'Testing the new website. Please forward mail to vinita_darole@patvin.co.in when received.', 1, CAST(0x0000AA1800207CBC AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (11, N'Mangesh Pawar', N'9820534427', N'mangesh7375@gmail.com', N'Sales Inquiry', N'Test', 1, CAST(0x0000AA180043F58F AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (12, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Dispatch related', N'Testing the new website. Pls forward this mail to vinita_darole@patvin.co.in when received. ', 1, CAST(0x0000AA1E01644EC7 AS DateTime), NULL)
GO
INSERT [dbo].[ContactUs] ([Id], [Name], [ContactNo], [Email], [Subject], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (13, N'rahul', N'1234567893', N'rahul@gmail.com', N'Sales Inquiry', N'testing', 1, CAST(0x0000AA2A00B9843B AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[ContactUs] OFF
GO
SET IDENTITY_INSERT [dbo].[ContactUsMap] ON 

GO
INSERT [dbo].[ContactUsMap] ([Id], [Latitude], [Longitude], [Icon], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Title]) VALUES (1, CAST(18.979607 AS Numeric(12, 6)), CAST(72.830580 AS Numeric(12, 6)), N'Pointer-5d439ca3-94fb-4d30-93e9-7ebbc92bb08b.png', N'<h1>
	Patvin Engineering Pvt. Ltd.</h1>
<h2>
	(Registered Office)</h2>
<p>
	106, Veena Killedar Industrial Estate,<br />
	10/14, Pais Street Byculla (w),<br />
	Mumbai &ndash; 400011</p>
', 1, CAST(0x0000AA0600E40D05 AS DateTime), CAST(0x0000AA0600E55D79 AS DateTime), NULL, N'HEAD OFFICE')
GO
SET IDENTITY_INSERT [dbo].[ContactUsMap] OFF
GO
SET IDENTITY_INSERT [dbo].[CurrentOpening] ON 

GO
INSERT [dbo].[CurrentOpening] ([Id], [Title], [Location], [Post], [Qualification], [Experience], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Resident Sales Engineer (Male)', N'Pune', N'1', N'BE & Dipl. Mechanical', N'3 to 7 yrs ', N'Experience in Selling of Engineering Capital Goods, should be well versed with generating leads, Business Development etc. Will be responsible for Client servicing & Revenue Collection. Presentation Skill. Will operate from his residence. Not pursuing any education/ Course.', 1, CAST(0x0000AA0401380872 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[CurrentOpening] ([Id], [Title], [Location], [Post], [Qualification], [Experience], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'Resident Sales Engineer (Female)', N'Pune', N'1', N'BE & Dipl. Mechanical', N'3 to 7 yrs ', N'Experience in Selling of Engineering Capital Goods, should be well versed with generating leads, Business Development etc. Will be responsible for Client servicing & Revenue Collection. Presentation Skill. Will operate from his residence. Not pursuing any education/ Courses.', 1, CAST(0x0000AA0401384A29 AS DateTime), CAST(0x0000AA04013A8E2E AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[CurrentOpening] OFF
GO
SET IDENTITY_INSERT [dbo].[GetInTouchForm] ON 

GO
INSERT [dbo].[GetInTouchForm] ([Id], [Name], [ContactNo], [Email], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (1, N'test', N'1234567895', N'test@mail.com', N'hello', 1, CAST(0x0000AA0801554BF8 AS DateTime), NULL)
GO
INSERT [dbo].[GetInTouchForm] ([Id], [Name], [ContactNo], [Email], [Comment], [IsActive], [AddedOn], [DeletedOn]) VALUES (2, N'Vinita Darole', N'9819085169', N'vinita_darole@patvin.co.in', N'Testing the new website. Pls forward this mail to vinita_darole@patvin.co.in when received.', 1, CAST(0x0000AA1B01635B59 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[GetInTouchForm] OFF
GO
SET IDENTITY_INSERT [dbo].[HomeGreenWorld] ON 

GO
INSERT [dbo].[HomeGreenWorld] ([Id], [Descrption], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Patvin''s innovative solutions help customers save energy with products and solutions that are highly efficient and consume less power.', N'HomeIndustries-273aeed1-c0bd-400e-9b6b-266ac471e71d.jpg', 1, CAST(0x0000A9F20123F6BF AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[HomeGreenWorld] OFF
GO
SET IDENTITY_INSERT [dbo].[HomeIndustries] ON 

GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Automotive & Transportation', N'HomeIndustries-3ed44979-3dc5-4950-8022-6f2a906843b4.png', N'/Industries/automotive-transportation', 1, CAST(0x0000A9F200D138B5 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'Auto Components', N'HomeIndustries-d8bd7de1-e74f-463d-ac92-ef75e7c54832.png', N'/Industries/auto-components', 1, CAST(0x0000A9F200D4DEE8 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'Pharmaceutical', N'HomeIndustries-68d3a2d6-d723-48e2-8a9d-10c8b1913f78.png', N'/Industries/pharmaceutical', 1, CAST(0x0000A9F200D4E0CE AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'cz', NULL, N'zccgdg', 0, CAST(0x0000A9F500DA5B65 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'dad', N'HomeIndustries-790847be-232b-40d5-a54b-a8aabbca9107.jpg', N'adda', 0, CAST(0x0000A9F500DCB77F AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (6, N'Aerospace', N'HomeIndustries-3e81c8e5-8d0d-41d7-8191-4047018d042a.png', N'/Industries/aerospace', 1, CAST(0x0000AA100180CEA6 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (7, N'White Goods', N'HomeIndustries-ae94fc2e-6d9b-4b20-9807-b30e2ee2377c.png', N'/Industries/white-goods', 1, CAST(0x0000AA1001815681 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (8, N'Industrial Products', N'HomeIndustries-3c71e35e-517c-4598-853c-b7e8287964ee.png', N'/Industries/industrial-products', 1, CAST(0x0000AA1001817D35 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeIndustries] ([Id], [Heading], [Icon], [Url], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (9, N'FMCG', N'HomeIndustries-d853b84d-a50a-4ee1-971b-5347d02e9429.png', N'/Industries/fmcg', 1, CAST(0x0000AA100181AE00 AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[HomeIndustries] OFF
GO
SET IDENTITY_INSERT [dbo].[HomeProductSolutions] ON 

GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Paint Circulation Systems', N'Our paint systems provide precise flow, velocity and temperature, to contribute to outstanding paint finishes.', N'solutions/paint-circulation-systems', N'HomeProductSolutions-3cb50d1a-e089-4c2f-8556-f68ac6fba924.jpg', 1, CAST(0x0000AA0601108512 AS DateTime), CAST(0x0000AA2500FC936F AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'Sealant and Adhesive Systems', N'Smart and superior technology for handling a wide variety of viscous materials', N'Solutions/sealant-and-adhesive-systems', N'HomeProductSolutions-9f9697e0-f859-4e6d-b237-87a107a4107c.JPG', 1, CAST(0x0000AA11001571ED AS DateTime), CAST(0x0000AA2500FCC945 AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'Two Component Paint System - 2K System', N'The system delivers a superior finish quality with an accurate ratio assurance.', N'Solutions/two-component-paint-system-2k-system', N'HomeProductSolutions-3bf994cd-3e16-4068-aa1f-a1a80446b789.jpg', 1, CAST(0x0000AA11002DF8CD AS DateTime), CAST(0x0000AA2501037F92 AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'Paint Application System', N'Ensure highest levels of production efficiency, operating flexibility and superior performance', N'Solutions/paint-application-systems', N'HomeProductSolutions-10b76dff-20e1-4652-ae02-d6834fb295d0.JPG', 1, CAST(0x0000AA110030AAB1 AS DateTime), CAST(0x0000AA250103AB26 AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'Color Change System', N'Fast reliable, upgradeable and user friendly, our color change systems can be used with most automatic and manual', N'Solutions/color-change-systems', N'HomeProductSolutions-98de50b9-2570-4171-a138-386ed0bf855c.JPG', 1, CAST(0x0000AA14007B461C AS DateTime), CAST(0x0000AA250103C28A AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (6, N'Automation in Sealant Adhesives', N'Next generation robotic sealing applications for higher efficiency and precision.', N'Solutions/automation-in-sealant-adhesive', N'HomeProductSolutions-3ba4efd4-4ae5-4e52-93ee-dc587d0b6a4a.jpg', 1, CAST(0x0000AA1500263F41 AS DateTime), CAST(0x0000AA250103EDEC AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (7, N'NVH Applications', N'First Time / Every time Correct Ratio and Volume, Repeatibility, Reduction in Scrap and On -Ratio Dispensing.', N'Solutions/nvh-application-systems', N'HomeProductSolutions-7324f59b-22b6-482d-a8d7-58a0933dfbfe.JPG', 1, CAST(0x0000AA150028A4DF AS DateTime), CAST(0x0000AA2501040814 AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (8, N'Lubrication System', N'Automatic material filling with level monitor & warning and handling high volume to extremely low volume feed', N'Solutions/lubrication-systems', N'HomeProductSolutions-4e4a68b8-2169-4d62-92ca-8c9ce0c5023f.jpg', 1, CAST(0x0000AA15002AD185 AS DateTime), CAST(0x0000AA250104204F AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (9, N'Piggable Paint System', N'Our pigging solutions have helped our customers recover significant amounts of the usable product, over 95%', N'Solutions/piggable-paint-systems', N'HomeProductSolutions-7665b5d1-4638-4e54-a522-7ffed58e967b.jpg', 1, CAST(0x0000AA15002D546C AS DateTime), CAST(0x0000AA2501043794 AS DateTime), NULL)
GO
INSERT [dbo].[HomeProductSolutions] ([Id], [Title], [Description], [Url], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (10, N'UR Cobots', N'Patvin partners with Universal Robots to provide Collaborative robots (Cobots) for a range of industrial applications. ', N'Solutions/ur-cobots', N'HomeProductSolutions-4aff56db-6a44-43b6-9d9c-92156183eaa9.jpg', 1, CAST(0x0000AA15002E23FA AS DateTime), CAST(0x0000AA2501044DF1 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[HomeProductSolutions] OFF
GO
SET IDENTITY_INSERT [dbo].[HomeSolutions] ON 

GO
INSERT [dbo].[HomeSolutions] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'HomeSolution-20cde123-4e0c-4a8f-83f4-d3348f47c7b0.png', 3, 1, CAST(0x0000A9F100FF6EBD AS DateTime), CAST(0x0000AA1400110F2A AS DateTime), NULL)
GO
INSERT [dbo].[HomeSolutions] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'HomeSolution-7f7986fd-5d86-4e79-a0a0-8fa9af2f2544.png', 1, 1, CAST(0x0000A9F100FFC44D AS DateTime), CAST(0x0000AA140010E855 AS DateTime), NULL)
GO
INSERT [dbo].[HomeSolutions] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'HomeSolution-06521164-ff03-4644-87c2-88e5dd6d0789.png', 4, 1, CAST(0x0000A9F100FFD8CA AS DateTime), CAST(0x0000AA14001119E0 AS DateTime), NULL)
GO
INSERT [dbo].[HomeSolutions] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'HomeSolution-cc17e8a1-01d1-426f-99d0-35c1c77c7337.png', 1, 0, CAST(0x0000A9F10100158B AS DateTime), NULL, NULL)
GO
INSERT [dbo].[HomeSolutions] ([Id], [Image], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'HomeSolution-e2786f41-7db2-484c-b128-aa0a3c96afe8.png', 2, 1, CAST(0x0000AA100183046C AS DateTime), CAST(0x0000AA140010FE8B AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[HomeSolutions] OFF
GO
SET IDENTITY_INSERT [dbo].[HomeStatistic] ON 

GO
INSERT [dbo].[HomeStatistic] ([Id], [Icon], [Counting], [Description], [AddedOn], [UpdatedOn], [DeletedOn], [IsActive]) VALUES (1, N'fa fa-users', N'100', N'Engineers & Workers
', CAST(0x0000A9F1013D8A01 AS DateTime), CAST(0x0000A9F1013EF9FE AS DateTime), NULL, 1)
GO
INSERT [dbo].[HomeStatistic] ([Id], [Icon], [Counting], [Description], [AddedOn], [UpdatedOn], [DeletedOn], [IsActive]) VALUES (2, N'fas fa-business-time', N'30', N'Years of Experience', CAST(0x0000A9F1013F2DDD AS DateTime), CAST(0x0000A9F101429ECD AS DateTime), NULL, 1)
GO
INSERT [dbo].[HomeStatistic] ([Id], [Icon], [Counting], [Description], [AddedOn], [UpdatedOn], [DeletedOn], [IsActive]) VALUES (3, N'fas fa-toolbox', N'500', N'Products in Stock', CAST(0x0000A9F1013F913E AS DateTime), NULL, NULL, 1)
GO
INSERT [dbo].[HomeStatistic] ([Id], [Icon], [Counting], [Description], [AddedOn], [UpdatedOn], [DeletedOn], [IsActive]) VALUES (4, N'fas fa-award', N'1000', N'Successful Projects', CAST(0x0000A9F1013FFFBB AS DateTime), CAST(0x0000A9F10144831A AS DateTime), NULL, 1)
GO
SET IDENTITY_INSERT [dbo].[HomeStatistic] OFF
GO
SET IDENTITY_INSERT [dbo].[InfrastructureFacilities] ON 

GO
INSERT [dbo].[InfrastructureFacilities] ([Id], [Image], [Heading], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'InfrastructureFacilities-0313aa16-78f6-430a-a885-6e5988de7b4f.jpg', N'Design Centre', 0, 0, CAST(0x0000A9F500E8D832 AS DateTime), CAST(0x0000A9F500E8F236 AS DateTime), CAST(0x0000A9F500E8F52A AS DateTime))
GO
INSERT [dbo].[InfrastructureFacilities] ([Id], [Image], [Heading], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'InfrastructureFacilities-edea02ca-15f6-4d92-a089-6a077aded1da.JPG', N'Design Centre', 0, 1, CAST(0x0000A9F500E8FCE5 AS DateTime), CAST(0x0000AA10002DBB87 AS DateTime), NULL)
GO
INSERT [dbo].[InfrastructureFacilities] ([Id], [Image], [Heading], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'InfrastructureFacilities-3fd4d7ed-a434-4c81-a840-0a9b0aeb739a.JPG', N'Engineering and Projects Facility', 0, 1, CAST(0x0000A9F501018B9F AS DateTime), CAST(0x0000AA10003F4F48 AS DateTime), NULL)
GO
INSERT [dbo].[InfrastructureFacilities] ([Id], [Image], [Heading], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'InfrastructureFacilities-49cb6956-4f49-4e2e-8995-b640c9e6a7b8.JPG', N'Manufacturing Unit', 0, 1, CAST(0x0000AA100034B769 AS DateTime), CAST(0x0000AA10003CAF6C AS DateTime), NULL)
GO
INSERT [dbo].[InfrastructureFacilities] ([Id], [Image], [Heading], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'InfrastructureFacilities-d1fcd8fb-d518-42f8-abd9-9e50b4a54b9a.JPG', N'Demonstration & Development Lab', 0, 1, CAST(0x0000AA10003517D4 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[InfrastructureFacilities] ([Id], [Image], [Heading], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (6, N'InfrastructureFacilities-affd689e-5d24-4b0a-8094-b9c559fb455b.JPG', N'Training Centre', 0, 1, CAST(0x0000AA10003F0677 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[InfrastructureFacilities] ([Id], [Image], [Heading], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (7, N'InfrastructureFacilities-53ffefeb-7866-4b97-86bc-48f7ec876a35.jpg', N'Service Centre', 0, 1, CAST(0x0000AA10003F37A5 AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[InfrastructureFacilities] OFF
GO
SET IDENTITY_INSERT [dbo].[InnerBanner] ON 

GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'InnerBanner-739ee7a6-4a0b-41ea-afbd-f2eeb86acada.jpg', N'Who We Are', 1, CAST(0x0000A9ED01079BEF AS DateTime), CAST(0x0000AA1400138F3B AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'InnerBanner-5b45d321-f42a-46e9-9806-0ab37fa064fe.JPG', N'Our Values', 1, CAST(0x0000AA0900D5FD54 AS DateTime), CAST(0x0000AA100179A14C AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'InnerBanner-b9dd8534-b18c-45f9-b199-66d49a5ddfdd.jpg', N'Health & Safety', 1, CAST(0x0000AA0900D64CEA AS DateTime), CAST(0x0000AA1001790CFE AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'InnerBanner-805782de-f61b-4721-96eb-808f87ee880c.jpg', N'Products', 1, CAST(0x0000AA0900D66619 AS DateTime), CAST(0x0000AA1B004817C0 AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'InnerBanner-19a4d6bb-84ca-4698-a08d-9415d59dc386.jpg', N'Case Studies', 1, CAST(0x0000AA0900D67EF3 AS DateTime), CAST(0x0000AA110177802F AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (6, N'InnerBanner-65138dfb-850b-4080-99e5-c74663b4e0b5.jpg', N'Services', 1, CAST(0x0000AA0900D690E2 AS DateTime), CAST(0x0000AA1001852774 AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (7, N'InnerBanner-3dd2a981-caf7-41cd-bc99-92beaa724092.jpg', N'Newsroom', 1, CAST(0x0000AA0900D6A788 AS DateTime), CAST(0x0000AA150168154B AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (8, N'InnerBanner-254ae1d9-e6a4-42a7-a253-fb32cfd69700.jpg', N'Careers', 1, CAST(0x0000AA0900D6BC9D AS DateTime), CAST(0x0000AA0E002A25FD AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (9, N'InnerBanner-33abb5f0-c652-473c-9535-83974e245d26.JPG', N'Contact Us', 1, CAST(0x0000AA0900D6CDB4 AS DateTime), CAST(0x0000AA140188FC70 AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (10, N'InnerBanner-af945e3a-d13e-480b-a3ee-31565233157b.jpg', N'Search', 1, NULL, CAST(0x0000AA17016E7362 AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (11, N'InnerBanner-aa8f5a7f-45e5-43c3-a6c2-eb47f53f071d.jpg', N'Green Solutions', 1, CAST(0x0000AA11006C3AE1 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (12, N'InnerBanner-28a2c072-0ce5-45d4-a850-293069f42e83.jpg', N'Management', 1, NULL, CAST(0x0000AA150166715C AS DateTime), NULL)
GO
INSERT [dbo].[InnerBanner] ([Id], [Image], [PageName], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (14, N'InnerBanner-dae5181d-1763-4507-8084-6b4867f03f8e.jpg', N'Testimonials', 1, NULL, CAST(0x0000AA150161D14F AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[InnerBanner] OFF
GO
SET IDENTITY_INSERT [dbo].[Management] ON 

GO
INSERT [dbo].[Management] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Designation]) VALUES (1, N'Management-7ff42385-0f00-4724-8962-f9bdca02a939.jpg', N'testing111', N'testing111', 1, 0, CAST(0x0000AA120117DD82 AS DateTime), CAST(0x0000AA12011877BB AS DateTime), CAST(0x0000AA1201187ECB AS DateTime), NULL)
GO
INSERT [dbo].[Management] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Designation]) VALUES (2, N'Management-e71ec8c3-9c08-4d04-8fb1-f0375651ebd4.jpg', N'Gavin D‘costa', N'Mr. Gavin D‘costa is the Managing Director of Patvin. A visionary and a strategist, his insightful guidance has helped Patvin retain its leadership position amidst the growing competition. He drives the company objectives and is responsible for managing the entire Patvin operation with special focus towards customer satisfaction. He holds a degree in Mechanical Engineering and an advanced training in Robotics with M/s Carl Cloos GmbH, Germany. He has over two decades of rich industrial experience.', 1, 1, CAST(0x0000AA120118999C AS DateTime), CAST(0x0000AA17017F3C9A AS DateTime), NULL, N'Managing Director')
GO
INSERT [dbo].[Management] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Designation]) VALUES (3, N'Management-6ffdc280-2bd6-4b31-bd55-07433bd56c37.jpg', N'Mangesh Pawar', N'A veteran of the engineering industry, Mr. Mangesh Pawar leads the business as the Vice President. A  Mechanical engineer by qualification, Mr. Pawar brings a diverse experience of more than 23 years directing key functions including Sales & Marketing, Service and Operations. With an incessant commitment towards Quality, Productivity and Delivery Excellence, he has been instrumental in taking the company’s vision forward since 1996.', 3, 1, CAST(0x0000AA14003E67C8 AS DateTime), CAST(0x0000AA180024A4E2 AS DateTime), NULL, N'Vice President')
GO
INSERT [dbo].[Management] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Designation]) VALUES (4, N'Management-4bbeb41c-d2a8-4d0f-aaf6-2dfe065b044c.jpg', N'Vinita D’Costa', N'As the Finance Director, Mrs. Vinita D’Costa is responsible for Business control, Financial management and the Profitability of the company. She is a Certified Public Accountant and a practicing Chartered Accountant. Her expert financial acumen has helped the company achieve its financial objectives while managing a positive cash flow and providing fuel for the growth by funding new product development initiatives at Patvin. ', 2, 1, CAST(0x0000AA14003E9DD2 AS DateTime), CAST(0x0000AA17017FBDD1 AS DateTime), NULL, N'Finance Director')
GO
SET IDENTITY_INSERT [dbo].[Management] OFF
GO
SET IDENTITY_INSERT [dbo].[MetaTag] ON 

GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'/', N'Patvin: Fluid handling & Industrial solutions', N'Spray finishing, Paint circulation, Sealant dispensing, Adhesive dispensing, lubricant dispensing', N'We deliver Turnkey systems for spray finishing, paint circulation, sealant, adhesives & lubricant dispensing for manual as well as robotic applications.', 1, CAST(0x0000AA2300DB3710 AS DateTime), CAST(0x0000AA240020DAF9 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'WhoWeAre', N'Who we are| About Us | Patvin', N'WhoWeAre', N'Patvin is leading provider of Turnkey systems for spray finishing, paint circulation, sealant, adhesives & lubricant dispensing including manual as well as Robotic solutions.', 1, CAST(0x0000AA2300DB7E83 AS DateTime), CAST(0x0000AA2400211786 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'Management', N'Management', N'Management', N'Management', 0, CAST(0x0000AA23013651F5 AS DateTime), CAST(0x0000AA2300BEA2A6 AS DateTime), CAST(0x0000AA2300C27E57 AS DateTime))
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'NewsRoomDetail/founder-s-day-celebration-at-patvin', N'Founder''s Day Celebration 2019 | Patvin', N'Founders day Patvin', N'As every year, Patvin celebrated the Founder’s Day 2019 with a grand celebration. ', 1, CAST(0x0000AA23013709DE AS DateTime), CAST(0x0000AA2501051C09 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'Solutions/paint-circulation-systems', N'Paint Circulation Systems | Patvin', N'paint circulation systems', N'Patvin has a 30 year long experience providing high quality energy efficient paint circulation systems for Solvent Borne, Water Borne, Medium Solid, High Solid and 2K Paint circulation applications.', 1, CAST(0x0000AA23013CE819 AS DateTime), CAST(0x0000AA2501053808 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (6, N'Solutions/paint_application_systems', N'paint application systems', N'paint_application_systems', N'paint_application_systems', 0, CAST(0x0000AA23013D25CC AS DateTime), CAST(0x0000AA2300C0867F AS DateTime), CAST(0x0000AA2300C22CF3 AS DateTime))
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (7, N'green-solutions', N'Green solutions by Patvin', N'green solutions, energy efficiency, environment friendly solutions, paint circulation', N'Every solution we provide is designed with a special impetus in controlling environmental emissions, increase productivity, improving quality, saving materials and at the same time consuming less energy.', 1, CAST(0x0000AA23013EC8CC AS DateTime), CAST(0x0000AA2501057D83 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (8, N'Management', N'Patvin''s Senior Management Team', N'Patvin management', N'The management team operates on a clear philosophy - creating a culture of technical excellence within the company and delivering best value for customers. ', 1, CAST(0x0000AA240023244D AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (9, N'OurValues', N'Our Values | About Us | Patvin', N'Patvin Values, Values, Reliability, Commitment, Efficiency, Innovation, Passion', N'Our values form the roots of our company culture. They guide the way we treat our business partners, our communities and each other.', 1, CAST(0x0000AA240023960D AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (10, N'HealthSafety', N'Heath & Safety| About Us | Patvin', N'Health and safety, Health & safety, safety at work, environmental safety, safety in projects', N'The foremost interest of Patvin in all projects is Safety and Environmental protection. We demonstrate Safety in all our operations in conjunction with the Client''s safety policy and standards.', 1, CAST(0x0000AA240023F53A AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (11, N'products', N'Products | Patvin', N'Paint applicators, Pumps, Spray Packages, Plural component equipment, Valves, Speciality Adapters, I', N'Patvin represents global leaders like Graco, Carlisle, Hosco and Universal Robots as their authorised distributor in India for supplying Paint applicators, Pumps, Packages, Plural component equipment, Valves, Speciality Adapters, Collaborative robots etc', 1, CAST(0x0000AA2400247A44 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (12, N'Solutions/paint-application-systems', N'Paint Application | Spray Finishing | Manual and Robotic | Patvin', N'Paint application, spray finishing, robotic spray systems, automated spray systems', N'Patvin delivers turnkey spray finishing systems that offer world class finishes with maximum paint savings. While offering our customers an unlimited range of coating options, we ensure highest levels of production efficiency, operating flexibility and superior performance.', 1, CAST(0x0000AA240025E97E AS DateTime), CAST(0x0000AA2501059903 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (13, N'Solutions/color-change-systems', N'Color change Systems | Patvin', N'Colour change systems, automated color change, paint recovery, minimal colour change time', N'Eliminate colour selection error, achieve 94% reduction of material wastage and minimize colour change time with Patvin''s automated colour change sytems.', 1, CAST(0x0000AA240026804C AS DateTime), CAST(0x0000AA250106097B AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (14, N'Solutions/two-component-paint-system-2k-system', N'Two component 2K Paint Systems | Patvin', N'Two component paint system, 2K paint system', N'Our two component 2K Paint systems deliver a superior finish quality with an accurate ratio assurance. With intrinsically safe fluid panels, the systems easily integrates into hazardous areas, single or multi-color and catalyst applications. ', 1, CAST(0x0000AA240026FD7E AS DateTime), CAST(0x0000AA2501063455 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (15, N'Solutions/sealant-and-adhesive-systems', N'Sealant & Adhesive Systems | Patvin', N'Sealant dispensing, Adhesives dispensing, Hem-flange, Anti-flutter, Sound deadener, Seam sealing, Gl', N'Patvin offers smart and superior technology for handling a wide variety of viscous materials and applications like Hem-flange, Anti-flutter, Sound deadener, Seam sealing, Glass Gluing, Under Coating, Stone Guard Coating, Sole Sealing etc', 1, CAST(0x0000AA2400275BCF AS DateTime), CAST(0x0000AA2501065684 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (16, N'Solutions/automation-in-sealant-adhesive', N'Automation in Sealant & Adhesives|Robotic solutions| Patvin', N'Automated sealant dispensing, robotic sealant spray systems, Robotic adhesive dispensing, Robotic Se', N'Patvin provides automated sealant and adhesive dispensing systems that ensure precision, higher efficiency and reduced time.', 1, CAST(0x0000AA240027F4BC AS DateTime), CAST(0x0000AA2501067480 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (17, N'Solutions/nvh-application-systems', N'NVH Application systems | Patvin', N'NVH application, NVH foam, HFR metering systems', N'Advanced testing equipment and facilities at our inhouse Lab have helped us design high accuracy NVH systems. With precision and cost efficiency as key objectives, Patvin designs affordable modular  systems configurable in PU Processing.', 1, CAST(0x0000AA2400286C43 AS DateTime), CAST(0x0000AA250106A6B6 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (18, N'Solutions/lubrication-systems', N'Lubrication Systems | Lubricant dispensing | Patvin', N'Lubricant dispensing, Centralized Lube Systems, metered Oil dispensing, metered grease dispensing ', N'Our lubricant dispensing systems can handle high volume to extremely low volume feed and have been applied on diverse applications including Centralized Lube Systems in Assembly, metered dispensing of Oil and Grease etc.', 1, CAST(0x0000AA240028CD2D AS DateTime), CAST(0x0000AA250106BEA6 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (19, N'Solutions/piggable-paint-systems', N'Piggable Paint Systems | Patvin', N'Piggable paint systems, paint recovery, colour change, paint line cleaning', N'With short paint changing times, over 95% recovery of material and lower consumption of flushing solvent, Patvin''s piggable paint systems can prove to be highly economical in your painting facilty.', 1, CAST(0x0000AA2400292F50 AS DateTime), CAST(0x0000AA250106D72F AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (20, N'Solutions/ur-cobots', N'Collaborative Robots | Universal Robots Distributor | Patvin', N'Collaborative robots, Cobots, UR distributor, Universal robots distributor', N'Patvin partners with Universal Robots to provide Collaborative robots (Cobots) for a range of industrial applications - Pick & Place, machine tending, gluiing & dispensing etc.', 1, CAST(0x0000AA240029AB7B AS DateTime), CAST(0x0000AA250106EE7D AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (21, N'Technology/automation', N'Automation | Technology | Patvin', N'Automated spray systems, automation in paint lines, automated sealant dispensing, automation in pain', N'Patvin has an intelligent solution for your every automation need. We design and integrate industrial products in-house for all key areas of fluid control, dispensing and handling systems. ', 1, CAST(0x0000AA24002AE30A AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (22, N'Technology/industrie-4-0', N'Industrie 4.0 | Technology | Patvin', N'Industrie 4.0, Industry 4.0 in a paint shop', N'We utilize the power of real time data analysis to achieve superior quaility, productivity by minimizing stoppages and downtimes, preventive maintenance, and higher efficiencies.', 1, CAST(0x0000AA24002B42AC AS DateTime), CAST(0x0000AA2501070A38 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (23, N'Technology/robotic-solutions', N'Robotic solutions for Paint, Sealing, Gluing | Technology | Patvin', N'Robotic paint systems, Robotic sealant dispensing, Robotic glass gluing', N'Patin offers turnkey Robotic solutions for Paint, Sealant and Gluing applications. Integrating robotics into conventional concepts, we supply customized solutions that preciselly fulfill your needs of precision, consistency and help you deliver a flawless final product.', 1, CAST(0x0000AA24002BBF72 AS DateTime), CAST(0x0000AA25010720AF AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (24, N'Technology/human-robot-collaboration', N'Human Robot Collaboration | Technology | Patvin', N'Human robot collaboration, Cobots, Collaborative robots', N'Increase your productivity with easy to use collaborative robots from UR that allow an efficient and safe human–robot collaboration.', 1, CAST(0x0000AA24002C403E AS DateTime), CAST(0x0000AA2501073FDB AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (25, N'CaseStudyListing', N'Case studies | Patvin', N'Patvin Case studies, Patvin projects', N'Our high quality, low maintenance systems have been working tirelessly 24x7 at many client locations across India and globally. ', 1, CAST(0x0000AA24002D2935 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (26, N'Testimonials', N'Testimonials| Patvin', N'Patvin Testimonials, Customer satisfaction', N'A large portion of our business is repeat business from satisfied clients. Know what our clients have to say about us.', 1, CAST(0x0000AA24002DE697 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (27, N'Services', N'Services | Patvin', N'Patvin services, system integration, Patvin after sales support, Patvin turnkey solutions', N'Our services extend across the entire gamut including project management, design and engineering, system integration, installation, training and after sales support.', 1, CAST(0x0000AA24002E9A85 AS DateTime), CAST(0x0000AA24017CBB76 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (28, N'Industries/automotive-transportation', N'Solutions for Automotive & Transportation | Patvin', N'paint circulation for Automotive, paint application for automotive, sealant systems for automotive ', N'A 30 year long experience delivering to Automotive OEM’s, proficiency in local standards, exposure to a variety of fluid handling and finishing systems, makes us an ideal partner to our clients in the Automotive industry. ', 1, CAST(0x0000AA240032C679 AS DateTime), CAST(0x0000AA2501078C41 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (29, N'Industries/auto-components', N'Solutions for Autocomponents| Patvin', N'Painting, Sealing, Gluing and lubrication applications for autocomponents', N'We provide integrated systems for Painting, Sealing, Gluing and lubrication applications to Tier 1 & 2 Automotive suppliers. Whether small volume or large robotic systems, Patvin is the first choice.', 1, CAST(0x0000AA240033049C AS DateTime), CAST(0x0000AA250107A6C3 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (30, N'Industries/pharmaceutical', N'Solutions for Pharmaceuticals| Patvin', N'Tablet coating, Fluid handling Pharmaceutical, bulk transfer Pharmaceutical, tablet coating, dosing ', N'We deliver turnkey solutions for tablet coating, fluid handling, transfer of drugs and dosing systems for the Pharmaceutical Industry.', 1, CAST(0x0000AA2400335FF7 AS DateTime), CAST(0x0000AA250107D78B AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (31, N'Industries/aerospace', N'Solutions for Aerospace| Patvin', N'fluid handling solutions for Aerospace, finishing systems for Aerospace, sealing and gluing systems ', N'Patvin delivers customized fluid handling and finishing solutions to Aerospace component manufacturers.', 1, CAST(0x0000AA24003496A5 AS DateTime), CAST(0x0000AA250107BD8D AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (32, N'Industries/white-goods', N'Solutions for White Goods | Patvin', N'spray paint systems for white goods, sealing systems for white goods, gluing systems for white goods', N'Patvin offers cost efficient and high quality custom made solutions to compliment your finishing systems.', 1, CAST(0x0000AA2400356084 AS DateTime), CAST(0x0000AA250107F021 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (33, N'Industries/industrial-products', N'Solutions for Industrial Products| Patvin', N'lubricant dispensing for industrial products, sealing systems for industrial products, Collaborative', N'We provide integrated systems for Painting, Sealing, Gluing and lubrication applications to Industrial product manufacturers.', 1, CAST(0x0000AA240035EBF3 AS DateTime), CAST(0x0000AA25010807FB AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (34, N'Industries/fmcg', N'Solutions for FMCG industry | Patvin', N'Fluid transfer systems FMCG, Bulk transfer systems FMCG', N'We deliver Fluid handling solutions including Fluid and Bulk Transfer systems for a range of applications within the FMCG Industry.', 1, CAST(0x0000AA2400364ECD AS DateTime), CAST(0x0000AA25010826F3 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (35, N'Newsroom', N'Newsroom | Patvin', N'Patvin news, Patvin events, Patvin technical articles', N'Find out more about us through our latest news, events and updates', 1, CAST(0x0000AA240036ABC8 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (36, N'Careers', N'Careers | Life at Patvin | Patvin', N'Patvin careers, Patvin job vacancies, Patvin Job openings, Life at Patvin', N'Know what it''s like to work for us. Explore career opportunities at Patvin. Join us in our exciting journey!', 1, CAST(0x0000AA24003783A2 AS DateTime), CAST(0x0000AA24003F3A1C AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (37, N'NewsRoomDetail/patvin-represent-ur-robots', N'Patvin represents Universal Robots', N'Universal Robot distributor, Collaborative robots', N'Patvin partners with UR Robots to provide Collaborative robots (Cobots) for a range of industrial applications.', 1, CAST(0x0000AA240037FCD8 AS DateTime), CAST(0x0000AA25010857E1 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (38, N'NewsRoomDetail/manual-or-auto-colour-changing-', N'Manual or Automatic colour change | Patvin', N'Manual or automatic colour change, Automated Colour change system', N'Whether a manual color change or an automatic color change, this article can guide you towards making the right choice!', 1, CAST(0x0000AA24003A9884 AS DateTime), CAST(0x0000AA25010885FA AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (39, N'NewsRoomDetail/advantages-of-piggable-paint-system', N'Advantages of Piggable paint system | Patvin', N'Advantages of Piggable paint system | Paint recovery and cleaning', N'Before the development of piggable paint systems, paint shops needed individual lines and systems for each colour. Know how Piggable paint systems have solved this challenge.', 1, CAST(0x0000AA24003B9A5D AS DateTime), CAST(0x0000AA250108EF37 AS DateTime), NULL)
GO
INSERT [dbo].[MetaTag] ([Id], [PageUrl], [Title], [MetaKeywords], [MetaDescription], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (40, N'ContactUs', N'Contact us | Patvin', N'Patvin contact, patvin address, patvin locations, Patvin sales network, patvin service network, Flui', N'Patvin delivers innovative industrial solutions including fluid handling, dispensing and automation solutions to diverse markets. Contact us for an expert solution! ', 1, CAST(0x0000AA24003DA66E AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[MetaTag] OFF
GO
SET IDENTITY_INSERT [dbo].[Milestones] ON 

GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'1988', N'Patvin signs Distributorship agreement with for M/s. GRACO Inc., USA.', 0, CAST(0x0000A9F500D7E7C5 AS DateTime), CAST(0x0000A9F500D7EEF9 AS DateTime), CAST(0x0000A9F500D7FA18 AS DateTime))
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'1987', N'Patvin signs Distributorship agreement with for M/s. GRACO Inc., USA.', 1, CAST(0x0000A9F500D8047C AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'1995', N'Supplied first turn key project to General Motors during their factory installation in India.', 1, CAST(0x0000A9F500D81CB6 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'1996', N'Delivered large project to Mahindra Ford JV.', 1, CAST(0x0000A9F500D8304B AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'1997', N'Delivered first Hydraulically powered PCS systems, to Hyundai Motors Chennai, India.', 1, CAST(0x0000A9F500D840FF AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (6, N'1998', N'Delivered first project to Toyoto Kirloskar Pumps, Bangalore', 1, CAST(0x0000AA1000505260 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (7, N'2002', N'Patvin signs an agreement to work with ITW Automotive (now Carlisle) for the Paint application business.', 1, CAST(0x0000AA10005071E9 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (8, N'2004', N'Delivered Auto color changer system to Mahindra and Mahindra', 1, CAST(0x0000AA1000508301 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (9, N'2006', N'Inaugurated new Plant at Navi Mumbai.', 1, CAST(0x0000AA1000509313 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (10, N'2007', N'Inaugurated Paint & Sealant Application Laboratory.', 1, CAST(0x0000AA100050A39A AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (11, N'2009', N'Inaugurated service centre facility at Pune.', 1, CAST(0x0000AA100050B698 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (12, N'2011', N'Delivered first electrically powered PCS systems to Mahindra, Nagpur.', 1, CAST(0x0000AA100050C989 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (13, N'2012', N'Inaugurated Plant 2 for Manufacturing.
Completed glorious 25 years of successful Business.', 1, CAST(0x0000AA100050D992 AS DateTime), CAST(0x0000AA100051EF9A AS DateTime), NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (14, N'2012', N'Completed glorious 25 years of successful Business.', 0, CAST(0x0000AA10005136C5 AS DateTime), NULL, CAST(0x0000AA100051B59C AS DateTime))
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (15, N'2017', N'Inaugurated Plant 3', 1, CAST(0x0000AA10005145D5 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (16, N'2018', N'Patvin partners with Universal Robots ', 1, CAST(0x0000AA1000515139 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Milestones] ([MilesId], [Year], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (17, N'2019', N'Inaugurated new Paint & Sealant Lab.', 1, CAST(0x0000AA1000516155 AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[Milestones] OFF
GO
SET IDENTITY_INSERT [dbo].[NewsRoom] ON 

GO
INSERT [dbo].[NewsRoom] ([NewsId], [Type], [Title], [Image], [Date], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Slug]) VALUES (1, N'Events', N'Founder''s Day Celebration at Patvin', N'NewsRoom-d6da941a-5a95-445a-b16d-07b126d28087.JPG', CAST(0x803F0B00 AS Date), N'<p>
	As every year, Patvin celebrated the Founder&rsquo;s Day 2019 with a grand celebration, at the Tunga Hotels, Vashi. The program had a dynamic start with the Strategy 2019 presentation, followed by entertaining games and a live band performance by singer Anish Matthew. As the music too charge, the dance floor rocked with Patvin&rsquo;s talented crew.</p>
<p>
	The team finished the day in a relaxed atmosphere with a gala dinner together!&nbsp;</p>
', 1, CAST(0x0000AA01014A0AF7 AS DateTime), CAST(0x0000AA2500F8708D AS DateTime), NULL, N'founder-s-day-celebration-at-patvin')
GO
INSERT [dbo].[NewsRoom] ([NewsId], [Type], [Title], [Image], [Date], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Slug]) VALUES (2, N'News', N'Patvin represent UR Robots', N'NewsRoom-134a256a-919f-4573-99cf-d3bfe32c1fc5.jpg', CAST(0x803F0B00 AS Date), N'<p>
	Patvin partners with UR Robots to provide Collaborative robots (Cobots) for a range of industrial applications.</p>
<p>
	UR develops industrial collaborative robots that automate and streamline repetitive industrial processes. The 6-axis industrial robotic arms are safe, flexible, and easy-to-use &nbsp;for businesses of every size, all over the world.&nbsp; &nbsp;</p>
<p>
	Patvin can help you integrate a cobot into your production assembly. Our experts assess the technical and economic feasibility of cobots within your industrial process to design a customized solution that meets your system needs.&nbsp;</p>
', 1, CAST(0x0000AA0300C4F5AF AS DateTime), CAST(0x0000AA2500F90346 AS DateTime), NULL, N'patvin-represent-ur-robots')
GO
INSERT [dbo].[NewsRoom] ([NewsId], [Type], [Title], [Image], [Date], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Slug]) VALUES (3, N'Technical Articles', N'Manual or Auto Colour changing?', N'NewsRoom-2cf8060b-161f-4648-b187-137bd9fac976.png', CAST(0x803F0B00 AS Date), N'<address>
	01-03-2019 | Mangesh Pawar</address>
<p>
	A color change is the course of shifting from one colour to another in a paint application process. The same must be achieved by purging of one color from the paint applicator and the charging of a second color.</p>
<p>
	Colour changes can have a critical effect on the quality, quantity, waste, and maintenance and hence can considerably impact the overall productivity of a paint process.</p>
<p>
	Whether a manual color change or an automatic color change, this article can guide you towards making the right choice!</p>
<p>
	<strong>Colour change time </strong></p>
<p>
	During a colour change the painting process is at a halt, so longer the colour change time, lesser is the output of your painting process bringing down your total productivity. It is observed that, in automated systems, typical color change times are one second per foot of paint hose from the color valve stack to the applicator, while manual systems can vary extensively. With an automated design, faster colour change times can be achieved. An integrated color changer and cleaning system that starts at the last trigger off can save 5 to 10 seconds of cycle time.</p>
<p>
	<strong>Repeatability, Ease and Waste recovery </strong></p>
<p>
	Automatic color changes must to be designed to purge and load the lines efficiently and repeatably, with consistent, smooth &amp; seamless color change. Also, the design should be such that the system can be easily introduced into an existing paint system. Except for a paint push out, all material used during a manual color change is waste and requires to be disposed of. In automatic color changes, the discharge system automatically separates and isolates the original paint from the new color paint, so that the purged paint may be collected in a segregated container and re-used. Paint is an expensive material and recovering the same can enable huge savings.</p>
<p>
	<strong>Manual Vs. Automatic Color Changing </strong></p>
<p>
	Before the emergence of smarter technologies for colour changes, industries depended on human labour to implement color changes. Many facilities even today rely on the operator for colour change. While this can seem like an economical solution at the first instance, it can add to your total production cost in many ways. Primarily, manual color change can be heavily time consuming than automatic colour change increasing cycle times. Further, there is always a possibility of faulty colour selection by the operator, especially in close shades. Next, the operational cost of a manual system is much higher than an automatic color change system. The increase in labour costs, material costs, and waste management can be substantially large. Also, the quality of the color change can be uncertain in every cycle.</p>
<h2 style="color: red;">
	<small><span style="color:#ff8c00;"><big><strong>Patvin&rsquo;s RECOCLEAN system for Recovery + Cleaning</strong> </big></span></small></h2>
<p>
	Patvin brings you an intelligent solution for colour change, a pig-less paint push-back system placed between the color change units and the atomizer. RECOCLEAN uses dry air to push back paint from the Atomizer to the Color Changer and clean the fluid line including build-in components.</p>
<p>
	<strong>Why RecoClean?</strong></p>
<p>
	Eliminate color selection error, achieve 94% reduction of material wastage and minimal colour change time, less than 5 secs with Patvin&rsquo;s RECOCLEAN system. Fast reliable, upgradeable and user friendly, our color change systems can be used with most automatic and manual painting devices. Compact, space saving modular design reduces clutter within the booth. Color change command is obtained from the color selection panel with zero color change time. Clever recovery of paint during the color change, saves paint and other solvents.</p>
<p>
	Virtually service free, the systems are design for a long operating life.</p>
<p>
	&', 1, CAST(0x0000AA11017374DB AS DateTime), CAST(0x0000AA2500F90FF3 AS DateTime), NULL, N'manual-or-auto-colour-changing-')
GO
INSERT [dbo].[NewsRoom] ([NewsId], [Type], [Title], [Image], [Date], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Slug]) VALUES (4, N'News', N'test', N'NewsRoom-5efabbc8-4a9e-4cfc-814a-47c16ba1145e.jpg', CAST(0x6C3F0B00 AS Date), N'<p>
	test</p>
', 0, CAST(0x0000AA110188C6CA AS DateTime), NULL, CAST(0x0000AA11018B0A4F AS DateTime), NULL)
GO
INSERT [dbo].[NewsRoom] ([NewsId], [Type], [Title], [Image], [Date], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Slug]) VALUES (5, N'Events', N'Founder''s Day Celebration at Patvin ', N'NewsRoom-8a50c204-4dd6-4621-b2ea-c87ed7a70a1e.JPG', CAST(0x733F0B00 AS Date), N'<div>
	As every year, Patvin celebrated the Founder&rsquo;s Day 2019 with a grand celebration, at the Tunga Hotels, Vashi. The program had a dynamic start with the Strategy 2019 presentation, followed by entertaining games and a live band performance by singer Anish Matthew. As the music too charge, the dance floor rocked with Patvin&rsquo;s talented crew.</div>
<div>
	The team finished the day in a relaxed atmosphere with a gala dinner together!&nbsp;</div>
', 0, CAST(0x0000AA160005A58F AS DateTime), CAST(0x0000AA1800025CE1 AS DateTime), CAST(0x0000AA180007D1FC AS DateTime), N'founder_s_day_celebration_at_patvin_')
GO
INSERT [dbo].[NewsRoom] ([NewsId], [Type], [Title], [Image], [Date], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Slug]) VALUES (6, N'Technical Articles', N'Advantages of Piggable Paint System', N'NewsRoom-bedac15c-3c1d-4a32-9b16-424f5de795ff.jpg', CAST(0x803F0B00 AS Date), N'<address>
	26/03/2019 | Shashikant Katelia and Reshma Sinha.</address>
<p>
	The Paint industry has been getting increasingly competitive &amp; demanding. Owing to the growing variety of paints, not only special and customer specific but also small batches of paint for try out or special colours like anniversary editions need to be applied.</p>
<p>
	This creates a need for a process that enables efficient and quick cleaning of paint lines. The simplest process that fulfils this critical demand is the Pigging process.</p>
<h3 style="color: blue;">
	<small><span style="color:#333333;"><strong><big>What is Pigging?</big></strong></span></small></h3>
<p>
	In the paint industry, Pigging refers to the process of cleaning Paint feed lines using an object known as &quot;pig&quot; to efficiently empty the paint lines with minimum consumption of solvents, either for charging a different coloured material or to perform various maintenance operations.</p>
<h3 style="color: blue;">
	<small><strong style="color: rgb(51, 51, 51);"><big>Why Pigging?</big></strong></small></h3>
<p>
	Same paint line can be used for different /unlimited colors</p>
<p>
	There is a limitation of space in Paint Kitchens/ Paint Mix rooms and it is not feasible to have unlimited number of Paint Feeding systems, which further limits the possibility to offer a wider choice of colors to end customers.</p>
<p>
	These limitations can be overcome by Pigging systems that enable the possibility to offer unlimited colors to our end customers without investing into dedicated paint supply lines and without increasing CCV&rsquo;s in the robot.</p>
<p>
	Faster cleaning time</p>
<p>
	The cleaning is done by a product called &ldquo;pig&rdquo;, which moves at a very high speed inside the paint line along with air and minimal amount of solvent, appreciably reducing the cycle time.</p>
<p>
	<span style="color:#333333;"><strong><big>Conventional Vs. Pigging system </big></strong></span></p>
<p>
	Before the development of piggable paint systems, paint shops needed individual lines and systems for each colour, but now with the use of pigging system we can achieve multiple benefits.</p>
<p>
	Capital expenditure (capex) is very less since dedicated paint lines are not required.</p>
<p>
	System is environment friendly as it takes very less amount of solvent. 99% of flushing is done by the pig, reducing the disposal cost.</p>
<p>
	Quick colour change can be done on dead-end lines also. &bull; Requires less space since different system for different colour is not required.</p>
<p>
	Reduces paint loss during colour changes while pushing back the paint to containers for re-use.</p>
<h1 style="color: red;">
	<small><span style="color:#ff8c00;">Patvin&rsquo;s Piggable Paint System</span></small></h1>
<p>
	Patvin&rsquo;s innovation in Pigging technology is a unique combination of Dual/Single Pig system. It is a modular Paint supply system featuring pigging technology that can be deployed for water and solvent-based paint applications, capable of delivering multiple colour with minimum colour change time and predefined amount of solvent.</p>
<p>
	Patvin has always contributed towards the sustainability of the environment with environment friendly innovations. Every solution by Patvin is designed with a special impetus in controlling environmental waste by effective use and recovery of raw materials, also increasing productivity.</p>
', 1, CAST(0x0000AA1B0051759E AS DateTime), CAST(0x0000AA2500F93DEA AS DateTime), NULL, N'advantages-of-piggable-paint-system')
GO
SET IDENTITY_INSERT [dbo].[NewsRoom] OFF
GO
SET IDENTITY_INSERT [dbo].[OurValues] ON 

GO
INSERT [dbo].[OurValues] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (1, N'OurValues-ee640e80-af70-4bf8-802a-64f7a34c7f6b.png', N'RELIABILITY', N'We are a reliable partner to our clients and stakeholders, a reliable employer to our people and reliable global citizens. Patvin sees reliable and competent customer service as the basis for customer satisfaction and assumes complete responsibility for reliable and successful functioning of our products.  The leading position that Patvin enjoys today in the core area of business with repeat business from most of our clients stands to our promise of reliability. ', 1, 1, CAST(0x0000A9F501480853 AS DateTime), CAST(0x0000AA1C018142B3 AS DateTime), NULL, N'OurValues')
GO
INSERT [dbo].[OurValues] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (2, N'OurValues-d87ec317-aa10-46ec-b573-3c52ab0939a6.png', N'COMMITMENT', N'We promise to be on time, every time.
It is rightly said Time is Money! Our solid experience, indepth expertise, effective team work and right attitude help us fulfill our commitment towards our customers. Patvin is proud to have earned the reputation that it enjoys - of being dependable and resourceful.', 2, 1, CAST(0x0000A9F501482DE7 AS DateTime), CAST(0x0000AA1000575E5D AS DateTime), NULL, N'OurValues')
GO
INSERT [dbo].[OurValues] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (3, N'OurValues-f89c80b9-e66c-42a5-9adf-d8bd3034a0db.png', N'Efficiency', N'We ensure highest levels of efficiency at all times in all aspects - materials, energy, efforts, cost, quality and time. We strive hard to help customers solve difficult manufacturing problems, increase productivity, improve quality, conserve energy, save expensive materials, control environmental emissions and reduce labour costs. Patvin thus enables its customers to realize investments quickly, reliably and consequently allowing time, cost, and quality advantages to be achieved. ', 3, 1, CAST(0x0000AA100057130B AS DateTime), NULL, NULL, N'OurValues')
GO
INSERT [dbo].[OurValues] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (4, N'OurValues-ef81b1b1-89ea-418b-9c93-e25a5f5d392c.png', N'Innovation', N'We are here for nothing but to innovate! Leaner, flatter and an adaptable enterprise, we are geared to work smarter and respond quickly to changing market dynamics. We innovate new solutions that keep you at par with the industry and ahead of your competition.', 4, 1, CAST(0x0000AA1000581955 AS DateTime), NULL, NULL, N'OurValues')
GO
INSERT [dbo].[OurValues] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Url]) VALUES (5, N'OurValues-1fd7c58c-e464-4ab7-8bf1-2df630b83560.png', N'Passion', N'We are team of individuals who are passionate about delivering our best. From the smallest task to complex activities, we enjoy making a difference and adding value to every task that we undertake. That is what helps us excel and deliver the highest levels of quality.', 5, 1, CAST(0x0000AA10005853BE AS DateTime), CAST(0x0000AA10017EE24C AS DateTime), NULL, N'OurValues')
GO
SET IDENTITY_INSERT [dbo].[OurValues] OFF
GO
SET IDENTITY_INSERT [dbo].[ProductPartners] ON 

GO
INSERT [dbo].[ProductPartners] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'ProductPartners-499d3b52-df8f-46f6-8e12-980b995cbe23.png', N'31', N'<p>
	Graco is a leader in fluid handling components and systems. Graco products move, measure, control, dispense and apply a wide range of fluids and viscous materials used in vehicle lubrication, commercial and industrial settings. Patvin is an official distributor of Graco products for a variety of applications including Finishing, Lubrication, Coating &amp; Foam, Process, Sanitary, Sealants &amp; Adhesives.</p>
<p>
	With more than 30 years of successful experience, we have the expertise to help you find systems for fluid handling to fit the specification needed in your applications.</p>
', 1, 1, CAST(0x0000A9F80108CB9A AS DateTime), CAST(0x0000AA1001894C81 AS DateTime), NULL)
GO
INSERT [dbo].[ProductPartners] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'ProductPartners-82b57509-40c7-4c6d-9c74-bf2654bfe827.png', N'12', N'<p>
	Carlisle Fluid Technologies (CFT) manufactures spraying, pumping, mixing, metering, and curing equipment for a variety of coatings used in the transportation, general industrial, protective coating, wood, specialty and auto refinishing markets. Some of their brands - DeVilbiss&reg;, Ransburg&reg; and Binks&reg; provide advanced technology for finishing applications in particular and the general industry.</p>
<p>
	For the last 12 years, Patvin has been associated with Carlisle Fluid Technologies delivering systems and equipments within Liquid finishing , Electrostatics, Automotive Refinishing and Paint Finishing Systems.</p>
', 2, 0, CAST(0x0000A9F801091661 AS DateTime), NULL, CAST(0x0000A9F801091D85 AS DateTime))
GO
INSERT [dbo].[ProductPartners] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'ProductPartners-1a88233f-4b07-4667-b38e-7e452e55efbd.png', N'12', N'<p>
	Carlisle Fluid Technologies (CFT) manufactures spraying, pumping, mixing, metering, and curing equipment for a variety of coatings used in the transportation, general industrial, protective coating, wood, specialty and auto refinishing markets. Some of their brands - DeVilbiss&reg;, Ransburg&reg; and Binks&reg; provide advanced technology for finishing applications in particular and the general industry.</p>
<p>
	For the last 12 years, Patvin has been associated with Carlisle Fluid Technologies delivering systems and equipments within Liquid finishing , Electrostatics, Automotive Refinishing and Paint Finishing Systems.</p>
', 2, 1, CAST(0x0000A9F801092FBE AS DateTime), CAST(0x0000AA1001878F60 AS DateTime), NULL)
GO
INSERT [dbo].[ProductPartners] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'ProductPartners-5c9348f4-88cd-4997-b238-d4a357af5f12.png', N'8', N'<p>
	HOSCO is a manufacturer of smooth bore, &quot;cavity-free&quot; stainless steel fittings and accessories designed specifically for the use in paint circulating and application finishing systems. Patvin associates with HOSCO for the last 8 years for their range of products.</p>
', 3, 1, CAST(0x0000AA1000599590 AS DateTime), CAST(0x0000AA11004210D5 AS DateTime), NULL)
GO
INSERT [dbo].[ProductPartners] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'ProductPartners-39a5e93e-0cfc-42da-86a4-78e08e253d70.png', N'1', N'<p>
	In 2018 Patvin partnered with Universal Robots (UR) to bring safe, flexible, and easy-to-use 6-axis industrial robotic arms to businesses of every size, all over the world. UR develops industrial collaborative robots that automate and streamline repetitive industrial processes. There are three different collaborative robot sizes that are easily integrated into existing production environments. With six articulation points and a wide scope of flexibility, these collaborative robot arms are designed to mimic the range of motion of a human arm. The collaborative robots have 3 different payloads 3, 5 and 10 kgs. UR robot arms can automate virtually anything, we mean virtually anything; from assembly to painting, from screw-driving to labeling, from packing to polishing, from injection molding to welding, and whatever other processing task you can think of. Thanks to the flexibility of the UR family, the robot arms are economically viable for even small-batch and mixed-product assembly.</p>
', 4, 1, CAST(0x0000AA100062AA47 AS DateTime), CAST(0x0000AA1100421EC3 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[ProductPartners] OFF
GO
SET IDENTITY_INSERT [dbo].[QualityMissionVison] ON 

GO
INSERT [dbo].[QualityMissionVison] ([Id], [Title], [Icon], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (1, N' Quality Policy', N'far fa-thumbs-up', N'<p>
	We, the Management and employees of Patvin Engineering Private Limited, are committed to consistently <strong>deliver enhanced value to our customers</strong> in the systems that we deliver.</p>
<p>
	We would achieve this through continual improvement of our processes and systems by way of <strong class="orangeColor">Innovation, Technology, Knowledge Management and Employee Training</strong>.</p>
', 1, CAST(0x0000A9F401454807 AS DateTime), NULL, NULL, 1)
GO
INSERT [dbo].[QualityMissionVison] ([Id], [Title], [Icon], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (2, N'Quality Policy', N'far fa-thumbs-up', N'<p>
	We, the Management and employees of Patvin Engineering Private Limited, are committed to consistently <strong>deliver enhanced value to our customers</strong> in the systems that we deliver.</p>
<p>
	We would achieve this through continual improvement of our processes and systems by way of <strong class="orangeColor">Innovation, Technology, Knowledge Management and Employee Training</strong>.</p>
', 0, CAST(0x0000A9F40145A39F AS DateTime), CAST(0x0000A9F40149DBD5 AS DateTime), CAST(0x0000A9F40149ED7A AS DateTime), 2)
GO
INSERT [dbo].[QualityMissionVison] ([Id], [Title], [Icon], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (3, N'Our Mission', N'fas fa-bullseye', N'<p>
	Providing <strong>High Quality Innovative Solutions</strong> to our Clients along with continual Growth Opportunities to our Partners and team Members.</p>
<p>
	We are <strong class="orangeColor">Committed to Excellence</strong> without compromising our Core Objectives of Diligence and Integrity.</p>
', 1, CAST(0x0000A9F4014A41BA AS DateTime), NULL, NULL, 2)
GO
INSERT [dbo].[QualityMissionVison] ([Id], [Title], [Icon], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (4, N'Our Vision', N'far fa-eye', N'<p>
	To <strong>Make Complex Operations/ Solutions Simple</strong>, Robust and Effective without compromising Our High Quality.</p>
', 1, CAST(0x0000A9F4014A88AA AS DateTime), NULL, NULL, 3)
GO
SET IDENTITY_INSERT [dbo].[QualityMissionVison] OFF
GO
SET IDENTITY_INSERT [dbo].[RangeOfProduct] ON 

GO
INSERT [dbo].[RangeOfProduct] ([Id], [Equipment], [Applications], [Pumps], [IsActive], [UpdatedOn]) VALUES (2, N'<ul>
	<li>
		Spray Guns</li>
	<li>
		Electrostatic Spray Guns</li>
	<li>
		2K Systems</li>
	<li>
		Finishing Accessories</li>
	<li>
		Pumps</li>
	<li>
		Rotary Bell Applicators</li>
	<li>
		Spray Packages</li>
	<li>
		Ball Valves</li>
	<li>
		Smooth Bore Fitting and Adaptors</li>
	<li>
		Quick Disconnects</li>
	<li>
		Restrictors and Flow Regulators</li>
	<li>
		Paint and Air Hoses</li>
</ul>
', N'<ul>
	<li>
		Hot Melt Equipment</li>
	<li>
		Meter-Mix-Dispense Equipment</li>
	<li>
		Metering Packages</li>
	<li>
		Supply Systems</li>
</ul>
', N'<ul>
	<li>
		Paint Transfer Applications</li>
	<li>
		Lubrication Applications</li>
	<li>
		Bulk Transfer - Chemical and Pharma.</li>
	<li>
		Small Pumping Stations</li>
</ul>
', 1, CAST(0x0000AA0900CD88A0 AS DateTime))
GO
SET IDENTITY_INSERT [dbo].[RangeOfProduct] OFF
GO
SET IDENTITY_INSERT [dbo].[ReachUs] ON 

GO
INSERT [dbo].[ReachUs] ([Id], [Heading], [Description], [Contact], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image], [Fax], [Latitude], [Longitude], [Title]) VALUES (1, N'HEAD OFFICE', N'<p>W-193, TTC Industrial Area,<br/> Khairane MIDC, Behind Reliance Silicon,<br/>Thane - Belapur road,<br/>Navi Mumbai - 400709,<br/>India</p>', N'+91 22 62863333,<br> +91 22 27780310,<br> +91 22 33831500', 1, CAST(0x0000AA0600D25EF2 AS DateTime), CAST(0x0000AA1A0167A82C AS DateTime), NULL, N'Pointer-d1ca2ed5-cd49-4202-8e50-2e632ec5513d.png', NULL, CAST(19.097034 AS Numeric(12, 6)), CAST(73.017273 AS Numeric(12, 6)), N'Patvin Engineering Pvt. Ltd.')
GO
INSERT [dbo].[ReachUs] ([Id], [Heading], [Description], [Contact], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image], [Fax], [Latitude], [Longitude], [Title]) VALUES (2, N'REGISTERED OFFICE', N'106, Veena Killedar Industrial Estate,<br/>
10/14, Pais Street, Byculla (w),<br/>
Mumbai: 400011,<br/>
India
', NULL, 1, CAST(0x0000AA0600D5E6AA AS DateTime), CAST(0x0000AA1A016706C7 AS DateTime), NULL, N'Pointer-f42d7b81-0071-4afb-8d9e-e951ca6e91b8.png', NULL, CAST(18.979607 AS Numeric(12, 6)), CAST(72.830580 AS Numeric(12, 6)), N'Patvin Engineering Pvt. Ltd.')
GO
INSERT [dbo].[ReachUs] ([Id], [Heading], [Description], [Contact], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image], [Fax], [Latitude], [Longitude], [Title]) VALUES (3, N'FACTORY', N'Unit 2, A-196, TTC Industrial Area,<br/>
Khairane MIDC, Behind Reliance Silicon,<br/>
Thane - Belapur road,<br/>
Navi Mumbai - 400709<br/>
India', N'+91 22 33831500,<br> +91 22 27780310', 1, CAST(0x0000AA0900E271C6 AS DateTime), CAST(0x0000AA1A0166C206 AS DateTime), NULL, N'Pointer-b2947332-05d1-4361-a204-86f4a8f244c4.png', NULL, CAST(19.100086 AS Numeric(12, 6)), CAST(73.018837 AS Numeric(12, 6)), N'Patvin Engineering Pvt. Ltd')
GO
INSERT [dbo].[ReachUs] ([Id], [Heading], [Description], [Contact], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image], [Fax], [Latitude], [Longitude], [Title]) VALUES (4, N'SERVICE OFFICE', N'Plot No. 86, H block,<br/>
Pimpri - 1,<br/>
Pune - 411018,<br/>
India', N'+91 20 46772727', 1, CAST(0x0000AA0900E2C0F2 AS DateTime), CAST(0x0000AA1A0167CE08 AS DateTime), NULL, N'Pointer-df513195-0d9f-442d-a757-3b66f46ad71e.png', N'+91 20 46772728', CAST(18.638575 AS Numeric(12, 6)), CAST(73.805229 AS Numeric(12, 6)), N'Patvin Engineering Pvt. Ltd.')
GO
SET IDENTITY_INSERT [dbo].[ReachUs] OFF
GO
SET IDENTITY_INSERT [dbo].[Services] ON 

GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Turnkey Projects', N'<p>
	<strong>Patvin</strong> is a leading supplier of Paint Circulation &amp; Sealant systems. Whether small volume or large robotic paint/ Sealant systems, we are the first choice. From design &amp; engineering to system integration, installation, training and service, Patvin offers superior turnkey solutions. Our team includes expert engineering professionals, designers, programmers, service engineers, and technicians with broad experience in the field.</p>
<p>
	The experience gained over the years and the consistent drive for technical innovations and improvements have helped Patvin attain the leadership position in offering innovative solutions to diverse global markets. We continue to develop innovative solutions for complex applications, investing 3% of our revenues into research and development.</p>
', N'Servicesicon-635f3af4-6658-4801-8c45-2ed2634c2d26.png', N'Servicesimg-939b684f-429a-40a5-9783-e48b90bf1cbd.jpg', 0, CAST(0x0000AA0100E49B9A AS DateTime), CAST(0x0000AA0100E4D976 AS DateTime), NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'Turnkey Projects', N'<p>
	Patvin offers Turnkey solutions for fluid handling systems, primarily, Paint Circulation, High viscosity Sealant systems, Robotic paint/ Sealant application, Lubrication systems etc. Our services extend across the entire gamut including project management, design and engineering, system integration and installation.&nbsp;</p>
<p>
	Patvin utilizes an integrated product development process operating on the philosophy - Understand, Define, Ideate, Prototype, Test. We execute projects within a structured methodology of detailed procedures and guidelines. Our focus is to deliver innovative design with frugal engineering and lean manufacturing solutions. Our strict quality control process, under the guidelines of ISO 9001, ensures that all key requirements on new or existing products are fulfilled at every stage of development.</p>
<p>
	With the advantages available from Patvin, let us support you in integrating your whole project - from art to part!</p>
', N'Servicesicon-5351215f-8942-4bf4-91cb-a3f6c50181de.png', N'Servicesimg-e01ff183-1ed2-4bb0-b64b-ae0042a68f78.jpg', 1, CAST(0x0000AA0100E55742 AS DateTime), CAST(0x0000AA1000046A78 AS DateTime), NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'System Refurbishment', N'<p>
	If your existing system no longer meets your application requirements, you might probably be looking our for a refurbishment. Or whether it is to automate manual operations, or a process optimization, Patvin can help you refurbish your existing system to suit your precise need. Refurbishing complex systems, needs an experienced team with a proven track record. Our expert technicians have successfully serviced hundreds of fluid handling systems over the years, making us an expert in the field. Let us know your requirement and we will find the right solution!</p>
', N'Servicesicon-973b3d17-e3dd-442e-b305-8b2311f6023c.png', N'Servicesimg-de5d05e3-4c37-454e-a3c2-0f93deb22c0a.jpg', 1, CAST(0x0000AA0100E59844 AS DateTime), CAST(0x0000AA1000047F79 AS DateTime), NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (4, N'Turnkey Projects2', N'<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	Patvin offers Turnkey solutions for fluid handling systems, primarily, Paint Circulation, High viscosity Sealant systems, Robotic paint/ Sealant application, Lubrication systems etc.&nbsp;</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	&nbsp;</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	Our services extend across the entire gamut including project management, design and engineering, system integration and installation.&nbsp;</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	&nbsp;</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	Patvin utilizes an integrated product development process operating on the philosophy - Understand, Define, Ideate, Prototype, Test. We execute projects within a structured methodology of detailed procedures and guidelines. Our focus is to deliver innovative design with frugal engineering and lean manufacturing solutions.</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	&nbsp;</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	Our strict quality control process, under the guidelines of ISO 9001, ensures that all key requirements on new or existing products are fulfilled at every stage of development.</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	&nbsp;</div>
<div style="box-sizing: border-box; color: rgb(115, 135, 156); font-family: &quot;Helvetica Neue&quot;, Roboto, Arial, &quot;Droid Sans&quot;, sans-serif; font-size: 13px; text-align: center; background-color: rgb(249, 249, 249);">
	With the advantages available from Patvin, let us support you in integrating your whole project - from art to part!</div>
', N'Servicesicon-6a05e2c5-405b-4729-bf5c-0f771bf341a6.png', N'Servicesimg-8f71e0ee-4ac0-45b5-8476-b430c98c8e32.jpg', 0, CAST(0x0000AA0E01869BEE AS DateTime), NULL, NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (5, N'Service & AMC', N'<p>
	We have an extensive and trained service network throughout the country with staff positioned strategically near customers to offer timely assistance. Our service engineers and technicians are trained by our principals, to offer efficient service support and training to our clients.</p>
<p>
	Patvin&#39;s support infrastructure for spares and repairs further provides our customers with timely and effective on-site preventive maintenance and repair service. A detailed study is undertaken based on the customer&rsquo;s production pattern to identify the critical spares, expected life cycle of wear &amp; tear parts and spares consumption. An attempt is made to gather this data and stock the most critical spares for all the customers.</p>
<p>
	Your benefit - Effective and timely warranty and repair support in India with out the need for carrying a fixed overhead for the service team.</p>
<p>
	Email: service@patvin.co.in&nbsp;</p>
<p>
	Call: +91 20 46772727&nbsp;</p>
', N'Servicesicon-443f41b1-3867-4c41-8b8f-34d914777865.png', N'Servicesimg-615fe25a-b3ec-405c-bb21-7bdd1897d41d.jpg', 1, CAST(0x0000AA0F0173A4A0 AS DateTime), CAST(0x0000AA1A016BFD83 AS DateTime), NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (6, N'Spare Parts ', N'Extend the life of your equipment with our timely service and original parts. We hold an impressive inventory of spares to offer efficient after sales services to our customers, minimizing stoppages & reducing your downtime. With our extensive catalogue of spares, you can be rest assured that you will receive the right spares when you need them.', N'Servicesicon-ccc50834-92bd-4c3d-b7cc-a00f576a8b5d.png', N'Servicesimg-81d90c70-29f7-4804-aa65-56f40b81fef3.JPG', 1, CAST(0x0000AA0F01761027 AS DateTime), CAST(0x0000AA1B004C8A82 AS DateTime), NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (7, N'Technical Assistance ', N'<p>
	With a long term partnership approach towards our customers, we strive to be there when you need us. Consult our technical team for an expert solution to your need. Minimize stoppages &amp; reduce your downtime with expert help from Patvin!</p>
', N'Servicesicon-4657ba24-cbba-40ae-951a-aae80bef0b67.png', N'Servicesimg-c378bb04-d5e1-4f3e-8428-4d04327d3c57.JPG', 0, CAST(0x0000AA0F017748B8 AS DateTime), CAST(0x0000AA1100047220 AS DateTime), NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (8, N'Customer Experience Centre', N'Our state-of-art laboratory ensures that system requirements and customer demands are met effectively. The lab also allows the design & engineering team to conceptualize innovative solutions for our customers. Patvin has realized many such Innovations in fluid handling, many of which are in successful operation with our customers. We can validate and optimize your processes, testing your product preferences for the target application. 
Consult our technical team for an expert solution to your need.
', N'Servicesicon-fbe603e5-210d-4fed-8801-29ffe85c797d.png', N'Servicesimg-4152bc89-1b4c-4e50-9c27-c6e0335886f1.JPG', 1, CAST(0x0000AA0F0178A611 AS DateTime), CAST(0x0000AA1B004CC929 AS DateTime), NULL)
GO
INSERT [dbo].[Services] ([ServId], [Title], [Description], [Icon], [Image], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (9, N'Training', N'<p>
	We understand that in most cases, the delivery of a machine is just the beginning of an integration process. Our team can enable smooth integration of our systems into your processes by providing prompt onsite assistance during these early stages.</p>
<p>
	After sales training is an integral part of the Patvin sales strategy. Patvin trains customers by offering a team to run the production activities of the system during the Plant start up phase. We strive hard to maintain zero downtime at the customers end and have periodical checks and audits of the system as part of this effort. Our commitment towards this has resulted in repeated orders from various leading automotive companies in India.</p>
', N'Servicesicon-59160d76-965e-494d-8fcc-9bcce57cc2f2.png', N'Servicesimg-a1d96368-4e4e-4795-a8a2-12cb87dcffaa.JPG', 1, CAST(0x0000AA0F017A4456 AS DateTime), CAST(0x0000AA1B0048C3F3 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[Services] OFF
GO
SET IDENTITY_INSERT [dbo].[ServiceSpareParts] ON 

GO
INSERT [dbo].[ServiceSpareParts] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'ServiceSpareParts-5202a6c7-65e7-4410-b736-f21675cde4d4.png', N'Dedicated Service Team', N'We have an extensive and trained service network throughout the country with staff positioned strategically near customers to offer timely assistance. Our service engineers and technicians are trained by our principals, to offer efficient service support and training to our clients.
', 1, 1, CAST(0x0000A9F900CCC550 AS DateTime), CAST(0x0000AA1D00086083 AS DateTime), NULL)
GO
INSERT [dbo].[ServiceSpareParts] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'ServiceSpareParts-777ba315-408a-465f-a911-ea076c162a25.png', N'Adequate Spares Inventory', N'Our support infrastructure for spares and repairs further provides our customers with timely and effective on-site preventive maintenance and repair service. We hold an inventory of spares to the tune of INR 50 Million to offer efficient after sales services to our customers.
', 2, 1, CAST(0x0000A9F900CCFA91 AS DateTime), CAST(0x0000AA1D00086E7A AS DateTime), NULL)
GO
INSERT [dbo].[ServiceSpareParts] ([Id], [Image], [Heading], [Description], [DisplayOrder], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'ServiceSpareParts-74bf54bc-d34a-41dd-be74-522a08f55eb1.png', N'Perceptive Approach', N'A detailed study is undertaken based on the client&rsquo;s production pattern to identify the critical spares, expected life cycle of wear &amp; tear parts and spares consumption. An attempt is made to gather this data and stock the most critical spares for all the customers.
', 3, 1, CAST(0x0000A9F900CD3137 AS DateTime), CAST(0x0000AA1D00087D03 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[ServiceSpareParts] OFF
GO
SET IDENTITY_INSERT [dbo].[SocialMedia] ON 

GO
INSERT [dbo].[SocialMedia] ([Id], [opentime], [CallUs], [MailUs], [Sales], [Services], [Purchasing], [Dispatches], [IsActive], [AddedOn], [UpdatedOn], [Sales1]) VALUES (1, N'Sales: Mon – Sat | 09:00 – 18:00 
Service: 24 X 7', N'+91 22 33831500/ 62863333/ 27780310 ', N'enquiry@patvin.co.in', NULL, NULL, NULL, NULL, 1, CAST(0x0000AA00011FC0F6 AS DateTime), CAST(0x0000AA1A01658840 AS DateTime), NULL)
GO
INSERT [dbo].[SocialMedia] ([Id], [opentime], [CallUs], [MailUs], [Sales], [Services], [Purchasing], [Dispatches], [IsActive], [AddedOn], [UpdatedOn], [Sales1]) VALUES (2, NULL, NULL, NULL, N'sales@patvin.co.in', N'service@patvin.co.in', N'purchase@patvin.co.in', N'logistics@patvin.co.in', 1, CAST(0x0000AA000120398A AS DateTime), CAST(0x0000AA06013D7F6B AS DateTime), N'enquiry@patvin.co.in')
GO
SET IDENTITY_INSERT [dbo].[SocialMedia] OFF
GO
SET IDENTITY_INSERT [dbo].[SolAndTechDetailsTable] ON 

GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (20, 1, 1, N'High quality energy efficient solutions', N'<div class="productContent">
	<div>
		For the Last 30 years, we have been an integral part of the Indian automotive growth journey. Our high quality, low maintainance systems have been working tirelessly 24x7 at many client locations across India. Our paint systems provide precise flow, velocity and temperature, to contribute to outstanding paint finishes.&nbsp;</div>
	<div>
		&nbsp;</div>
	<div>
		Solvent Borne</div>
	<div>
		Water Borne</div>
	<div>
		Medium Solid</div>
	<div>
		High Solid</div>
	<div>
		2K Paint circulation</div>
	<div>
		&nbsp;</div>
	<div>
		Patvin can custom design a Paint Circulation System to meet your needs. Contact us to know more</div>
</div>
<p>
	&nbsp;</p>
', 0, CAST(0x0000AA0900CFB893 AS DateTime), CAST(0x0000AA0E001651B6 AS DateTime), NULL, N'InnerBanner-c8e57c40-19b2-409b-9907-d02170a35074.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (21, 1, 2, N'World class finishes with maximum paint savings', N'<p>
	At Patvin, we utilize our decades of technical expertise and diverse experience to deliver turnkey paint finishing systems that ensure highest levels of production efficiency, operating flexibility and superior performance, while offering our customers an unlimited range of coating options.</p>
<p>
	Our application systems have been enhanced with Intelligent Control system for high volume paint production line and further a remarkable increase in paint shop <span style="font-size:14px;">efficiency</span>.</p>
<p>
	We can also further enhance the system efficiency and performance with the addition of:&nbsp;</p>
<ul>
	<li>
		Single Axis and Two Axes reciprocating systems.</li>
	<li>
		High Speed Rotary Atomisers (Bells).</li>
	<li>
		High Speed Turbo Disc Applicators</li>
	<li>
		Automatic Spray Guns &ndash; Conventional / HVLP</li>
	<li>
		Automatic Electrostatic Spray Guns</li>
	<li>
		High Voltage Generators with Automatic In-built Safety with di / dt</li>
	<li>
		Robotic Spray Applicators with Controls</li>
</ul>
', 1, CAST(0x0000AA0900D07BE2 AS DateTime), CAST(0x0000AA2200547CFC AS DateTime), NULL, N'InnerBanner-7c072df5-3fa7-48c3-8c4e-654cd4250cd8.JPG')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (22, 2, 12, N'Achieve superior quality and enhance productivity with real time data analysis', N'<div class="productContent">
	<div>
		We have entered a smart new era where energy efficiency is no longer the only focus. Sharing the attention equally is profitability using smarter technologies and equipment. We offer you smart automation solutions to be at par with the fourth industrial revolution and enter the world of Industrie 4.0.</div>
	<div>
		&nbsp;</div>
	<div>
		At Patvin, Industry 4.0 begins with continuous and innovative development of existing solutions and technologies, facilitating productive changes in routine practices and processes.</div>
	<div>
		We utilize the power of real time data anlysis to achieve superior quality and enhance the productivity by minimizing stoppages and downtimes, preventive maintenance, and higher efficiencies.</div>
	<div>
		&nbsp;</div>
	<div>
		We integrate smart systems that control their own process individually. Building communication along the value chain of a product, we ensure that the product&acute;s life cycle is fully perceptible.</div>
	<div>
		&nbsp;</div>
</div>
<p>
	&nbsp;</p>
', 0, CAST(0x0000AA0900D344FB AS DateTime), CAST(0x0000AA0E016850A3 AS DateTime), NULL, N'InnerBanner-bd1acf01-739d-4c1f-8509-254dea9705db.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (23, 3, 14, N'Leadership position: 
Almost every other vehicle in India is manufactured using systems supplied by Patvin. 
', N'<p>
	We are the leading suppliers of integrated fluid handling and finishing systems for the automotive industry. Almost every other vehicle in India is manufactured using systems supplied by Patvin.&nbsp;</p>
<p>
	A 30 year long experience delivering to Automotive OEM&rsquo;s, proficiency in local standards,&nbsp;exposure to a variety of applications and systems makes us an ideal partner to our clients.</p>
<p>
	The ever evolving automotive industry requires an innovative approach that Patvin has consistently delivered bringing us to the position where we are today.</p>
<p>
	After enjoying a steady leadership position in the finishing industry of the Indian automotive market, Patvin is also emerging as a supplier of choice for our international clients in the segment.</p>
<p>
	Our solutions for the automotive sector:</p>
<ul>
	<li>
		Paint Circulation Systems</li>
	<li>
		PVC Sealant Systems</li>
	<li>
		PVC Underbody Systems</li>
	<li>
		Wax Injection Systems</li>
	<li>
		Centralized Lubrication Systems</li>
	<li>
		BIW/Weld Shop Sealants &amp; Adhesive systems</li>
	<li>
		Robotics Sealant Application Systems</li>
	<li>
		Glass Bonding Urethane Systems</li>
	<li>
		NVH &amp; Structural Foam Applications</li>
	<li>
		RTV Silicone Application Systems</li>
	<li>
		Liquid Applied Sound Deadener</li>
	<li>
		Fuel Transfer Systems</li>
	<li>
		Oil Transfer Systems</li>
	<li>
		Glue Application Systems</li>
</ul>
', 1, CAST(0x0000AA0900D3C43C AS DateTime), CAST(0x0000AA230180DACF AS DateTime), NULL, N'InnerBanner-74edec82-8e36-44dc-a90c-b5005667edd5.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (24, 1, 1, N'High quality energy efficient solutions', N'<p>
	For the Last 30 years, we have been an integral part of the Indian automotive growth journey. Our high quality, low maintainance systems have been working tirelessly 24x7 at many client locations across India. Our paint systems provide precise flow, velocity and temperature, to contribute to outstanding paint finishes.</p>
<ul>
	<li>
		Solvent Borne</li>
	<li>
		Water Borne</li>
	<li>
		Medium Solid</li>
	<li>
		High Solid</li>
	<li>
		2K Paint circulation</li>
</ul>
<p>
	Patvin can custom design a Paint Circulation System to meet your needs. Contact us&nbsp;to know more.</p>
', 1, CAST(0x0000AA0E0014E63F AS DateTime), CAST(0x0000AA2500CB7670 AS DateTime), NULL, N'InnerBanner-468789b4-d686-4bc7-b15a-33d8914e6a0f.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (25, 1, 6, N'Next generation robotic sealing applications ', N'<p>
	The use of automated systems in sealant and adhesive dispensing ensures precision, higher efficiency and reduced time.</p>
<p>
	Patvin provides engineered customized solutions for automated sealant and adhesive dispensing systems for multiple applications including Engine Gasketing Systems, Sound deadening &amp; Rocker Panel,&nbsp; Seam Sealing, Body Panel reinforcement, Hem Flange Bead &amp; Swirl etc for the automotive industry.</p>
', 1, CAST(0x0000AA0E0019734D AS DateTime), CAST(0x0000AA0E015C1B93 AS DateTime), NULL, N'InnerBanner-68ec65cb-e56d-467d-a1bc-27e48db9230e.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (26, 1, 7, N'On -Ratio Dispensing, First Time / Every time Correct Ratio and Volume', N'<p>
	Patvin has a long experience catering to industrial NVH projects earning the trust of our customers who are OEM&rsquo;s and automotive suppliers.</p>
<p>
	Our partnership with global leaders like Graco and extensive research in the field ensures that our systems are always up to date, cost effective and superior in quality.</p>
<p>
	Advanced testing equipment and facilities at our inhouse Lab have helped us design high accuracy NVH systems. With precision and cost efficiency as key objectives, we design affordable modular&nbsp; systems configurable in PU Processing.</p>
<p>
	Our afforadable compact systems, have proven to fulfill a&nbsp; First Time / Every time Correct Ratio and Volume, Repeatibility, Reduction in Scrap and On -Ratio Dispensing.</p>
<ul>
	<li>
		Able to spray in all strategic and important areas for reduction in NVH.</li>
	<li>
		2k expandable foam is injected in Cavities.&nbsp;&nbsp;</li>
	<li>
		Arrests Body leakage.</li>
	<li>
		Improves NVH values thereby resulting in quieter cabins in the vehicle.&nbsp;&nbsp;</li>
	<li>
		Cost effective solutions as compare to traditional baffles.</li>
</ul>
', 1, CAST(0x0000AA0E00413AF4 AS DateTime), CAST(0x0000AA110076521F AS DateTime), NULL, N'InnerBanner-d63a08f8-d6ce-4c29-8c9c-fc658f1633de.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (27, 1, 3, N'Eliminate color selection error, 94% reduction of material wastage, Zero color change time', N'<p>
	To eliminate expensive repainting costs, we recommend Patvin&#39;s highly efficient color change systems.</p>
<p>
	Fast reliable, upgradeable and user friendly, our color change systems can be used with most automatic and manual painting devices.</p>
<p>
	Compact, space saving modular design reduces clutter within the booth. Color change command is obtained from the color selection panel with zero color change time. Clever recovery of paint during the color change, saves paint and other solvents.</p>
<p>
	Virtually service free, the systems are design for a long operating life.</p>
', 1, CAST(0x0000AA0E015BDA99 AS DateTime), CAST(0x0000AA1100774959 AS DateTime), NULL, N'InnerBanner-de29325f-6aca-4483-bb27-3f0356ad61c8.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (28, 1, 4, N'Accurate ratio assurance for superior finish quality', N'<p>
	Patvin has for many years delivered both manual and automatic 2K painting systems to the surface treatment industry.&nbsp;</p>
<p>
	With intrinsically safe fluid panels, the systems easily integrates into hazardous areas, single or multi-color and catalysts.</p>
<p>
	The system delivers a superior finish quality with an accurate ratio assurance.&nbsp;</p>
<p>
	Simple programming allows fast set up and efficiency.&nbsp; Process control Advanced web interface provides consolidated system management and reporting. The system comes with a potlife alarm and optional autoflush features, material consumption data &amp; statistical process data.&nbsp;</p>
<p>
	System options:&nbsp;</p>
<ul>
	<li>
		Flow Meters - Spur Gear, Helical Gear, Non Intrusive Coriolis Meters&nbsp;&nbsp;</li>
	<li>
		Gun Flush Box - Automated Flushing for Manual Spray Guns</li>
	<li>
		Integrated Flow Control - Produces quick responses and superior process control&nbsp;&nbsp;</li>
	<li>
		Communication Gateway - Offers integration flexibility with multiple protocol capabilities on Auto &amp; Manual Systems</li>
	<li>
		PD2K - The ProMix PD2K mixes the material close to the gun so the flush zone is significantly smaller. This allows customers to use less paint, spend less on disposal costs and allows for faster color changes. And because there is limited mixed material in the system, the ProMix PD2K is ideal for short pot life materials. It is also compatible with acid-based materials.&nbsp;</li>
</ul>
', 1, CAST(0x0000AA0E015E9694 AS DateTime), CAST(0x0000AA1C001BB713 AS DateTime), NULL, N'InnerBanner-94f530c2-c366-4ae3-9050-55d7a3f4cb5a.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (29, 1, 8, N'Automatic material filling, handles high volume to extremely low volume feed, accurately. ', N'<p>
	We have delivered advanced systems for Lubrication applications enabling automatic material filling with level monitor and warning and handling high volume to extremely low volume feed.&nbsp;</p>

<p>
	Our lubrication systems have been tested on a variety of applications including but not limited to the following:</p>

<ul>
	<li>
		Centralized Lube Systems in Assembly&nbsp;&nbsp;</li>
	<li>
		Vehicle On Board Lubrication</li>
	<li>
		Parts Assembly Lube Systems&nbsp;&nbsp;</li>
	<li>
		Mining and Site Lube Equipment</li>
	<li>
		Bulk Storage and Feed&nbsp;&nbsp;</li>
	<li>
		Oil transfer and Metered Dispensing</li>
	<li>
		Grease Transfer and Metered Dispensing&nbsp;&nbsp;</li>
	<li>
		Fuel Transfer</li>
	<li>
		Engine Oil Filling</li>
	<li>
		Gear Box Oil Filling</li>
</ul>', 1, CAST(0x0000AA0E01609D05 AS DateTime), CAST(0x0000AA110075C1F8 AS DateTime), NULL, N'InnerBanner-e7fa6009-9879-4593-a69a-751917d220bd.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (30, 1, 5, N'Smart and superior technology for handling a wide variety of viscous materials', N'<p>
	Smart and superior technology for handling a wide variety of viscous materials has been the requirement of the industry which Patvin diligently has been delivering to its customers.</p>
<p>
	Two component adhesives can be easily handled with a variety of accurate and reliable system designs. Special ram units can handle even the most viscous materials from 1-gallon, 5-gallon, 55-gallon, or 300-gallon tote supply containers. Specialized systems are available to handle multiple drops from a centralized location to reduce material handling costs.&nbsp;</p>
<p>
	Applications:</p>
<ul>
	<li>
		Hem-flange</li>
	<li>
		Anti-flutter</li>
	<li>
		Sound deadener</li>
	<li>
		Seam sealing</li>
	<li>
		Glass Gluing</li>
	<li>
		Under Coating</li>
	<li>
		Stone Guard Coating</li>
	<li>
		Sole Sealing</li>
</ul>
', 1, CAST(0x0000AA0E016323FB AS DateTime), CAST(0x0000AA140020EF7A AS DateTime), NULL, N'InnerBanner-b52617e4-e229-434f-bbaa-a3a1c4cb53ce.JPG')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (31, 1, 9, N'Minimum wastage by paint recovery, over 95%', N'<p>
	With short paint changing times and recovery of material, pig systems can prove highly economical in a painting facility.</p>
<p>
	Our pigging solutions have helped our customers recover significant amounts of the useable product, over 95%, lower consumption of flushing solvent, reduce paint requirement for batch/trial color.</p>
<p>
	Whatever be your requirements, Patvin can design a solution that meets your precise needs. Patvin&#39;s pigging systems can be easily integrated to your existing system. Easy to operate, ours system&#39;s offer unlimited trial color option without addition of CCV&#39;s in robot.</p>
', 1, CAST(0x0000AA0E0163E117 AS DateTime), CAST(0x0000AA150049F473 AS DateTime), NULL, N'InnerBanner-a859adc6-fd20-4c1b-8cd4-f166f13b296f.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (32, 1, 10, N'Improve Quality and Efficiency while you automate your industrial processes with superior robotic integrated systems.', N'<p>
	Over the years Patvin has experienced and proven itself in the field of Robotic integrated systems and can provide customized solutions for varied applications like Painting, Sealant dispensing and Glass gluing. Designed for precision and consistency, our robotic systems enable our customers create a <a href="/Careers">flawless</a> final <a href="http://localhost:63846/Careers"><span style="font-size:14px;">product</span></a>.</p>
<p>
	<strong>Association with Universal Robots</strong></p>
<p>
	Patvin partners with Universal Robots to provide Collaborative robots (Cobots) for a range of industrial applications as follows:</p>
<ul>
	<li>
		Pick and Place</li>
	<li>
		Packing and Palletizing</li>
	<li>
		Machine Tending</li>
	<li>
		Gluing and Dispensing</li>
	<li>
		Assembly</li>
	<li>
		Screw driving</li>
	<li>
		Quality Inspection</li>
</ul>
<p>
	With UR cobots, we can automate virtually anything, we mean virually anything, from assembly and screw driving to labelling, from pick and place to packaging, from injection moulding to welding and whatever other processing tasks. The UR arms are viable for small-batch and mixed-product assembly.</p>
<p>
	<span style="font-size:20px;"><strong>Why install a Collaborative Robot?</strong></span></p>
<p>
	UR Cobots have proven to be 85% more productive than a human or robot alone.</p>
<p>
	<img alt="" class="imgFull" src="/Content/uploads/Images/Picture1.png" /></p>
<p>
	<span style="font-size:20px;"><strong>Patvin can help you integrate a cobot into your production assembly.</strong></span></p>
<p>
	Our experts assess the technical and economic feasibility of cobots within your industrial process to design a customized solution that meets your system needs.</p>
<p>
	Our advanced testing capabilities allow us to create a dummy environment that duplicates your production environment. Further, we share with you a conceptual design including all the specifications, of a suitable cobot application, without you having to disturb your own production assembly.</p>
<p>
	<strong>Contact us for an expert Cobot solution!</strong></p>
<p>
	<img alt="" class="imgFull" src="/Content/uploads/Images/UR Cobots specs.jpg" /></p>
<p>
	<span style="font-size:14px;"><strong style="font-size: 18px;">e-Series Specifications</strong></span></p>
<p>
	<img alt="" class="imgFull" src="/Content/uploads/Images/Salesforce customization.jpg" style="height: 550px; width: 750px;" /></p>
', 1, CAST(0x0000AA0E01647434 AS DateTime), CAST(0x0000AA25012C22D7 AS DateTime), NULL, N'InnerBanner-e53ade35-5d42-4486-809a-a2d8b05bba35.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (33, 2, 11, N'Smart automation for your industrial processes', N'<p>
	Patvin has an intelligent solution for your every automation need. Applying basic physical principles to overcome your process challenge and years of expertise in diverse areas of business, we are sure to find an optimum solution, across all industry boundaries.</p>
<p>
	We know exactly which of our solutions combined with which physical principle solves your challenge. Superior in quality and highly reliable, our automated systems guarantee flawless and safe services.</p>
<p>
	We design and integrate industrial products in-house for all key areas of fluid control systems. Our solutions help you achieve appreciable time and cost savings.</p>
<p>
	Many a times, a customer enquiry has led us to the development of a new system family. With many such standard solutions available in our system library, we can create a customised solution for you in a short time, thus reducing your time to market and cutting down your investment costs.</p>
', 1, CAST(0x0000AA0E01658413 AS DateTime), CAST(0x0000AA11007A853F AS DateTime), NULL, N'InnerBanner-b81b0f79-a2d6-4432-91fa-51337002a102.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (34, 2, 15, N'Achieve superior quality and enhanced productivity with real time data analysis', N'<p>
	We have entered a smart new era where energy efficiency is no longer the only focus. Sharing the attention equally is profitability using smarter technologies and equipment. We offer you smart automation solutions to be at par with the fourth industrial revolution and enter the world of Industrie 4.0.</p>
<p>
	At Patvin, Industry 4.0 begins with continuous and innovative development of existing solutions and technologies, facilitating productive changes in routine practices and processes.</p>
<p>
	We utilize the power of real time data analysis to achieve superior quaility, productivity by minimizing stoppages and downtimes, preventive maintenance, and higher efficiencies.</p>
<p>
	We integrate smart systems that control their own process individually. Building communication along the value chain of a product, we ensure that the product&acute;s life cycle is fully perceptible.</p>
', 1, CAST(0x0000AA0F015F1A5B AS DateTime), CAST(0x0000AA2301810ABF AS DateTime), NULL, N'InnerBanner-c73407d3-5185-42a8-b8c0-95ad90b8b13b.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (35, 2, 21, N'Turnkey Robotic solutions for Paint, Sealant and Gluing applications. ', N'<p>
	Patvin&#39;s diverse experience and solid process knowledge, ensure a holistic approach to both brownfield and greenfield requirements. We invest in researching solutions that enhance quality, optimize energy consumption, and reduce costs. Integrating robotics into conventional concepts, we can supply customized solutions that preciselly fulfill customer needs and demands. Designed for precision and consistency, our robotic systems enable our customers create a flawless final product.&nbsp;</p>

<p>
	Patvin excels in Robotic solutions for Paint, Sealant and Gluing applications.&nbsp;</p>

<p>
	As a turnkey system supplier, Patvin has extensive expertise within automated vehicle sealing, painting and gluing applications. A wide variety of materials are applied, such as highly viscous PVC plastisols for sealing and coating, acrylates and rubber materials for structural sound deadening, and waxes for cavity conservation.</p>', 1, CAST(0x0000AA0F015F6DE3 AS DateTime), CAST(0x0000AA11007B70D4 AS DateTime), NULL, N'InnerBanner-4962337b-d56d-475b-9704-2874825d5f98.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (36, 3, 16, N'Solutions for the Aerospace industry', N'
	<p>
		Serving the most exclusive and demanding industry in terms of aesthetics, performance, safety and quality, we understand the challenges Aerospace manufacturers face. We work with you to create customized fluid handling solutions that will seamlessly fit in with your system.</p>
	<ul>
		<li>
			Paint Systems and Equipment</li>
		<li>
			Sealant and Gluing Systems</li>
		<li>
			Insulation &amp; Structural Foam Applications</li>
		<li>
			Wax Injection Systems</li>
		<li>
			Centralized Lubrication Systems</li>
		<li>
			Fuel Transfer Systems</li>
		<li>
			Oil Transfer Systems</li>
	</ul>

', 1, CAST(0x0000AA0F01627109 AS DateTime), CAST(0x0000AA11007C29B6 AS DateTime), NULL, N'InnerBanner-bc4f0bc3-84a0-4dd3-83a2-c52647b6ea77.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (37, 3, 23, N'Serving the Auto component industry since three decades', N'
	<p>
		We provide Fluid handling and Finishing systems as follows to Tier 1 &amp; 2 Automotive suppliers.</p>
	<ul>
		<li>
			Paint Circulation Systems</li>
		<li>
			Paint Transfer equipment</li>
		<li>
			PVC Sealant Systems</li>
		<li>
			PVC Underbody Systems</li>
		<li>
			Wax Injection Systems</li>
		<li>
			Centralized Lubrication Systems</li>
		<li>
			BIW/Weld Shop Sealants &amp; Adhesive systems</li>
		<li>
			Automatic Spray Systems</li>
		<li>
			Robotics Sealant Application Systems</li>
		<li>
			Glass Bonding Urethane Systems</li>
		<li>
			Glue Application Systems</li>
		<li>
			Head lamp Bonding</li>
	</ul>

', 1, CAST(0x0000AA0F01632381 AS DateTime), CAST(0x0000AA11007CE067 AS DateTime), NULL, N'InnerBanner-cf134d7e-356b-459b-b1b1-511233e4bd8d.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (38, 3, 17, N'Turnkey solutions for the Pharma Industry', N'<p>
	We deliver solutions for fluid handling, transfer of drugs and dosing systems for the Pharmaceutical Industry.</p>
<p>
	We provide turnkey solutions for the following :</p>
<ul>
	<li>
		Fluid Transfer Systems</li>
	<li>
		Semi-Solids Bulk Transfer Systems</li>
	<li>
		Paste/Chemical Transfer systems</li>
	<li>
		Bulk Transfer systems</li>
	<li>
		Tablet Coating</li>
</ul>
', 1, CAST(0x0000AA0F01643838 AS DateTime), CAST(0x0000AA1B004D8405 AS DateTime), NULL, N'InnerBanner-b8d38047-fd9f-46d6-ac6c-addffcdfa947.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (39, 3, 18, N'We provide a range of solutions to the White Good industry', N'
	<p>
		Patvin offers cost efficient and high quality custom made solutions to compliment your finishing systems:</p>
	<ul>
		<li>
			Paint Systems and Equipment</li>
		<li>
			Sealant and Gluing Systems</li>
		<li>
			NVH &amp; Structural Foam Applications</li>
	</ul>

', 1, CAST(0x0000AA0F0164C35D AS DateTime), CAST(0x0000AA11007C7FB1 AS DateTime), NULL, N'InnerBanner-0edf1faf-9c27-4c5c-ae36-190dc7b62d8a.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (40, 3, 19, N'Diverse solutions for Industrial products', N'<p>
	We provide Turnkey solutions for:</p>
<ul>
	<li>
		Paint systems</li>
	<li>
		Fluid and Chemical transfer</li>
	<li>
		Lube Equipment and systems</li>
	<li>
		High viscosity material transfer</li>
	<li>
		Precision application</li>
	<li>
		Sealant systems</li>
</ul>
<p>
	<strong>Collaborative robots by Universal Robots:</strong></p>
<p>
	Patvin partners with Universal Robots to provide Collaborative robots (Cobots) for a range of industrial applications including Pick and Place, Palletizing, Gluing, Assembly, Machine Tending, Quality Inspection etc. UR Cobots have proven to be 85% more productive than a human or robot alone.</p>
<p>
	We can help you integrate a cobot into your production assembly. Our experts assess the technical and economic feasibility of cobots within your industrial process to design a customized solution that meets your system needs.</p>
<p>
	&nbsp;</p>
', 1, CAST(0x0000AA0F01660947 AS DateTime), CAST(0x0000AA23018860C1 AS DateTime), NULL, N'InnerBanner-abb715d3-d328-46f0-a755-0c9fd19d2c9b.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (41, 3, 20, N'Solutions for the FMCG industry', N'
	<p>
		We deliver Fluid handling solutions for a range of applications within the FMCG Industry:</p>
	<ul>
		<li>
			Fluid Transfer Systems</li>
		<li>
			Semi-Solids Bulk Transfer Systems</li>
		<li>
			Centralized Conveyor Lubrications System</li>
		<li>
			Paste/Chemical Transfer systems</li>
		<li>
			Bulk Transfer systems</li>
	</ul>
	<p>
		Our solutions can be applied to various segments including Food products, Chocolates, Beverages, Dairy applications, Brewery etc.</p>

', 1, CAST(0x0000AA0F0166A871 AS DateTime), CAST(0x0000AA11007CC2B7 AS DateTime), NULL, N'InnerBanner-1d967135-5b9f-44e1-9a7a-1f9cf0a74116.jpg')
GO
INSERT [dbo].[SolAndTechDetailsTable] ([Id], [MasterId], [SubId], [Heading], [Description], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Image]) VALUES (42, 2, 22, N'Combining the benefits of accuracy, speed and repeatability from a robot, with the flexibility and cognitive skills of humans', N'<p>
	After Robots dominated the advancement in the manufacturing industry for a long period, the industry is now witnessing a trend of collaborative robots. Easy-to-use, the collaborative robots allow humans and robots to share their tasks and skills, writing a new story in the history of robotics. Combining the benefits of accuracy, speed and repeatability from a robot, with the flexibility and cognitive skills of humans, they achieve an efficient and safe human&ndash;robot collaboration.</p>
<p>
	Why companies are moving from Robots to Cobots for possible tasks?</p>
<ul>
	<li>
		Companies can retain human manpower, thus not affecting employment.</li>
	<li>
		Safe human Involvement.</li>
</ul>
<p>
	Patvin partners with Universal Robots to provide Collaborative robots (Cobots) for a range of industrial applications.</p>
<p>
	&nbsp;</p>
<p>
	<img alt="" class="imgFull" src="/Patvin_Project/Content/uploads/Images/UR Cobot Leaflet - Copy.jpg" /></p>
', 1, CAST(0x0000AA0F016F2E31 AS DateTime), CAST(0x0000AA24001AAEB5 AS DateTime), NULL, N'InnerBanner-5cca4cb7-1537-4f49-b227-5d5efdc15763.jpg')
GO
SET IDENTITY_INSERT [dbo].[SolAndTechDetailsTable] OFF
GO
SET IDENTITY_INSERT [dbo].[SolAndTechGalleryTable] ON 

GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (17, 20, N'Image 1', N'Image', N'Img-6aee1ca0-b438-436c-b9cc-aaf4070304f6.jpg', N'', N'', 1, CAST(0x0000AA0900CFF4EA AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (18, 20, N'image 2', N'Image', N'Img-30c831bb-ddc9-4985-a1d1-a04207dee878.jpg', N'', N'', 1, CAST(0x0000AA0900D009B0 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (19, 21, N'Video', N'Video', N'', N'https://www.youtube.com/watch?v=qSnUdIAsTLs&amp;autoplay=1', N'ThumbImg-7d1c0bc2-939a-417a-a447-c6716f42776b.jpg', 0, CAST(0x0000AA0900D0B2FF AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (20, 22, N'image', N'Image', N'Img-3736a45a-91e2-436d-a53c-76e88207f42f.jpg', N'', N'', 1, CAST(0x0000AA0900D36B3E AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (21, 23, N'Video', N'Video', N'', N'https://www.youtube.com/watch?v=qSnUdIAsTLs&amp;autoplay=1', N'ThumbImg-97f4445e-e424-45a1-bd3e-ce0fcfc805be.jpg', 0, CAST(0x0000AA0900D3EAE9 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (22, 21, N'image1', N'Image', N'Img-681d372c-3b46-4cac-98c5-a40ad7ad026a.jpg', N'', N'', 0, CAST(0x0000AA0E0061BD2C AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (23, 21, N'Robotic Paint Application system', N'Image', N'Img-7013af2c-d96d-4978-9ea3-6ee617a60fe3.JPG', N'', N'', 1, CAST(0x0000AA10001474F6 AS DateTime), CAST(0x0000AA21017DAF74 AS DateTime), NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (24, 21, N'Paint Circulation Systems', N'Image', N'Img-d1384358-9bd4-4f62-9f16-5b2367d5ee19.jpg', N'', N'', 0, CAST(0x0000AA100014D5C8 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (25, 24, N'Paint Circulation systems', N'Image', N'Img-fc92ff29-3782-4331-b288-aacd3c6cc698.jpg', N'', N'', 1, CAST(0x0000AA100016441D AS DateTime), CAST(0x0000AA22005A458A AS DateTime), NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (26, 24, N'Paint Circulation system 2', N'Image', N'Img-6ee58118-d7e1-4987-b532-5fe87b9bbedd.jpg', N'', N'', 1, CAST(0x0000AA1000178BD6 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (27, 31, N'Piggable Paint system', N'Image', N'Img-34eb06d5-fbb8-4bb2-8b51-9882f19fdbb6.jpg', N'', N'', 1, CAST(0x0000AA10001EB029 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (28, 25, N'Robotic Sealant dispensing system', N'Video', N'', N'https://youtu.be/aneF8S1q9Dk', N'ThumbImg-c1cdf1f6-9a8f-4720-a042-129e29db4c43.jpg', 1, CAST(0x0000AA10001F7B59 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (29, 23, N'Paint Circulation System', N'Image', N'Img-5197c1ee-01a7-4b91-8d42-26e73120b0e3.jpg', N'', N'', 1, CAST(0x0000AA140015AF73 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (30, 23, N'Robotic Paint application', N'Image', N'Img-f8b6298e-9173-49d0-948a-b48a05957305.JPG', N'', N'', 1, CAST(0x0000AA140018D82B AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (31, 23, N'2K Paint System', N'Image', N'Img-0a5c3a3a-caf6-4aac-88ca-ad28d0435824.jpg', N'', N'', 1, CAST(0x0000AA1400191BFF AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (32, 23, N'Robotic Gluing System', N'Image', N'Img-fe1ab006-85a8-43b5-a6b7-d70cd2649f99.jpg', N'', N'', 1, CAST(0x0000AA140019477D AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (33, 23, N'Metered Oil dispensing', N'Image', N'Img-8974fc1a-a6a4-463a-ba2b-531788ebdd03.jpg', N'', N'', 1, CAST(0x0000AA140019995B AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (34, 40, N'Lubrication systems', N'Image', N'Img-28030cdf-10b4-4d7e-bd76-0b61a40a3c6f.jpg', N'', N'', 1, CAST(0x0000AA14001E94DE AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (35, 40, N'Sealer dispensing', N'Image', N'Img-6ebd99d5-5088-4a07-b401-9a285a2b2624.jpg', N'', N'', 1, CAST(0x0000AA14001F56B2 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (36, 30, N'Sealer Booster Pumps', N'Image', N'Img-c89cda01-2f37-4ae7-9e68-e38a471687df.png', N'', N'', 1, CAST(0x0000AA1400230028 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (37, 30, N'Sealer System', N'Image', N'Img-af4e0c4b-de4f-44a7-9658-22d0302aa5d3.jpg', N'', N'', 1, CAST(0x0000AA1400233426 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (38, 28, N'2K Paint system', N'Image', N'Img-24781169-ab5d-4b6c-bbf3-04b561339d91.jpg', N'', N'', 1, CAST(0x0000AA14002BBFFE AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (39, 28, N'Promix 2K System', N'Image', N'Img-12db5990-edd4-4b98-a7dc-2f03d902b94e.JPG', N'', N'', 1, CAST(0x0000AA14002C6742 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (40, 29, N'Grease dispensing system', N'Image', N'Img-b6951222-bb14-49b5-8b3e-d8d7066614c0.jpg', N'', N'', 1, CAST(0x0000AA14002DF7F3 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (41, 37, N'Paint Circulation Systems', N'Image', N'Img-98075322-c857-45c5-9d25-13a685790ac1.jpg', N'', N'', 1, CAST(0x0000AA14002FB074 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (42, 37, N'2K Promix system', N'Image', N'Img-4b6d3b51-1559-4695-b00b-ba5f0b693ee5.jpg', N'', N'', 1, CAST(0x0000AA14002FCFFE AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (43, 32, N'UR Cobot Gluing application', N'Image', N'Img-022e2c8c-4237-4052-9ecf-3c6f0bea3b7c.jpg', N'', N'', 1, CAST(0x0000AA15003EE301 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (44, 32, N'UR Cobot Pick & Place application', N'Image', N'Img-ff4dee7e-1292-4583-9a21-a2ef7c296d91.jpg', N'', N'', 1, CAST(0x0000AA15003F0468 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (45, 32, N'UR Cobot polishing application', N'Image', N'Img-830c5e3f-1e47-4758-879a-1ee06b18509a.jpg', N'', N'', 1, CAST(0x0000AA15003F1C19 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (46, 32, N'UR Cobot Screw driving application', N'Image', N'Img-dabaf73e-0336-4486-8386-e21b5986f265.jpg', N'', N'', 1, CAST(0x0000AA15003F36B8 AS DateTime), NULL, NULL)
GO
INSERT [dbo].[SolAndTechGalleryTable] ([GalleryId], [Id], [Title], [Type], [Image], [Url], [ThumbImg], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (47, 28, N'Promix PD2K sytem', N'Image', N'Img-0ed502c4-522c-4cf2-a303-87792ced77b5.jpg', N'', N'', 1, CAST(0x0000AA1B000950AA AS DateTime), NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[SolAndTechGalleryTable] OFF
GO
SET IDENTITY_INSERT [dbo].[SolAndTechMasterTable] ON 

GO
INSERT [dbo].[SolAndTechMasterTable] ([MasterId], [Title], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (1, N'Solutions', 1, NULL, NULL, NULL)
GO
INSERT [dbo].[SolAndTechMasterTable] ([MasterId], [Title], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (2, N'Technology', 1, NULL, NULL, NULL)
GO
INSERT [dbo].[SolAndTechMasterTable] ([MasterId], [Title], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn]) VALUES (3, N'Industries', 1, NULL, NULL, NULL)
GO
SET IDENTITY_INSERT [dbo].[SolAndTechMasterTable] OFF
GO
SET IDENTITY_INSERT [dbo].[SolAndTechSubTable] ON 

GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (1, 1, N'Paint Circulation Systems', N'paint-circulation-systems', 1, CAST(0x0000A9FB012141AC AS DateTime), CAST(0x0000AA2500CA238D AS DateTime), NULL, 1)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (2, 1, N'Paint Application Systems', N'paint-application-systems', 1, CAST(0x0000A9FB012CBB82 AS DateTime), CAST(0x0000AA2500F5EC7A AS DateTime), NULL, 2)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (3, 1, N'Color Change Systems', N'color-change-systems', 1, CAST(0x0000A9FB0133FB32 AS DateTime), CAST(0x0000AA2500F5C199 AS DateTime), CAST(0x0000A9FC013B11E2 AS DateTime), 3)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (4, 1, N'Two Component Paint System - 2K System', N'two-component-paint-system-2k-system', 1, CAST(0x0000A9FB01387AC8 AS DateTime), CAST(0x0000AA2500F5C52D AS DateTime), NULL, 4)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (5, 1, N'Sealant and Adhesive Systems', N'sealant-and-adhesive-systems', 1, CAST(0x0000A9FB0138BC54 AS DateTime), CAST(0x0000AA2500F5C91D AS DateTime), NULL, 5)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (6, 1, N'Automation in Sealant Adhesive', N'automation-in-sealant-adhesive', 1, CAST(0x0000A9FB013C7C1D AS DateTime), CAST(0x0000AA2500F5CC86 AS DateTime), NULL, 6)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (7, 1, N'NVH Application Systems', N'nvh-application-systems', 1, CAST(0x0000A9FB013EC568 AS DateTime), CAST(0x0000AA2500F5D034 AS DateTime), NULL, 7)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (8, 1, N'Lubrication Systems', N'lubrication-systems', 1, CAST(0x0000A9FB013EC585 AS DateTime), CAST(0x0000AA2500F5D682 AS DateTime), NULL, 8)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (9, 1, N'Piggable Paint Systems', N'piggable-paint-systems', 1, CAST(0x0000A9FC00B4FF36 AS DateTime), CAST(0x0000AA2500F5DA76 AS DateTime), NULL, 9)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (10, 1, N'UR Cobots', N'ur-cobots', 1, CAST(0x0000A9FC00BF2FE4 AS DateTime), CAST(0x0000AA2500F5DEE0 AS DateTime), NULL, 10)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (11, 2, N'Automation', N'automation', 1, CAST(0x0000A9FC00BFCBC5 AS DateTime), CAST(0x0000AA2500F5E736 AS DateTime), NULL, 11)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (12, 2, N'industrie 4.0', N'industrie_4.0', 0, CAST(0x0000A9FC00CD14D1 AS DateTime), CAST(0x0000AA0D0053A387 AS DateTime), CAST(0x0000AA0F015E9EF4 AS DateTime), 0)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (14, 3, N'Automotive & Transportation', N'automotive-transportation', 1, CAST(0x0000AA0100BB9538 AS DateTime), CAST(0x0000AA2500F5F660 AS DateTime), NULL, 15)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (15, 2, N'Industrie 4.0', N'industrie-4-0', 1, CAST(0x0000AA0F015D8705 AS DateTime), CAST(0x0000AA2500F5FFAE AS DateTime), NULL, 12)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (16, 3, N'Aerospace', N'aerospace', 1, CAST(0x0000AA0F015D9B83 AS DateTime), CAST(0x0000AA2500F60681 AS DateTime), NULL, 17)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (17, 3, N'Pharmaceutical', N'pharmaceutical', 1, CAST(0x0000AA0F015DFFB3 AS DateTime), CAST(0x0000AA2500F60C1E AS DateTime), NULL, 18)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (18, 3, N'White Goods', N'white-goods', 1, CAST(0x0000AA0F015E0DE2 AS DateTime), CAST(0x0000AA2500F61CCE AS DateTime), NULL, 19)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (19, 3, N'Industrial Products', N'industrial-products', 1, CAST(0x0000AA0F015E1AAA AS DateTime), CAST(0x0000AA2500F62593 AS DateTime), NULL, 20)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (20, 3, N'FMCG', N'fmcg', 1, CAST(0x0000AA0F015E24CF AS DateTime), CAST(0x0000AA2500F62B8F AS DateTime), NULL, 21)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (21, 2, N'Robotic Solutions', N'robotic-solutions', 1, CAST(0x0000AA0F015E325B AS DateTime), CAST(0x0000AA2500F63303 AS DateTime), NULL, 13)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (22, 2, N'Human Robot Collaboration', N'human-robot-collaboration', 1, CAST(0x0000AA0F015E5445 AS DateTime), CAST(0x0000AA2500F63A89 AS DateTime), NULL, 14)
GO
INSERT [dbo].[SolAndTechSubTable] ([SubId], [MasterId], [Title], [Slug], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [DisplayOrder]) VALUES (23, 3, N'Auto components', N'auto-components', 1, CAST(0x0000AA0F0162BB84 AS DateTime), CAST(0x0000AA2500F64115 AS DateTime), NULL, 16)
GO
SET IDENTITY_INSERT [dbo].[SolAndTechSubTable] OFF
GO
SET IDENTITY_INSERT [dbo].[SuperAdminLogin] ON 

GO
INSERT [dbo].[SuperAdminLogin] ([Id], [Name], [Username], [Password], [Emailid], [Mobileno], [AddedOn], [IsActive], [Remark], [UpdatedOn], [LoginOn], [LogoutOn]) VALUES (1, N'Alchemy Capital Management', N'admin', N'admin@123', N'rahul@kwebmaker.com', N'1212121212', CAST(0x0000A92000000000 AS DateTime), 1, N'#SuperAdmin', CAST(0x0000A9EA01496948 AS DateTime), CAST(0x0000AADF00D20949 AS DateTime), NULL)
GO
SET IDENTITY_INSERT [dbo].[SuperAdminLogin] OFF
GO
SET IDENTITY_INSERT [dbo].[TechnologyImageVideoCaption] ON 

GO
INSERT [dbo].[TechnologyImageVideoCaption] ([Id], [Type], [Image], [Video], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Title]) VALUES (1, N'Image', N'TechnologySolution-b326fa7d-0e58-4abf-b3ec-0009fabeafcf.jpg', NULL, 0, CAST(0x0000A9F901419186 AS DateTime), NULL, CAST(0x0000A9F90144F6E7 AS DateTime), N'image1')
GO
INSERT [dbo].[TechnologyImageVideoCaption] ([Id], [Type], [Image], [Video], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Title]) VALUES (2, N'Image', N'TechnologySolution-6b16a088-151b-45d5-a024-70b944d74f81.jpg', NULL, 1, CAST(0x0000A9F90141C43D AS DateTime), CAST(0x0000A9F901465CCF AS DateTime), NULL, N'image1')
GO
INSERT [dbo].[TechnologyImageVideoCaption] ([Id], [Type], [Image], [Video], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Title]) VALUES (3, N'Video', NULL, N'https://www.youtube.com/watch?v=qSnUdIAsTLs&amp;autoplay=1', 0, CAST(0x0000A9F901472DDC AS DateTime), NULL, CAST(0x0000A9FA00B999CF AS DateTime), N'video')
GO
INSERT [dbo].[TechnologyImageVideoCaption] ([Id], [Type], [Image], [Video], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Title]) VALUES (4, N'Image', N'TechnologySolution-cd4e839f-c404-441d-90ac-680127d5b75b.jpg', NULL, 1, CAST(0x0000A9F9014BFF73 AS DateTime), NULL, NULL, N'image2')
GO
INSERT [dbo].[TechnologyImageVideoCaption] ([Id], [Type], [Image], [Video], [IsActive], [AddedOn], [UpdatedOn], [DeletedOn], [Title]) VALUES (5, N'Video', NULL, N'https://www.youtube.com/watch?v=qSnUdIAsTLs&amp;autoplay=1', 1, CAST(0x0000A9FA00BD648D AS DateTime), NULL, NULL, N'video')
GO
SET IDENTITY_INSERT [dbo].[TechnologyImageVideoCaption] OFF
GO
SET IDENTITY_INSERT [dbo].[Testimonials] ON 

GO
INSERT [dbo].[Testimonials] ([Id], [Name], [Position], [CompanyName], [Description], [IsActive], [AddedOn], [DeletedOn], [UpdatedOn], [DisplayOrder]) VALUES (1, N'Harshad Parulekar', N'Dy. General Manager Asset Management ', N'Mahindra & Mahindra Ltd. ', N'<p>
	&quot;We at Mahindra &amp; Mahindra really appreciate un-conditional support that we get from team Patvin. Ownership shown by your team in timely completion of projects is unbeatable.</p>
<p>
	We wish a very bright and prosperous future for Patvin.&quot;</p>
', 1, CAST(0x0000A9FA010CC976 AS DateTime), NULL, CAST(0x0000AA1B0158E7E5 AS DateTime), 2)
GO
INSERT [dbo].[Testimonials] ([Id], [Name], [Position], [CompanyName], [Description], [IsActive], [AddedOn], [DeletedOn], [UpdatedOn], [DisplayOrder]) VALUES (2, N'nhhhhd', N'sssghh', N'kweb', N'<p>
	fffffjjjjj</p>
', 0, CAST(0x0000A9FA010E4D8A AS DateTime), NULL, CAST(0x0000A9FA010ED8E0 AS DateTime), 2)
GO
INSERT [dbo].[Testimonials] ([Id], [Name], [Position], [CompanyName], [Description], [IsActive], [AddedOn], [DeletedOn], [UpdatedOn], [DisplayOrder]) VALUES (3, N'Rishi Kant Dubey', N'Divisional Head-Frame', N'Honda Car India ', N'<p>
	&quot;We congratulate M/S Patvin for meeting our expectations in terms of efficient design, trouble free equipment and on-time service support.&nbsp;</p>
<p>
	We are using &lsquo;Patvin&rsquo; supplied equipment&rsquo;s in our Paint shop and Welding shop.&nbsp;</p>
<p>
	For our Paint shop,&nbsp;Patvin has engineered and supplied High Viscosity Circulation System comprising of UBC/Sealer/Wax Dispensing System.&nbsp;High performance equipment selection have helped in Zero breakdown condition. However there is scope of improvement for reducing wear-out parts consumption.&nbsp;Service support is highly appreciable.&nbsp;We undoubtedly recommend them for their reliability approach in engineering design.</p>
<p>
	For our welding shop, we had purchased sealer dispensing system from Patvin for manual line.&nbsp;Performance of product is very nice. Trouble free. Service support is very good.&quot;</p>
', 1, CAST(0x0000A9FA010F38AB AS DateTime), NULL, CAST(0x0000AA1B0052B195 AS DateTime), 1)
GO
SET IDENTITY_INSERT [dbo].[Testimonials] OFF
GO
